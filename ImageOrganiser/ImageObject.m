//
//  ImageObject.m
//  ImageOrganiser
//
//  Created by Kirti Nikam on 17/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "ImageObject.h"

@implementation ImageObject
-(id) mutableCopyWithZone: (NSZone *) zone
{
    ImageObject *imgObjCopy = [[ImageObject allocWithZone: zone] init];
    if (imgObjCopy) {
        [imgObjCopy setImagePath:_imagePath];
        [imgObjCopy setIsNew:_isNew];
        [imgObjCopy setIsDeleted:_isDeleted];
        [imgObjCopy setIsUpdated:_isUpdated];
        return imgObjCopy;
    }
    return nil;
}
@end
