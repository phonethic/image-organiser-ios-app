//
//  IOSettingsViewController.h
//  ImageOrganiser
//
//  Created by Kirti Nikam on 10/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <sqlite3.h>

@interface IOSettingsViewController : UIViewController <MFMailComposeViewControllerDelegate,UITextFieldDelegate>
{
    sqlite3 *ioDB;
}
@property (strong, nonatomic) IBOutlet UITableView *settingsTableView;

@end
