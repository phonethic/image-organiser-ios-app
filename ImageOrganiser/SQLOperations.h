//
//  SQLOperations.h
//  ImageOrganiser
//
//  Created by Kirti Nikam on 05/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQLOperations : NSObject
+(NSString *)getGroupName:(int) objectId;
+(int)getGroupId:(NSString *)groupName;
@end
