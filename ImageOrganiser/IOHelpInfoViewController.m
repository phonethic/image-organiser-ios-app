//
//  IOHelpInfoViewController.m
//  ImageOrganiser
//
//  Created by Kirti Nikam on 28/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "IOHelpInfoViewController.h"

@interface IOHelpInfoViewController ()

@end

@implementation IOHelpInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
