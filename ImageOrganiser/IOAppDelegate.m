//
//  IOAppDelegate.m
//  ImageOrganiser
//
//  Created by Kirti Nikam on 02/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
#import <sqlite3.h>
#import "IOAppDelegate.h"
#import "IOViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "SplashScreenControllerViewController.h"

@implementation IOAppDelegate
@synthesize networkavailable;
@synthesize databasePath;
@synthesize navController;
@synthesize imageCache;
@synthesize splashScreenViewController;

-(void)addSplashScreen{
    splashScreenViewController = [[SplashScreenControllerViewController alloc] initWithNibName:@"SplashScreenControllerViewController" bundle:[NSBundle mainBundle]];
    [self.window addSubview:self.splashScreenViewController.view];
}
-(void) removeSplashScreen
{
    [splashScreenViewController.view removeFromSuperview];
    splashScreenViewController = nil;
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];

    UIViewController *viewController = [[IOViewController alloc] initWithNibName:@"IOViewController" bundle:nil];
    navController = [[UINavigationController alloc] initWithRootViewController:viewController];
    self.window.rootViewController = navController;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
    [self addSplashScreen];

    [self.window makeKeyAndVisible];
    
    imageCache = [[NSCache alloc] init];
    [imageCache setName:@"IOImageCache"];
    
    //Add Flurry
    [Flurry startSession:FLURRY_APPID];
    [Flurry logEvent:@"EMR App Started"];
    
    //Add Appirater
    //[self setAppRateAlert];
    //[Appirater appLaunched:YES];
    
    // Create Database
    [self readAndCreateDatabase];
    
    //Rechability : Start for checking rechability
    [self startCheckNetwork];
    
    [[UINavigationBar appearance] setBackgroundImage:[self blueBarBackground] forBarMetrics:UIBarMetricsDefault];
  
//    navController.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
//    navController.navigationBar.layer.shadowOpacity = 1;
//    navController.navigationBar.layer.shadowOffset = CGSizeMake(0,4);
    
//    CGRect shadowPath = CGRectMake(navController.navigationBar.layer.bounds.origin.x - 10, navController.navigationBar.layer.bounds.size.height - 6, navController.navigationBar.layer.bounds.size.width + 20, 5);
//    navController.navigationBar.layer.shadowPath = [UIBezierPath bezierPathWithRect:shadowPath].CGPath;
//    navController.navigationBar.layer.shouldRasterize = YES;
    
    [[UINavigationBar appearance] setTitleTextAttributes: @{
                                UITextAttributeTextColor: [UIColor whiteColor],
//                          UITextAttributeTextShadowColor: [UIColor blackColor],
//                         UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0.0f, 1.0f)],
                                     UITextAttributeFont: DEFAULT_BOLD_FONT(18.0)
     }];
    
    
    [[UIBarButtonItem appearance] setTintColor:BGCOLOR];
    
//    UIImage *button30 = [[UIImage imageNamed:@"Button40x30.png"]
//                         resizableImageWithCapInsets:UIEdgeInsetsMake(0, 5, 0, 5)];
//    UIImage *button24 = [[UIImage imageNamed:@"Button40x30.png"]
//                         resizableImageWithCapInsets:UIEdgeInsetsMake(0, 5, 0, 5)];
//    
//    [[UIBarButtonItem appearance] setBackgroundImage:button30 forState:UIControlStateNormal
//                                          barMetrics:UIBarMetricsDefault];
//    [[UIBarButtonItem appearance] setBackgroundImage:button24 forState:UIControlStateNormal
//                                          barMetrics:UIBarMetricsLandscapePhone];
    
    [[UIBarButtonItem appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
//      [UIColor whiteColor],
//      UITextAttributeTextColor,
//      nil,
//      UITextAttributeTextShadowColor,
//      nil,
//      UITextAttributeTextShadowOffset,
      [UIFont systemFontOfSize:15.0],
      UITextAttributeFont,
      nil] forState:UIControlStateNormal];
    
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -2.0) forBarMetrics:UIBarMetricsDefault];

    return YES;
}

-(NSString *)doCapitalFirstLetter:(NSString *)string
{
    NSString *firstCapChar = [[string substringToIndex:1] capitalizedString];
    NSString *cappedString = [string stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstCapChar];
    return cappedString;
}
- (UIColor *)mainloopBlueColor
{
    //return [UIColor colorWithRed:0.0542516 green:0.44115  blue:0.699654 alpha:1]; //bluecolor
  //  return [UIColor colorWithRed:39/255.0 green:174/255.0 blue:96/255.0 alpha:1];
   return DEFAULT_COLOR;
}
- (UIImage *)blueBarBackground
{
    /* Create a DeviceRGB color space. */
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    /* Create a bitmap context. The context draws into a bitmap which is `width'
     pixels wide and `height' pixels high*/
    CGContextRef composedImageContext = CGBitmapContextCreate(NULL,
                                                              10,
                                                              10,
                                                              8,
                                                              10*4,
                                                              colorSpace,
                                                              kCGImageAlphaPremultipliedFirst);
    
    CGColorSpaceRelease(colorSpace);
    
    
    CGContextSetFillColorWithColor(composedImageContext, [TABLE_CELL_BGCOLOR CGColor]);
    CGContextFillRect(composedImageContext, CGRectMake(0, 0, 10, 10));
    /* Return an image containing a snapshot of the bitmap context `context'.*/
    CGImageRef cgImage = CGBitmapContextCreateImage(composedImageContext);
    
    return [UIImage imageWithCGImage:cgImage];
}

#pragma Appirater Methods
-(void) setAppRateAlert
{
    [Appirater setAppId:APPIRATER_APPID];
    [Appirater setDaysUntilPrompt:2];
    [Appirater setUsesUntilPrompt:2];
    [Appirater setSignificantEventsUntilPrompt:-1];
    [Appirater setTimeBeforeReminding:3];
    //[Appirater setDebug:YES];
}
#pragma LocalDB sqlite methods
-(void)readAndCreateDatabase{
    DebugLog(@"==========================read and create local database ==========================");
	// Get the path to the documents directory and append the databaseName
	NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	databasePath = [documentsDir stringByAppendingPathComponent:DATABASE_NAME];
    DebugLog(@"==========================databasePath %@==========================",databasePath);
	// Execute the "checkAndCreateDatabase" function
	[self checkAndCreateDatabase];
}

-(void) checkAndCreateDatabase{
	// Check if the SQL database has already been saved to the users phone, if not then copy it over
	BOOL success;
    
	// Create a FileManager object, we will use this to check the status
	// of the database and to copy it over if required
	NSFileManager *fileManager = [NSFileManager defaultManager];
    
	// Check if the database has already been created in the users filesystem
	success = [fileManager fileExistsAtPath:databasePath];
    
	// If the database already exists then return without doing anything
	if(success)
    {
        DebugLog(@"========================== database exists at path %@==========================",databasePath);
        return;
    }
    else
    {
        DebugLog(@"========================== creating database at path %@==========================",databasePath);
        sqlite3 *ioDB;
        
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &ioDB) == SQLITE_OK)
        {
            DebugLog(@"Database opened successfully");
            char *errMsg;
            
            const char *sql_stmt_category = "CREATE TABLE IF NOT EXISTS CATEGORIES (ID INTEGER PRIMARY KEY AUTOINCREMENT,CATEGORY_NAME TEXT,POSITION INTEGER,VISIBLE INTEGER)";
            if (sqlite3_exec(ioDB, sql_stmt_category, NULL, NULL, &errMsg) == SQLITE_OK)
            {
                DebugLog(@"CATEGORIES table created");
            }
            else
            {
                DebugLog(@"Failed to create CATEGORIES table");
            }
            
            const char *sql_stmt_imgmaster = "CREATE TABLE IF NOT EXISTS IMAGE_MASTER(ID INTEGER PRIMARY KEY AUTOINCREMENT,TITLE TEXT,NOTES TEXT,CATEGORY_ID INTEGER,LOCATION TEXT,LATITUDE REAL,LONGITUDE REAL,DATE REAL,FOREIGN KEY(CATEGORY_ID) REFERENCES CATEGORIES(ID))";
            
            if (sqlite3_exec(ioDB, sql_stmt_imgmaster, NULL, NULL, &errMsg) == SQLITE_OK)
            {
                DebugLog(@"IMAGE_MASTER table created");
            }
            else
            {
                DebugLog(@"Failed to create IMAGE_MASTER table");
            }
            
            const char *sql_stmt_img_set_relation = "CREATE TABLE IF NOT EXISTS IMAGE_SET_RELATION(IMAGE_MASTER_ID INTEGER,IMAGE_PATH TEXT,FOREIGN KEY(IMAGE_MASTER_ID) REFERENCES IMAGE_MASTER(ID))";
            if (sqlite3_exec(ioDB, sql_stmt_img_set_relation, NULL, NULL, &errMsg) == SQLITE_OK)
            {
                DebugLog(@"IMAGE_SET_RELATION table created");
            }
            else
            {
                DebugLog(@"Failed to create IMAGE_SET_RELATION table");
            }
            
            const char *sql_stmt_tags = "CREATE TABLE IF NOT EXISTS TAGS(ID INTEGER PRIMARY KEY AUTOINCREMENT,TAG_NAME TEXT)";
            if (sqlite3_exec(ioDB, sql_stmt_tags, NULL, NULL, &errMsg) == SQLITE_OK)
            {
                DebugLog(@"TAGS table created");
            }
            else
            {
                DebugLog(@"Failed to create TAGS table");
            }
            
            const char *sql_stmt_icons = "CREATE TABLE IF NOT EXISTS ICONS(ID INTEGER PRIMARY KEY AUTOINCREMENT,ICON_PATH TEXT)";
            if (sqlite3_exec(ioDB, sql_stmt_icons, NULL, NULL, &errMsg) == SQLITE_OK)
            {
                DebugLog(@"ICONS table created");
            }
            else
            {
                DebugLog(@"Failed to create ICONS table");
            }
            
            const char *sql_stmt_groups = "CREATE TABLE GROUPS(ID INTEGER PRIMARY KEY AUTOINCREMENT,GROUP_NAME TEXT,ICON_ID INTEGER,FOREIGN KEY(ICON_ID) REFERENCES ICONS(ID))";
            if (sqlite3_exec(ioDB, sql_stmt_groups, NULL, NULL, &errMsg) == SQLITE_OK)
            {
                DebugLog(@"GROUPS table created");
            }
            else
            {
                DebugLog(@"Failed to create GROUPS table");
            }
//            const char *sql_stmt_groups = "CREATE TABLE GROUPS(ID INTEGER PRIMARY KEY AUTOINCREMENT,GROUP_NAME TEXT,ICON_PATH TEXT)";
//            if (sqlite3_exec(ioDB, sql_stmt_groups, NULL, NULL, &errMsg) == SQLITE_OK)
//            {
//                DebugLog(@"GROUPS table created");
//            }
//            else
//            {
//                DebugLog(@"Failed to create GROUPS table");
//            }
            
            const char *sql_stmt_img_tag_relation = "CREATE TABLE IF NOT EXISTS IMAGE_TAG_RELATION(IMAGE_MASTER_ID INTEGER,TAG_ID INTEGER)";
            if (sqlite3_exec(ioDB, sql_stmt_img_tag_relation, NULL, NULL, &errMsg) == SQLITE_OK)
            {
                DebugLog(@"IMAGE_TAG_RELATION table created");
            }
            else
            {
                DebugLog(@"Failed to create IMAGE_TAG_RELATION table");
            }
            
            const char *sql_stmt_img_group_relation = "CREATE TABLE IF NOT EXISTS IMAGE_GROUP_RELATION(IMAGE_MASTER_ID INTEGER,GROUP_ID INTEGER)";
            if (sqlite3_exec(ioDB, sql_stmt_img_group_relation, NULL, NULL, &errMsg) == SQLITE_OK)
            {
                DebugLog(@"IMAGE_GROUP_RELATION table created");
            }
            else
            {
                DebugLog(@"Failed to create IMAGE_GROUP_RELATION table");
            }

            NSArray *catgArray = [[NSArray alloc] initWithObjects:@"catg1",@"catg2",@"catg3",@"catg4", nil];
            for(int i = 0; i < catgArray.count ; i++)
            {
                sqlite3_stmt    *statement;
                NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO CATEGORIES (CATEGORY_NAME,POSITION,VISIBLE) VALUES (\"%@\",%d,1)", [catgArray objectAtIndex:i],i];
                const char *insert_stmt = [insertSQL UTF8String];
                sqlite3_prepare_v2(ioDB, insert_stmt, -1, &statement, NULL);
                if (sqlite3_step(statement) == SQLITE_DONE)
                {
                    DebugLog(@"%d - %@ row inserted",i,[catgArray objectAtIndex:i]);
                }
                else
                {
                    DebugLog(@"%d - %@ row insertion failed",i,[catgArray objectAtIndex:i]);
                }
                sqlite3_finalize(statement);
            }
            
            NSArray *tagsArray = [[NSArray alloc] initWithObjects:@"tag1",@"tag2",@"tag3",@"tag4", nil];
            for(int i = 0; i < tagsArray.count ; i++)
            {
                sqlite3_stmt    *statement;
                NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO TAGS (TAG_NAME) VALUES (\"%@\")", [tagsArray objectAtIndex:i]];
                const char *insert_stmt = [insertSQL UTF8String];
                sqlite3_prepare_v2(ioDB, insert_stmt, -1, &statement, NULL);
                if (sqlite3_step(statement) == SQLITE_DONE)
                {
                    DebugLog(@"%d - %@ row inserted",i,[tagsArray objectAtIndex:i]);
                }
                else
                {
                    DebugLog(@"%d - %@ row insertion failed",i,[tagsArray objectAtIndex:i]);
                }
                sqlite3_finalize(statement);
            }
            NSArray *iconsArray = [[NSArray alloc] initWithObjects:@"folder1.png",@"folder2.png",@"folder3.png",@"folder4.png",@"folder5.png",@"folder6.png", nil];
            sqlite3_stmt *statement;
            for(int i = 0; i < iconsArray.count ; i++)
            {
                NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO ICONS (ICON_PATH) VALUES (\"%@\")", [iconsArray objectAtIndex:i]];
                const char *insert_stmt = [insertSQL UTF8String];
                sqlite3_prepare_v2(ioDB, insert_stmt, -1, &statement, NULL);
                if (sqlite3_step(statement) == SQLITE_DONE)
                {
                    DebugLog(@" icon_path row inserted");
                }
                else
                {
                    DebugLog(@" icon_path row insertion failed");
                }
                sqlite3_finalize(statement);
            }
            NSArray *groupsArray = [[NSArray alloc] initWithObjects:FAVOURITE_GROUPNAME,UNGROUPED_GROUPNAME,@"group1",@"group2", nil];
            for(int i = 0; i < groupsArray.count ; i++)
            {
                sqlite3_stmt *statement;
                NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO GROUPS(GROUP_NAME,ICON_ID) VALUES (\"%@\",%d)", [[groupsArray objectAtIndex:i] lowercaseString],i+1];
                const char *insert_stmt = [insertSQL UTF8String];
                sqlite3_prepare_v2(ioDB, insert_stmt, -1, &statement, NULL);
                if (sqlite3_step(statement) == SQLITE_DONE)
                {
                    DebugLog(@"%d - %@ row inserted",i,[groupsArray objectAtIndex:i]);
                }
                else
                {
                    DebugLog(@"%d - %@ row insertion failed",i,[groupsArray objectAtIndex:i]);
                }
                sqlite3_finalize(statement);
            }
            
            const char *sql_stmt_groupstemp = "CREATE TABLE GROUPS_TEMP(GROUP_ID INTEGER,GROUP_NAME TEXT,ICON_ID INTEGER,SELECTED INTEGER)";
            if (sqlite3_exec(ioDB, sql_stmt_groupstemp, NULL, NULL, &errMsg) == SQLITE_OK)
            {
                DebugLog(@"GROUPS_TEMP table created");
            }
            else
            {
                DebugLog(@"Failed to create GROUPS_TEMP table");
            }
            
            const char *sql_stmt_tagstemp = "CREATE TABLE TAGS_TEMP(TAG_ID INTEGER,TAG_NAME TEXT,SELECTED INTEGER)";
            if (sqlite3_exec(ioDB, sql_stmt_tagstemp, NULL, NULL, &errMsg) == SQLITE_OK)
            {
                DebugLog(@"TAGS_TEMP table created");
            }
            else
            {
                DebugLog(@"Failed to create TAGS_TEMP table");
            }
        }
        else {
            DebugLog(@"Failed to open/create database");
        }
        
        sqlite3_close(ioDB);
    }
}


#pragma Reachability Methods
-(void) startCheckNetwork
{
    
	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
	
    internetReach = [Reachability reachabilityForInternetConnection];
	[internetReach startNotifer];
    [self updateReachabitlityFlag:internetReach];
}

-(void) stopCheckNetwork
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
	internetReach = nil;
}

- (void)reachabilityChanged:(NSNotification *)note
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
	[self updateReachabilityStatus: curReach];
}

- (void) updateReachabilityStatus: (Reachability*) curReach
{
	if(curReach == internetReach)
	{
		[self updateReachabitlityFlag: curReach];
    }
}

- (void) updateReachabitlityFlag: (Reachability*) curReach
{
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    BOOL connectionRequired=[curReach connectionRequired];
    
    switch (netStatus)
    {
        case NotReachable:
        {
            networkavailable =  NO;
            break;
        }
            
        case ReachableViaWWAN:
        {
			if(connectionRequired==NO)
			{
                networkavailable = YES;
			}
			else
			{
				//this is for invoking internet library
				NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:@"maps.google.com"] cachePolicy:NO timeoutInterval:15.0] ;
				
				NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
				if (theConnection) {
					connectionRequired	= NO;
				}
			}
			break;
        }
        case ReachableViaWiFi:
        {
            if(connectionRequired==NO)
            {
                networkavailable = YES;
            }
            break;
		}
    }
    if(connectionRequired)
    {
		networkavailable = NO;
    }
    //DebugLog(@"network status = %d",networkavailable);
}

// ============= Load local Image ===========================
- (UIImage *)loadImage:(NSString *)limgName
{
    DebugLog(@"fullpath = %@" , limgName);
    if(limgName != nil) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* fullPath = [documentsDirectory stringByAppendingPathComponent:limgName];
        DebugLog(@"fullpath = %@" , fullPath);
        UIImage* image = [UIImage imageWithContentsOfFile:fullPath];
        if(image != nil)
            return image;
    }
    UIImage *tempImage = [UIImage imageNamed:@"placeholder.png"];
    return tempImage;
}
// ============= save Image ===========================
// ============= Save Image Locally ===========================
- (NSString *)saveImage: (UIImage*)image
{
#if TARGET_IPHONE_SIMULATOR
    if (image == nil) {
        image = [UIImage imageNamed:@"placeholder.png"];
    }
#endif
    if (image != nil)
    {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,  NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filename = [NSString stringWithFormat: @"io_%llu.png",(long long)[[NSDate date] timeIntervalSinceReferenceDate]];
        DebugLog(@"filename = %@" , filename);
        NSString* fullPath = [documentsDirectory stringByAppendingPathComponent:filename];
        DebugLog(@"fullpath = %@" , fullPath);
        NSData* data = UIImagePNGRepresentation(image);
        BOOL success = [fileManager createFileAtPath:fullPath contents:data attributes:nil];
        if(success == YES)
            return filename;
        else
            return @"";
    } else {
        return  @"";
    }
}
// ============= remove local Image ===========================
- (void)removeImage:(NSString*)lpath {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:lpath];
    BOOL success = [fileManager removeItemAtPath: fullPath error:NULL];
    if(success == YES)
        DebugLog(@"image removed");
    else
        DebugLog(@"image NOT removed");
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    DebugLog(@"applicationWillEnterForeground");
    [Appirater appEnteredForeground:YES];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
