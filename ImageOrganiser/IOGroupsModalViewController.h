//
//  IOGroupsModalViewController.h
//  ImageOrganiser
//
//  Created by Kirti Nikam on 02/05/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

#define COPY_MODE 0
#define MOVE_MODE 1

@interface IOGroupsModalViewController : UIViewController
{
    sqlite3 *ioDB;
}
@property (readwrite,nonatomic) int currentGroupId;
@property (readwrite,nonatomic) int currentImgId;
@property (readwrite,nonatomic) int mode;
@property (strong, nonatomic) IBOutlet UITableView *groupsTableView;
@property (retain,nonatomic) NSMutableArray *groupArray;
@end
