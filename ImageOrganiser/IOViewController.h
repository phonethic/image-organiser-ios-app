//
//  IOViewController.h
//  ImageOrganiser
//
//  Created by Kirti Nikam on 02/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>
#import "RecordObject.h"
#import <MessageUI/MFMailComposeViewController.h>

@class RecordObject;

@interface IOViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate,UIPickerViewDataSource,UIPickerViewDelegate,MFMailComposeViewControllerDelegate>
{
    sqlite3 *ioDB;
//    NSCache *imageCache;
    
    int moreBtnState;
    
    UIActionSheet *actionSheet;
    
    BOOL addfirstResposerToSearchBar;
    BOOL isSearchOn;
    BOOL canSelectRow;
}
@property (strong, nonatomic) IBOutlet UIView *transparentDropdownView;
@property (strong, nonatomic) IBOutlet UIView *dropdownView;
@property (copy,nonatomic) NSString *sortByString;
@property (retain,nonatomic) NSMutableArray *sortArray;
@property (retain,nonatomic) NSMutableArray *recordObjectsArray;
@property (retain,nonatomic) NSMutableArray *moreArray;
@property (retain,nonatomic) NSArray* buttonData;
@property (retain,nonatomic) NSIndexPath* lastIndexPath;
@property (retain,nonatomic) NSIndexPath* deleteRecIndexPath;

//@property (nonatomic) float openCellLastTX;
@property (nonatomic, strong) NSIndexPath *openCellIndexPath;

@property (strong, nonatomic) IBOutlet UITableView *listTableView;
@property (strong, nonatomic) IBOutlet UILabel *emptyInfoLabel;
@property (strong, nonatomic) IBOutlet UIImageView *emptyInfoImageView;

@property (strong, nonatomic) IBOutlet UIPickerView *pickerView;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *moreTableView;
@property (strong, nonatomic) IBOutlet UIButton *dropDownBackBtn;
- (IBAction)dropDownBackBtnClicked:(id)sender;
@end
