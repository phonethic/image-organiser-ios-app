//
//  IOSegmentViewController.h
//  ImageOrganiser
//
//  Created by Kirti Nikam on 09/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IOAddRecordViewController.h"
#import "IOAddRecordPageViewController.h"

@interface IOSegmentViewController : UIViewController
{
    IOAddRecordViewController *addRecordListController;
    IOAddRecordPageViewController *addRecordPageController;
}
@property (nonatomic, readwrite) int currentImgRowId;
// Array of view controllers to switch between
@property (nonatomic, copy) NSArray *allViewControllers;

// Currently selected view controller
@property (nonatomic, strong) UIViewController *currentViewController;
@end
