//
//  IOGalleryViewController.m
//  ImageOrganiser
//
//  Created by Kirti Nikam on 17/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "IOGalleryViewController.h"
#import "ImageViewController.h"
#import "ImageObject.h"
#import "constants.h"
#import "IOAddRecordViewController.h"

@interface IOGalleryViewController ()
@property (assign) NSUInteger page;
@end
NSUInteger kNumberOfPages;

@implementation IOGalleryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    
	for (NSUInteger i =0; i < [_viewControllers count]; i++) {
		[self loadScrollViewWithPage:i];
	}
    
//	_pageControl.currentPage = 0;
//	_page = 0;
	[_pageControl setNumberOfPages:kNumberOfPages];
    
	UIViewController *viewController = [_viewControllers objectAtIndex:_pageControl.currentPage];
	if (viewController.view.superview != nil) {
		[viewController viewWillAppear:animated];
	}
    
	_scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width * kNumberOfPages, _scrollView.frame.size.height);
}


- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	UIViewController *viewController = [_viewControllers objectAtIndex:_pageControl.currentPage];
	if (viewController.view.superview != nil) {
		[viewController viewDidAppear:animated];
	}
}
- (void)viewWillDisappear:(BOOL)animated {
    if (_viewControllers.count > 0) {
        UIViewController *viewController = [_viewControllers objectAtIndex:_pageControl.currentPage];
        if (viewController.view.superview != nil) {
            [viewController viewWillDisappear:animated];
        }
    }
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    if (_viewControllers.count > 0) {
        UIViewController *viewController = [_viewControllers objectAtIndex:_pageControl.currentPage];
        if (viewController.view.superview != nil) {
            [viewController viewDidDisappear:animated];
        }
    }
	[super viewDidDisappear:animated];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(deleteImageClicked:)];
    
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 50, 32)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    DebugLog(@"IOGallery : _imageObjsArray = %@",_imageObjsArray);
    for(ImageObject *obj in _imageObjsArray)
    {
        DebugLog(@"IOGallery : Image %@ isNew %d isDEleted %d",obj.imagePath,obj.isNew,obj.isDeleted);
    }
    
    NSMutableArray *controllers = [[NSMutableArray alloc] init];
    for (unsigned i = 0; i < [_imageObjsArray count]; i++)
    {
        ImageObject *imgObj = (ImageObject *)[_imageObjsArray objectAtIndex:i];
        if (!imgObj.isDeleted) {
            [controllers addObject:[NSNull null]];
        }
    }
    _viewControllers = controllers;
    kNumberOfPages =_viewControllers.count;
    DebugLog(@"IOGallery : kNumberOfPages %d",kNumberOfPages);

    // a page is the width of the scroll view
    _scrollView.clipsToBounds = YES;
	_scrollView.scrollEnabled = YES;
    _scrollView.pagingEnabled = YES;
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width * kNumberOfPages, _scrollView.frame.size.height + 280);
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.scrollsToTop = YES;
    _scrollView.delegate = self;
    _scrollView.bounces = NO;
    _scrollView.directionalLockEnabled = YES;
    //scrollView.maximumZoomScale = 4.0;
	//scrollView.minimumZoomScale = 0.75;
    //scrollView.bouncesZoom = NO;
    //pageControl.frame = CGRectMake(0, 375, 320, 36);
    _pageControl.backgroundColor = [UIColor blackColor];
    _pageControl.numberOfPages = kNumberOfPages;
    _pageControl.currentPage = 0;
    _pageControl.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
//    for (int i = 0; i < 2 && i< kNumberOfPages; i++) {
//        [self loadScrollViewWithPage:i];
//    }
    self.title = [NSString stringWithFormat:@"%d of %d",_pageControl.currentPage+1,_viewControllers.count];
}
- (void)backBtnPressed:(id)sender
{
    UIViewController *previousViewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    DebugLog(@"IOGallery : parent %@",previousViewController);
    if ([previousViewController isKindOfClass:[IOAddRecordViewController class]])
    {
        IOAddRecordViewController *parent = (IOAddRecordViewController *) previousViewController;
        if(parent != nil)
        {
            [parent reloadImageCell];
        }
        parent = nil;
    }
    [self.navigationController popViewControllerAnimated:YES];
    previousViewController = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setScrollView:nil];
    [self setPageControl:nil];
    [super viewDidUnload];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
//- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
//    UIViewController *viewController = [_viewControllers objectAtIndex:_pageControl.currentPage];
//	[viewController willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
//}
//
//- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
//    
//	UIViewController *viewController = [_viewControllers objectAtIndex:_pageControl.currentPage];
//	[viewController willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
//    
//	_scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width * kNumberOfPages, _scrollView.frame.size.height);
//	NSUInteger page = 0;
//	for (viewController in _viewControllers) {
//		CGRect frame = _scrollView.frame;
//		frame.origin.x = frame.size.width * page;
//		frame.origin.y = 0;
//		viewController.view.frame = frame;
//		page++;
//	}
//	
//	CGRect frame = _scrollView.frame;
//    frame.origin.x = frame.size.width * _page;
//    frame.origin.y = 0;
//	[_scrollView scrollRectToVisible:frame animated:NO];
//    
//}

- (IBAction)changePage:(id)sender
{
    DebugLog(@"IOGallery : Page changed");
    int page = _pageControl.currentPage;
	
	
	// update the scroll view to the appropriate page
    CGRect frame = _scrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    
	UIViewController *oldViewController = [_viewControllers objectAtIndex:_page];
	UIViewController *newViewController = [_viewControllers objectAtIndex:_pageControl.currentPage];
	[oldViewController viewWillDisappear:YES];
	[newViewController viewWillAppear:YES];
    
    [UIView animateWithDuration:1
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{ [_scrollView scrollRectToVisible:frame animated:NO]; }
                     completion:NULL];
    
    //[scrollView scrollRectToVisible:frame animated:YES];
    
	// Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
    pageControlUsed = YES;
}


- (void)unloadScrollViewWithPage:(int)page {
    if (page < 0) return;
    if (page >= kNumberOfPages) return;
    
    UIViewController *controller = [_viewControllers objectAtIndex:page];
    
    if ((NSNull *)controller != [NSNull null]) {
        if (nil != controller.view.superview) {
            [controller.view removeFromSuperview];
            controller.view=nil;
        }
        
        [_viewControllers replaceObjectAtIndex:page withObject:[NSNull null]];
    }
}
-(int)getImageIndex:(int)currentPage
{
    int imageIndex = 0;
    ImageObject *imageObj = nil;
    for (int i = 0; i < _imageObjsArray.count; i++) {
        imageObj = (ImageObject *)[_imageObjsArray objectAtIndex:i];
        if (!imageObj.isDeleted) {
            {
                if (imageIndex == currentPage) {
                    imageIndex = i;
                    break;
                }
                imageIndex ++;
            }
        }
    }
    DebugLog(@"IOGallery : imageIndex %d",imageIndex);
    return imageIndex;
}
- (void)loadScrollViewWithPage:(int)page {
    DebugLog(@"IOGallery : loadScrollViewWithPage %d",page);
    DebugLog(@"IOGallery : total _viewControllers.count %d _imageObjsArray.count %d",_viewControllers.count,_imageObjsArray.count);

    if (page < 0) return;
    if (page >= kNumberOfPages) return;
   
    // replace the placeholder if necessary
    UIViewController *controller = [_viewControllers objectAtIndex:page];
        if ((NSNull *)controller == [NSNull null])
        {
            ImageObject *imageObj = (ImageObject *)[_imageObjsArray objectAtIndex:[self getImageIndex:page]];
            controller = (UIViewController *)[[ImageViewController alloc] initWithPageNumber:page imagepath:imageObj.imagePath];
            [_viewControllers replaceObjectAtIndex:page withObject:controller];
        }
    DebugLog(@"IOGallery : _pageControl.currentPage %d _viewControllers %d",_pageControl.currentPage,_viewControllers.count);
    ImageViewController *viewC = (ImageViewController *)controller;
    DebugLog(@"IOGallery : ImageViewcontroller %@",viewC.imagePath);
    
    // add the controller's view to the scroll view
    if (controller.view.superview == nil) {
            CGRect frame = _scrollView.frame;
            frame.origin.x = frame.size.width * page;
            frame.origin.y = 0;
            controller.view.frame = frame;
            [_scrollView addSubview:controller.view];
    }
}

//- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
//	UIViewController *oldViewController = [_viewControllers objectAtIndex:_page];
//	UIViewController *newViewController = [_viewControllers objectAtIndex:_pageControl.currentPage];
//	[oldViewController viewDidDisappear:YES];
//	[newViewController viewDidAppear:YES];
//	_page = _pageControl.currentPage;
//}


- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    // We don't want a "feedback loop" between the UIPageControl and the scroll delegate in
    // which a scroll event generated from the user hitting the page control triggers updates from
    // the delegate method. We use a boolean to disable the delegate logic when the page control is used.
    if (pageControlUsed)
    {
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    }
	
    // Switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = _scrollView.frame.size.width;
    int page = floor((_scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if (_pageControl.currentPage != page) {
		UIViewController *oldViewController = [_viewControllers objectAtIndex:_pageControl.currentPage];
		UIViewController *newViewController = [_viewControllers objectAtIndex:page];
		[oldViewController viewWillDisappear:YES];
		[newViewController viewWillAppear:YES];
		_pageControl.currentPage = page;
		[oldViewController viewDidDisappear:YES];
		[newViewController viewDidAppear:YES];
		_page = page;
	}
    
}

// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
    self.title = [NSString stringWithFormat:@"%d of %d",_pageControl.currentPage+1,_viewControllers.count];
}


-(void)deleteImageClicked:(id)sender
{
    // set isDeleted = TRUE in imageObjectsArray
    int imageIndex = [self getImageIndex:_pageControl.currentPage];
    ImageObject *tempObj = (ImageObject *)[_imageObjsArray objectAtIndex:imageIndex];
    tempObj.isDeleted = YES;
    [_imageObjsArray replaceObjectAtIndex:imageIndex withObject:tempObj];
    tempObj = nil;
    
    // check is done or not
    for(ImageObject *obj in _imageObjsArray)
    {
        DebugLog(@"IOGallery : Image %@ isNew %d isDEleted %d",obj.imagePath,obj.isNew,obj.isDeleted);
    }
    
    // remove that controller from _viewcontroller arrays and from scrollview.
    DebugLog(@"IOGallery : _pageControl.currentPage %d _viewControllers %d",_pageControl.currentPage,_viewControllers.count);
    ImageViewController *viewC = (ImageViewController *)[_viewControllers objectAtIndex:_pageControl.currentPage];
    DebugLog(@"IOGallery : ImageViewcontroller %@",viewC.imagePath);
    DebugLog(@"IOGallery : _scrollView.subviews count %d",_scrollView.subviews.count);

    [self unloadScrollViewWithPage:_pageControl.currentPage];
    [_viewControllers removeObjectAtIndex:_pageControl.currentPage];
    DebugLog(@"IOGallery : _viewControllers %d",_viewControllers.count);
    DebugLog(@"IOGallery : _scrollView.subviews count %d",_scrollView.subviews.count);

    //Change count of total number of pages
    kNumberOfPages =_viewControllers .count;
    
    if (kNumberOfPages > 0) {
        // rearrange views in scrollviews
        for(int start = _pageControl.currentPage; start<_viewControllers.count;start++)
        {
            UIView *view = [_scrollView.subviews objectAtIndex:start];
            CGRect frame = _scrollView.frame;
            frame.origin.x = frame.size.width * start;
            frame.origin.y = 0;
            view.frame = frame;
            //[view removeFromSuperview];
            //[_scrollView addSubview:view];
        }
        self.title = [NSString stringWithFormat:@"%d of %d",_pageControl.currentPage+1,_viewControllers.count];
        [self viewWillAppear:YES];
    }
    else
    {
        [_scrollView setHidden:YES];
        [_pageControl setHidden:YES];
        self.navigationItem.rightBarButtonItem = nil;
        self.title = @"";
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                            message:@"No image available"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
}
@end
