//
//  IOGroupDetailViewController.m
//  ImageOrganiser
//
//  Created by Kirti Nikam on 25/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
#import <objc/runtime.h>
#import "UIImage+Resize.h"

#import "IOGroupDetailViewController.h"
#import "IOAppDelegate.h"
#import "RecordObject.h"
#import "IOGroupsModalViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "IOAddRecordViewController.h"
#import "constants.h"
#import "CommonCallback.h"

#define RENAME_GROUP @"Rename Group"
#define ADD_NEW_RECORD @"Add New Record"

static char * const kIndexPathAssociationKey1 = "IO_indexPath1";

@interface IOGroupDetailViewController ()

@end

@implementation IOGroupDetailViewController
@synthesize openCellIndexPath;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self readFromGroupRecordsFromTable];
    [_detailTableView reloadData];
    actionBtnState = 0;
}
-(void)viewDidDisappear:(BOOL)animated
{
    DebugLog(@"ManageGroupsDetail : viewDidDisappear");
    [super viewDidDisappear:animated];
    [self closeMorePullDownMenu];
    if (_lastIndexPath != nil) {
        UITableViewCell *cell  = (UITableViewCell *)[_detailTableView cellForRowAtIndexPath:_lastIndexPath];
        [self closeSwipedCell:cell];
        cell = nil;
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //self.view.backgroundColor = BGCOLOR;
    self.view.backgroundColor = [UIColor clearColor];

    [_detailTableView setBackgroundColor:[UIColor clearColor]];
    
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 50, 32)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    UIBarButtonItem *actionButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(actionClicked:)];
    self.navigationItem.rightBarButtonItem = actionButton;
    
//    UIButton *actionButton =  [UIButton buttonWithType:UIButtonTypeCustom];
//    [actionButton setImage:[UIImage imageNamed:@"add_bar_button.png"] forState:UIControlStateNormal];
//    [actionButton addTarget:self action:@selector(actionClicked:) forControlEvents:UIControlEventTouchUpInside];
//    [actionButton setFrame:CGRectMake(0, 0, 30, 30)];
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:actionButton];
    
    _buttonData = [[NSArray alloc] initWithObjects:@"action",@"trash", nil];
    
//    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
//                                          initWithTarget:self action:@selector(handleLongPress:)];
//    lpgr.minimumPressDuration = 2.0; //seconds
//    lpgr.delegate = self;
//    [_detailTableView addGestureRecognizer:lpgr];

    _moreArray = [[NSMutableArray alloc] initWithObjects:RENAME_GROUP,ADD_NEW_RECORD, nil];
    int favGroupId = [self getGroupId:FAVOURITE_GROUPNAME];
    int ungroupId = [self getGroupId:UNGROUPED_GROUPNAME];
    if (self.currentGroupId == favGroupId || self.currentGroupId == ungroupId) {
        [_moreArray removeObject:RENAME_GROUP];
        [_actionTableView reloadData];
    }
     [_actionTableView sizeToFit];
    _dropDownMenuView.frame = CGRectMake(_dropDownMenuView.frame.origin.x, -(_dropDownMenuView.frame.size.height-_dropDownMenuView.frame.origin.y), _actionTableView.frame.size.width,_actionTableView.frame.size.height);
    
    _actionTableView.backgroundColor = [UIColor colorWithRed:53/255.0 green:53/255.0 blue:52/255.0 alpha:1];
    _actionTableView.separatorColor = [UIColor colorWithPatternImage:[CommonCallback separatorImage]];
    
    _dropDownMenuView.backgroundColor = [UIColor colorWithRed:53/255.0 green:53/255.0 blue:52/255.0 alpha:1];
    _dropDownMenuView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    _dropDownMenuView.layer.shadowColor = [UIColor blackColor].CGColor;
    _dropDownMenuView.layer.shadowOffset = CGSizeMake(0, 3);
    _dropDownMenuView.layer.shadowOpacity = 1;
    _dropDownMenuView.layer.shadowRadius = 4;
    _dropDownMenuView.layer.shouldRasterize = YES;
    _dropDownMenuView.layer.rasterizationScale = [UIScreen mainScreen].scale;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setDetailTableView:nil];
    [self setActionTableView:nil];
    [self setDropDownMenuView:nil];
    [super viewDidUnload];
}

- (void)backBtnPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:_detailTableView];
    
    NSIndexPath *indexPath = [_detailTableView indexPathForRowAtPoint:p];
    if (indexPath == nil)
        DebugLog(@"ManageGroupsDetail : long press on table view but not on a row");
    else
        DebugLog(@"ManageGroupsDetail : long press on table view at row %d", indexPath.row);
}

-(void)actionClicked:(id)sender
{
    DebugLog(@"ManageGroupsDetail : actionClicked");
    if (actionBtnState == 0)
    {
        [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
        [UIView animateWithDuration:0.2 animations:^{
            _actionTableView.frame = CGRectMake(0, 20, self.navigationController.navigationBar.frame.size.width,_actionTableView.frame.size.height);
            _dropDownMenuView.frame = CGRectMake(0, 0, self.navigationController.navigationBar.frame.size.width, _actionTableView.frame.size.height+20);
            
        } completion:^(BOOL finished)
         {
             [UIView animateWithDuration:0.3 animations:^{
                 _actionTableView.frame = CGRectMake(0, 0, self.navigationController.navigationBar.frame.size.width,_actionTableView.frame.size.height);
                 _dropDownMenuView.frame = CGRectMake(0, 0, self.navigationController.navigationBar.frame.size.width,_actionTableView.frame.size.height-20);
             }];
         }];
        actionBtnState = 1;
    }
    else
    {
        [self closeMorePullDownMenu];
    }
}

-(void)closeMorePullDownMenu
{
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView animateWithDuration:0.2 animations:^{
        _actionTableView.frame = CGRectMake(0,20, _actionTableView.frame.size.width,_actionTableView.frame.size.height);
        _dropDownMenuView.frame = CGRectMake(0, 0, self.navigationController.navigationBar.frame.size.width, _dropDownMenuView.frame.size.height+20);
    } completion:^(BOOL finished)
     {
         [UIView animateWithDuration:0.3 animations:^{
             _actionTableView.frame = CGRectMake(0,0, _actionTableView.frame.size.width,_actionTableView.frame.size.height);
             _dropDownMenuView.frame = CGRectMake(0, -(_dropDownMenuView.frame.size.height-_dropDownMenuView.frame.origin.y)-20, self.navigationController.navigationBar.frame.size.width, _dropDownMenuView.frame.size.height-20);
         }];
     }];
    actionBtnState = 0;
}

-(void)reloadListOfRecordsData{
    [self reloadOnlyGroupNames];
    [_detailTableView reloadData];
}

#pragma UITableView methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:_detailTableView]) {
        return [self getGroupRecordsRowsCount];

    }else if([tableView isEqual:_actionTableView]) {
            return _moreArray.count;
        }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:_detailTableView]) {
        return 90;
    }
    return 50;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:_detailTableView])
    {
        
        static NSString *cellIdentifier = @"detailcell";
        
        UIImageView *thumbImgView;
        UIImageView *favImgView;
        
        UIView *topView;
        UIView *swipeView;
        UILabel *lblTitle;
        UILabel *lblDate;
        UILabel *lblGroups;
        

        UITableViewCell *cell= (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
            
            // Create the top view
            topView = [[UIView alloc] initWithFrame:CGRectMake(10,10,300, 80)];
            topView.tag = 444;
            topView.clipsToBounds = YES;
            [topView setBackgroundColor:YELLOW_COLOR];
            
            thumbImgView = [[UIImageView alloc] initWithFrame:CGRectMake(-1,0, 80, 80)];
            thumbImgView.tag = 101;
            thumbImgView.contentMode = UIViewContentModeScaleToFill;
            thumbImgView.layer.masksToBounds = YES;
            thumbImgView.image = [UIImage imageNamed:@"Publish_ImageView.png"];
            //[cell.contentView addSubview:thumbImgView];
            [topView addSubview:thumbImgView];
            
            favImgView = [[UIImageView alloc] initWithFrame:CGRectMake(thumbImgView.frame.origin.x,thumbImgView.frame.origin.y, 30, 30)];
            favImgView.tag = 105;
            favImgView.contentMode = UIViewContentModeScaleToFill;
            favImgView.layer.masksToBounds = YES;
            [topView addSubview:favImgView];
            
            lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImgView.frame) + 10,2, topView.frame.size.width - (CGRectGetMaxX(thumbImgView.frame) +30), 30)];
            lblTitle.tag = 102;
            lblTitle.shadowOffset  = CGSizeMake(0.0, 2.0);
            lblTitle.font = DEFAULT_BOLD_FONT(18.0);
            lblTitle.textAlignment = UITextAlignmentLeft;
            lblTitle.textColor = DEFAULT_COLOR;
            lblTitle.backgroundColor =  [UIColor clearColor];
            //  [cell.contentView addSubview:lblTitle];
            [topView addSubview:lblTitle];
            
            lblDate = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImgView.frame) + 10,CGRectGetMaxY(lblTitle.frame)+3,160, 15 )];
            lblDate.tag = 103;
            lblDate.font = DEFAULT_FONT(13.0);
            lblDate.textAlignment = UITextAlignmentLeft;
            lblDate.textColor = [UIColor darkGrayColor];
            lblDate.backgroundColor =  [UIColor clearColor];
            // [cell.contentView addSubview:lblDate];
            [topView addSubview:lblDate];
            
            lblGroups = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImgView.frame) + 10, CGRectGetMaxY(lblDate.frame) + 2, lblTitle.frame.size.width, 20)];
            lblGroups.tag = 104;
            lblGroups.shadowOffset  = CGSizeMake(0.0, 2.0);
            lblGroups.font = DEFAULT_FONT(13.0);
            lblGroups.textAlignment = UITextAlignmentLeft;
            lblGroups.textColor = [UIColor darkGrayColor];
            lblGroups.backgroundColor =  [UIColor clearColor];
            lblGroups.numberOfLines = 3;
            [topView addSubview:lblGroups];
            
//            UIImageView *arrowImgView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lblDate.frame) + 10,CGRectGetMaxY(lblTitle.frame)-8, 30, 30)];
//            arrowImgView.tag = 105;
//            arrowImgView.contentMode = UIViewContentModeScaleToFill;
//            arrowImgView.layer.masksToBounds = YES;
//            arrowImgView.image = [UIImage imageNamed:@"Mark.png"];
//            [topView addSubview:arrowImgView];
            
            
            // Create the swipe view
            swipeView = [[UIView alloc] initWithFrame:CGRectMake(10, 10,300, 80)];
            swipeView.tag = 555;
            swipeView.clipsToBounds = YES;
            // Add the background pattern
            swipeView.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"dotted-pattern.png"]];
            
            // Overlay a shadow image that adds a subtle darker drop shadow around the edges
            /* UIImage* shadow = [[UIImage imageNamed:@"inner-shadow.png"] stretchableImageWithLeftCapWidth:0 topCapHeight:0];
             UIImageView* shadowImageView = [[UIImageView alloc] initWithFrame:swipeView.frame];
             shadowImageView.alpha = 0.6;
             shadowImageView.image = shadow;
             shadowImageView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
             [swipeView addSubview:shadowImageView];*/
            
            // Iterate through the button data and create a button for each entry
            CGFloat leftEdge = 200;
            CGFloat button_space = 20;
            DebugLog(@"ManageGroupsDetail : _buttonData.count %d",_buttonData.count);
            for (int btnindex = 0;btnindex < _buttonData.count;btnindex++)
            {
                DebugLog(@"ManageGroupsDetail : btnindex %d -- leftedge %f -- %@",btnindex,leftEdge,[_buttonData objectAtIndex:btnindex]);
                // Create the button
                UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
                // Make sure the button ends up in the right place when the cell is resized
                button.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
                
                NSString *imageName = [_buttonData objectAtIndex:btnindex];
                UIImage* buttonImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png",imageName]];
                UIImage* highlightedbuttonImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_act.png",imageName]];
                
                button.frame = CGRectMake(leftEdge, (swipeView.center.y - buttonImage.size.height/2.0) - 5, buttonImage.size.width, buttonImage.size.height);
                //  UIImage* grayImage = [self imageFilledWith:[UIColor colorWithWhite:0.9 alpha:1.0] using:buttonImage];
                [button setImage:buttonImage forState:UIControlStateNormal];
                [button setImage:highlightedbuttonImage forState:UIControlStateHighlighted];
                
                [button addTarget:self action:@selector(touchUpInsideAction:event:) forControlEvents:UIControlEventTouchUpInside];
                button.tag = [[NSString stringWithFormat:@"555%d",btnindex] intValue];
                [swipeView addSubview:button];
                // Move the left edge in prepartion for the next button
                leftEdge = leftEdge + buttonImage.size.width + button_space;
            }
            
            topView.layer.cornerRadius = 5.0;
            swipeView.layer.cornerRadius = 5.0;
            topView.layer.borderColor = [UIColor clearColor].CGColor;
            swipeView.layer.borderColor = [UIColor clearColor].CGColor;
            
            // Add views to contentView
            [cell.contentView addSubview:swipeView];
            [cell.contentView addSubview:topView];
            
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor colorWithRed:0.0542516 green:0.44115  blue:0.699654 alpha:1];
            cell.selectedBackgroundView = bgColorView;

        }

        if ( _recordObjectsArray!= nil && _recordObjectsArray.count > 0) {
            DebugLog(@"ManageGroupsDetail : _recordObjectsArray %@",[_recordObjectsArray objectAtIndex:indexPath.row] );

            RecordObject *recObj = (RecordObject *)[_recordObjectsArray objectAtIndex:indexPath.row];
            
            DebugLog(@"ManageGroupsDetail : recObj %@",recObj.title);
            
            topView = (UIView *)[cell viewWithTag:444];
            swipeView = (UIView *)[cell viewWithTag:555];
//            UIButton* button = (UIButton *)[swipeView viewWithTag:5550];
            
            favImgView = (UIImageView *)[topView viewWithTag:105];
            
            if (recObj.isFav) {
                favImgView.image = [UIImage imageNamed:@"yellowstar.png"];
            }
//            else
//            {
//                [button setImage:[UIImage imageNamed:@"star-hollow.png"] forState:UIControlStateNormal];
//            }
            if (recObj.imgNamePath != nil && ![recObj.imgNamePath isEqualToString:@""])
            {
                NSString *imageFilename = recObj.imgNamePath;
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString* imagePath = [documentsDirectory stringByAppendingPathComponent:recObj.imgNamePath];
                
                
                thumbImgView = (UIImageView *)[topView viewWithTag:101];
                UIImage *image = [[IO_APP_DELEGATE imageCache] objectForKey:imageFilename];
                
                if (image) {
                    thumbImgView.image = image;
                } else {
                    thumbImgView.image = [UIImage imageNamed:@"Publish_ImageView.png"];
                    
                    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
                    
                    // Get the height of the cell to pass to the block.
                    CGFloat cellHeight = [tableView rowHeight];
                    
                    // Now, we can’t cancel a block once it begins, so we’ll use associated objects and compare
                    // index paths to see if we should continue once we have a resized image.
                    objc_setAssociatedObject(cell,
                                             kIndexPathAssociationKey1,
                                             indexPath,
                                             OBJC_ASSOCIATION_RETAIN);
                    
                    dispatch_async(queue, ^{
                        UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
                        
                        //  UIImage *resizedImage = [IO_APP_DELEGATE resizeImage:image newSize:CGSizeMake(cellHeight, cellHeight)];
                        
                        UIImage *resizedImage = [image resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:CGSizeMake(cellHeight, cellHeight) interpolationQuality:kCGInterpolationHigh];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSIndexPath *cellIndexPath =
                            (NSIndexPath *)objc_getAssociatedObject(cell, kIndexPathAssociationKey1);
                            
                            if ([indexPath isEqual:cellIndexPath]) {
                                thumbImgView.image = resizedImage;
                            }
                            
                            [[IO_APP_DELEGATE imageCache] setObject:resizedImage forKey:imageFilename];
                        });
                    });
                }
            }
            else
            {
                thumbImgView = (UIImageView *)[topView viewWithTag:101];
                thumbImgView.image = [UIImage imageNamed:@"Publish_ImageView.png"];
            }
            
            lblTitle = (UILabel *)[topView viewWithTag:102];
            if ([recObj.title isEqualToString:@""]) {
                lblTitle.text = @"";
            }
            else
            {
                lblTitle.text = [IO_APP_DELEGATE doCapitalFirstLetter:recObj.title];
            }
            
            lblDate = (UILabel *)[topView viewWithTag:103];
            NSDate *date = [NSDate dateWithTimeIntervalSinceReferenceDate:recObj.dateDouble];
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"dd MMM yyyy HH:mm a"];
            lblDate.text = [dateFormat stringFromDate:date];
            
            
            lblGroups = (UILabel *)[topView viewWithTag:104];
            lblGroups.text = recObj.groupsName;
            cell.tag = recObj.rowId;
            
        }
        // Create the pan gesture recognizers
        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
        pan.delegate = self;
        [cell addGestureRecognizer:pan];
        
        // Prevent selection highlighting
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.accessoryType = UITableViewCellAccessoryNone;
        [cell setNeedsLayout];
        
        [cell.contentView bringSubviewToFront:topView];
        return cell;
    }
    else if([tableView isEqual:_actionTableView])
    {
        static NSString *cellIdentifier = @"actionCell";
        UITableViewCell *cell= (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
            
            
            cell.textLabel.textAlignment = UITextAlignmentLeft;
            cell.textLabel.font = DEFAULT_BOLD_FONT(18.0);
            
            cell.textLabel.backgroundColor = [UIColor clearColor];
            cell.textLabel.textColor = [UIColor whiteColor];
            cell.textLabel.highlightedTextColor = [UIColor colorWithRed:128/255.0 green:126/255.0 blue:124/255.0 alpha:1];
            cell.textLabel.shadowColor = [UIColor blackColor];
            cell.textLabel.shadowOffset = CGSizeMake(0, -1);
            
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:1];
            cell.selectedBackgroundView = bgColorView;
            
        }
        cell.textLabel.text = [_moreArray objectAtIndex:indexPath.row];
        return cell;
    }
    return nil;
}

#pragma mark Tableview Delegate methods
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([tableView isEqual:_detailTableView])
    {
        
    }else if ([tableView isEqual:_actionTableView]) {
        if ([[_moreArray objectAtIndex:indexPath.row] isEqualToString:RENAME_GROUP]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Rename Group" message:@"Enter new group name" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:@"Cancel",nil];
            alert.alertViewStyle = UIAlertViewStylePlainTextInput;
            alert.tag = 2;
            [alert show];
        }
        else  if ([[_moreArray objectAtIndex:indexPath.row] isEqualToString:ADD_NEW_RECORD]) {
            IOAddRecordViewController *addRecordViewController = [[IOAddRecordViewController alloc] initWithNibName:@"IOAddRecordViewController" bundle:nil];
            addRecordViewController.currentImgRowId = -1;
            addRecordViewController.selectedGroupId = _currentGroupId;
            [self.navigationController pushViewController:addRecordViewController animated:YES];

        }
        [self closeMorePullDownMenu];
    }
}

-(void)didSwipeRightInCell:(UISwipeGestureRecognizer *)sender
{
    
    UITableViewCell *cell = (UITableViewCell *)sender.view;
    [self closeSwipedCell:cell];
}

#pragma mark swipe gesture methods
-(void)didSwipeLeftInCell:(UISwipeGestureRecognizer *)sender
{
    if (_lastIndexPath != nil) {
        [self closeSwipedCell:(UITableViewCell *)[_detailTableView cellForRowAtIndexPath:_lastIndexPath]];
    }
    UITableViewCell *cell = (UITableViewCell *)sender.view;
    UIView *topView = (UIView *)[cell viewWithTag:444];
    UIView *swipeView = (UIView *)[cell viewWithTag:555];
    _lastIndexPath = [_detailTableView indexPathForCell:cell];
    
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    // Swipe top view left
    [UIView animateWithDuration:1.0 animations:^{
        [topView setFrame:CGRectMake(-110, 0, 320, 60)]; } completion:^(BOOL finished) {
            // Bounce lower view
            [UIView animateWithDuration:0.15 animations:^{
                [swipeView setFrame:CGRectMake(10, 0, 320, 60)]; } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.15 animations:^{
                        [swipeView setFrame:CGRectMake(0, 0, 320, 60)]; }];
                }]; }];
    
}
-(void) closeSwipedCell:(UITableViewCell *)cell
{
    _lastIndexPath = nil;
    UIView *topView = (UIView *)[cell viewWithTag:444];
    
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView animateWithDuration:1.0 animations:^{
        [topView setFrame:CGRectMake(-10, 0, 320, 60)];
    } completion:^(BOOL finished)
     {
         [UIView animateWithDuration:0.15 animations:^{
             [topView setFrame:CGRectMake(0, 0, 320, 60)];
         }];
     }];
}
-(BOOL)isSwiped:(NSIndexPath *)indexPath
{
    UITableViewCell *cell  = (UITableViewCell *)[_detailTableView cellForRowAtIndexPath:indexPath];
    UIView *topView = (UIView *)[cell viewWithTag:444];
    DebugLog(@"ManageGroupsDetail : topView.frame.origin.x %f",topView.frame.origin.x);
    if (topView.frame.origin.x == -160) {
        return YES;
    }
    return NO;
}


// pandraggin Animation-------

#pragma mark - Gesture recognizer delegate
- (BOOL)gestureRecognizerShouldBegin:(UIPanGestureRecognizer *)panGestureRecognizer
{
    UITableViewCell *cell = (UITableViewCell *)[panGestureRecognizer view];
    CGPoint translation = [panGestureRecognizer translationInView:[cell superview]];
    return (fabs(translation.x) / fabs(translation.y) > 1) ? YES : NO;
}

#pragma mark - Gesture handlers

-(void)handlePan:(UIPanGestureRecognizer *)panGestureRecognizer
{
    float threshold = (PAN_OPEN_X+PAN_CLOSED_X)/2.0;
    float vX = 0.0;
    float compare = 0.0;
    float finalX = 0.0;
    DebugLog(@"ManageGroupsDetail : threshold %f compare %f vX %f finalX %f",threshold,compare,vX,finalX);
    
    UITableViewCell *cell = (UITableViewCell *)panGestureRecognizer.view;
    UIView *topView = (UIView *)[cell viewWithTag:444];
    NSIndexPath *indexPath = [self.detailTableView indexPathForCell:cell];
    DebugLog(@"ManageGroupsDetail : topView %f",topView.transform.tx);
    
    switch ([panGestureRecognizer state]) {
        case UIGestureRecognizerStateBegan:
            DebugLog(@"ManageGroupsDetail : UIGestureRecognizerStateBegan");
            
            if (self.openCellIndexPath != nil || self.openCellIndexPath.section != indexPath.section || self.openCellIndexPath.row != indexPath.row) {
                DebugLog(@"ManageGroupsDetail : closing last one %d",openCellIndexPath.row);
                
                UITableViewCell *cell = (UITableViewCell *)[self.detailTableView cellForRowAtIndexPath:openCellIndexPath];
                UIView *lasttopView = (UIView *)[cell viewWithTag:444];
                [self snapView:lasttopView toX:PAN_CLOSED_X animated:YES];
                [self setOpenCellIndexPath:nil];
                // [self setOpenCellLastTX:0];
            }
            break;
        case UIGestureRecognizerStateEnded:
            DebugLog(@"ManageGroupsDetail : UIGestureRecognizerStateEnded");
            
            vX = (FAST_ANIMATION_DURATION/2.0)*[panGestureRecognizer velocityInView:self.view].x;
            compare = topView.transform.tx + vX;
            
            DebugLog(@"ManageGroupsDetail : compare %f = %f + %f",compare,topView.transform.tx, vX);
            if (compare > threshold) {
                finalX = MAX(PAN_OPEN_X,PAN_CLOSED_X);
                //   [self setOpenCellLastTX:0];
            } else {
                finalX = MIN(PAN_OPEN_X,PAN_CLOSED_X);
                //    [self setOpenCellLastTX:topView.transform.tx];
            }
            DebugLog(@"ManageGroupsDetail : before openCellIndexPath %d ,_lastIndexPath %d,_deleteRecIndexPath %d",openCellIndexPath.row,_lastIndexPath.row,_deleteRecIndexPath.row);
            [self snapView:topView toX:finalX animated:YES];
            if (finalX == PAN_CLOSED_X) {
                [self setOpenCellIndexPath:nil];
            } else {
                [self setOpenCellIndexPath:indexPath];
            }
            break;
        case UIGestureRecognizerStateChanged:
            DebugLog(@"ManageGroupsDetail : UIGestureRecognizerStateChanged");
            
            //compare = self.openCellLastTX+[panGestureRecognizer translationInView:self.view].x;
            compare = [panGestureRecognizer translationInView:self.view].x;
            DebugLog(@"ManageGroupsDetail : ---------UIGestureRecognizerStateChanged compare %f = %f ------------",compare,topView.transform.tx);
            if (compare > MAX(PAN_OPEN_X,PAN_CLOSED_X))
                compare = MAX(PAN_OPEN_X,PAN_CLOSED_X);
            else if (compare < MIN(PAN_OPEN_X,PAN_CLOSED_X))
                compare = MIN(PAN_OPEN_X,PAN_CLOSED_X);
            DebugLog(@"ManageGroupsDetail : ---------UIGestureRecognizerStateChanged compare %f = %f ------------",compare,topView.transform.tx);
            [topView setTransform:CGAffineTransformMakeTranslation(compare, 0)];
            break;
        default:
            break;
    }
}

-(void)snapView:(UIView *)view toX:(float)x animated:(BOOL)animated
{
    [UIView animateWithDuration:0.15 animations:^{
        if (x == PAN_OPEN_X) {
            [view setTransform:CGAffineTransformMakeTranslation(x-10, 0)];
        }else{
            [view setTransform:CGAffineTransformMakeTranslation(x+10, 0)];
        }
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:FAST_ANIMATION_DURATION animations:^{
            [view setTransform:CGAffineTransformMakeTranslation(x, 0)];
        }];
    }];
    
    DebugLog(@"ManageGroupsDetail : snapView openCellIndexPath %d _lastIndexPath %d,_deleteRecIndexPath %d",openCellIndexPath.row,_lastIndexPath.row,_deleteRecIndexPath.row);
    
    // _lastIndexPath = openCellIndexPath;
}

#pragma mark -
#pragma UIScrollViewDelegate methods

- (void)scrollViewWillBeginDragging:(UIScrollView *)sender {
    if ([sender isEqual:[self detailTableView]]) {
        UITableViewCell *openCell = (UITableViewCell *) [self.detailTableView cellForRowAtIndexPath:openCellIndexPath];
        UIView *topView = (UIView *)[openCell viewWithTag:444];
        [self snapView:topView toX:PAN_CLOSED_X animated:YES];
    }
}

//// end of pandragging Animation-------

#pragma mark Button touch up inside action

- (IBAction) touchUpInsideAction:(id)sender event:(id)event
{
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:_detailTableView];
    NSIndexPath *indexPath = [_detailTableView indexPathForRowAtPoint: currentTouchPosition];
    if (indexPath != nil)
    {
        UITableViewCell *cell  = (UITableViewCell *)[_detailTableView cellForRowAtIndexPath:indexPath];
        UIView *topView = (UIView *)[cell viewWithTag:444];
        
        [UIView animateWithDuration:FASTEST_ANIMATION_DURATION animations:^{
            [topView setTransform:CGAffineTransformMakeTranslation(PAN_CLOSED_X+10, 0)];
        } completion:^(BOOL finished) {
            [topView setTransform:CGAffineTransformMakeTranslation(PAN_CLOSED_X, 0)];
            [self setOpenCellIndexPath:nil];
            
                UIButton *btn = (UIButton *)sender;
                DebugLog(@"ManageGroupsDetail : btn.tag %d %d",btn.tag,cell.tag);
                if (btn.tag == 5551)
                {
                    DebugLog(@"ManageGroupsDetail : trash");
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: ALERT_TITLE message: @"Are you sure you want to delete this  data? If you delete this data then you can't recover it again." delegate: self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
                    alert.tag = 1;
                    [alert show];
                    _deleteRecIndexPath = indexPath;
                }
                else
                {
                    DebugLog(@"ManageGroupsDetail : action");
                    UIActionSheet *actionSheet= [[UIActionSheet alloc]
                                                 initWithTitle: @"Choose action to copy/move record into groups"
                                                 delegate:self
                                                 cancelButtonTitle:@"CANCEL"
                                                 destructiveButtonTitle:nil
                                                 otherButtonTitles:@"Copy to",@"Move to", nil];
                    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
                    [actionSheet showInView:self.navigationController.view];
                    actionSheet.tag = cell.tag;
                }
//                [self closeSwipedCell:cell];
             }];
        }
}
#pragma actionSheet delegate Method
-(void)actionSheet:(UIActionSheet *)localactionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    DebugLog(@"ManageGroupsDetail : %d",buttonIndex);
    if (buttonIndex == [localactionSheet numberOfButtons]-1)
        return;
    
    else if ([[localactionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Copy to"])
    {
        DebugLog(@"ManageGroupsDetail : Copy to");
        IOGroupsModalViewController *groupsModalViewController =  [[IOGroupsModalViewController alloc] initWithNibName:@"IOGroupsModalViewController" bundle:nil];
        groupsModalViewController.mode = COPY_MODE;
        groupsModalViewController.currentGroupId = _currentGroupId;
        groupsModalViewController.currentImgId = localactionSheet.tag;
        UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:groupsModalViewController];
        [self.navigationController presentModalViewController:navBar animated:YES];
    }
    else if ([[localactionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Move to"])
    {
        DebugLog(@"ManageGroupsDetail : Move to");
        IOGroupsModalViewController *groupsModalViewController =  [[IOGroupsModalViewController alloc] initWithNibName:@"IOGroupsModalViewController" bundle:nil];
        groupsModalViewController.mode = MOVE_MODE;
        groupsModalViewController.currentGroupId = _currentGroupId;
        groupsModalViewController.currentImgId = localactionSheet.tag;
        UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:groupsModalViewController];
        [self.navigationController presentModalViewController:navBar animated:YES];
    }
}
#pragma alertView delegate Method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 1)
    {
        switch (buttonIndex) {
            case 0:
            {
                break;
            }
            case 1:
            {
                DebugLog(@"ManageGroupsDetail : Delete");
                UITableViewCell *cell  = (UITableViewCell *)[_detailTableView cellForRowAtIndexPath:_deleteRecIndexPath];
                RecordObject *recObj = [_recordObjectsArray objectAtIndex:_deleteRecIndexPath.row];
                if (recObj.imgNamePath != nil && ![recObj.imgNamePath isEqualToString:@""]) {
                    [[IO_APP_DELEGATE imageCache] removeObjectForKey:recObj.imgNamePath];
                }
                recObj = nil;
                [_recordObjectsArray removeObjectAtIndex:_deleteRecIndexPath.row];
                [self DeleteRecord:cell.tag];
                [_detailTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:_deleteRecIndexPath] withRowAnimation:UITableViewRowAnimationMiddle];
            }
                break;
            default:
                break;
        }
        
    }
    else if (alertView.tag == 2)
    {
        switch (buttonIndex) {
            case 0:
            {
                NSString *groupName = [alertView textFieldAtIndex:0].text;
                DebugLog(@"ManageGroupsDetail : alertView %@",groupName);

                if(groupName == nil || [groupName isEqualToString:@""]){
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message: @"Enter group name" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                }else{
                    [self renameGroup:[alertView textFieldAtIndex:0].text];
                }
            }
                break;
            default:
                break;
        }
        
    }
}

#pragma sqlite methods
-(NSInteger)getGroupRecordsRowsCount
{
    NSInteger rowCount = 0;
    
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
        NSString *queryString = [NSString stringWithFormat:@"SELECT count (*) FROM IMAGE_GROUP_RELATION WHERE GROUP_ID = %d",_currentGroupId];
        const char *queryCharP = [queryString UTF8String];
        
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            if (sqlite3_step(stmt) == SQLITE_ROW) {
                rowCount = sqlite3_column_int(stmt, 0);
            }
        }
        sqlite3_finalize(stmt);
        sqlite3_close(ioDB);
    }
    DebugLog(@"GROUPS rowCount %d",rowCount);
    return rowCount;
} 
-(void)readFromGroupRecordsFromTable
{
    DebugLog(@"ManageGroupsDetail : --readFromGroupRecordsFromTable-- with _currentGroupId %d",_currentGroupId);
    
    if (_recordObjectsArray == nil) {
        _recordObjectsArray = [[NSMutableArray alloc] init];
    }
    else
    {
        [_recordObjectsArray removeAllObjects];
    }
    RecordObject *recObj = nil;
    int rowlId = 0;
    double dateDouble = 0;

    NSString *title = @"";
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK)
    {
        NSString *queryString = [NSString stringWithFormat:@"SELECT ID,TITLE,DATE FROM IMAGE_MASTER WHERE ID IN (SELECT IMAGE_MASTER_ID FROM IMAGE_GROUP_RELATION WHERE GROUP_ID = %d)",_currentGroupId];
        const char *queryCharP = [queryString UTF8String];
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            while (sqlite3_step(stmt) == SQLITE_ROW) {
                rowlId = sqlite3_column_int(stmt, 0);
                title = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 1)];
                dateDouble  = sqlite3_column_double(stmt, 2);

                recObj = [[RecordObject alloc] init];
                recObj.rowId = rowlId;
                recObj.title = title;
                recObj.dateDouble = dateDouble;
                [_recordObjectsArray addObject:recObj];
                recObj = nil;
            }
        }
        sqlite3_finalize(stmt);
        
        for (int index = 0; index < _recordObjectsArray.count; index++)
        {
            recObj = (RecordObject *)[_recordObjectsArray objectAtIndex:index];
            NSString *imagequeryString = [NSString stringWithFormat:@"SELECT IMAGE_PATH FROM IMAGE_SET_RELATION WHERE IMAGE_MASTER_ID = %d AND IMAGE_PATH <> \"\" LIMIT 1",recObj.rowId];
            DebugLog(@"ManageGroupsDetail : ------%@-----",imagequeryString);
            const char *imgqueryCharP = [imagequeryString UTF8String];
            if (sqlite3_prepare_v2(ioDB, imgqueryCharP, -1, &stmt, NULL) == SQLITE_OK) {
                if(sqlite3_step(stmt) == SQLITE_ROW)
                {
                    NSString *path = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 0)];
                    DebugLog(@"ManageGroupsDetail : image path %@ ",path);
                    
                    recObj.imgNamePath = path;
                    [_recordObjectsArray replaceObjectAtIndex:index withObject:recObj];
                    path = nil;
                }
            }
            sqlite3_finalize(stmt);
            
            if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
                NSString *queryString = [NSString stringWithFormat:@"SELECT GROUP_NAME FROM GROUPS WHERE ID in (select GROUP_ID from IMAGE_GROUP_RELATION where IMAGE_MASTER_ID = %d)",recObj.rowId];
                const char *queryCharP = [queryString UTF8String];
                if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
                    NSMutableString *groupsString = [[NSMutableString alloc] initWithString:@""];
                    while (sqlite3_step(stmt) == SQLITE_ROW)
                    {
                        
                        NSString *name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 0)];
                        if (name != nil && ![name isEqualToString:@""] && ![name isEqualToString:[FAVOURITE_GROUPNAME lowercaseString]] && ![name isEqualToString:[UNGROUPED_GROUPNAME lowercaseString]]) {
                            if ([groupsString isEqualToString:@""]) {
                                [groupsString appendFormat:@"%@",name];
                            }else{
                                [groupsString appendFormat:@",%@",name];
                            }
                        }
                    }
                    recObj.groupsName = groupsString;
                    [_recordObjectsArray replaceObjectAtIndex:index withObject:recObj];
                    groupsString = nil;
                }
                sqlite3_finalize(stmt);
            }
            recObj = nil;
        }
        
        sqlite3_close(ioDB);
    }
    DebugLog(@"ManageGroupsDetail : --_recordObjectsArray-- %@",_recordObjectsArray);

}
-(void)reloadOnlyGroupNames
{
    DebugLog(@"ManageGroupsDetail : --reloadOnlyGroupNames-- with _currentGroupId %d",_currentGroupId);
    RecordObject *recObj = nil;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK)
    {
        sqlite3_stmt *stmt = nil;
        for (int index = 0; index < _recordObjectsArray.count; index++)
        {
            recObj = (RecordObject *)[_recordObjectsArray objectAtIndex:index];
            
            if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
                NSString *queryString = [NSString stringWithFormat:@"SELECT GROUP_NAME FROM GROUPS WHERE ID in (select GROUP_ID from IMAGE_GROUP_RELATION where IMAGE_MASTER_ID = %d)",recObj.rowId];
                const char *queryCharP = [queryString UTF8String];
                if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
                    NSMutableString *groupsString = [[NSMutableString alloc] initWithString:@""];
                    while (sqlite3_step(stmt) == SQLITE_ROW)
                    {
                        
                        NSString *name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 0)];
                        if (name != nil && ![name isEqualToString:@""] && ![name isEqualToString:[FAVOURITE_GROUPNAME lowercaseString]] && ![name isEqualToString:[UNGROUPED_GROUPNAME lowercaseString]]) {
                            if ([groupsString isEqualToString:@""]) {
                                [groupsString appendFormat:@"%@",name];
                            }else{
                                [groupsString appendFormat:@",%@",name];
                            }
                        }
                    }
                    recObj.groupsName = groupsString;
                    [_recordObjectsArray replaceObjectAtIndex:index withObject:recObj];
                    groupsString = nil;
                }
                sqlite3_finalize(stmt);
            }
            recObj = nil;
        }
        sqlite3_close(ioDB);
    }
    DebugLog(@"ManageGroupsDetail : --_recordObjectsArray-- %@",_recordObjectsArray);
}

- (void) DeleteRecord :(int) imgId
{
    BOOL deleteSuccess = NO;
    sqlite3_stmt *statement;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM IMAGE_MASTER WHERE ID=\"%d\"", imgId];
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(ioDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            deleteSuccess = YES;
            DebugLog(@"ManageGroupsDetail : deleted");
        } else {
            DebugLog(@"ManageGroupsDetail : Failed to delete");
        }
        sqlite3_finalize(statement);
        
        if (deleteSuccess)
        {
            
            NSMutableArray *imageArray = [[NSMutableArray alloc] init];
            NSString *path = nil;
            NSString *imagequeryString = [NSString stringWithFormat:@"SELECT IMAGE_PATH FROM IMAGE_SET_RELATION WHERE IMAGE_MASTER_ID = %d AND IMAGE_PATH <> \"\"",imgId];
            DebugLog(@"ManageGroupsDetail : ------%@-----",imagequeryString);
            const char *imgqueryCharP = [imagequeryString UTF8String];
            if (sqlite3_prepare_v2(ioDB, imgqueryCharP, -1, &statement, NULL) == SQLITE_OK) {
                while(sqlite3_step(statement) == SQLITE_ROW)
                {
                    path = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                    DebugLog(@"ManageGroupsDetail : image path %@ ",path);
                    [imageArray addObject:path];
                    path =nil;
                }
            }
            sqlite3_finalize(statement);
            
            NSString *imgSetdeleteSQL = [NSString stringWithFormat:@"DELETE FROM IMAGE_SET_RELATION WHERE IMAGE_MASTER_ID=\"%d\"", imgId];
            const char *imgset_delete_stmt = [imgSetdeleteSQL UTF8String];
            sqlite3_prepare_v2(ioDB, imgset_delete_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE) {
                DebugLog(@"ManageGroupsDetail : deleted from IMAGE_SET_RELATION");
                for (NSString *pathStr in imageArray) {
                    DebugLog(@"ManageGroupsDetail : remove image %@",pathStr);
                    [IO_APP_DELEGATE removeImage:pathStr];
                }
            }else{
                DebugLog(@"ManageGroupsDetail : Failed to delete from IMAGE_SET_RELATION");
            }
            sqlite3_finalize(statement);
            
            [imageArray removeAllObjects];
            imageArray = nil;
            
            NSString *imgtag_deleteSQL = [NSString stringWithFormat:@"DELETE FROM IMAGE_TAG_RELATION WHERE IMAGE_MASTER_ID=\"%d\"", imgId];
            const char *imgtag_delete_stmt = [imgtag_deleteSQL UTF8String];
            sqlite3_prepare_v2(ioDB, imgtag_delete_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                DebugLog(@"ManageGroupsDetail : deleted from IMAGE_TAG_RELATION");
            } else {
                DebugLog(@"ManageGroupsDetail : Failed to delete from IMAGE_TAG_RELATION");
            }
            sqlite3_finalize(statement);
            
            NSString *imggrp_deleteSQL = [NSString stringWithFormat:@"DELETE FROM IMAGE_GROUP_RELATION WHERE IMAGE_MASTER_ID=\"%d\"", imgId];
            const char *imggrp_delete_stmt = [imggrp_deleteSQL UTF8String];
            sqlite3_prepare_v2(ioDB, imggrp_delete_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                DebugLog(@"ManageGroupsDetail : deleted from IMAGE_GROUP_RELATION");
            } else {
                DebugLog(@"ManageGroupsDetail : Failed to delete from IMAGE_GROUP_RELATION");
            }
            sqlite3_finalize(statement);
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message: @"Record deleted successfully." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message: @"Failed to delete record." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }
    sqlite3_close(ioDB);
}
-(void)renameGroup:(NSString *)renameString
{
    DebugLog(@"ManageGroupsDetail : --renameGroup--");
    int success = 0;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
        NSString *updateSQL = [NSString stringWithFormat:@"UPDATE GROUPS SET GROUP_NAME = \"%@\" WHERE ID=\"%d\"",renameString,_currentGroupId];
        const char *update_stmnt = [updateSQL UTF8String];
        sqlite3_stmt *statement = nil;
        sqlite3_prepare_v2(ioDB, update_stmnt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"ManageGroupsDetail : updated in GROUPS with %@",renameString);
            success = 1;
        } else {
            DebugLog(@"ManageGroupsDetail : Failed to update in GROUPS");
            success = 0;
        }
        sqlite3_finalize(statement);
        sqlite3_close(ioDB);
    }
    NSString *msg;
    if (success) {
        msg = @"Grouped renamed successfully";
        self.title = renameString;
    }else{
        msg = @"Failed to rename Group";
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Rename Group" message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
    [self reloadListOfRecordsData];
}

-(int)getGroupId:(NSString *)groupName
{
    int groupId = -1;
	if(sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK)
    {
        // Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"Select ID from GROUPS WHERE GROUP_NAME=\"%@\"",[groupName lowercaseString]];
        const char *sqlStatement = [querySQL UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(ioDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                groupId = sqlite3_column_int(compiledStatement, 0);
                DebugLog(@"groupId -> %d",groupId);
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(ioDB);
    return groupId;
}
@end
