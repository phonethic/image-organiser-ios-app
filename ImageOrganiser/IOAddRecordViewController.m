//
//  IOAddRecordViewController.m
//  ImageOrganiser
//
//  Created by Kirti Nikam on 02/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import <ImageIO/ImageIO.h>

#import "IOAddRecordViewController.h"
#import "constants.h"
#import "IOAppDelegate.h"
#import "IOAddGroupTagViewController.h"
#import "UIImage+fixOrientation.h"
#import "DetailMapViewController.h"
#import "IOGalleryViewController.h"
#import "ImageFilteringViewController.h"
#import "CommonCallback.h"

#import "GroupObject.h"
#import "TagObject.h"
#import "DisplayMap.h"

#define TITLE_TAG 1
#define ADD_IMAGE_LABEL_TAG 2
#define IMAGE_TAG 3
#define CATEGORY_TAG 4
#define TAGS_TAG 5
#define GROUPS_TAG 6
#define ADDRESS_TAG 7
#define GEOPOINT_TAG 8
#define DATE_TAG 9
#define TIME_TAG 10
#define ADD_IMAGE_BTN_TAG 11

#define CELL_ROW_HEIGHT 60


#define LOCATION_PLACEHOLDER1 @"Fetching your location ...."
#define LOCATION_PLACEHOLDER2 @"Enter Address"
#define SHARE_ACTIONSHEET_TITLE @"Share on"
#define ADDIMAGE_ACTIONSHEET_TITLE @"Add a photo"

@interface IOAddRecordViewController ()
@property (strong,nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (strong,nonatomic) IBOutlet UISegmentedControl *pagedSegmentedControl;
- (void)useDropPinNotificationWithString:(NSNotification *)notification;
@end
NSUInteger kNumberOfPages;

@implementation IOAddRecordViewController
@synthesize pagedSegmentedControl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma view Lifecycle
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    DebugLog(@"AddRecord:viewDidDisappear");
    if (theConnection != nil) {
        [theConnection cancel];
        theConnection = nil;
    }
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    DebugLog(@"AddRecord:view will Appear");
    if (_segmentedControl == nil) {
        [self colorSetup];

        [self addControlsInScrollView];
        [self reloadScrollViewData];
//        [notesTextView setFrame:CGRectMake(_openNotesBtn.frame.size.width+5, 10, notesTextView.frame.size.width,self.view.frame.size.height-10)];
        [notesTextView setFrame:CGRectMake(15,60, notesTextView.frame.size.width,self.view.frame.size.height-50)];
//        _notesView.frame = CGRectMake(self.view.frame.size.width,_notesView.frame.origin.y, _notesView.frame.size.width,_notesView.frame.size.height);
        _notesView.frame = CGRectMake(0,_notesView.frame.origin.y+self.view.frame.size.height, _notesView.frame.size.width,_notesView.frame.size.height);
        [_groupTableView reloadData];
        
        
        DebugLog(@"%f %f %f",_groupTableView.frame.origin.y,_groupTableView.contentSize.height,self.view.frame.size.height);

        if ((_groupTableView.frame.origin.y+_groupTableView.contentSize.height) < self.view.frame.size.height-50){
            [_groupTableView setFrame:CGRectMake(_groupTableView.frame.origin.x,
                                                 _groupTableView.frame.origin.y,
                                                 _groupTableView.frame.size.width,
                                                 _groupTableView.contentSize.height)];
        }else{
            [_groupTableView setFrame:CGRectMake(_groupTableView.frame.origin.x,
                                                 _groupTableView.frame.origin.y,
                                                 _groupTableView.frame.size.width,
                                                 _groupView.frame.size.height-100)];
        }
    }
    else
    {
        if (_segmentedControl.selectedSegmentIndex == 0) {
            pagedSegmentedControl.hidden = FALSE;
        }
        else
        {
            pagedSegmentedControl.hidden = TRUE;
        }
    }
    [self reloadImageCell];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (_segmentedControl == nil) {
        _segmentedControl = [[UISegmentedControl alloc] init];// initWithItems:[NSArray arrayWithObjects:@"List",@"Page",nil]];
        
        [_segmentedControl insertSegmentWithImage:[UIImage imageNamed:@"slide_page.png"] atIndex:0 animated:YES];
        [_segmentedControl insertSegmentWithImage:[UIImage imageNamed:@"slide_list.png"] atIndex:1 animated:YES];
        
        _segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
        [_segmentedControl setSelectedSegmentIndex:1];
        [_segmentedControl addTarget:self action:@selector(indexDidChangeForSegmentedControl:) forControlEvents:UIControlEventValueChanged];
        _segmentedControl.frame = CGRectMake(0, 0, 90, 30);
        _segmentedControl.tintColor = BGCOLOR;
        [self.navigationController.navigationBar.topItem setTitleView:_segmentedControl];
        [self indexDidChangeForSegmentedControl:_segmentedControl];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 50, 32)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    
    [self addGestures];
    [self addNSNotificationCenter];

    nextGroupIndex = 0;
    savedInDB = NO;
    imageCellHeight = CELL_ROW_HEIGHT;

    _categoryArray  = [[NSMutableArray alloc] init];
    _imagesArray = [[NSMutableArray alloc] init];
    _groupArray  = [[NSMutableArray alloc] init];
    _tagsArray= [[NSMutableArray alloc] init];
    
    [self loadCategoryArray];
    [self addPickerWithDoneButton];
    [self loadGroupsArray];
//    [self iconsDictionaryFromIconsTable];
    DebugLog(@"AddRecord:icons dictionay : %@",_iconsDic);
    DebugLog(@"AddRecord:grouparray %@",_groupArray);
    [self setFrameOfNotesControls];

    if (_currentImgRowId == -1) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveClicked:)];
        
//        UIButton *saveButton =  [UIButton buttonWithType:UIButtonTypeCustom];
//        [saveButton setTitle:@"Save" forState:UIControlStateNormal];
//        saveButton.titleLabel.font = DEFAULT_FONT(15.0);
//        [saveButton setBackgroundImage:[UIImage imageNamed:@"bar_button.png"] forState:UIControlStateNormal];
//        [saveButton addTarget:self action:@selector(saveClicked:) forControlEvents:UIControlEventTouchUpInside];
//        [saveButton setFrame:CGRectMake(0, 0, 70, 30)];
//        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:saveButton];
        
        numberOfGroups  = 1;
        numberOfTags    = 1;
        _reverseAddress = LOCATION_PLACEHOLDER1;
        if ([CLLocationManager locationServicesEnabled])
        {
            locationManager = [[CLLocationManager alloc] init];
            locationManager.delegate = self;
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
            locationManager.distanceFilter = kCLDistanceFilterNone;
            [locationManager startUpdatingLocation];
            [self performSelector:@selector(stopUpdatingCoreLocation:) withObject:@"Timed Out" afterDelay:10];
        }
        else
        {
            UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle:@"Location Services Disabled" message:@"You currently have all location services for this device disabled. If you proceed, you will be asked to confirm whether location services should be reenabled." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [servicesDisabledAlert show];
        }
    }
    else
    {
        DebugLog(@"AddRecord:currentImageId %d",_currentImgRowId);
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Update" style:UIBarButtonItemStyleBordered target:self action:@selector(updateClicked:)];
        
//        UIButton *updateButton =  [UIButton buttonWithType:UIButtonTypeCustom];
//        [updateButton setTitle:@"Update" forState:UIControlStateNormal];
//        updateButton.titleLabel.font = DEFAULT_FONT(15.0);
//        [updateButton setBackgroundImage:[UIImage imageNamed:@"bar_button.png"] forState:UIControlStateNormal];
//        [updateButton addTarget:self action:@selector(updateClicked:) forControlEvents:UIControlEventTouchUpInside];
//        [updateButton setFrame:CGRectMake(0, 0, 70, 30)];
//
//        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:updateButton];
        
        
        [self readRowFromDatabase];
       // [self readSelectedTagsFromImageTagRelationTable];
        [self readImagesFromTable];
        [self reloadGroupsArrayWithSelectedGroups];
        [self readTagsFromTable];
        DebugLog(@"AddRecord:tagarray %@",_tagObjectsArray);
        [_addRecordTableView reloadData];
    }
    [self loadGroupsTempTable];

    [_addRecordTableView setHidden:NO];
    [_addRecordScrollView setHidden:YES];
    [_pageControl setHidden:YES];
    [pagedSegmentedControl setHidden:YES];
    [_notesView setHidden:YES];
    [_openNotesBtn setHidden:YES];
    
    [self showGalleryBtn];

    _keyBoardHideView.frame = CGRectMake(_keyBoardHideView.frame.origin.x,self.view.frame.size.height+_keyBoardHideView.frame.origin.y, _keyBoardHideView.frame.size.width,_keyBoardHideView.frame.size.height);
    _keyBoardHideView.backgroundColor = TABLE_CELL_BGCOLOR;
    
    _hideKeyboardBtn.titleLabel.font = DEFAULT_FONT(16.0);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark Orientation methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate
{
    return NO;
}
- (NSUInteger)supportedInterfaceOrientations{
#if __IPHONE_OS_VERSION_MAX_ALLOWED <= __IPHONE_5_1
    return UIInterfaceOrientationPortrait;
#else
    return UIInterfaceOrientationMaskPortrait;
#endif
}

- (void)viewDidUnload {
    [self setAddRecordScrollView:nil];
    [self setPageControl:nil];
    [self setTitleimgView:nil];
    [self setCategoryView:nil];
    [self setTagView:nil];
    [self setGroupView:nil];
    [self setLocationDateTimeView:nil];
    [self setTitleTextField:nil];
    [self setImageView:nil];
    [self setCategoryTextField:nil];
    [self setGroupTextField:nil];
    [self setAddressTextView:nil];
    [self setGeopointsTextField:nil];
    [self setDateTextField:nil];
    [self setTimeTextField:nil];
    [self setCategoryTableView:nil];
    [self setGroupTableView:nil];
    [self setGroupImageView:nil];
    [self setGroupImageView:nil];
    [self setCreateGroup:nil];
    [self setGroupImagesScrollView:nil];
    [self setChangeGroupIconBtn:nil];
    [self setOpenNotesBtn:nil];
    //    [self setNotesTextView:nil];
    [self setAddImageBtn:nil];
    [self setOpenGalleryBtn:nil];
    [self setNotesView:nil];
    [self setKeyBoardHideView:nil];
    [self setHideKeyboardBtn:nil];
    [self setPinImageView:nil];
    [self setPinArrowRightImageView:nil];
    [self setPageMapView:nil];
    [self setTagInfoLabel:nil];
    [self setAddTagsLabel:nil];
    [super viewDidUnload];
}


#pragma UIBarButtonItem callback methods
- (void)backBtnPressed:(id)sender
{
    DebugLog(@"AddRecord:backbtnPressed: ");
    if (!savedInDB) {
        for (ImageObject *imgObj in _imagesArray)
        {
            DebugLog(@"AddRecord:backbtnPressed:IMAGE_SET_RELATION: imagePath %@ isNew %d isDeleted %d isUpdated %d",imgObj.imagePath,imgObj.isNew,imgObj.isDeleted,imgObj.isUpdated);
            if (imgObj.isNew)
            {
                [IO_APP_DELEGATE removeImage:imgObj.imagePath];
                if (imgObj.isUpdated)
                {
                    [IO_APP_DELEGATE removeImage:[NSString stringWithFormat: @"old_%@",imgObj.imagePath]];
                }
            }
            if (!imgObj.isNew && imgObj.isUpdated)
            {
                NSFileManager *fileManager = [NSFileManager defaultManager];
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString *oldfilename = [NSString stringWithFormat: @"old_%@",imgObj.imagePath];
                NSString* oldfullPath = [documentsDirectory stringByAppendingPathComponent:oldfilename];
                DebugLog(@"AddRecord:backbtnPressed:oldfullPath = %@" , oldfullPath);

                UIImage *oldImage = [UIImage imageWithContentsOfFile:oldfullPath];
                if(oldImage != nil)
                {
                    NSString *newfilename = imgObj.imagePath;
                    DebugLog(@"AddRecord:newfilename name  = %@" , newfilename);
                    NSString* newfullPath = [documentsDirectory stringByAppendingPathComponent:newfilename];
                    DebugLog(@"AddRecord:backbtnPressed:imgObj.imagePath = %@" , newfullPath);
                    
                    NSData* oldImagedata = UIImagePNGRepresentation(oldImage);
                    BOOL oldsavesuccess = [fileManager createFileAtPath:newfullPath contents:oldImagedata attributes:nil];
                    if(oldsavesuccess == YES)
                    {
                        DebugLog(@"AddRecord:backbtnPressed:success on old file delete");
                        [IO_APP_DELEGATE removeImage:oldfilename];
                    }
                    else
                    {
                        DebugLog(@"AddRecord:backbtnPressed:failed to save old file back");
                    }
                }
            }
        }
    }
    [self DeleteAllRowsFromGroupsTempTable];
    [self DeleteAllRowsFromTagsTempTable];
    [locationManager stopUpdatingLocation];
    [theConnection cancel];
    theConnection = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ADDRECORDNOTIFICATIONNAME object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)saveClicked:(id)sender
{
    DebugLog(@"AddRecord:saveClicked");
    DebugLog(@"AddRecord:_selectedTitle %@ category id %d -- category %@",_selectedTitle,[self getCategoryId:_selectedCategory],_selectedCategory);
    DebugLog(@"AddRecord:Notes %@",notesTextView.text);
    
    DebugLog(@"AddRecord:date %f",[[_datetimePickerView date] timeIntervalSinceReferenceDate]);
    
    if (_selectedTitle == nil || [_selectedTitle isEqualToString:@""]) {
        _selectedTitle = @"Untitled";
    }
    if ([_reverseAddress isEqualToString:LOCATION_PLACEHOLDER1] || [_reverseAddress isEqualToString:LOCATION_PLACEHOLDER2]) {
        _reverseAddress = @"";
    }
    int catgId = [self getCategoryId:_selectedCategory];
    sqlite3_stmt *statement;
    int imgID = -1;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String],&ioDB) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat:
                               @"INSERT INTO IMAGE_MASTER(TITLE,NOTES,CATEGORY_ID,LOCATION,LATITUDE,LONGITUDE,DATE) VALUES (\"%@\",\"%@\", %d, \"%@\",%f,%f,%f)",
                               _selectedTitle,notesTextView.text,catgId,_reverseAddress,_currentLatitude,_currentLongitude,[[_datetimePickerView date] timeIntervalSinceReferenceDate]];
        DebugLog(@"AddRecord:---%@---",insertSQL);
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(ioDB, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            imgID = sqlite3_last_insert_rowid(ioDB);
            DebugLog(@"AddRecord:IMAGE_MASTER : successfully saved : %d",imgID);
        } else {
            DebugLog(@"AddRecord:IMAGE_MASTER : failed to save");
        }
        sqlite3_finalize(statement);
        
        if (imgID != -1)
        {
            
            for (ImageObject *imgObj in _imagesArray)
            {
                
                DebugLog(@"AddRecord:save : _imageObjectToFilter.imagePath = %@ isNew %d isDeleted %d isUpdated %d" , imgObj.imagePath,imgObj.isNew,imgObj.isDeleted,imgObj.isUpdated);
                
                if (imgObj.isNew && !imgObj.isDeleted)
                {
                    DebugLog(@"AddRecord:IMAGE_SET_RELATION: imagePath %@",imgObj.imagePath);
                    NSString *insertSQL2 = [NSString stringWithFormat:
                                            @"INSERT INTO IMAGE_SET_RELATION(IMAGE_MASTER_ID,IMAGE_PATH) VALUES (%d,\"%@\")",
                                            imgID,imgObj.imagePath];
                    DebugLog(@"AddRecord:---%@---",insertSQL2);
                    
                    const char *insert_group_stmt = [insertSQL2 UTF8String];
                    sqlite3_prepare_v2(ioDB, insert_group_stmt, -1, &statement, NULL);
                    if (sqlite3_step(statement) == SQLITE_DONE)
                    {
                        DebugLog(@"AddRecord:IMAGE_SET_RELATION: successfully saved : %lld",sqlite3_last_insert_rowid(ioDB));
                    } else {
                        DebugLog(@"AddRecord:IMAGE_SET_RELATION: failed to save");
                    }
                    sqlite3_finalize(statement);
                }
                else if (imgObj.isNew && imgObj.isDeleted)
                {
                    [IO_APP_DELEGATE removeImage:imgObj.imagePath];
                }
                if (imgObj.isUpdated)
                {
                    DebugLog(@"AddRecord:imgObj.isUpdated removing image");
                    [IO_APP_DELEGATE removeImage:[NSString stringWithFormat: @"old_%@",imgObj.imagePath]];
                }
            }
            
            // 1) first get ROWID from TAGS_TABLE
            // 2) if yes then --> insert into IMAGE_TAG_RELATION else
            // 3) if no --> insert into TAGS_TABLE, and repeat step
            DebugLog(@"AddRecord:Tags array %@",_tagsArray);
            if (_tagsArray == nil) {
                _tagsArray = [[NSMutableArray alloc] init];
            }
            else
            {
                [_tagsArray removeAllObjects];
            }
            if (_segmentedControl.selectedSegmentIndex == 0) {
                [_tagsArray addObjectsFromArray:tokenFieldViewInPage.tokenTitles];
            }
            else
            {
                [_tagsArray addObjectsFromArray:tokenFieldView.tokenTitles];
            }
            for (NSString *tagName in _tagsArray)
            {
                int TAG_ROWID = [self getTagId:tagName];
                if (TAG_ROWID == -1)
                {
                    TAG_ROWID = [self insertIntoTags:tagName];
                }
                if (TAG_ROWID != -1)
                {
                    DebugLog(@"AddRecord:IMAGE_TAG_RELATION : RowId ---%d---",TAG_ROWID);
                    NSString *insertSQL1 = [NSString stringWithFormat:
                                            @"INSERT INTO IMAGE_TAG_RELATION(IMAGE_MASTER_ID,TAG_ID) VALUES (%d,%d)",
                                            imgID,TAG_ROWID];
                    DebugLog(@"AddRecord:---%@---",insertSQL1);
                    
                    const char *insert_tag_stmt = [insertSQL1 UTF8String];
                    sqlite3_prepare_v2(ioDB, insert_tag_stmt, -1, &statement, NULL);
                    if (sqlite3_step(statement) == SQLITE_DONE)
                    {
                        DebugLog(@"AddRecord:IMAGE_TAG_RELATION: successfully saved : %lld",sqlite3_last_insert_rowid(ioDB));
                    } else {
                        DebugLog(@"AddRecord:IMAGE_TAG_RELATION: failed to save");
                    }
                    sqlite3_finalize(statement);
                }
            }
            int unselectedgroupCount = 0;
            for (GroupObject *grpObj in _groupArray)
            {
                if (grpObj.selected == 1) {
                    unselectedgroupCount ++;
                    DebugLog(@"AddRecord:IMAGE_GROUP_RELATION: group %@ : selected",grpObj.groupName);
                    NSString *insertSQL2 = [NSString stringWithFormat:
                                            @"INSERT INTO IMAGE_GROUP_RELATION(IMAGE_MASTER_ID,GROUP_ID) VALUES (%d,%d)",
                                            imgID,grpObj.groupRowId];
                    DebugLog(@"AddRecord:---%@---",insertSQL2);
                    
                    const char *insert_group_stmt = [insertSQL2 UTF8String];
                    sqlite3_prepare_v2(ioDB, insert_group_stmt, -1, &statement, NULL);
                    if (sqlite3_step(statement) == SQLITE_DONE)
                    {
                        DebugLog(@"AddRecord:IMAGE_GROUP_RELATION: successfully saved : %lld",sqlite3_last_insert_rowid(ioDB));
                    } else {
                        DebugLog(@"AddRecord:IMAGE_GROUP_RELATION: failed to save");
                    }
                    sqlite3_finalize(statement);
                }
                
            }
            if (unselectedgroupCount == 0) {
                    int groupID = -1;
                    NSString *querySQL = [NSString stringWithFormat:@"Select id from GROUPS WHERE GROUP_NAME=\"%@\"",[UNGROUPED_GROUPNAME lowercaseString]];
                    const char *sqlStatement = [querySQL UTF8String];
                    if(sqlite3_prepare_v2(ioDB, sqlStatement, -1, &statement, NULL) == SQLITE_OK)
                    {
                        if(sqlite3_step(statement) == SQLITE_ROW)
                        {
                            groupID = sqlite3_column_int(statement, 0);
                            DebugLog(@"AddRecord:groupId -> %d",groupID);
                        }
                    }
                    sqlite3_finalize(statement);
                
                if (groupID != -1) {
                    NSString *insertSQL2 = [NSString stringWithFormat:
                                            @"INSERT INTO IMAGE_GROUP_RELATION(IMAGE_MASTER_ID,GROUP_ID) VALUES (%d,%d)",
                                            imgID,groupID];
                    DebugLog(@"AddRecord:---%@---",insertSQL2);
                    
                    const char *insert_group_stmt = [insertSQL2 UTF8String];
                    sqlite3_prepare_v2(ioDB, insert_group_stmt, -1, &statement, NULL);
                    if (sqlite3_step(statement) == SQLITE_DONE)
                    {
                        DebugLog(@"AddRecord:IMAGE_GROUP_RELATION: successfully saved in ungroup : %lld",sqlite3_last_insert_rowid(ioDB));
                    } else {
                        DebugLog(@"AddRecord:IMAGE_GROUP_RELATION: failed to save in ungroup ");
                    }
                    sqlite3_finalize(statement);
                }
            }
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message: @"Record saved successfully." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message: @"Failed to save record." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        sqlite3_close(ioDB);
    }
    savedInDB = YES;
    [self backBtnPressed:nil];
}
-(void)updateClicked:(id)sender
{
    DebugLog(@"AddRecord:updateClicked");
    DebugLog(@"AddRecord:category id %d -- category %@",[self getCategoryId:_selectedCategory],_selectedCategory);
    DebugLog(@"AddRecord: -- date %f",[[_datetimePickerView date] timeIntervalSinceReferenceDate]);
    if (_selectedTitle == nil || [_selectedTitle isEqualToString:@""]) {
        _selectedTitle = @"Untitled";
    }
    if ([_reverseAddress isEqualToString:LOCATION_PLACEHOLDER1]|| [_reverseAddress isEqualToString:LOCATION_PLACEHOLDER2]) {
        _reverseAddress = @"";
    }
    int catgId = [self getCategoryId:_selectedCategory];
    DebugLog(@"AddRecord:Notes %@",notesTextView.text);
    
    sqlite3_stmt *statement;
    int imgID = -1;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String],&ioDB) == SQLITE_OK)
    {
        NSString *updateSQL = [NSString stringWithFormat:
                               @"UPDATE IMAGE_MASTER SET TITLE = \"%@\", NOTES = \"%@\", CATEGORY_ID = %d, LOCATION = \"%@\",LATITUDE = %f, LONGITUDE = %f,DATE = %f WHERE ID = %d" ,
                               _selectedTitle,notesTextView.text,catgId,_reverseAddress,_currentLatitude,_currentLongitude,[[_datetimePickerView date] timeIntervalSinceReferenceDate],_currentImgRowId];
        DebugLog(@"AddRecord:---%@---",updateSQL);
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(ioDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            imgID = _currentImgRowId;
            DebugLog(@"AddRecord:IMAGE_MASTER : successfully updated : %d",imgID);
        } else {
            DebugLog(@"AddRecord:IMAGE_MASTER : failed to update");
        }
        sqlite3_finalize(statement);
        
        DebugLog(@"AddRecord:IMAGE_MASTER : imgID : %d",imgID);
        if (imgID != -1)
        {
            
            for (ImageObject *imgObj in _imagesArray)
            {
                DebugLog(@"AddRecord:updated : _imageObjectToFilter.imagePath = %@ isNew %d isDeleted %d isUpdated %d" , imgObj.imagePath,imgObj.isNew,imgObj.isDeleted,imgObj.isUpdated);
                
                if (imgObj.isNew && imgObj.isDeleted)
                {
                    [IO_APP_DELEGATE removeImage:imgObj.imagePath];
                }
                else if (imgObj.isNew && !imgObj.isDeleted)
                {
                    NSString *insertSQL2 = [NSString stringWithFormat:
                                            @"INSERT INTO IMAGE_SET_RELATION(IMAGE_MASTER_ID,IMAGE_PATH) VALUES (%d,\"%@\")",
                                            imgID,imgObj.imagePath];
                    DebugLog(@"AddRecord:---%@---",insertSQL2);
                    
                    const char *insert_group_stmt = [insertSQL2 UTF8String];
                    sqlite3_prepare_v2(ioDB, insert_group_stmt, -1, &statement, NULL);
                    if (sqlite3_step(statement) == SQLITE_DONE)
                    {
                        DebugLog(@"AddRecord:IMAGE_SET_RELATION: successfully saved : %lld",sqlite3_last_insert_rowid(ioDB));
                    } else {
                        DebugLog(@"AddRecord:IMAGE_SET_RELATION: failed to save");
                    }
                    sqlite3_finalize(statement);
                }
                else if(!imgObj.isNew && imgObj.isDeleted)
                {
                    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM IMAGE_SET_RELATION WHERE IMAGE_MASTER_ID =\"%d\" AND IMAGE_PATH = \"%@\"",imgID,imgObj.imagePath];
                    const char *delete_stmt = [deleteSQL UTF8String];
                    sqlite3_prepare_v2(ioDB, delete_stmt, -1, &statement, NULL);
                    if (sqlite3_step(statement) == SQLITE_DONE)
                    {
                        DebugLog(@"AddRecord:deleted from IMAGE_SET_RELATION");
                        [IO_APP_DELEGATE removeImage:imgObj.imagePath];
                    } else {
                        DebugLog(@"AddRecord:Failed to delete from IMAGE_SET_RELATION");
                    }
                    sqlite3_finalize(statement);
                }
                if (imgObj.isUpdated)
                {
                    DebugLog(@"AddRecord:imgObj.isUpdated removing image");
                    [IO_APP_DELEGATE removeImage:[NSString stringWithFormat: @"old_%@",imgObj.imagePath]];
                }
            }
            
            int tagSuccess = [self DeleteAllRowsFromTagsTableWithImageId];
            if(tagSuccess)
            {
                if (_tagsArray == nil) {
                    _tagsArray = [[NSMutableArray alloc] init];
                }
                else
                {
                    [_tagsArray removeAllObjects];
                }
                if (_segmentedControl.selectedSegmentIndex == 0) {
                    [_tagsArray addObjectsFromArray:tokenFieldViewInPage.tokenTitles];
                }
                else
                {
                    [_tagsArray addObjectsFromArray:tokenFieldView.tokenTitles];
                }
                for (NSString *tagName in _tagsArray)
                {
                    int TAG_ROWID = [self getTagId:tagName];
                    if (TAG_ROWID == -1)
                    {
                        TAG_ROWID = [self insertIntoTags:tagName];
                    }
                    if (TAG_ROWID != -1)
                    {
                        NSString *insertSQL1 = [NSString stringWithFormat:
                                                @"INSERT INTO IMAGE_TAG_RELATION(IMAGE_MASTER_ID,TAG_ID) VALUES (%d,%d)",
                                                imgID,TAG_ROWID];
                        DebugLog(@"AddRecord:---%@---",insertSQL1);
                        
                        const char *insert_tag_stmt = [insertSQL1 UTF8String];
                        sqlite3_prepare_v2(ioDB, insert_tag_stmt, -1, &statement, NULL);
                        if (sqlite3_step(statement) == SQLITE_DONE)
                        {
                            DebugLog(@"AddRecord:IMAGE_TAG_RELATION: successfully saved : %lld",sqlite3_last_insert_rowid(ioDB));
                        } else {
                            DebugLog(@"AddRecord:IMAGE_TAG_RELATION: failed to save");
                        }
                        sqlite3_finalize(statement);
                    }
                }
            }
            int  groupsSuccess = [self DeleteAllRowsFromGroupsTableWithImageId];
            if(groupsSuccess)
            {
                int unselectedgroupCount = 0;
                for (GroupObject *grpObj in _groupArray)
                {
                    if (grpObj.selected == 1) {
                        unselectedgroupCount ++;
                        DebugLog(@"AddRecord:IMAGE_GROUP_RELATION: group %@ : selected",grpObj.groupName);
                        NSString *insertSQL2 = [NSString stringWithFormat:
                                                @"INSERT INTO IMAGE_GROUP_RELATION(IMAGE_MASTER_ID,GROUP_ID) VALUES (%d,%d)",
                                                _currentImgRowId,grpObj.groupRowId];
                        DebugLog(@"AddRecord:---%@---",insertSQL2);
                        
                        const char *insert_group_stmt = [insertSQL2 UTF8String];
                        sqlite3_prepare_v2(ioDB, insert_group_stmt, -1, &statement, NULL);
                        if (sqlite3_step(statement) == SQLITE_DONE)
                        {
                            DebugLog(@"AddRecord:IMAGE_GROUP_RELATION: successfully updated : %lld",sqlite3_last_insert_rowid(ioDB));
                        } else {
                            DebugLog(@"AddRecord:IMAGE_GROUP_RELATION: failed to update");
                        }
                        sqlite3_finalize(statement);
                    }
                }
                
                if (unselectedgroupCount == 0) {
                    int groupID = -1;
                    NSString *querySQL = [NSString stringWithFormat:@"Select id from GROUPS WHERE GROUP_NAME=\"%@\"",[UNGROUPED_GROUPNAME lowercaseString]];
                    const char *sqlStatement = [querySQL UTF8String];
                    if(sqlite3_prepare_v2(ioDB, sqlStatement, -1, &statement, NULL) == SQLITE_OK)
                    {
                        if(sqlite3_step(statement) == SQLITE_ROW)
                        {
                            groupID = sqlite3_column_int(statement, 0);
                            DebugLog(@"AddRecord:groupId -> %d",groupID);
                        }
                    }
                    sqlite3_finalize(statement);
                    
                    if (groupID != -1) {
                        NSString *insertSQL2 = [NSString stringWithFormat:
                                                @"INSERT INTO IMAGE_GROUP_RELATION(IMAGE_MASTER_ID,GROUP_ID) VALUES (%d,%d)",
                                                imgID,groupID];
                        DebugLog(@"AddRecord:---%@---",insertSQL2);
                        
                        const char *insert_group_stmt = [insertSQL2 UTF8String];
                        sqlite3_prepare_v2(ioDB, insert_group_stmt, -1, &statement, NULL);
                        if (sqlite3_step(statement) == SQLITE_DONE)
                        {
                            DebugLog(@"AddRecord:IMAGE_GROUP_RELATION: successfully saved in ungroup : %lld",sqlite3_last_insert_rowid(ioDB));
                        } else {
                            DebugLog(@"AddRecord:IMAGE_GROUP_RELATION: failed to save in ungroup ");
                        }
                        sqlite3_finalize(statement);
                    }
                }

            }
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message: @"Record updated successfully." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message: @"Failed to update record." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        sqlite3_close(ioDB);
    }
    savedInDB = YES;
    [self backBtnPressed:nil];
}


#pragma internal methods
-(void)setFrameOfNotesControls
{
    DebugLog(@"AddRecord:_timetextfield %f",CGRectGetMaxY(_timeTextField.frame));
    DebugLog(@"AddRecord:self.view.frame.size.height-_openNotesBtn.frame.size.height %f",self.view.frame.size.height-_openNotesBtn.frame.size.height);
    _openNotesBtn.frame = CGRectMake(self.view.frame.size.width-_openNotesBtn.frame.size.width-8,self.view.frame.size.height-_openNotesBtn.frame.size.height, _openNotesBtn.frame.size.width,_openNotesBtn.frame.size.height);
    _openNotesBtn.titleLabel.font = DEFAULT_BOLD_FONT(13.0);
    _openNotesBtn.titleLabel.textColor = [UIColor blackColor];
    [_openNotesBtn.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [_openNotesBtn.layer setShadowOpacity:1];
    [_openNotesBtn.layer setShadowOffset:CGSizeMake(0, -3)];

    notesTextView = [[NoteView alloc] init];
    [notesTextView setFrame:CGRectMake(15,60,300,self.view.frame.size.height-50)];
    [notesTextView setDelegate:self];
    [notesTextView setFont:[UIFont fontWithName:@"Bradley Hand" size:20]];
    notesTextView.inputAccessoryView = _keyBoardHideView;
    //    notesTextView.text = @"abc";
    [_notesView addSubview:notesTextView];
}
-(void)addControlsInScrollView
{
    //------------- Setting prperties of scrollview--------------------------
    _addRecordScrollView.backgroundColor = [UIColor clearColor];
    _addRecordScrollView.clipsToBounds = YES;
	_addRecordScrollView.scrollEnabled = YES;
    _addRecordScrollView.pagingEnabled = YES;
    _addRecordScrollView.showsHorizontalScrollIndicator = NO;
    _addRecordScrollView.showsVerticalScrollIndicator = NO;
    _addRecordScrollView.scrollsToTop = YES;
    _addRecordScrollView.delegate = self;
    _addRecordScrollView.bounces = NO;
    _addRecordScrollView.directionalLockEnabled = YES;
    //    _pageControl.currentPageIndicatorTintColor = DEFAULT_COLOR;          //ios6
    //    _pageControl.pageIndicatorTintColor = [UIColor grayColor];
    _pageControl.autoresizingMask = UIViewAutoresizingFlexibleWidth ;
    _pageControl.numberOfPages = _addRecordScrollView.subviews.count;
    _pageControl.currentPage = 0;
    
    //------Add segmentControl------------------------------------
    pagedSegmentedControl = [[UISegmentedControl alloc] initWithFrame:CGRectMake(0, 0, 320, 36)];
    [pagedSegmentedControl setHidden:YES];
    [pagedSegmentedControl insertSegmentWithTitle:@"Record" atIndex:0 animated:NO];
    [pagedSegmentedControl insertSegmentWithTitle:@"Tags" atIndex:1 animated:NO];
    [pagedSegmentedControl insertSegmentWithTitle:@"Groups" atIndex:2 animated:NO];
    [pagedSegmentedControl insertSegmentWithTitle:@"Location" atIndex:3 animated:NO];
    [pagedSegmentedControl setSelectedSegmentIndex:0];
    pagedSegmentedControl.segmentedControlStyle = UISegmentedControlStylePlain;
    [pagedSegmentedControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];

    [pagedSegmentedControl setTitleTextAttributes:@{
                  UITextAttributeTextColor: TABLE_CELL_BGCOLOR,
                       UITextAttributeFont: DEFAULT_FONT(16.0f)
     } forState:UIControlStateNormal];
    
//    [pagedSegmentedControl setTitleTextAttributes:@{
//                         UITextAttributeTextColor: YELLOW_COLOR,
//     //            UITextAttributeTextShadowColor: [UIColor blackColor],
//     //           UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0.0f, 1.0f)],
//                              UITextAttributeFont: [UIFont boldSystemFontOfSize:16.0f]
//     } forState:UIControlStateSelected];
    
     // Set divider images
    [pagedSegmentedControl setDividerImage:[UIImage imageNamed:@"divider-none-selected.png"]
      forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    [pagedSegmentedControl setDividerImage:[UIImage imageNamed:@"divider-left-selected.png"] forLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    [pagedSegmentedControl setDividerImage:[UIImage imageNamed:@"divider-right-selected.png"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
    // Set background images
    UIImage *normalBackgroundImage = [UIImage imageNamed:@"UnSelected_tab@2x.png"];
    UIImage *selectedBackgroundImage = [UIImage imageNamed:@"Selected_tab@2x.png"];

    [pagedSegmentedControl setBackgroundImage:normalBackgroundImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [pagedSegmentedControl setBackgroundImage:selectedBackgroundImage forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
//    [pagedSegmentedControl setContentPositionAdjustment:UIOffsetMake(4, 0)
//                        forSegmentType:UISegmentedControlSegmentLeft
//                            barMetrics:UIBarMetricsDefault];
//    [pagedSegmentedControl setContentPositionAdjustment:UIOffsetMake(-4, 0)
//                        forSegmentType:UISegmentedControlSegmentRight
//                            barMetrics:UIBarMetricsDefault];
    
    [self.view addSubview:pagedSegmentedControl];
    [self.view insertSubview:_notesView aboveSubview:pagedSegmentedControl];
    [self.view insertSubview:_openNotesBtn aboveSubview:_notesView];

    //------------- Rearrange subviews of scrollview--------------------------
    [_categoryView removeFromSuperview];
    _categoryTextField.enabled = FALSE;

    UIView *view = nil;
    CGFloat curXLoc = 0;
	for (view in [_addRecordScrollView subviews])
	{
		if ([view isKindOfClass:[UIView class]])
		{
            view.backgroundColor = [UIColor clearColor];
			CGRect frame = view.frame;
			frame.origin = CGPointMake(curXLoc, 0);
			view.frame = frame;
			
			curXLoc += (_addRecordScrollView.frame.size.width);
		}
	}
    
    //------------set the content size so it can be scrollable
    DebugLog(@"AddRecord:_addRecordScrollView.subviews.count %d",_addRecordScrollView.subviews.count);
	[_addRecordScrollView setContentSize:CGSizeMake((_addRecordScrollView.subviews.count * _addRecordScrollView.frame.size.width), _addRecordScrollView.frame.size.height)];
    
    
    // Will rearrange / change ui
    // 1) 1st Page -> Record
    // 2) 2nd Page -> Tags
    // 3) 3rd Page -> Groups
    // 4) 4th Page -> Location

    // 1) 1st Page -> Record
    //----------Reframe the controls-----------

    _imageView.frame = CGRectMake(_imageView.frame.origin.x, _titleTextField.frame.origin.y+_titleTextField.frame.size.height+10, _imageView.frame.size.width, _imageView.frame.size.height);
    
    _addImageBtn.frame = CGRectMake(_imageView.frame.origin.x,CGRectGetMaxY(_imageView.frame)- _addImageBtn.frame.size.height, _addImageBtn.frame.size.width, _addImageBtn.frame.size.height);
    
    _openGalleryBtn.frame = CGRectMake(CGRectGetMaxX(_imageView.frame) - _openGalleryBtn.frame.size.width,CGRectGetMaxY(_imageView.frame)-_openGalleryBtn.frame.size.height, _openGalleryBtn.frame.size.width, _openGalleryBtn.frame.size.height);

    
    _dateTextField.frame = CGRectMake(_imageView.frame.origin.x,CGRectGetMaxY(_imageView.frame)+10, _dateTextField.frame.size.width, _dateTextField.frame.size.height);
    _timeTextField.frame = CGRectMake(_dateTextField.frame.origin.x,CGRectGetMaxY(_dateTextField.frame)+10, _timeTextField.frame.size.width, _timeTextField.frame.size.height);
    
//    _geopointsTextField.frame = CGRectMake(_geopointsTextField.frame.origin.x, _addressTextView.frame.origin.y+_addressTextView.frame.size.height+20, _geopointsTextField.frame.size.width,_geopointsTextField.frame.size.height);
//    _pinImageView.frame = CGRectMake(_pinImageView.frame.origin.x, _geopointsTextField.frame.origin.y+5, _pinImageView.frame.size.width,_pinImageView.frame.size.height);
//    _PinArrowRightImageView.frame = CGRectMake(_PinArrowRightImageView.frame.origin.x, _geopointsTextField.frame.origin.y+5, _PinArrowRightImageView.frame.size.width,_PinArrowRightImageView.frame.size.height);
    
    _imageView.layer.borderColor = [UIColor clearColor].CGColor;
    _imageView.clipsToBounds = YES;
    _imageView.layer.borderWidth = 2.0;
    _imageView.layer.cornerRadius = 10.0;
    
    [CommonCallback setTextFieldPropertiesWithBorder:_titleTextField text:@""];
    [CommonCallback setTextFieldPropertiesWithBorder:_dateTextField text:@""];
    [CommonCallback setTextFieldPropertiesWithBorder:_timeTextField text:@""];
    _titleTextField.delegate = self;
    _dateTextField.delegate = self;
    _timeTextField.delegate = self;


    // 2) 2nd Page -> Tags
    //----------Add token field control in scrollview-----------
    _addTagsLabel.textColor = [UIColor whiteColor];
    _addTagsLabel.font = DEFAULT_BOLD_FONT(20.0);

    tokenFieldViewInPage = [[TITokenFieldView alloc] initWithFrame:CGRectMake(20.0f, 55.0f, 280.0f, 120.0f)];
    [tokenFieldViewInPage.tokenField setFrame:CGRectMake(tokenFieldViewInPage.tokenField.frame.origin.x, tokenFieldViewInPage.tokenField.frame.origin.y, tokenFieldViewInPage.tokenField.frame.size.width, 150.0f)];
    [tokenFieldViewInPage.tokenField setDelegate:self];
    [tokenFieldViewInPage.tokenField setPromptText:@""];
    [tokenFieldViewInPage setBackgroundColor:TABLE_EDITCELL_BGCOLOR];
    [tokenFieldViewInPage.tokenField setTextColor:[UIColor blackColor]];
    [tokenFieldViewInPage.tokenField setPlaceholder:@"Enter Tag Name"];
//    [tokenFieldViewInPage.tokenField addTarget:self action:@selector(tokenFieldFrameDidChange:) forControlEvents:TITokenFieldControlEventFrameDidChange];
    [tokenFieldViewInPage.tokenField setTokenizingCharacters:[NSCharacterSet characterSetWithCharactersInString:@",;."]];
    [_tagView addSubview:tokenFieldViewInPage];
    tokenFieldViewInPage.tokenField.returnKeyType = UIReturnKeyDone;
    
    //-----Set border for controls------------
    tokenFieldViewInPage.layer.borderColor = TABLE_EDITCELL_BGCOLOR.CGColor;
    tokenFieldViewInPage.layer.borderWidth = 2.0;
    tokenFieldViewInPage.layer.cornerRadius = 10.0;
    
    _tagInfoLabel.frame = CGRectMake(_tagInfoLabel.frame.origin.x,CGRectGetMaxY(tokenFieldViewInPage.frame)+50, _tagInfoLabel.frame.size.width, _tagInfoLabel.frame.size.height);
    _tagInfoLabel.numberOfLines = 0;
    _tagInfoLabel.font = DEFAULT_FONT(15.0);
    _tagInfoLabel.textColor = TABLE_CELL_BGCOLOR;//[UIColor blackColor];
    _tagInfoLabel.text = @"   Note :\n   To seperate tags, press ( '.' or ',' or ';'\n   or space bar two times ) .";
    _tagInfoLabel.backgroundColor = TABLE_EDITCELL_BGCOLOR;
    
    _tagInfoLabel.layer.borderColor = BGCOLOR.CGColor;
    _tagInfoLabel.layer.borderWidth = 2.0;
    _tagInfoLabel.layer.cornerRadius = 10.0;
    // 3) 3rd Page -> Groups
    //------------reload group icons in scrollView
    //    [self addIconsInGroupScrollView];
    [CommonCallback setTextFieldPropertiesWithBorder:_groupTextField text:@""];
    _groupTableView.backgroundColor = [UIColor clearColor];
    _groupTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _groupTableView.separatorColor = [UIColor clearColor];
    _groupTableView.layer.cornerRadius = 10.0;
    
    // 4) 4th Page -> Location
    _addressTextView.font = DEFAULT_FONT(15);
    [CommonCallback setTextViewProperties:_addressTextView text:LOCATION_PLACEHOLDER1];
    _addressTextView.delegate = self;
    _addressTextView.layer.cornerRadius = 10.0;

    //-----Add pin in map view of scrollview------------
    _pageMapView.layer.cornerRadius = 10.0;
    [_pageMapView setMapType:MKMapTypeStandard];
    [_pageMapView setZoomEnabled:YES];
    [_pageMapView setScrollEnabled:YES];
    [_pageMapView setDelegate:self];
    [self addAnnotationInMapView];
}

-(void)addIconsInGroupScrollView
{
    [_groupImagesScrollView setCanCancelContentTouches:NO];
	_groupImagesScrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
	_groupImagesScrollView.clipsToBounds = YES;		// default is NO, we want to restrict drawing within our gridScrollView
	_groupImagesScrollView.scrollEnabled = YES;
    _groupImagesScrollView.bounces = NO;
    _groupImagesScrollView.showsVerticalScrollIndicator = NO;
    _groupImagesScrollView.directionalLockEnabled = YES;
    _groupImagesScrollView.delegate = self;
    _groupImagesScrollView.pagingEnabled = YES;
//    [self roundeWithBorder:_groupImagesScrollView];
    _groupImagesScrollView.frame = CGRectMake(_groupImagesScrollView.frame.origin.x, _groupImageView.frame.origin.y+_groupImageView.frame.size.height, _groupImagesScrollView.frame.size.width, _groupImagesScrollView.frame.size.height);
    [_groupImagesScrollView setCanCancelContentTouches:NO];
    
    double NUMBEROFBUTTONSPERPAGE = 9.0;
    int pageCount = (int)ceil([_iconsDic.allKeys count]/NUMBEROFBUTTONSPERPAGE);
    DebugLog(@"AddRecord:[_iconsDic.allKeys count] %d",[_iconsDic.allKeys count]);
    
    DebugLog(@"AddRecord:pageCount %d",pageCount);
    for (int pageIndex= 1; pageIndex <= pageCount; pageIndex++)
	{
        //	int tag = pageIndex * 100;
        
        UIView *view=[[UIView alloc] init];
		CGRect rect;
        view.backgroundColor=[UIColor clearColor];
        
		rect.size.height = _groupImagesScrollView.frame.size.height;
		rect.size.width = _groupImagesScrollView.frame.size.width;
		view.frame = rect;
		view.tag = pageIndex;	// tag our images for later use when we place them in serial fashion
		
        int X=35,Y=25;
        int Hspace=30;
        int Vspace=30;
        
        
        int width=50;
        int height=50;
        int labelHeight=0;//20;
        //  int labelWidth=110;
        UIButton *button;
        
        CGRect frame;
        for (int buttonIndexOnPage=0;buttonIndexOnPage<NUMBEROFBUTTONSPERPAGE; buttonIndexOnPage++) {
            
            if ((buttonIndexOnPage+(NUMBEROFBUTTONSPERPAGE*(pageIndex-1))) == [_iconsDic.allKeys count])
            {
                break;
            }
            int index = (buttonIndexOnPage+(NUMBEROFBUTTONSPERPAGE*(pageIndex-1)));
            NSString *name = [_iconsDic valueForKey:[_iconsDic.allKeys objectAtIndex:index]];
            frame = CGRectMake(X, Y, width, height);
            button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = frame;
            button.backgroundColor=[UIColor clearColor];
            button.titleLabel.font=DEFAULT_BOLD_FONT(10);
            
            [button setImage:[UIImage imageNamed:name] forState:UIControlStateNormal];
            // [button setTitle:name forState:UIControlStateNormal];
            button.tag = [[_iconsDic.allKeys objectAtIndex:index] intValue]; //tag+buttonIndexOnPage;
            [button addTarget:self action:@selector(groupIconBtnClicked:)forControlEvents:UIControlEventTouchUpInside];
            
            [view addSubview:button];
            
            button=nil;
            int NextX = X+width+Hspace;
            
            if (NextX < view.frame.size.width-60)
            {
                X=NextX;
            }
            else
            {
                X=35;
                Y=Y+height+labelHeight+Vspace;
            }
            
        }
        [_groupImagesScrollView addSubview:view];
    }
    [self layoutScrollImages:pageCount];
    [_groupImagesScrollView setHidden:YES];
}
- (void)layoutScrollImages:(int)count
{
    DebugLog(@"AddRecord:count %d",count);
	UIView *view = nil;
	NSArray *subviews = [_groupImagesScrollView subviews];
    
	// reposition all subviews in a horizontal serial fashion
	CGFloat curXLoc = 0;
	for (view in subviews)
	{
		if ([view isKindOfClass:[UIView class]] && view.tag > 0)
		{
			CGRect frame = view.frame;
			frame.origin = CGPointMake(curXLoc, 0);
			view.frame = frame;
			
			curXLoc += (_groupImagesScrollView.frame.size.width);
		}
	}
	
	// set the content size so it can be scrollable
	[_groupImagesScrollView setContentSize:CGSizeMake((count * _groupImagesScrollView.frame.size.width), _groupImagesScrollView.frame.size.height)];
}
-(void)reloadScrollViewData
{
    _titleTextField.text = _selectedTitle;
    _categoryTextField.text = _selectedCategory;
    _addressTextView.text = _reverseAddress;
    _geopointsTextField.text = [NSString stringWithFormat:@"%f,%f",_currentLatitude,_currentLongitude];
    _dateTextField.text = _selectedDate;
    _timeTextField.text = _selectedTime;
    
    DebugLog(@"AddRecord:index of [_categoryArray indexOfObject:_selectedCategory] %d",[_categoryArray indexOfObject:_selectedCategory]);
    int index = [_categoryArray indexOfObject:_selectedCategory];
    if (index > -1 && index < _categoryArray.count) {
        [_categoryTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
        [_pickerView selectRow:index inComponent:0 animated:YES];
    }
    [self reloadTokenField:-1];  //load tags field
}
-(void)reloadTokenField:(int)type
{
    if (type == -1) //@"firsttime"
    {
        if (_tagsArray.count > 0)
        {
            [tokenFieldView.tokenField removeAllTokens];
            [tokenFieldViewInPage.tokenField removeAllTokens];
            for (NSString *tokenTitle in _tagsArray) {
                [tokenFieldView.tokenField addTokenWithTitle:tokenTitle];
                [tokenFieldViewInPage.tokenField addTokenWithTitle:tokenTitle];
            }
            [tokenFieldView.tokenField layoutTokensAnimated:YES];
            [tokenFieldViewInPage.tokenField layoutTokensAnimated:YES];
            [tokenFieldView resignFirstResponder];
            [tokenFieldViewInPage resignFirstResponder];
        }
    }
    else if(type == 0) //@"FromPageToList"
    {
        [tokenFieldView.tokenField removeAllTokens];
        for (NSString *tokenT in tokenFieldViewInPage.tokenTitles) {
            [tokenFieldView.tokenField addTokenWithTitle:tokenT];
        }
        [tokenFieldView resignFirstResponder];
        [tokenFieldView.tokenField layoutTokensAnimated:YES];
    }
    else if(type == 1) //@"FromListToPage"
    {
        [tokenFieldViewInPage.tokenField removeAllTokens];
        for (NSString *tokenT in tokenFieldView.tokenTitles) {
            [tokenFieldViewInPage.tokenField addTokenWithTitle:tokenT];
        }
        [tokenFieldViewInPage resignFirstResponder];
        [tokenFieldViewInPage.tokenField layoutTokensAnimated:YES];
    }
    [self scrollTobottom];
}

// to reload reverse address in textfield
-(void)reloadReverseAddress
{
    UITableViewCell *cell = [_addRecordTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:5]];
    UITextView *textView = (UITextView *) [cell viewWithTag:ADDRESS_TAG];
    textView.text = _reverseAddress;
    cell = [_addRecordTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:5]];
    UILabel *geoLabel = (UILabel *)[cell viewWithTag:GEOPOINT_TAG];
    geoLabel.text = [NSString stringWithFormat:@"%f,%f",_currentLatitude,_currentLongitude];
    
    _addressTextView.text = _reverseAddress;
    _geopointsTextField.text = [NSString stringWithFormat:@"%f,%f",_currentLatitude,_currentLongitude];
    
    [self addAnnotationInMapView];
}
-(void)addAnnotationInMapView
{
    if (_currentLatitude != 0 && _currentLongitude != 0)
    {
        
        MKCoordinateRegion region = { {0.0, 0.0 }, { 0.0, 0.0 } };
        
        region.center.latitude = _currentLatitude;
        region.center.longitude = _currentLongitude;
        
        region.span.longitudeDelta = 0.01f;
        region.span.latitudeDelta = 0.01;
        [_pageMapView setRegion:region animated:YES];
        
        DisplayMap *geoPointAnnotation = nil;
        if ([_pageMapView annotations].count > 0)
        {
            geoPointAnnotation = (DisplayMap *)[[_pageMapView annotations] objectAtIndex:0];
        }
        else
        {
            geoPointAnnotation = [[DisplayMap alloc] init];
            [_pageMapView addAnnotation:geoPointAnnotation];
        }
        geoPointAnnotation.title = _reverseAddress;
        geoPointAnnotation.subtitle = @"";
        geoPointAnnotation.coordinate = region.center;
    }
}
-(void) addPickerWithDoneButton
{
    actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                              delegate:nil
                                     cancelButtonTitle:nil
                                destructiveButtonTitle:nil
                                     otherButtonTitles:nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    
    CGRect categoryPickerFrame = CGRectMake(0, 40, 0, 0);
    
    _pickerView = [[UIPickerView alloc] initWithFrame:categoryPickerFrame];
    _pickerView.showsSelectionIndicator = YES;
    _pickerView.dataSource = self;
    _pickerView.delegate = self;
    [actionSheet addSubview:_pickerView];
    
    CGRect datetimePickerFrame = CGRectMake(0, 40, 0, 0);
    
    _datetimePickerView = [[UIDatePicker alloc] initWithFrame:datetimePickerFrame];
    _datetimePickerView.timeZone = [NSTimeZone localTimeZone];
    _datetimePickerView.maximumDate = [NSDate date];
    [_datetimePickerView addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:_datetimePickerView];
    
    UISegmentedControl *doneButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Done"]];
    doneButton.momentary = YES;
    doneButton.frame = CGRectMake(260, 7.0f, 50.0f, 30.0f);
    doneButton.segmentedControlStyle = UISegmentedControlStyleBar;
    doneButton.tintColor = BGCOLOR;
    [doneButton addTarget:self action:@selector(doneFilterBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:doneButton];
    
    UISegmentedControl *cancelButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Cancel"]];
    cancelButton.momentary = YES;
    cancelButton.frame = CGRectMake(10, 7.0f, 60.0f, 30.0f);
    cancelButton.segmentedControlStyle = UISegmentedControlStyleBar;
    cancelButton.tintColor = [UIColor blackColor];
    [cancelButton addTarget:self action:@selector(cancelFilterBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:cancelButton];
    
    [_pickerView reloadAllComponents];
    if([_pickerView numberOfRowsInComponent:0] != -1)
    {
        [_pickerView selectRow:0 inComponent:0 animated:YES];
    }
    [self dateChanged:nil];
}
- (void)doneFilterBtnClicked:(id)sender
{
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    DebugLog(@"AddRecord:cuurent case %d",CURRENTCASE);
    switch (CURRENTCASE) {
        case CATEGORY_TAG:
        {
            UITableViewCell *cell = [_addRecordTableView cellForRowAtIndexPath:_selectedIndexPath];
            UITextField *catgTextField = (UITextField *)[cell viewWithTag:CATEGORY_TAG];
            _selectedCategory = [_categoryArray objectAtIndex:[_pickerView selectedRowInComponent:0]];
            catgTextField.text = _selectedCategory;
            _categoryTextField.text = _selectedCategory;
        }
            break;
            //        case TAGS_TAG:
            //        {
            //            UITableViewCell *cell = [_addRecordTableView cellForRowAtIndexPath:_selectedIndexPath];
            //            UITextField *tagsTextField = (UITextField *)[cell viewWithTag:TAGS_TAG];
            //            _selectedTag = [_pickerArray objectAtIndex:[_pickerView selectedRowInComponent:0]];
            //            tagsTextField.text = _selectedTag;
            //        }
            //            break;
            //        case GROUPS_TAG:
            //        {
            //            UITableViewCell *cell = [_addRecordTableView cellForRowAtIndexPath:_selectedIndexPath];
            //            UITextField *groupTextField = (UITextField *)[cell viewWithTag:GROUPS_TAG];
            //            _selectedGroup = [_pickerArray objectAtIndex:[_pickerView selectedRowInComponent:0]];
            //            groupTextField.text = _selectedGroup;
            //        }
            break;
        case DATE_TAG:
        {
            UITableViewCell *cell = [_addRecordTableView cellForRowAtIndexPath:_selectedIndexPath];
            UITextField *dateTextField = (UITextField *)[cell viewWithTag:DATE_TAG];
            DebugLog(@"AddRecord:_selectedDate %@",_selectedDate);
            dateTextField.text = _selectedDate;
            _dateTextField.text = _selectedDate;
        }
            break;
        case TIME_TAG:
        {
            UITableViewCell *cell = [_addRecordTableView cellForRowAtIndexPath:_selectedIndexPath];
            UITextField *timeTextField = (UITextField *)[cell viewWithTag:TIME_TAG];
            DebugLog(@"AddRecord:_selectedTime %@",_selectedTime);
            timeTextField.text = _selectedTime;
            _timeTextField.text = _selectedTime;
        }
            break;
        default:
            break;
    }
    CURRENTCASE = 0;
    
}

- (void)cancelFilterBtnClicked:(id)sender
{
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    CURRENTCASE = 0;
}

- (void) dateChanged:(id)sender
{
    NSDate *pickerDate = [_datetimePickerView date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMMM yyyy"];
    _selectedDate = [dateFormat stringFromDate:pickerDate];
    [dateFormat setDateFormat:@"HH:mm a"];
    _selectedTime = [dateFormat stringFromDate:pickerDate];
    
    DebugLog(@"AddRecord:%@ %@",_selectedDate,_selectedTime);
}

-(int)getSelectedGroupCountFromArray
{
    DebugLog(@"AddRecord:getSelectedGroupCountFromArray");
    int count = 0;
    GroupObject *recObj;
    for (int index = 0; index < _groupArray.count; index++) {
        recObj = (GroupObject *)[_groupArray objectAtIndex:index];
        if (recObj.selected == 1) {
            count ++;
        }
    }
    return count;
}
-(void)insertIntoGroups:(NSString *)groupName iconid:(int)iconId
{
    DebugLog(@"AddRecord:--insertIntoGroups--");
    int RowId = -1;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String],&ioDB) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO GROUPS(GROUP_NAME,ICON_ID) VALUES (\"%@\",%d)",[groupName lowercaseString],iconId];
        DebugLog(@"AddRecord:---%@---",insertSQL);
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_stmt *statement = nil;
        sqlite3_prepare_v2(ioDB, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            RowId = sqlite3_last_insert_rowid(ioDB);
            DebugLog(@"AddRecord:GROUPS : successfully saved :");
            
        } else {
            DebugLog(@"AddRecord:GROUPS : failed to save");
        }
        sqlite3_finalize(statement);
        sqlite3_close(ioDB);
    }
    if (RowId != -1) {
        GroupObject *grp = [[GroupObject alloc] init];
        grp.groupRowId = RowId;
        grp.groupName = groupName;
        grp.selected = 1;
        grp.iconId = iconId;
        [self insertIntoGroupTemp:grp];
        
        [_groupArray addObject:grp];
        [_groupTableView beginUpdates];
        [_groupTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:_groupArray.count-1 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        [_groupTableView endUpdates];
        
        DebugLog(@"%f %f %f",_groupTableView.frame.origin.y,_groupTableView.contentSize.height,self.view.frame.size.height);

        if (_groupTableView.frame.origin.y+_groupTableView.contentSize.height < self.view.frame.size.height-50) {
            [UIView animateWithDuration:0.5 animations:^{
                [_groupTableView setFrame:CGRectMake(_groupTableView.frame.origin.x,
                                                     _groupTableView.frame.origin.y,
                                                     _groupTableView.frame.size.width,
                                                     _groupTableView.contentSize.height)];
                
            } completion:^(BOOL finished) {
            }];
        }
        grp = nil;
    }
}
-(void)colorSetup
{
   // self.view.backgroundColor = BGCOLOR;
    self.view.backgroundColor = [UIColor clearColor];

    _addRecordScrollView.backgroundColor = [UIColor clearColor];
    _addRecordTableView.separatorColor   = [UIColor clearColor];
    
    _notesView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"noteBg.jpg"]];
    [notesTextView setBackgroundColor:[UIColor clearColor]];
    
    _groupImagesScrollView.backgroundColor = [UIColor blackColor];
    
}
-(void)addGestures
{
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    viewTap.numberOfTapsRequired = 1;
    viewTap.cancelsTouchesInView = NO;
    [_addRecordTableView  addGestureRecognizer:viewTap];
    
    UITapGestureRecognizer *viewTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(scrollviewTapDetected:)];
    viewTap1.numberOfTapsRequired = 1;
    viewTap1.cancelsTouchesInView = NO;
//    _addRecordScrollView.multipleTouchEnabled = YES;
    [_addRecordScrollView  addGestureRecognizer:viewTap1];
    
    UITapGestureRecognizer *imageViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(imageTapDetected:)];
    imageViewTap.numberOfTapsRequired = 1;
    imageViewTap.cancelsTouchesInView = NO;
    [_imageView addGestureRecognizer:imageViewTap];
    
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(textViewSwipeDetected:)];
    swipeGesture.direction = UISwipeGestureRecognizerDirectionRight;
    [_notesView addGestureRecognizer:swipeGesture];
}
-(void)addNSNotificationCenter
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(useDropPinNotificationWithString:) name:ADDRECORDNOTIFICATIONNAME object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)showGalleryBtn
{
    int photocount = [self getPhotoCount];
    
    if (photocount == 0) {
        [_addImageBtn setHidden:YES];
        [_openGalleryBtn setHidden:YES];
        _imageView.image = [UIImage imageNamed:@"placeholder.png"];
    }
    else
    {
        [_addImageBtn setHidden:NO];
        [_openGalleryBtn setHidden:NO];
    }
}

-(void)setPlaceholder:(UITextView *)ltxtview
{
    ltxtview.textColor = [UIColor lightGrayColor];
    ltxtview.text = LOCATION_PLACEHOLDER2;
}

-(void)showAddImageActionSheet
{
    [self.view endEditing:YES];
    int photocount = [self getPhotoCount];
    if (photocount == 9)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message: @"You can't add more than 9 images" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            UIActionSheet *uiActionSheetP= [[UIActionSheet alloc]
                                            initWithTitle: @""
                                            delegate:self
                                            cancelButtonTitle:@"CANCEL"
                                            destructiveButtonTitle:nil
                                            otherButtonTitles:@"Take Photo",@"Choose Existing Photo", nil];
            uiActionSheetP.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
            uiActionSheetP.title = ADDIMAGE_ACTIONSHEET_TITLE;
            [uiActionSheetP showInView:self.navigationController.view];
        }
        else
        {
            UIActionSheet *uiActionSheetP= [[UIActionSheet alloc]
                                            initWithTitle: @""
                                            delegate:self
                                            cancelButtonTitle:@"CANCEL"
                                            destructiveButtonTitle:nil
                                            otherButtonTitles:@"Choose Existing Photo", nil];
            uiActionSheetP.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
            uiActionSheetP.title = ADDIMAGE_ACTIONSHEET_TITLE;
            [uiActionSheetP showInView:self.navigationController.view];
        }
    }
}


-(void)showPicker:(int) pickerType
{
    switch (pickerType) {
        case CATEGORY_TAG:
        case TAGS_TAG:
        case GROUPS_TAG:
        {
            [_pickerView setHidden:NO];
            [_datetimePickerView setHidden:YES];
        }
            break;
        case DATE_TAG:
        {
            [_pickerView setHidden:YES];
            [_datetimePickerView setHidden:NO];
            _datetimePickerView.datePickerMode = UIDatePickerModeDate;
        }
            break;
        case TIME_TAG:
        {
            [_pickerView setHidden:YES];
            [_datetimePickerView setHidden:NO];
            _datetimePickerView.datePickerMode = UIDatePickerModeTime;
        }
            break;
        default:
            return;
    }
    [actionSheet showInView:self.view];
    [actionSheet setBounds:CGRectMake(0, 0, 320, 485)];
}


-(void)scrollTobottom
{
    CGPoint bottomOffset = CGPointMake(0, 0);
    [_addRecordTableView setContentOffset:bottomOffset animated:YES];
}

-(int)getImageIndex:(int)currentIndex
{
    int imageIndex = 0;
    ImageObject *imageObj = nil;
    for (int i = 0; i < _imagesArray.count; i++) {
        imageObj = (ImageObject *)[_imagesArray objectAtIndex:i];
        if (!imageObj.isDeleted) {
            {
                if (imageIndex == currentIndex) {
                    imageIndex = i;
                    break;
                }
                imageIndex ++;
            }
        }
        imageObj = nil;
    }
    DebugLog(@"AddRecord:imageIndex %d",imageIndex);
    return imageIndex;
}

//-(int)getImageIndex:(int)currentPage
//{
//    int imageIndex = 0;
//    MWPhoto *imageObj = nil;
//    for (int i = 0; i < _photos.count; i++) {
//        imageObj = (MWPhoto *)[_photos objectAtIndex:i];
//        if (!imageObj.isDeleted) {
//            {
//                if (imageIndex == currentPage) {
//                    imageIndex = i;
//                    break;
//                }
//                imageIndex ++;
//            }
//        }
//        imageObj = nil;
//
//    }
//    DebugLog(@"AddRecord:imageIndex %d",imageIndex);
//    return imageIndex;
//}
-(int)getPhotoCount
{
    int count = 0;
    ImageObject *imageObj = nil;
    for (int i = 0; i < _imagesArray.count; i++) {
        imageObj = (ImageObject *)[_imagesArray objectAtIndex:i];
        DebugLog(@"AddRecord:Image %@ isNew %d isDEleted %d",imageObj.imagePath,imageObj.isNew,imageObj.isDeleted);
        if (!imageObj.isDeleted)
        {
                count ++;
        }
    }
    DebugLog(@"AddRecord:count %d",count);
    return count;
}


#pragma mark - MWPhotoBrowserDelegate
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return kNumberOfPages;
}

- (MWPhoto *)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < _photos.count)
    {
        return [_photos objectAtIndex:[self getImageIndex:index]];
        
    }
    return nil;
}

-(void)photoBrowser:(MWPhotoBrowser *)photoBrowser deleteImageAtIndex:(int)index
{
    if (index < _photos.count)
    {
        int imageIndex = [self getImageIndex:index];
        MWPhoto *tempObj = (MWPhoto *)[_photos objectAtIndex:imageIndex];
        tempObj.isDeleted = YES;
        [_photos replaceObjectAtIndex:imageIndex withObject:tempObj];
        
        ImageObject *tempImageObj = (ImageObject *)[_imagesArray objectAtIndex:imageIndex];
        tempImageObj.isDeleted = YES;
        [_imagesArray replaceObjectAtIndex:imageIndex withObject:tempImageObj];
        
        tempObj = nil;
        tempImageObj = nil;
        
        kNumberOfPages =  [self getPhotoCount];
        // check is done or not
        for(ImageObject *obj in _imagesArray)
        {
            DebugLog(@"AddRecord:image %@ isNew %d isDEleted %d",obj.imagePath,obj.isNew,obj.isDeleted);
        }
        [photoBrowser reloadData];
    }
}
-(void)photoBrowser:(MWPhotoBrowser *)photoBrowser editImageAtIndex:(int)index
{
    if (index < _photos.count)
    {
        int imageIndex = [self getImageIndex:index];
        ImageObject *imgObj = (ImageObject *)[_imagesArray objectAtIndex:imageIndex];
        ImageFilteringViewController *filterViewController = [[ImageFilteringViewController alloc] initWithNibName:@"ImageFilteringViewController" bundle:nil];
        filterViewController.imageObjectToFilter = [imgObj mutableCopy];
        filterViewController.firsttime = NO;
        [UIView  transitionWithView:self.navigationController.view duration:0.8  options:UIViewAnimationOptionTransitionFlipFromLeft
                         animations:^(void) {
                             BOOL oldState = [UIView areAnimationsEnabled];
                             [UIView setAnimationsEnabled:NO];
                             [self.navigationController pushViewController:filterViewController animated:YES];
                             [UIView setAnimationsEnabled:oldState];
                         }
                         completion:nil];
        imgObj = nil;
    }
}
-(void)photoBrowser:(MWPhotoBrowser *)photoBrowser replaceImageAtIndex:(ImageObject *)imgObj
{
    [self reloadFilteredImage:imgObj];
    [photoBrowser reloadData];
}


#pragma CLLocationManager CallBack Methods
- (void) locationManager:(CLLocationManager *) manager didFailWithError:(NSError *) error
{
    [locationManager stopUpdatingLocation];
}

- (void)stopUpdatingCoreLocation:(NSString *)state
{
    [locationManager stopUpdatingLocation];
    [self saveCurrentocation:[locationManager location]];
}

-(void)saveCurrentocation:(CLLocation *)lLocation
{
    // If it's not possible to get a location, then return.
	if (!lLocation) {
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                             message:@"Failed to get your current location. Please check your internet connection and try again."
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil] ;
        [errorAlert show];
		return;
	}
    // Configure the new event with information from the location.
    _currentLatitude  = lLocation.coordinate.latitude;
    _currentLongitude = lLocation.coordinate.longitude;
    DebugLog(@"%f --- %f",_currentLatitude,_currentLongitude);
    [self getReverseAddress];
}
#pragma segmentcontrol delegate methods
- (void)indexDidChangeForSegmentedControl:(UISegmentedControl *)sender {
    
    NSUInteger index = sender.selectedSegmentIndex;
    if (index == 0)  //Page
    {
        [self tapDetected:nil];
        [_addRecordTableView setHidden:YES];
        [_addRecordScrollView setHidden:NO];
        [_pageControl setHidden:NO];
        [pagedSegmentedControl setHidden:NO];
        [self reloadTokenField:1];
        if (pagedSegmentedControl.selectedSegmentIndex == 0){
            _openNotesBtn.hidden = NO;
        }
        else
        {
            _openNotesBtn.hidden = YES;
        }
        [self showGalleryBtn];
    }
    else // List
    {
        [self scrollviewTapDetected:nil];
        [_addRecordTableView setHidden:NO];
//        [self reloadGroupsCells];
        [_addRecordScrollView setHidden:YES];
        [_pageControl setHidden:YES];
        [pagedSegmentedControl setHidden:YES];
        [self reloadTokenField:0];
        if (_notesView.hidden) {
            [_openNotesBtn setHidden:YES];
        }
    }
}

- (void)segmentedControlChangedValue:(UISegmentedControl *)lsegmentedControl {
	DebugLog(@"AddRecord:Selected index %i (via UIControlEventValueChanged)", lsegmentedControl.selectedSegmentIndex);
    if ((lsegmentedControl.selectedSegmentIndex == 0 && _segmentedControl.selectedSegmentIndex == 0) || _notesView.hidden == NO) {
        _openNotesBtn.hidden = NO;
    }
    else
    {
        _openNotesBtn.hidden = YES;
    }
    if (lsegmentedControl.selectedSegmentIndex != _pageControl.currentPage) {
        _pageControl.currentPage = lsegmentedControl.selectedSegmentIndex;
        [self pageChange:nil];
    }
}

#pragma Gesture callback methods
- (void)tapDetected:(UIGestureRecognizer *)sender {
    DebugLog(@"AddRecord:addrecord: tapDetected");
    // get location of tap
    CGPoint tapLocation = [sender locationInView:_addRecordTableView];
    // Query tableview and get location of cell
    NSIndexPath *indexpath = [_addRecordTableView indexPathForRowAtPoint:tapLocation];
    // Now get the actual cell
    UITableViewCell *cell = [_addRecordTableView cellForRowAtIndexPath:indexpath];
    if (cell == nil) {
        DebugLog(@"AddRecord:cell nil");
    }else{
        DebugLog(@"AddRecord:indexpath %@",indexpath);
        if (indexpath.section == 1 && indexpath.row == 0) {
            UIButton *btn = (UIButton *)[cell viewWithTag:ADD_IMAGE_BTN_TAG];
            if (btn != nil) {
                return;
            }
        }
    }
    [self.view endEditing:YES];
    [self scrollTobottom];
}

- (void)scrollviewTapDetected:(UIGestureRecognizer *)sender {
    DebugLog(@"AddRecord:scrollviewTapDetected");
    CGPoint tapLocation = [sender locationInView:_addRecordScrollView];
    UIView *view = [_addRecordScrollView hitTest:tapLocation withEvent:nil];
    
    if (pagedSegmentedControl.selectedSegmentIndex == 1) {
        DebugLog(@"AddRecord:got tap in tags");

//        DebugLog(@"AddRecord:TITokenFieldView %f %f %f %f",tokenFieldViewInPage.frame.origin.x,tokenFieldViewInPage.frame.origin.y,tokenFieldViewInPage.frame.origin.x+tokenFieldViewInPage.frame.size.width,tokenFieldViewInPage.frame.origin.y+tokenFieldViewInPage.frame.size.height);
        double tapx = tapLocation.x -320;
        double tapy = tapLocation.y;
        
        DebugLog(@"AddRecord:tapLocation %f %f",tapx,tapy);
        
        if (tapx >= tokenFieldViewInPage.frame.origin.x && tapx <= tokenFieldViewInPage.frame.origin.x+tokenFieldViewInPage.frame.size.width && tapy >= tokenFieldViewInPage.frame.origin.y && tapy <= tokenFieldViewInPage.frame.origin.y+tokenFieldViewInPage.frame.size.height) {
            DebugLog(@"AddRecord:TITokenFieldView");
            [tokenFieldViewInPage.tokenField becomeFirstResponder];
            return;
        }
    }
    [self.view endEditing:YES];
}

-(void)imageTapDetected:(UIGestureRecognizer *)sender {
    DebugLog(@"AddRecord:ImageTapped");
    [self.view endEditing:YES];
     [self showAddImageActionSheet];
}

-(void)textViewSwipeDetected:(UISwipeGestureRecognizer*)swipeGesture {
    DebugLog(@"AddRecord:textViewSwipeDetected");
    [self openNotesBtnClicked:nil];
}

- (void)useDropPinNotificationWithString:(NSNotification *)notification //use notification method and logic
{
    DebugLog(@"got useDropPinNotificationWithString notification ");
    NSDictionary *dictionary = [notification userInfo];
    if (![[dictionary valueForKey:REVERSEADDRESS] isEqualToString:@""])
    {
        DebugLog(@"got REVERSEADDRESS notification");
        _reverseAddress     = [dictionary valueForKey:REVERSEADDRESS];
        _currentLatitude    = [[dictionary valueForKey:@"Latitude"] doubleValue];
        _currentLongitude   = [[dictionary valueForKey:@"Longitude"] doubleValue];
        DebugLog(@"AddRecord:%@ %f %f",_reverseAddress,_currentLatitude,_currentLongitude);
        [self reloadReverseAddress];
    }
}

#pragma Send HTTP Method callbacks
-(void)getReverseAddress
{
    DebugLog(@"getReverseAddress %c %@",self.isViewLoaded,self.view.window);
    if(self.isViewLoaded && self.view.window){
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:REVERSE_API(_currentLatitude,_currentLongitude)] cachePolicy:YES timeoutInterval:20.0];
        theConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
    }
}

#pragma mark Connection Delegate Methods
-(void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *) response
{
    
}

-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *) data
{
    if(webData==nil)
	{
		webData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[webData appendData:data];
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *) error
{
    DebugLog(@"didFailWithError error %@",error);
}

-(void) connectionDidFinishLoading:(NSURLConnection *) connection
{
    DebugLog(@"connectionDidFinishLoading");
    if (webData)
    {
        NSString *result = [[NSString alloc] initWithData:webData encoding:NSASCIIStringEncoding];
		DebugLog(@"\n result:%@\n\n", result);
        
        NSString *xmlDataFromChannelSchemes = [[NSString alloc] initWithString:result];
		NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
		xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
		[xmlParser setDelegate:self];
		[xmlParser parse];
		webData = nil;
        theConnection = nil;
    }
    else
    {
        DebugLog(@"webdata = %@",webData);
    }
}

#pragma mark XML Parser Delegate Methods
-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"GeocodeResponse"])
    {
    }
    else if ([elementName isEqualToString:@"result"])
    {
    }
    else if ([elementName isEqualToString:@"formatted_address"])
    {
        elementFound = YES;
    }
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    
    if (elementFound)
    {
        _reverseAddress = string;
    }
    elementFound = FALSE;
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"formatted_address"])
    {
        if(_reverseAddress != nil)
        {
            DebugLog(@"AddRecord:reverse Address %@",_reverseAddress);
            [self reloadReverseAddress];
        }
        [xmlParser abortParsing];
    }
}

-(void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError{
}

-(void)parserDidEndDocument:(NSXMLParser *)parser{
    
}

#pragma mark Tableview DataSource methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([tableView isEqual:_addRecordTableView]) {
        return 8;
    }
    else 
        return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:_addRecordTableView]) {
            switch (section) {
                case 0: // title
                case 1: // image
                case 2: // category
                case 3: // tags
                case 4: // groups
                case 5: // location
                case 6: // date
                case 7: //time
                {
                    return 1;
                }
                    break;
                default:
                    break;
            }
    }
    else if([tableView isEqual:_categoryTableView])
    {
        return [_categoryArray count];
    }
    else if([tableView isEqual:_groupTableView])
    {
        return [_groupArray count];
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:_addRecordTableView]) {
        if (indexPath.section == 1) {
            return imageCellHeight;
        }
//        else if(indexPath.section == 3 && indexPath.row == 0)
//        {
//    //        DebugLog(@"AddRecord:heightForRowAtIndexPath : tokenFieldView.frame.size.height %f -------- %f ------------",tokenFieldView.frame.size.height,tagsListHeight);
//    //        if (tokenFieldView.frame.size.height > 100) {
//    //            return tokenFieldView.frame.size.height + 10;
//    //        }
//            return 100;
//        }
        return CELL_ROW_HEIGHT;
    }
    else if([tableView isEqual:_categoryTableView] || [tableView isEqual:_groupTableView])
    {
        return 40;
    }
    return 30;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     if ([tableView isEqual:_addRecordTableView]) {
         
         CGFloat cellHeight = CELL_ROW_HEIGHT;
         if (indexPath.section == 1) {
             cellHeight = imageCellHeight;
         }
         
         
         UIImageView *cellImageView;
         UIImageView *cellBackImageView;

        NSString *CellIdentifier = [NSString stringWithFormat:@"%d%d",indexPath.section,indexPath.row] ;
        UITableViewCell *cell= (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            
            cellBackImageView = [[UIImageView alloc] initWithFrame:CGRectMake(cell.frame.origin.x ,0, 60,cellHeight)];
            cellBackImageView.contentMode = UIViewContentModeScaleToFill;
            cellBackImageView.backgroundColor =  [UIColor clearColor];
            cellBackImageView.tag = 111;
//            cellBackImageView.layer.masksToBounds = YES;
//            cellBackImageView.clipsToBounds = YES;
//            cellBackImageView.layer.cornerRadius = 5.0;
            if (cellHeight == CELL_ROW_HEIGHT * 3) {
                cellBackImageView.image = [UIImage imageNamed:@"iconBg180.png"];
            }else if (cellHeight == CELL_ROW_HEIGHT * 2) {
                cellBackImageView.image = [UIImage imageNamed:@"iconBg120.png"];
            }else{
                cellBackImageView.image = [UIImage imageNamed:@"iconBg60.png"];
            }
            [cell.contentView addSubview:cellBackImageView];
            
            cellImageView = [[UIImageView alloc] initWithFrame:CGRectMake(cellBackImageView.frame.origin.x+10 ,10, 40, 40)];
            cellImageView.contentMode = UIViewContentModeScaleToFill;
            cellImageView.backgroundColor =  [UIColor clearColor];
            cellImageView.tag = 112;
            [cell.contentView addSubview:cellImageView];
            [cell.contentView insertSubview:cellImageView aboveSubview:cellBackImageView];
            
            switch (indexPath.section) {
                case 0:
                {
                    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cellBackImageView.frame)+10, 10.0f, 220.0f, 40.0f)];
                    textField.placeholder = @"Enter title";
                    [CommonCallback setTextFieldProperties:textField text:_selectedTitle];
                    textField.delegate = self;
                    [textField addTarget:self action:@selector(textChanged:) forControlEvents:UIControlEventEditingChanged];
                    textField.tag = TITLE_TAG;
                    [cell.contentView addSubview:textField];
                    cellImageView.image = [UIImage imageNamed:@"Title_Icon.png"];

                }
                    break;
                case 1:
                {
                    if(indexPath.row==0) {
                        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                        btn.tag = ADD_IMAGE_BTN_TAG;
                        [btn setFrame:CGRectMake(cell.frame.origin.x ,0, 60,CELL_ROW_HEIGHT)];
                        [btn setBackgroundColor:[UIColor clearColor]];
                        [btn addTarget:self action:@selector(showAddImageActionSheet) forControlEvents:UIControlEventTouchUpInside];
                        [cell.contentView addSubview:btn];
                        cellImageView.image = [UIImage imageNamed:@"Photo_Icon.png"];

//                        UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cellBackImageView.frame)+10, 10.0f, 220.0f, 40.0f)];
//                        [CommonCallback setTextFieldProperties:textField text:@"Add a photo"];
//                        textField.enabled = FALSE;
//                        textField.delegate =self;
//                        textField.tag = ADD_IMAGE_LABEL_TAG;
//                        [cell.contentView addSubview:textField];
                        
                        UIView *photosView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cellBackImageView.frame)+10, 0, 220, cellHeight)];
                        [photosView setBackgroundColor:[UIColor clearColor]];
                        photosView.tag = IMAGE_TAG;
                        [cell.contentView addSubview:photosView];
                        
                        double x = 0;
                        double y = 0;
                        double photowidth = 220 / 3.0;
                        double photoheight = 60;
                        UIImageView *gridImageView;
                        for (int index = 0; index < 9; index++) {
                            DebugLog(@"AddRecord:index %d x = %f y = %f photowidth %f photoheight %f",index,x,y,photowidth,photoheight);
                            gridImageView = [[UIImageView alloc] initWithFrame:CGRectMake(x,y,photowidth,photoheight)];
                            gridImageView.tag = [[NSString stringWithFormat:@"%d0%d",photosView.tag,index] intValue];
                            gridImageView.contentMode = UIViewContentModeScaleToFill;
                            gridImageView.backgroundColor =  [UIColor clearColor];
                            [photosView addSubview:gridImageView];
                            DebugLog(@"AddRecord:gridImageView.tag %d",gridImageView.tag);

                            if (index == 2 || index == 5) {
                                x = 0;
                            }else{
                                x += photowidth;
                            }
                            if (index == 2 || index == 5) {
                                y += photoheight;
                            }
                        }
                    }
                }
                    break;
                case 2:
                {
                    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cellBackImageView.frame)+10, 10.0f, 220.0f, 40.0f)];
                    [CommonCallback setTextFieldProperties:textField text:@"Add Notes"];
                    textField.enabled = FALSE;
                    textField.delegate =self;
                    textField.tag = CATEGORY_TAG;
                    [cell.contentView addSubview:textField];
//                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
                    cellImageView.image = [UIImage imageNamed:@"Notes_Icon.png"];
                }
                    break;
                case 3:
                {
                    if(indexPath.row==0) {
                        tokenFieldView = [[TITokenFieldView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cellBackImageView.frame), 5.0f, 220.0f, 50.0f)];
                        [tokenFieldView.tokenField setDelegate:self];
                        [tokenFieldView.tokenField setPromptText:@""];
                        [tokenFieldView.tokenField setTextColor:[UIColor blackColor]];
                        [tokenFieldViewInPage setBackgroundColor:TABLE_EDITCELL_BGCOLOR];
                        [tokenFieldView.tokenField setPlaceholder:@"Enter Tag Name"];
//                        [tokenFieldView.tokenField addTarget:self action:@selector(tokenFieldFrameDidChange:) forControlEvents:TITokenFieldControlEventFrameDidChange];
                        [tokenFieldView.tokenField addTarget:self action:@selector(tokenFieldFrameDidBegin:) forControlEvents:UIControlEventEditingDidBegin];
                        [tokenFieldView.tokenField setTokenizingCharacters:[NSCharacterSet characterSetWithCharactersInString:@",;."]];
                        tokenFieldView.tokenField.returnKeyType = UIReturnKeyDone;

                        tokenFieldView.tag = TAGS_TAG;
                        tokenFieldView.layer.cornerRadius = 2.0;
                        [cell.contentView addSubview:tokenFieldView];
                        cell.accessoryType = UITableViewCellAccessoryNone;
                        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
                        cellImageView.image = [UIImage imageNamed:@"Tag_Icon.png"];
                    }
                }
                    break;
                case 4:
                {
                    if(indexPath.row==0) {
                        UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cellBackImageView.frame)+10, 10.0f, 220.0f, 40.0f)];
                        [CommonCallback setTextFieldProperties:textField text:@"Add a group"];
                        textField.enabled = FALSE;
                        textField.delegate =self;
                        textField.tag = GROUPS_TAG;
                        [cell.contentView addSubview:textField];
                        cellImageView.image = [UIImage imageNamed:@"Group_Icon.png"];
                    }
                    else
                    {
                        UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(45.0f, 10.0f, 220.0f, 31.0f)];
                        DebugLog(@"when cell==nil, nextGroupIndex %d --------- _groupArray %@ ",nextGroupIndex,_groupArray);
                        if (_groupArray.count > 0 )
                        {
                           // GroupObject *tempObj = [self getGroupFromArray:nextGroupIndex];
                            GroupObject *tempObj = [_groupArray objectAtIndex:[self getGroupIndex:indexPath.row-1]];
                            if (tempObj!= nil) {
                                DebugLog(@"AddRecord:got groupName %@",tempObj.groupName);
                                [CommonCallback setTextFieldProperties:textField text:tempObj.groupName];
                     //           cell.imageView.image = [UIImage imageNamed:[_iconsDic valueForKey:[NSString stringWithFormat:@"%d",tempObj.iconId]]];
                                textField.tag = GROUPS_TAG + indexPath.row - 1;
                            }
                        }
                        [cell.contentView addSubview:textField];
                    }
                }
                    break;
                case 5:
                {
                    if(indexPath.row==0) {
                        UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cellBackImageView.frame)+10, 5.0f, 210.0f, 50.0f)];
                        [CommonCallback setTextViewProperties:textView text:_reverseAddress];
                        textView.delegate = self;
                        textView.tag = ADDRESS_TAG;
                        [cell.contentView addSubview:textView];

                        cellImageView.image = [UIImage imageNamed:@"Location_Icon.png"];

                        UIImageView *arrowImgView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(textView.frame)-6 ,15, 25, 30)];
                        arrowImgView.tag = 105;
                        arrowImgView.contentMode = UIViewContentModeScaleToFill;
                        arrowImgView.layer.masksToBounds = YES;
                        arrowImgView.image = [UIImage imageNamed:@"Mark.png"];
                        [cell.contentView addSubview:arrowImgView];

                    } else {
                        UILabel *geoLabel = [[UILabel alloc] initWithFrame:CGRectMake(45.0f, 10.0f,220.0f, 31.0f)];
                        geoLabel.text = [NSString stringWithFormat:@"%f,%f",_currentLatitude,_currentLongitude];
                        geoLabel.font = DEFAULT_FONT(14.0f);
                       // geoLabel.textAlignment = UITextAlignmentLeft;
                        geoLabel.textColor = [UIColor blackColor];
                        geoLabel.backgroundColor =  [UIColor clearColor];
                        geoLabel.tag = GEOPOINT_TAG;
                        [cell.contentView addSubview:geoLabel];
//                        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
//                        cell.imageView.image = [UIImage imageNamed:@"pin.png"];
                        cellImageView.image = [UIImage imageNamed:@"trans_icon.png"];
                    }
                }
                    break;
                    
                case 6:
                {
                        UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cellBackImageView.frame)+10, 10.0f, 220.0f, 40.0f)];
                        [CommonCallback setTextFieldProperties:textField text:_selectedDate];
                        textField.delegate =self;
                        textField.tag = DATE_TAG;
                        textField.enabled = FALSE;
                        [cell.contentView addSubview:textField];
                        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
                        cellImageView.image = [UIImage imageNamed:@"Calender_Icon.png"];

                }
                    break;
                case 7:
                {
                    
                    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cellBackImageView.frame)+10,  10.0f, 220.0f, 40.0f)];
                    [CommonCallback setTextFieldProperties:textField text:_selectedTime];
                    textField.delegate =self;
                    textField.enabled = FALSE;
                    textField.tag = TIME_TAG;
                    [cell.contentView addSubview:textField];
                    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
                    cellImageView.image = [UIImage imageNamed:@"Time_Icon.png"];
                }
                    break;
                default:
                    break;
            }
        }
        else
        {
            
            DebugLog(@"in else of cellForIndex : => sec %d row %d",indexPath.section,indexPath.row);
            switch (indexPath.section) {
                case 0:
                {
                    UITextField *titleTextField = (UITextField *) [cell viewWithTag:TITLE_TAG];
                    titleTextField.text = _selectedTitle;
                    
                }
                    break;
                case 1:
                {
                    if (cellHeight == CELL_ROW_HEIGHT * 3) {
                        cellBackImageView.image = [UIImage imageNamed:@"iconBg180.png"];
                    }else if (cellHeight == CELL_ROW_HEIGHT * 2) {
                        cellBackImageView.image = [UIImage imageNamed:@"iconBg120.png"];
                    }else{
                        cellBackImageView.image = [UIImage imageNamed:@"iconBg60.png"];
                    }
                    DebugLog(@"AddRecord:cellheight = %f",cellHeight);
                    cellBackImageView = (UIImageView *)[cell viewWithTag:111];
                    cellBackImageView.frame = CGRectMake(cell.frame.origin.x ,0, 60,cellHeight);
                    DebugLog(@"AddRecord:cellBackImageView.frame.size.height = %f",cellBackImageView.frame.size.height);

//                    UIView *photosView = (UIView *) [cell viewWithTag:IMAGE_TAG];
//                    int photocount = [self getPhotoCount];
//                    int imagetag;
//                     for (int index = 1; index <= photocount; index++) {
//                         imagetag =  [[NSString stringWithFormat:@"%d0%d",photosView.tag,index] intValue];
//                         UIImageView *gridImageView = (UIImageView *)[photosView viewWithTag:imagetag];
//                         int imageIndex = [self getImageIndex:index];
//                         ImageObject *imgObj = (ImageObject *)[_imagesArray objectAtIndex:imageIndex];
//                         UIImage *resizedImage = [CommonCallback resizeImage:[IO_APP_DELEGATE loadImage:imgObj.imagePath] newSize:CGSizeMake(60, 60)];
//                         gridImageView.image = resizedImage;
//                     }
                }
                    break;
                case 2:
                {
//                    UITextField *dateTextField = (UITextField *) [cell viewWithTag:CATEGORY_TAG];
//                    dateTextField.text = _selectedCategory;
                }
                    break;
                case 3:
                {
                    
                }
                    break;
                case 4:
                {
                    if (indexPath.row != 0 && _groupArray.count > 0 )
                        {
                            //GroupObject *tempObj = [self getGroupFromArray:nextGroupIndex];
                            GroupObject *tempObj = [_groupArray objectAtIndex:[self getGroupIndex:indexPath.row-1]];
                            if (tempObj!= nil) {
                                UITextField *textField = (UITextField *) [cell viewWithTag:GROUPS_TAG + indexPath.row - 1];
                                DebugLog(@"AddRecord:in else got groupName %@",tempObj.groupName);
                                [CommonCallback setTextFieldProperties:textField text:tempObj.groupName];
                        //        cell.imageView.image = [UIImage imageNamed:[_iconsDic valueForKey:[NSString stringWithFormat:@"%d",tempObj.iconId]]];
                                textField.tag = GROUPS_TAG + indexPath.row - 1;
                            }
                        }
                }
                    break;
                case 5:
                {
                    if(indexPath.row==0)
                    {
                        UITextView *textView = (UITextView *) [cell viewWithTag:ADDRESS_TAG];
                        textView.text = _reverseAddress;
                    } else
                    {
                        UILabel *geoLabel = (UILabel *)[cell viewWithTag:GEOPOINT_TAG];
                        geoLabel.text = [NSString stringWithFormat:@"%f,%f",_currentLatitude,_currentLongitude];
                    }
                }
                    break;
                    
                case 6:
                {
                    UITextField *dateTextField = (UITextField *) [cell viewWithTag:DATE_TAG];
                    dateTextField.text = _selectedDate;

                }
                    break;
                case 7:
                {
                    UITextField *timeTextField = (UITextField *) [cell viewWithTag:TIME_TAG];
                    timeTextField.text = _selectedTime;
                }
                    break;
                default:
                    break;
            }
        }
        cell.clipsToBounds = YES;
        cell.layer.masksToBounds = YES;

        cell.contentView.clipsToBounds = YES;
        cell.contentView.layer.masksToBounds = YES;

        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

        cell.backgroundColor = TABLE_EDITCELL_BGCOLOR;
        cell.layer.borderColor = TABLE_EDITCELL_BGCOLOR.CGColor;
        cell.layer.borderWidth = 0;
         
        return cell;
     }
     else if ([tableView isEqual:_categoryTableView])
     {
         NSString *cellIdentifier = @"categoryCell";
         UITableViewCell *cell= (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
         if (cell == nil)
         {
             cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
         }
         DebugLog(@"AddRecord:cetegoryCell %@",[_categoryArray objectAtIndex:indexPath.row]);
         cell.textLabel.text = [_categoryArray objectAtIndex:indexPath.row];
         cell.textLabel.font = DEFAULT_FONT(15.0);
         return cell;
     }
     else if([tableView isEqual:_groupTableView])
     {
         UIImageView *checkMarkImgView;

         NSString *cellIdentifier = @"groupRecordCell";
         UILabel *lblTitle;

         UITableViewCell *cell= (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
         if (cell == nil)
         {
             cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
             
             lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(10,2,150, 30)];
             lblTitle.tag = 101;
             lblTitle.font = DEFAULT_BOLD_FONT(15.0);
             lblTitle.textAlignment = UITextAlignmentLeft;
             lblTitle.textColor = [UIColor whiteColor];
             lblTitle.backgroundColor =  [UIColor clearColor];
             [cell.contentView addSubview:lblTitle];
             
//             cell.textLabel.highlightedTextColor = TABLE_CELL_BGCOLOR;
//             cell.textLabel.font = [UIFont boldSystemFontOfSize:15.0];
//             cell.textLabel.textColor = [UIColor whiteColor];
             
             cell.selectionStyle = UITableViewCellSelectionStyleBlue;
             cell.contentView.backgroundColor = TABLE_CELL_BGCOLOR;
             cell.layer.borderColor = TABLE_CELL_BGCOLOR.CGColor;
             cell.layer.borderWidth = 0;
             
             checkMarkImgView = [[UIImageView alloc] initWithFrame:CGRectMake(250,10, 20, 20)];
             checkMarkImgView.tag = 102;
             checkMarkImgView.contentMode = UIViewContentModeScaleToFill;
             checkMarkImgView.layer.masksToBounds = YES;
             checkMarkImgView.image = [UIImage imageNamed:@"check_mark_white.png"];
             [cell.contentView addSubview:checkMarkImgView];
             
//             UIView *bgColorView = [[UIView alloc] init];
//             bgColorView.backgroundColor = TABLE_EDITCELL_BGCOLOR;
//             bgColorView.layer.borderColor = [UIColor clearColor].CGColor;
//             cell.selectedBackgroundView = bgColorView;
             
         }
        if (_groupArray != nil && _groupArray.count > 0) {
            
             GroupObject *recObj = (GroupObject *)[_groupArray objectAtIndex:indexPath.row];
             DebugLog(@"AddRecord:groupCell %@",recObj.groupName);
             cell.tag = recObj.groupRowId;
             lblTitle = (UILabel *)[cell viewWithTag:101];
             lblTitle.text = [recObj.groupName capitalizedString];
             checkMarkImgView = (UIImageView *)[cell viewWithTag:102];
            
             if(recObj.selected)
             {
                 checkMarkImgView.image = [UIImage imageNamed:@"check_mark_blue.png"];
                 cell.contentView.backgroundColor = TABLE_EDITCELL_BGCOLOR;
                 lblTitle.textColor = TABLE_CELL_BGCOLOR;
             }
             else
             {
                 checkMarkImgView.image = nil;
                 cell.contentView.backgroundColor = TABLE_CELL_BGCOLOR;
                 lblTitle.textColor = [UIColor whiteColor];
             }
            
            cell.accessoryView = nil;
          }
         return cell;
     }
    return nil;
}
#pragma mark Tableview DataSource methods
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    UILabel *lblTitle = (UILabel *)[cell viewWithTag:101];
    UIImageView *checkMarkImgView = (UIImageView *)[cell viewWithTag:102];
    GroupObject *recObj = (GroupObject *)[_groupArray objectAtIndex:indexPath.row];
    if(recObj.selected)
    {
        checkMarkImgView.image = [UIImage imageNamed:@"check_mark_blue.png"];
        cell.contentView.backgroundColor = TABLE_EDITCELL_BGCOLOR;
        lblTitle.textColor = TABLE_CELL_BGCOLOR;
    }
    else
    {
        checkMarkImgView.image = nil;
        cell.contentView.backgroundColor = TABLE_CELL_BGCOLOR;
        lblTitle.textColor = [UIColor whiteColor];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     if ([tableView isEqual:_addRecordTableView])
     {
        [_addRecordTableView deselectRowAtIndexPath:indexPath animated:YES];
        _selectedIndexPath= [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
        switch (indexPath.section) {
            case 0:
            {
                DebugLog(@"AddRecord:title");
            }
                break;
            case 1:
            {
//                if(indexPath.row==0) {
//                    [self showAddImageActionSheet];
//                }
//                else
//                {
                int photocount = [self getPhotoCount];
                if (photocount > 0) {
                    [self openGalleryBtnClicked:nil];
                }
//                }
            }
                break;
            case 2:
            {
                DebugLog(@"AddRecord:category");
                
//                if (_categoryArray.count > 0)
//                {
//                    DebugLog(@"AddRecord:category.count > 0");
//                    CURRENTCASE = CATEGORY_TAG;
//                    [self showPicker:CATEGORY_TAG];
//                }
//                else
//                {
//                    DebugLog(@"AddRecord:category.count < 0");
//                }
                [self openNotesBtnClicked:nil];
            }
                break;
            case 3:
            {
                DebugLog(@"AddRecord:tag");
    //            IOAddGroupTagViewController *addGroupViewController = [[IOAddGroupTagViewController alloc] initWithNibName:@"IOAddGroupTagViewController" bundle:nil];
    //            addGroupViewController.imgId = _currentImgRowId;
    //            addGroupViewController.curretCase = TAGCASE;
    //            [self.navigationController pushViewController:addGroupViewController animated:YES];

            }
                break;
            case 4:
            {
                DebugLog(@"AddRecord:group");
                IOAddGroupTagViewController *addGroupViewController = [[IOAddGroupTagViewController alloc] initWithNibName:@"IOAddGroupTagViewController" bundle:nil];
                addGroupViewController.imgId = _currentImgRowId;
                addGroupViewController.curretCase = GROUPCASE;
                addGroupViewController.iconsDic = _iconsDic;
                [self.navigationController pushViewController:addGroupViewController animated:YES];
            }
                break;
            case 5:
            {
                if(indexPath.row==0)
                {
                    DebugLog(@"Location");
                    DetailMapViewController *detailviewController = [[DetailMapViewController alloc] initWithNibName:@"DetailMapViewController" bundle:nil];
                    detailviewController.geoPointLatitude = _currentLatitude;
                    detailviewController.geoPointLongitude = _currentLongitude;
                    detailviewController.reverseAddress = _reverseAddress;
                    [self.navigationController pushViewController:detailviewController animated:YES];
                }
            }
                break;
                
            case 6:
            {
                DebugLog(@"Date");
                CURRENTCASE = DATE_TAG;
                [self showPicker:DATE_TAG];

            }
                break;
            case 7:
            {
                DebugLog(@"Time");
                CURRENTCASE = TIME_TAG;
                [self showPicker:TIME_TAG];
            }
                break;
            default:
                break;
        }
     }
     else if ([tableView isEqual:_categoryTableView]) {
         DebugLog(@"AddRecord:You selected Category as %@",[_categoryArray objectAtIndex:indexPath.row]);
         _selectedCategory = [_categoryArray objectAtIndex:indexPath.row];
         _categoryTextField.text = _selectedCategory;
         [_pickerView selectRow:indexPath.row inComponent:0 animated:YES];
         CURRENTCASE = CATEGORY_TAG;
         [self doneFilterBtnClicked:nil];
     }
     else if([tableView isEqual:_groupTableView])
     {
         UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
         UILabel *lblTitle = (UILabel *)[cell viewWithTag:101];
         UIImageView *checkMarkImgView = (UIImageView *)[cell viewWithTag:102];
         GroupObject *recObj = (GroupObject *)[_groupArray objectAtIndex:indexPath.row];
         if(recObj.selected)
         {
             recObj.selected = 0;
             [self updateInGroupTemp:cell.tag selected:0];
             checkMarkImgView.image = nil;
             cell.contentView.backgroundColor = TABLE_CELL_BGCOLOR;
             lblTitle.textColor = [UIColor whiteColor];
         }
         else
         {
             recObj.selected = 1;
             [self updateInGroupTemp:cell.tag selected:1];
             checkMarkImgView.image = [UIImage imageNamed:@"check_mark_blue.png"];
             cell.contentView.backgroundColor = TABLE_EDITCELL_BGCOLOR;
             lblTitle.textColor = TABLE_CELL_BGCOLOR;
         }
         [_groupArray replaceObjectAtIndex:indexPath.row withObject:recObj];
         recObj = nil;
     }
}


#pragma UITextField delegate callback methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
   	return YES;
}

-(IBAction)textChanged:(UITextField *)textField
{
    DebugLog(@"AddRecord:textChanged %@",textField.text);
    _selectedTitle = textField.text;
    if (_segmentedControl.selectedSegmentIndex == 0) {
        UITableViewCell *cell = [_addRecordTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        UITextField *tittleTextField = (UITextField *)[cell viewWithTag:TITLE_TAG];
        tittleTextField.text = _selectedTitle;
    }
    else
    {
        _titleTextField.text = _selectedTitle;
    }
}

//- (void)textFieldDidEndEditing:(UITextField *)textField
//{
//    _selectedTitle = textField.text;
//}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if([textField isEqual:_dateTextField])
    {
        DebugLog(@"Date");
        CURRENTCASE = DATE_TAG;
        [self showPicker:DATE_TAG];
        [textField resignFirstResponder];
    }
    else if([textField isEqual:_timeTextField])
    {
        DebugLog(@"Time");
        CURRENTCASE = TIME_TAG;
        [self showPicker:TIME_TAG];
        [textField resignFirstResponder];
    }
    else if([textField isEqual:_geopointsTextField])
    {
        DebugLog(@"GeoPoint");
        [textField resignFirstResponder];
        DetailMapViewController *detailviewController = [[DetailMapViewController alloc] initWithNibName:@"DetailMapViewController" bundle:nil];
        detailviewController.geoPointLatitude = _currentLatitude;
        detailviewController.geoPointLongitude = _currentLongitude;
        detailviewController.reverseAddress = _reverseAddress;
        [self.navigationController pushViewController:detailviewController animated:YES];
    }else{
        
    }
}

#pragma mark textView Delegate Methods
- (void)textViewDidBeginEditing:(UITextView *)txtView
{
    if (txtView == notesTextView) {
//        _openNotesBtn.frame = CGRectMake(0,100, _openNotesBtn.frame.size.width,_openNotesBtn.frame.size.height);
    }
    else
    {
//        CGPoint convertedPoint = [txtView convertPoint:txtView.frame.origin toView:[self.view window]];
//        if(txtView.tag==ADDRESS_TAG && convertedPoint.y > 450)
//            [_addRecordTableView setContentOffset:CGPointMake(0,convertedPoint.y - 100) animated:YES];
//        else if(txtView.tag==ADDRESS_TAG && convertedPoint.y > 200)
//            [_addRecordTableView setContentOffset:CGPointMake(0,600) animated:YES];
//        DebugLog(@"AddRecord:%f %f",txtView.frame.origin.y,convertedPoint.y);
        
       if(txtView.tag==ADDRESS_TAG)
           [_addRecordTableView setContentOffset:CGPointMake(0,350) animated:YES];

        if ([txtView.text hasPrefix:@""])
        {
            [self setPlaceholder:txtView];
        }
        else if([txtView.text hasPrefix:LOCATION_PLACEHOLDER1]|| [_reverseAddress isEqualToString:LOCATION_PLACEHOLDER2])
        {
            txtView.text = @"";
            txtView.textColor = [UIColor blackColor];
        }
    }
}
- (void)textViewDidChange:(UITextView *)textView
{
    UITableViewCell *cell = (UITableViewCell *)[_addRecordTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:5]];
    UITextView *cellTextView = (UITextView *)[cell viewWithTag:ADDRESS_TAG];
    if (textView == _addressTextView || textView == cellTextView)
    {
        if (textView.text.length != 0)
        {
            _reverseAddress = textView.text;
        }
    }
    if (_segmentedControl.selectedSegmentIndex == 0) {
        
        cellTextView.text = _reverseAddress;
    }
    else
    {
        _addressTextView.text = _reverseAddress;
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    UITableViewCell *cell = (UITableViewCell *)[_addRecordTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:5]];
    UITextView *cellTextView = (UITextView *)[cell viewWithTag:ADDRESS_TAG];
    if (textView == _addressTextView || textView == cellTextView)
    {
        if (textView.text.length == 0)
        {
            [self setPlaceholder:_addressTextView];
            [self setPlaceholder:cellTextView];
            _reverseAddress = LOCATION_PLACEHOLDER2;
        }
    }
    if (_segmentedControl.selectedSegmentIndex == 0) {
        cellTextView.text = _reverseAddress;
    }
    else
    {
        _addressTextView.text = _reverseAddress;
    }
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([textView isEqual:_addressTextView] && [text isEqualToString:@"\n"])
    {
        [self.view endEditing:YES];
    }
    return YES;
}


#pragma mark actionSheet Delegate Methods
-(void)actionSheet:(UIActionSheet *)localactionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    DebugLog(@"AddRecord:%d",buttonIndex);
    
    if ([localactionSheet.title isEqualToString:ADDIMAGE_ACTIONSHEET_TITLE])
    {
        if (buttonIndex == [localactionSheet numberOfButtons]-1)
            return;
        
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && [[localactionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Take Photo"])
        {
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        }
        else if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum] && [[localactionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Choose Existing Photo"])
        {
            imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
        imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString *) kUTTypeImage,nil];
        imagePicker.allowsEditing = NO;
        [self presentModalViewController:imagePicker animated:YES];
    }
}
#pragma mark Picker Data Source Methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [_categoryArray count];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return self.view.frame.size.width;
}

#pragma mark Picker Delegate Methods

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    DebugLog(@"AddRecord:titleForRow");
    return [_categoryArray objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)thePickerView  didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
}

#pragma mark -
#pragma mark UIImagePickerController Delegate Methods
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage])
    {
        UIImage *originalImage = [info objectForKey:UIImagePickerControllerOriginalImage];
        DebugLog(@"originalImage %@",originalImage);
        if (originalImage != nil)
        {
            originalImage = [originalImage fixOrientation];
            
            ImageObject *imgObj = [[ImageObject alloc] init];
            imgObj.imagePath = [IO_APP_DELEGATE saveImage:originalImage];
            imgObj.isNew = YES;
            imgObj.isDeleted = NO;
            [_imagesArray addObject:imgObj];
                        
            ImageFilteringViewController *filterViewController = [[ImageFilteringViewController alloc] initWithNibName:@"ImageFilteringViewController" bundle:nil];
            filterViewController.imageObjectToFilter = [imgObj mutableCopy];
            filterViewController.firsttime = YES;
           // UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:filterViewController];
           // [self.navigationController presentModalViewController:navBar animated:YES];
          //  [self.navigationController pushViewController:filterViewController animated:YES];

            [UIView  transitionWithView:self.navigationController.view duration:0.8  options:UIViewAnimationOptionTransitionFlipFromLeft
                             animations:^(void) {
                                 BOOL oldState = [UIView areAnimationsEnabled];
                                 [UIView setAnimationsEnabled:NO];
                                 [self.navigationController pushViewController:filterViewController animated:YES];
                                 [UIView setAnimationsEnabled:oldState];
                             }
                             completion:nil];
            imgObj = nil;
        }
        [self dismissModalViewControllerAnimated:YES];
    }
    else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie])
    {
		// Code here to support video if enabled
	}
}

-(void)image:(UIImage *)image finishedSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message: @"Failed to save image" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

#pragma UIButton delegate callback methods
- (IBAction)openGroupIconGridBtnClicked:(id)sender {
    DebugLog(@"AddRecord:openGroupIconGridBtnClicked");
    if (_groupImagesScrollView.hidden) {
        [_groupImagesScrollView setHidden:NO];
    }
    else
    {
        [_groupImagesScrollView setHidden:YES];
    }
}

- (IBAction)openNotesBtnClicked:(id)sender
{
//    [pagedSegmentedControl setHidden:YES];
    [_openNotesBtn setHidden:YES];
    [_notesView setHidden:NO];
    
    if (!_openNotesBtn.isSelected) {
        [UIView beginAnimations:@"open" context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        _notesView.frame = CGRectMake(0,0, _notesView.frame.size.width,_notesView.frame.size.height);
        [UIView commitAnimations];
    }
    else
    {
        [self.view endEditing:YES];
//        if (_segmentedControl.selectedSegmentIndex == 0) {
//          [pagedSegmentedControl setHidden:NO];
//        }
        [UIView beginAnimations:@"close" context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
        [self.navigationController setNavigationBarHidden:NO animated:YES];
        _notesView.frame = CGRectMake(_notesView.frame.origin.x,_notesView.frame.origin.y+self.view.frame.size.height, _notesView.frame.size.width,_notesView.frame.size.height);

        [UIView commitAnimations];
    }
}

- (void)transitionDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    [_openNotesBtn setHidden:NO];
    if ([animationID isEqualToString:@"open"])
    {
        _openNotesBtn.frame = CGRectMake(_openNotesBtn.frame.origin.x,0, _openNotesBtn.frame.size.width,_openNotesBtn.frame.size.height);
        [_openNotesBtn setTitle:@"Close" forState:UIControlStateNormal];
        [_openNotesBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_openNotesBtn setBackgroundImage:[UIImage imageNamed:@"CloseNotes.png"] forState:UIControlStateNormal];
        _openNotesBtn.selected = TRUE;
        [_openNotesBtn.layer setShadowOffset:CGSizeMake(0, 3)];
    }
    else
    {
        _openNotesBtn.frame = CGRectMake(_openNotesBtn.frame.origin.x,self.view.frame.size.height-_openNotesBtn.frame.size.height, _openNotesBtn.frame.size.width,_openNotesBtn.frame.size.height);
        [_openNotesBtn setTitle:@"Note" forState:UIControlStateNormal];
        [_openNotesBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_openNotesBtn setBackgroundImage:[UIImage imageNamed:@"AddNotes.png"] forState:UIControlStateNormal];
        _openNotesBtn.selected = FALSE;
        [_openNotesBtn.layer setShadowOffset:CGSizeMake(0, -3)];

        if(_segmentedControl.selectedSegmentIndex == 1)
        {
            [_openNotesBtn setHidden:YES];
        }
        [_notesView setHidden:YES];
    }
}

- (void)groupIconBtnClicked:(id)sender {
    DebugLog(@"AddRecord:groupIconBtnClicked");
    UIButton *btn = (UIButton *)sender;
    DebugLog(@"AddRecord:You Clicked %d %@",btn.tag,[_iconsDic valueForKey:[NSString stringWithFormat:@"%d",btn.tag]]);
    _groupImageView.image   = [UIImage imageNamed:[_iconsDic valueForKey:[NSString stringWithFormat:@"%d",btn.tag]]];
    _groupImageView.tag     = btn.tag;
    [self openGroupIconGridBtnClicked:nil];
}

- (IBAction)addImageBtnClicked:(id)sender {
    [self showAddImageActionSheet];
}

- (IBAction)openGalleryBtnClicked:(id)sender {
    DebugLog(@"AddRecord:openGalleryBtnClicked \nimageArray = %@",_imagesArray);
//    IOGalleryViewController *galleryViewController = [[IOGalleryViewController alloc] initWithNibName:@"IOGalleryViewController" bundle:nil];
//    galleryViewController.imageObjsArray = _imagesArray;
//    [self.navigationController pushViewController:galleryViewController animated:YES];
    
    NSMutableArray *photos = [[NSMutableArray alloc] init];
    MWPhoto *photo;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    for(int i = 0; i < [_imagesArray count]; i++)
    {
        ImageObject *tempImageObj = (ImageObject *)[_imagesArray objectAtIndex:i];
       // photo = [MWPhoto photoWithFilePath:[[NSBundle mainBundle] pathForResource:@"notes" ofType:@"jpg"]];
        photo = [MWPhoto photoWithFilePath:[documentsDirectory stringByAppendingPathComponent:tempImageObj.imagePath]];
        photo.isNew = tempImageObj.isNew;
        photo.isDeleted = tempImageObj.isDeleted;
        [photos addObject:photo];
    }
    
    self.photos = photos;
    kNumberOfPages =  [self getPhotoCount];
    
    // Create browser
	MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayActionButton = YES;
    browser.wantsFullScreenLayout = YES;
    [browser setInitialPageIndex:0];
    [self.navigationController pushViewController:browser animated:YES];
}

- (IBAction)hideKeyboardBtnClicked:(id)sender {
    [notesTextView resignFirstResponder];
//    _openNotesBtn.frame = CGRectMake(0,(self.view.frame.size.height/2)+40, _openNotesBtn.frame.size.width,_openNotesBtn.frame.size.height);
}

- (IBAction)createGroupBtnClicked:(id)sender {
    NSString *groupName = _groupTextField.text;
    if ([groupName isEqualToString:@""]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                            message:@"Enter group name"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
    else
    {
        if ([self getGroupId:groupName] != -1) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                message:[NSString stringWithFormat:@"%@ group already exists",groupName]
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            [alertView show];
        }
        else
        {
            [self insertIntoGroups:groupName iconid:_groupImageView.tag];
        }
    }
}

#pragma sqlite methods
-(void)readRowFromDatabase
{
    // IMAGE_MASTER(ID INTEGER PRIMARY KEY AUTOINCREMENT,TITLE TEXT, CATEGORY_ID INTEGER,LOCATION TEXT,DATE REAL,FOREIGN KEY(CATEGORY_ID)
    //CREATE TABLE IF NOT EXISTS TAGS(ID INTEGER PRIMARY KEY AUTOINCREMENT,TAG_NAME TEXT)
    //ID INTEGER PRIMARY KEY AUTOINCREMENT,GROUP_NAME TEXT,ICON_ID INTEGER,FOREIGN KEY(ICON_ID) REFERENCES ICONS(ID)
    int catgId = 0;
    double dateDouble = 0;
    NSString *notes = @"";
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK)
    {
        NSString *queryString = [NSString stringWithFormat:@"SELECT TITLE,NOTES,CATEGORY_ID,LOCATION,LATITUDE,LONGITUDE,DATE FROM IMAGE_MASTER WHERE ID = %d",_currentImgRowId];
        const char *queryCharP = [queryString UTF8String];
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            if (sqlite3_step(stmt) == SQLITE_ROW) {
                _selectedTitle = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(stmt, 0)];
                notes = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(stmt, 1)];
                 catgId = sqlite3_column_int(stmt, 2);
                _reverseAddress= [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(stmt, 3)];
                _currentLatitude =  sqlite3_column_double(stmt, 4);
                _currentLongitude =  sqlite3_column_double(stmt, 5);
                dateDouble = sqlite3_column_double(stmt, 6);
                
                if (_reverseAddress == nil || [_reverseAddress isEqualToString:@""]) {
                    _reverseAddress = LOCATION_PLACEHOLDER2;
                }
            }
        }
        sqlite3_finalize(stmt);
    }
    DebugLog(@"AddRecord:date %f notes %@",dateDouble,notes);
    _selectedCategory   = [self getCategoryName:catgId];
    _datetimePickerView.date = [NSDate dateWithTimeIntervalSinceReferenceDate:dateDouble];
    notesTextView.text = notes;
    [notesTextView setNeedsDisplay];
    [self dateChanged:nil];
}
-(void)readImagesFromTable
{
    DebugLog(@"AddRecord:readImagesFromTable");
    
    if (_imagesArray == nil) {
        _imagesArray = [[NSMutableArray alloc] init];
    }
    else
    {
        [_imagesArray removeAllObjects];
    }
    ImageObject *imgObj = nil;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
        NSString *queryString = [NSString stringWithFormat:@"SELECT IMAGE_PATH FROM IMAGE_SET_RELATION WHERE IMAGE_MASTER_ID = %d",_currentImgRowId];
        DebugLog(@"AddRecord:------%@-----",queryString);
        const char *queryCharP = [queryString UTF8String];
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            while (sqlite3_step(stmt) == SQLITE_ROW)
            {
                NSString *path = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 0)];
                DebugLog(@"AddRecord:path %@ ",path);
                
                imgObj = [[ImageObject alloc] init];
                imgObj.imagePath = path;
                imgObj.isNew = NO;
                imgObj.isDeleted = NO;
                [_imagesArray addObject:imgObj];
                path = nil;
                imgObj = nil;
            }
        }
        sqlite3_finalize(stmt);
        sqlite3_close(ioDB);
    }
}
-(void)reloadFilteredImage:(ImageObject *)imgObj
{
    DebugLog(@"AddRecord:reloadFilteredImage");
    ImageObject *imgTempObj = nil;
    for(int arrIndex = 0; arrIndex < _imagesArray.count ; arrIndex++ )
    {
        imgTempObj = (ImageObject *)[_imagesArray objectAtIndex:arrIndex];
        DebugLog(@"AddRecord:imagePath %@",imgTempObj.imagePath);
        if ([imgTempObj.imagePath isEqualToString:imgObj.imagePath])
        {
            DebugLog(@"AddRecord:replacing objectat index = %d",arrIndex);
            [_imagesArray replaceObjectAtIndex:arrIndex withObject:imgObj];
            imgTempObj = nil;
            break;
        }
        imgTempObj = nil;
    }
    [self reloadImageCell];
}
-(void)reloadImageCell
{
    DebugLog(@"AddRecord:_imageObjectsArray : %@",_imagesArray);
    DebugLog(@"AddRecord:1) reloadImageCell");
    int photocount = [self getPhotoCount];
    DebugLog(@"AddRecord:2) getPhotoCount %d",photocount);
    if (photocount == 0 || (photocount >= 1 && photocount <= 3))
    {
        imageCellHeight = CELL_ROW_HEIGHT;
    }
    else if (photocount >= 4 && photocount <= 6)
    {
        imageCellHeight = CELL_ROW_HEIGHT * 2;
    }
    else if (photocount >= 7 && photocount <= 9)
    {
        imageCellHeight = CELL_ROW_HEIGHT * 3;
    }
    [_addRecordTableView beginUpdates];
    [_addRecordTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:1]] withRowAnimation:UITableViewRowAnimationAutomatic];
    [_addRecordTableView endUpdates];
    
    UITableViewCell *cell = (UITableViewCell *)[_addRecordTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    UIView *photosView = (UIView *) [cell viewWithTag:IMAGE_TAG];
    int imagetag;
    UIImageView *gridImageView;
    ImageObject *imgObj;
    UIImage *originalimage;
    UIImage *resizedImage;
    int imageIndex;
    for (int index = 0; index < photocount; index++) {
        
        imageIndex = [self getImageIndex:index];
        DebugLog(@"AddRecord:imageIndex %d",imageIndex);
        imgObj = (ImageObject *)[_imagesArray objectAtIndex:imageIndex];
        originalimage = [IO_APP_DELEGATE loadImage:imgObj.imagePath];
        resizedImage = [CommonCallback resizeImage:originalimage newSize:CGSizeMake(60, 60)];
        
        imagetag =  [[NSString stringWithFormat:@"%d0%d",photosView.tag,index] intValue];
        gridImageView = (UIImageView *)[photosView viewWithTag:imagetag];
        gridImageView.image = resizedImage;
        
        if (index == 0) {
            _imageView.image = [CommonCallback resizeImage:originalimage newSize:CGSizeMake(280, 176)];
        }
        
        imgObj = nil;
        originalimage = nil;
        resizedImage = nil;
    }
    
    //set image = nil
    for (int index = photocount; index < 9; index++) {
        imagetag =  [[NSString stringWithFormat:@"%d0%d",photosView.tag,index] intValue];
        gridImageView = (UIImageView *)[photosView viewWithTag:imagetag];
        gridImageView.image = nil;
    }
    [self showGalleryBtn];
    DebugLog(@"AddRecord:done with : reloadImageCell ");
}
-(void)reloadGroups
{
    DebugLog(@"AddRecord:_groupArray : %@",_groupArray);
    DebugLog(@"AddRecord:1) readGroupsFromGroupsTempTable");
    [self readGroupsFromGroupsTempTable];
    DebugLog(@"AddRecord:2) reloadGroupsCells");
//    [self reloadGroupsCells];
    [_groupTableView reloadData];
}
-(void)readGroupsFromGroupsTempTable
{
    
    if (_groupArray == nil) {
        _groupArray = [[NSMutableArray alloc] init];
    }
    else
    {
        [_groupArray removeAllObjects];
    }
    GroupObject *groupObj = nil;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
        NSString *queryString = [NSString stringWithFormat:@"SELECT GROUP_ID,GROUP_NAME,ICON_ID,SELECTED FROM GROUPS_TEMP"];
        const char *queryCharP = [queryString UTF8String];
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            while (sqlite3_step(stmt) == SQLITE_ROW)
            {
                int rowId = sqlite3_column_int(stmt, 0);
                NSString *name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 1)];
                int iconId = sqlite3_column_int(stmt, 2);
                int selected = sqlite3_column_int(stmt, 3);
                
                groupObj = [[GroupObject alloc]init];
                groupObj.groupRowId = rowId;
                groupObj.groupName = name;
                groupObj.iconId = iconId;
                groupObj.selected = selected;
                DebugLog(@"AddRecord:group Name : %@ selected %d",name,selected);
                [_groupArray addObject:groupObj];
                
                groupObj = nil;
            }
        }
        sqlite3_finalize(stmt);
        sqlite3_close(ioDB);
    }
}
-(void)reloadGroupsCells
{
    DebugLog(@"-------------------------- _groupArray  %@--------------------------",_groupArray);
    
    int NumberOfSelectedGroups = [self getSelectedGroupCountFromArray];
    NSInteger numberOfRowsINSection = [_addRecordTableView numberOfRowsInSection:4];
    DebugLog(@"number of rows %d",numberOfRowsINSection);
    int index = 1;
    nextGroupIndex = 0;
    if (numberOfRowsINSection - 1  >= NumberOfSelectedGroups) {
        for (index=1; index<= NumberOfSelectedGroups; index++)
        {
            DebugLog(@"AddRecord:nextGroupIndex %d",nextGroupIndex);
            UITableViewCell *cell = [_addRecordTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:4]];
            UITextField *textField = (UITextField *)[cell viewWithTag:GROUPS_TAG + index - 1];
            DebugLog(@"index %d GROUPS_TAG %d",index,GROUPS_TAG + index - 1);
            //GroupObject *tempObj = [self getGroupFromArray:nextGroupIndex];
            GroupObject *tempObj = [_groupArray objectAtIndex:[self getGroupIndex:index-1]];
            DebugLog(@"groupName %@",tempObj.groupName);
            textField.text = tempObj.groupName;
       //     cell.imageView.image = [UIImage imageNamed:[_iconsDic valueForKey:[NSString stringWithFormat:@"%d",tempObj.iconId]]];
        }
        DebugLog(@"now index for delete row %d", index);
        for (int start = numberOfRowsINSection - 1; start >= index; start--)
        {
            DebugLog(@"delete row %d ======== ", start);
            [_addRecordTableView beginUpdates];
            [_addRecordTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:start inSection:4]] withRowAnimation:UITableViewRowAnimationAutomatic];
            numberOfGroups = start;
            DebugLog(@"AddRecord:delete : numberOfGroups %d",numberOfGroups);
            [_addRecordTableView endUpdates];
        }
    }
    else
    {
        for (index=1; index < numberOfRowsINSection; index++)
        {
            DebugLog(@"AddRecord:nextGroupIndex %d",nextGroupIndex);
            UITableViewCell *cell = [_addRecordTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:4]];
            UITextField *textField = (UITextField *)[cell viewWithTag:GROUPS_TAG + index - 1];
            DebugLog(@"index %d GROUPS_TAG %d",index,GROUPS_TAG + index - 1);
           // GroupObject *tempObj = [self getGroupFromArray:nextGroupIndex];
            GroupObject *tempObj = [_groupArray objectAtIndex:[self getGroupIndex:index-1]];
            DebugLog(@"groupName %@",tempObj.groupName);
            textField.text = tempObj.groupName;
         //   cell.imageView.image = [UIImage imageNamed:[_iconsDic valueForKey:[NSString stringWithFormat:@"%d",tempObj.iconId]]];
        }
        DebugLog(@"now index for new row %d", index);
        for (int start = index; start <= NumberOfSelectedGroups; start++)
        {
            DebugLog(@"add new row %d===============", start);
            DebugLog(@"AddRecord:nextGroupIndex %d",nextGroupIndex);
            [_addRecordTableView beginUpdates];
            [_addRecordTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:start inSection:4]] withRowAnimation:UITableViewRowAnimationAutomatic];
            numberOfGroups = start + 1;
            DebugLog(@"AddRecord:insert : numberOfGroups %d",numberOfGroups);
            [_addRecordTableView endUpdates];
        }
    }
    nextGroupIndex = 0;
    [_addRecordTableView reloadData];
}
-(GroupObject *)getGroupFromArray:(int)StartIndex
{
    nextGroupIndex = StartIndex;
    DebugLog(@"AddRecord:1)getGroupFromArray: start nextGroupIndex %d",nextGroupIndex);
    GroupObject *recObj;
    for (int index = StartIndex; index < _groupArray.count; index++) {
        recObj = (GroupObject *)[_groupArray objectAtIndex:index];
        if (recObj.selected == 1) {
            nextGroupIndex = ++index;
            DebugLog(@"AddRecord:2)getGroupFromArray: got next nextGroupIndex %d",nextGroupIndex);
            return recObj;
            break;
        }
    }
    return nil;
}
-(int)getGroupIndex:(int)currentIndex
{
    int groupIndex = 0;
    GroupObject *recObj = nil;
    for (int i = 0; i < _groupArray.count; i++) {
        recObj = (GroupObject *)[_groupArray objectAtIndex:i];
        if (recObj.selected) {
            {
                if (groupIndex == currentIndex) {
                    groupIndex = i;
                    break;
                }
                groupIndex ++;
            }
        }
    }
    DebugLog(@"AddRecord:groupIndex %d",groupIndex);
    return groupIndex;
}
// Tags Table
-(void)readSelectedTagsFromImageTagRelationTable
{
    DebugLog(@"AddRecord:1) readSelectedTagsFromImageTagRelationTable");
    
    if (_tagObjectsArray == nil) {
        _tagObjectsArray = [[NSMutableArray alloc] init];
    }
    else
    {
        [_tagObjectsArray removeAllObjects];
    }
    TagObject *tagObj = nil;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
        NSString *queryString = [NSString stringWithFormat:@"SELECT ID,TAG_NAME FROM TAGS WHERE ID IN (SELECT TAG_ID FROM IMAGE_TAG_RELATION WHERE IMAGE_MASTER_ID = %d)",_currentImgRowId];
        const char *queryCharP = [queryString UTF8String];
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            while (sqlite3_step(stmt) == SQLITE_ROW)
            {
                int rowId = sqlite3_column_int(stmt, 0);
                NSString *name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 1)];
                
                tagObj = [[TagObject alloc]init];
                tagObj.tagRowId = rowId;
                tagObj.tagName = name;
                tagObj.selected = 1;
                [_tagObjectsArray addObject:tagObj];
                
                tagObj = nil;
            }
        }
        sqlite3_finalize(stmt);
        sqlite3_close(ioDB);
    }
}
-(void)readTagsFromTable
{
    if (_tagsArray == nil) {
        _tagsArray = [[NSMutableArray alloc] init];
    }
    else
    {
        [_tagsArray removeAllObjects];
    }
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
        NSString *queryString = [NSString stringWithFormat:@"SELECT TAG_NAME FROM TAGS WHERE ID IN (SELECT TAG_ID FROM IMAGE_TAG_RELATION WHERE IMAGE_MASTER_ID = %d)",_currentImgRowId];
        const char *queryCharP = [queryString UTF8String];
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            while (sqlite3_step(stmt) == SQLITE_ROW)
            {
                NSString *name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 0)];
                [_tagsArray addObject:name];
            }
        }
        sqlite3_finalize(stmt);
        sqlite3_close(ioDB);
    }
}
-(void)reloadTags
{
    DebugLog(@"AddRecord:1) readTagsFromTagsTempTable");
    [self readTagsFromTagsTempTable];
    DebugLog(@"AddRecord:2) reloadTagsCells");
    [self reloadTagsCells];
}
-(void)readTagsFromTagsTempTable
{
    if (_tagObjectsArray == nil) {
        _tagObjectsArray = [[NSMutableArray alloc] init];
    }
    else
    {
        [_tagObjectsArray removeAllObjects];
    }
    TagObject *tagObj = nil;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
        NSString *queryString = [NSString stringWithFormat:@"SELECT TAG_ID,TAG_NAME,SELECTED FROM TAGS_TEMP WHERE SELECTED = 1"];
        const char *queryCharP = [queryString UTF8String];
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            while (sqlite3_step(stmt) == SQLITE_ROW)
            {
                int rowId = sqlite3_column_int(stmt, 0);
                NSString *name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 1)];
                int selected = sqlite3_column_int(stmt, 2);
                
                tagObj = [[TagObject alloc]init];
                tagObj.tagRowId = rowId;
                tagObj.tagName = name;
                tagObj.selected = selected;
                [_tagObjectsArray addObject:tagObj];
                
                tagObj = nil;
            }
        }
        sqlite3_finalize(stmt);
        sqlite3_close(ioDB);
    }
}
-(void)reloadTagsCells
{
    DebugLog(@"-------------------------- _tagArray  %@--------------------------",_tagObjectsArray);
    
    NSInteger numberOfRowsINSection = [_addRecordTableView numberOfRowsInSection:3];
    DebugLog(@"number of rows %d",numberOfRowsINSection);
    int index = 1;
    TagObject *tagObj = nil;
    if (numberOfRowsINSection - 1  >= _tagObjectsArray.count) {
        for (index=1; index<=_tagObjectsArray.count; index++)
        {
            tagObj = (TagObject *)[_tagObjectsArray objectAtIndex:index-1];
            UITableViewCell *cell = [_addRecordTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:3]];
            DebugLog(@"cell %@",cell);
            UITextField *textField = (UITextField *)[cell viewWithTag:TAGS_TAG + index - 1];
            DebugLog(@"index %d TAGS_TAG %d",index,TAGS_TAG + index - 1);
            textField.text = tagObj.tagName;
        }
        DebugLog(@"now index for delete row %d", index);
        for (int start = numberOfRowsINSection - 1; start >= index; start--)
        {
            DebugLog(@"delete row %d", start);
            [_addRecordTableView beginUpdates];
            [_addRecordTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:start inSection:3]] withRowAnimation:UITableViewRowAnimationAutomatic];
            numberOfTags = start;
            DebugLog(@"AddRecord:delete : numberOfTags %d",numberOfTags);
            [_addRecordTableView endUpdates];
        }
    }
    else
    {
        for (index=1; index<numberOfRowsINSection; index++)
        {
            tagObj = (TagObject *)[_tagObjectsArray objectAtIndex:index-1];
            UITableViewCell *cell = [_addRecordTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:3]];
            DebugLog(@"cell %@",cell);
            UITextField *textField = (UITextField *)[cell viewWithTag:TAGS_TAG + index - 1];
            DebugLog(@"index %d GROUPS_TAG %d",index,TAGS_TAG + index - 1);
            textField.text = tagObj.tagName;
        }
        DebugLog(@"now index for new row %d", index);
        for (int start = index; start <= _tagObjectsArray.count; start++)
        {
            DebugLog(@"add new row %d", start);
            [_addRecordTableView beginUpdates];
            [_addRecordTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:start inSection:3]] withRowAnimation:UITableViewRowAnimationAutomatic];
            numberOfTags = start + 1;
            DebugLog(@"AddRecord:insert : numberOfTags %d",numberOfTags);
            [_addRecordTableView endUpdates];
        }
    }
}
-(void)loadCategoryArray
{
    if (_categoryArray == nil) {
        _categoryArray = [[NSMutableArray alloc] init];
    }
    else
    {
        [_categoryArray removeAllObjects];
    }
    
    NSString *tempString;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
        NSString *queryString = [NSString stringWithFormat:@"SELECT CATEGORY_NAME FROM CATEGORIES"];
        const char *queryCharP = [queryString UTF8String];
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            while (sqlite3_step(stmt) == SQLITE_ROW)
            {
                tempString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 0)];
                [_categoryArray addObject:tempString];
            }
        }
        sqlite3_finalize(stmt);
        sqlite3_close(ioDB);
    }
}
-(void)loadGroupsArray
{
    if (_groupArray == nil) {
        _groupArray = [[NSMutableArray alloc] init];
    }
    else
    {
        [_groupArray removeAllObjects];
    }
    GroupObject *groupObj = nil;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
        NSString *queryString = [NSString stringWithFormat:@"SELECT ID,GROUP_NAME,ICON_ID FROM GROUPS WHERE GROUP_NAME <> \"%@\"",[UNGROUPED_GROUPNAME lowercaseString]];
        const char *queryCharP = [queryString UTF8String];
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            while (sqlite3_step(stmt) == SQLITE_ROW)
            {
                int rowId = sqlite3_column_int(stmt, 0);
                NSString *name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 1)];
                int iconId = sqlite3_column_int(stmt, 2);
                
                groupObj = [[GroupObject alloc]init];
                groupObj.groupRowId = rowId;
                groupObj.groupName = name;
                groupObj.iconId = iconId;
                
                if (rowId == _selectedGroupId) {
                    groupObj.selected = 1;
                }else{
                   groupObj.selected = 0;
                }
                [_groupArray addObject:groupObj];
                
                groupObj = nil;
            }
        }
        sqlite3_finalize(stmt);
        sqlite3_close(ioDB);
    }
}
-(void)reloadGroupsArrayWithSelectedGroups
{
    DebugLog(@"AddRecord:reloadGroupsArrayWithSelectedGroups");
    NSMutableArray *groupIDArray = [[NSMutableArray alloc] init];
    
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
        NSString *queryString = [NSString stringWithFormat:@"SELECT ID FROM GROUPS WHERE ID IN (SELECT GROUP_ID FROM IMAGE_GROUP_RELATION WHERE IMAGE_MASTER_ID = %d)",_currentImgRowId];
        const char *queryCharP = [queryString UTF8String];
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            while (sqlite3_step(stmt) == SQLITE_ROW)
            {
                int rowId = sqlite3_column_int(stmt, 0);
                [groupIDArray addObject:[NSNumber numberWithInt:rowId]];
            }
        }
        sqlite3_finalize(stmt);
        sqlite3_close(ioDB);
    }
    DebugLog(@"AddRecord:groupIDArray = %@ and _groupArray %@",groupIDArray,_groupArray);
    numberOfGroups = groupIDArray.count+1;
    GroupObject *gpObj = nil;
    for(int i = 0; i < groupIDArray.count ; i++ )
    {
        for(int arrIndex = 0; arrIndex < _groupArray.count ; arrIndex++ )
        {
            gpObj = (GroupObject *)[_groupArray objectAtIndex:arrIndex];
            DebugLog(@"AddRecord:gpObg.id = %d groupId %@",gpObj.groupRowId,[groupIDArray objectAtIndex:i]);
            if (gpObj.groupRowId   == [[groupIDArray objectAtIndex:i] intValue])
            {
                gpObj.selected = 1;
                DebugLog(@"AddRecord:replacing objectat index = %d",arrIndex);
                [_groupArray replaceObjectAtIndex:arrIndex withObject:gpObj];
                gpObj = nil;
                break;
            }
            gpObj = nil;
        }
    }
}
-(void)loadGroupsTempTable
{
    for (GroupObject *grpObj in _groupArray) {
        [self insertIntoGroupTemp:grpObj];
    }
}
-(void)insertIntoGroupTemp:(GroupObject *)groupObj
{
    DebugLog(@"AddRecord:--insertIntoGroupTemp--");
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String],&ioDB) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat:
                               @"INSERT INTO GROUPS_TEMP(GROUP_ID,GROUP_NAME,ICON_ID,SELECTED) VALUES (%d,\"%@\",%d,%d)",
                               groupObj.groupRowId,[groupObj.groupName lowercaseString],groupObj.iconId,groupObj.selected];
        DebugLog(@"AddRecord:---%@---",insertSQL);
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_stmt *statement = nil;
        sqlite3_prepare_v2(ioDB, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"AddRecord:GROUPS_TEMP : successfully saved :");
        } else {
            DebugLog(@"AddRecord:GROUPS_TEMP : failed to save");
        }
        sqlite3_finalize(statement);
        sqlite3_close(ioDB);
    }
}
-(void)updateInGroupTemp:(int)groupId selected:(int)lvalue
{
    DebugLog(@"AddRecord:--updateInGroupTemp--");
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String],&ioDB) == SQLITE_OK)
    {
        NSString *updateSQL = [NSString stringWithFormat:@"UPDATE GROUPS_TEMP SET SELECTED = %d WHERE GROUP_ID=\"%d\"",lvalue, groupId];
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_stmt *statement = nil;
        sqlite3_prepare_v2(ioDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"AddRecord:updated in GROUPS_TEMP with %d",lvalue);
        } else {
            DebugLog(@"AddRecord:Failed to update in GROUPS_TEMP");
        }
        sqlite3_finalize(statement);
        sqlite3_close(ioDB);
    }
}

// ================= Category Table =================
-(NSString *)getCategoryName:(int) objectId
{
    NSString *catgTitle = nil;
	// Open the database from the users filessytem
	if(sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK)
    {
		// Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"Select CATEGORY_NAME from CATEGORIES WHERE id=%d",objectId];
        const char *sqlStatement = [querySQL UTF8String];
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(ioDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
			while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
				catgTitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                DebugLog(@"catgTitle -> %@",catgTitle);
            }
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(ioDB);
    return catgTitle;
}
-(int)getCategoryId:(NSString *)catgTitle
{
    int catgId = 0;
	if(sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK)
    {
        // Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"Select id from CATEGORIES WHERE CATEGORY_NAME=\"%@\"",catgTitle];
        const char *sqlStatement = [querySQL UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(ioDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                catgId = sqlite3_column_int(compiledStatement, 0);
                DebugLog(@"catgId -> %d",catgId);
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(ioDB);
    return catgId;
}

// ================= Tag Table ======================
-(NSString *)getTagName:(int) objectId
{
    NSString *tagName = nil;
	// Open the database from the users filessytem
	if(sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK)
    {
		// Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"Select TAG_NAME from TAGS WHERE id=%d",objectId];
        const char *sqlStatement = [querySQL UTF8String];
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(ioDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
			while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
				tagName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                DebugLog(@"catgTitle -> %@",tagName);
            }
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(ioDB);
    return tagName;
}
-(int)getTagId:(NSString *)tagName
{
    int tagId = -1;
	if(sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK)
    {
        // Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"Select id from TAGS WHERE TAG_NAME=\"%@\"",[tagName lowercaseString]];
        const char *sqlStatement = [querySQL UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(ioDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                tagId = sqlite3_column_int(compiledStatement, 0);
                DebugLog(@"tagId -> %d",tagId);
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement);
    }
   // sqlite3_close(ioDB);
    return tagId;
}
-(int)insertIntoTags:(NSString *)tagName
{
    DebugLog(@"AddRecord:--insertIntoTags--");
    int RowId = -1;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String],&ioDB) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO TAGS(TAG_NAME) VALUES (\"%@\")",[tagName lowercaseString]];
        DebugLog(@"AddRecord:---%@---",insertSQL);
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_stmt *statement = nil;
        sqlite3_prepare_v2(ioDB, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            RowId = sqlite3_last_insert_rowid(ioDB);
            DebugLog(@"AddRecord:TAGS : successfully saved : %d",RowId);
            
        } else {
            DebugLog(@"AddRecord:TAGS : failed to save");
        }
        sqlite3_finalize(statement);
    }
    DebugLog(@"AddRecord:-RowId--%d---",RowId);
    return RowId;
}
- (int) DeleteAllRowsFromTagsTableWithImageId
{
    DebugLog(@"=============== Deleting  rows ================");
    int success = 0;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String],&ioDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM IMAGE_TAG_RELATION WHERE IMAGE_MASTER_ID = %d",_currentImgRowId];
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_stmt *statement;
        sqlite3_prepare_v2(ioDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            success = 1;
            DebugLog(@"AddRecord:=============== Success on deleting rows from IMAGE_TAG_RELATION================");
            
        } else
        {
            DebugLog(@"AddRecord:=============== Error while deleting rows from IMAGE_TAG_RELATION ================");
        }
        sqlite3_finalize(statement);
        //   sqlite3_close(ioDB);
    }
    return success;
}
// ================= Group Table ====================

-(NSString *)getGroupName:(int) objectId
{
    NSString *groupName = nil;
	// Open the database from the users filessytem
	if(sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK)
    {
		// Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"Select GROUP_NAME from GROUPS WHERE id=%d",objectId];
        const char *sqlStatement = [querySQL UTF8String];
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(ioDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
			while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
				groupName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                DebugLog(@"groupName -> %@",groupName);
            }
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(ioDB);
    return groupName;
}

-(int)getGroupId:(NSString *)groupName
{
    int groupId = -1;
	if(sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK)
    {
        // Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"Select id from GROUPS WHERE GROUP_NAME=\"%@\"",[groupName lowercaseString]];
        const char *sqlStatement = [querySQL UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(ioDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                groupId = sqlite3_column_int(compiledStatement, 0);
                DebugLog(@"AddRecord:groupId -> %d",groupId);
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(ioDB);
    return groupId;
}

- (int) DeleteAllRowsFromGroupsTableWithImageId
{
    DebugLog(@"=============== Deleting  rows ================");
    int success = 0;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String],&ioDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM IMAGE_GROUP_RELATION WHERE IMAGE_MASTER_ID = %d",_currentImgRowId];
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_stmt *statement;
        sqlite3_prepare_v2(ioDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            success = 1;
            DebugLog(@"AddRecord:=============== Success on deleting rows from IMAGE_GROUP_RELATION================");
            
        } else
        {
            DebugLog(@"AddRecord:=============== Error while deleting rows from IMAGE_GROUP_RELATION ================");
        }
        sqlite3_finalize(statement);
        //   sqlite3_close(ioDB);
    }
    return success;
}

// ================= Group Temp Table ====================

- (void) DeleteAllRowsFromGroupsTempTable
{
    DebugLog(@"=============== Deleting  rows ================");
    
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String],&ioDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM GROUPS_TEMP"];
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_stmt *statement;
        sqlite3_prepare_v2(ioDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"AddRecord:=============== Success on deleting rows from GROUPS_TEMP================");
            
        } else
        {
            DebugLog(@"AddRecord:=============== Error while deleting rows from GROUPS_TEMP ================");
        }
        sqlite3_finalize(statement);
        sqlite3_close(ioDB);
    }
}
// ================= Tag Temp Table ====================

- (void) DeleteAllRowsFromTagsTempTable
{
    DebugLog(@"=============== Deleting  rows ================");
    
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String],&ioDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM TAGS_TEMP"];
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_stmt *statement;
        sqlite3_prepare_v2(ioDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"AddRecord:=============== Success on deleting rows from TAGS_TEMP================");
            
        } else
        {
            DebugLog(@"AddRecord:=============== Error while deleting rows from TAGS_TEMP ================");
        }
        sqlite3_finalize(statement);
        sqlite3_close(ioDB);
    }
}

-(void)iconsDictionaryFromIconsTable
{
    if (_iconsDic == nil) {
        _iconsDic = [[NSMutableDictionary alloc] init];
    }
    else
    {
        [_iconsDic removeAllObjects];
    }
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
        NSString *queryString = [NSString stringWithFormat:@"SELECT ID,ICON_PATH FROM ICONS"];
        const char *queryCharP = [queryString UTF8String];
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            while (sqlite3_step(stmt) == SQLITE_ROW)
            {
                int rowId = sqlite3_column_int(stmt, 0);
                NSString *name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 1)];
                
                [_iconsDic setValue:name forKey:[NSString stringWithFormat:@"%d",rowId]];
            }
        }
        sqlite3_finalize(stmt);
        sqlite3_close(ioDB);
    }
}

#pragma scrollView Delegate Methods
- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    if (!_notesView.hidden) {
        // to update NoteView
        [notesTextView setNeedsDisplay];
    }
    
    if (pageControlUsed)
    {
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    }
    CGFloat pageWidth = _addRecordScrollView.frame.size.width;
    int page = floor((_addRecordScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if(_pageControl.currentPage != page) {
        _pageControl.currentPage = page;
        pagedSegmentedControl.selectedSegmentIndex = page;
    }
}

// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
    if (!_notesView.hidden) {
        _openNotesBtn.hidden = NO;
    }
    else
    {
        _openNotesBtn.hidden = YES;
    }
}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if ((_pageControl.currentPage == 0 && _segmentedControl.selectedSegmentIndex == 0) || _notesView.hidden == NO) {
        _openNotesBtn.hidden = NO;
    }
    else
    {
        _openNotesBtn.hidden = YES;
    }
    pageControlUsed = NO;
}

#pragma pageControl delegate method
- (IBAction)pageChange:(id)sender {
    int page = _pageControl.currentPage;
	
	
	// update the scroll view to the appropriate page
    CGRect frame = _addRecordScrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{ [_addRecordScrollView scrollRectToVisible:frame animated:NO]; }
                     completion:NULL];
    
    //[gridScrollView scrollRectToVisible:frame animated:YES];
    
	// Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
    pageControlUsed = YES;
}


#pragma TokenField Delegate Methods
- (BOOL)tokenField:(TITokenField *)tokenField willRemoveToken:(TIToken *)token {
	return YES;
}

- (void)tokenFieldChangedEditing:(TITokenField *)tokenField {
	// There's some kind of annoying bug where UITextFieldViewModeWhile/UnlessEditing doesn't do anything.
	[tokenField setRightViewMode:(tokenField.editing ? UITextFieldViewModeAlways : UITextFieldViewModeNever)];
}
-(void)tokenFieldFrameDidBegin:(TITokenField *)tokenField
{
    DebugLog(@"AddRecord:tokenFieldFrameDidBegin");
    [_addRecordTableView setContentOffset:CGPointMake(0,200) animated:YES];
    [tokenField setRightViewMode:UITextFieldViewModeAlways];
}
- (void)tokenFieldFrameDidChange:(TITokenField *)tokenField
{
//	[self textViewDidChange:nil];
//    
//    CGFloat oldHeight = tokenFieldView.frame.size.height - tokenFieldView.tokenField.frame.size.height;
//	CGFloat newHeight = 100;
//    DebugLog(@"AddRecord:oldHeight %f ",oldHeight);
//    DebugLog(@"AddRecord:newHeight %f ",newHeight);
//	
//	CGRect newFrame = tokenFieldView.contentView.frame;
//	newFrame.size.height = newHeight;
//	
//	if (newHeight < oldHeight){
//		newFrame.size.height = oldHeight;
//	}
//    
//	[tokenFieldView.contentView setFrame:newFrame];
//	[tokenFieldView updateContentSize];
//    DebugLog(@"AddRecord:tokenFieldView.size.height %f ",tokenFieldView.frame.size.height);
//
//    DebugLog(@"AddRecord:newFrame.size.height %f ",newFrame.size.height);
//    [_addRecordTableView beginUpdates];
//    tagsListHeight = newFrame.size.height;
//    [_addRecordTableView endUpdates];
}

- (void)keyboardWillShow:(NSNotification*)notification {
    [self moveTextViewForKeyboard:notification up:YES];
}

- (void)keyboardWillHide:(NSNotification*)notification {
    [self moveTextViewForKeyboard:notification up:NO];
}

- (void)moveTextViewForKeyboard:(NSNotification*)notification up:(BOOL)up {
    NSDictionary *userInfo = [notification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardRect;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    animationDuration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    keyboardRect = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    if (up == YES) {
        CGFloat keyboardTop = keyboardRect.origin.y;
        CGRect newTextViewFrame = notesTextView.frame;
        originalTextViewFrame = notesTextView.frame;
        newTextViewFrame.size.height = (keyboardTop - notesTextView.frame.origin.y - 10);
        notesTextView.frame = newTextViewFrame;
    } else {
        // Keyboard is going away (down) - restore original frame
        notesTextView.frame = originalTextViewFrame;
    }
    
    [UIView commitAnimations];
}



#pragma mark - MKMapViewDelegate
-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    MKPinAnnotationView *pinView = nil;
    if(annotation != _pageMapView.userLocation)
    {
        static NSString *defaultPinID = @"PageMapPin";
        pinView = (MKPinAnnotationView *)[_pageMapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        if ( pinView == nil )
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultPinID];
        
        pinView.pinColor = MKPinAnnotationColorRed;
        pinView.canShowCallout = YES;
        pinView.animatesDrop = YES;
        pinView.draggable = YES;
        //    UIImage *pinImage = [UIImage imageNamed:@"micra_32x32.png"];
        //    [pinView setImage:pinImage];
    }
    else {
        [_pageMapView.userLocation setTitle:@"I am here"];
    }
    return pinView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)annotationView didChangeDragState:(MKAnnotationViewDragState)newState fromOldState:(MKAnnotationViewDragState)oldState
{
    if (![annotationView isKindOfClass:[MKPinAnnotationView class]]) {
        return;
    }
    if (newState == MKAnnotationViewDragStateEnding)
    {
        CLLocationCoordinate2D droppedAt = annotationView.annotation.coordinate;
        DebugLog(@"AddRecord:Pin dropped at %f,%f", droppedAt.latitude, droppedAt.longitude);
        _currentLatitude = droppedAt.latitude;
        _currentLongitude = droppedAt.longitude;
        [self getReverseAddress];
    }
}
@end
