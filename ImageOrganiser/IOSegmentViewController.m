//
//  IOSegmentViewController.m
//  ImageOrganiser
//
//  Created by Kirti Nikam on 09/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "IOSegmentViewController.h"

@interface IOSegmentViewController ()
@property (strong,nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@end

@implementation IOSegmentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.topItem.titleView = nil;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    _segmentedControl = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"One",@"Two",nil]];
    
    _segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
    [_segmentedControl setSelectedSegmentIndex:0];
    //segmentedControl.momentary = YES;
    [_segmentedControl addTarget:self action:@selector(indexDidChangeForSegmentedControl:) forControlEvents:UIControlEventValueChanged];
    _segmentedControl.frame = CGRectMake(0, 0, 90, 40);
    //    [segmentedControl setFrame:[self.navigationController.toolbar bounds]];
    [self.navigationController.navigationBar.topItem setTitleView:_segmentedControl];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
  
    addRecordListController =   [[IOAddRecordViewController alloc] initWithNibName:@"IOAddRecordViewController" bundle:nil];
    addRecordListController.currentImgRowId = _currentImgRowId;
    addRecordPageController =   [[IOAddRecordPageViewController alloc] initWithNibName:@"IOAddRecordPageViewController" bundle:nil];
    addRecordPageController.currentImgRowId = _currentImgRowId;
    
    _allViewControllers = [[NSArray alloc] initWithObjects:addRecordListController, addRecordPageController, nil];
    
    // Ensure a view controller is loaded
    [self cycleFromViewController:self.currentViewController toViewController:[self.allViewControllers objectAtIndex:_segmentedControl.selectedSegmentIndex]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - View controller switching and saving

- (void)cycleFromViewController:(UIViewController*)oldVC toViewController:(UIViewController*)newVC {
    
    // Do nothing if we are attempting to swap to the same view controller
    if (newVC == oldVC) return;
    
    // Check the newVC is non-nil otherwise expect a crash: NSInvalidArgumentException
    if (newVC) {
        
        // Set the new view controller frame (in this case to be the size of the available screen bounds)
        // Calulate any other frame animations here (e.g. for the oldVC)
        newVC.view.frame = CGRectMake(CGRectGetMinX(self.view.bounds), CGRectGetMinY(self.view.bounds), CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds));
        
        // Check the oldVC is non-nil otherwise expect a crash: NSInvalidArgumentException
        if (oldVC) {
            
            // Start both the view controller transitions
            [oldVC willMoveToParentViewController:nil];
            [self addChildViewController:newVC];
            
            // Swap the view controllers
            // No frame animations in this code but these would go in the animations block
            [self transitionFromViewController:oldVC
                              toViewController:newVC
                                      duration:0.25
                                       options:UIViewAnimationOptionLayoutSubviews
                                    animations:^{}
                                    completion:^(BOOL finished) {
                                        // Finish both the view controller transitions
                                        [oldVC removeFromParentViewController];
                                        [newVC didMoveToParentViewController:self];
                                        // Store a reference to the current controller
                                        self.currentViewController = newVC;
                                    }];
            
        } else {
            
            // Otherwise we are adding a view controller for the first time
            // Start the view controller transition
            [self addChildViewController:newVC];
            
            // Add the new view controller view to the ciew hierarchy
            [self.view addSubview:newVC.view];
            
            // End the view controller transition
            [newVC didMoveToParentViewController:self];
            
            // Store a reference to the current controller
            self.currentViewController = newVC;
        }
    }
}

- (IBAction)indexDidChangeForSegmentedControl:(UISegmentedControl *)sender {
    
    NSUInteger index = sender.selectedSegmentIndex;
    
    if (UISegmentedControlNoSegment != index) {
        UIViewController *incomingViewController = [self.allViewControllers objectAtIndex:index];
        [self cycleFromViewController:self.currentViewController toViewController:incomingViewController];
    }
    
}

-(void)changeSegment:(id)sender
{
    NSLog(@"changeSegment self.virew %@",[self.view subviews]);
    if ([self.view subviews].count > 0 ) {
        [[[self.view subviews] lastObject] removeFromSuperview];
    }
    NSLog(@"changeSegment self.virew %@",[self.view subviews]);
    UISegmentedControl *segment = (UISegmentedControl *)sender;
    if (segment.selectedSegmentIndex == 0)
    {
        [self.view addSubview:addRecordListController.view];
    }
    else
    {
        [self.view addSubview:addRecordPageController.view];
    }
    
}
@end
