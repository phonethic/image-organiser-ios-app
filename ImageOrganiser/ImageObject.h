//
//  ImageObject.h
//  ImageOrganiser
//
//  Created by Kirti Nikam on 17/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageObject : NSObject <NSMutableCopying>
@property (copy,nonatomic) NSString *imagePath;
@property (nonatomic, readwrite) int isNew;
@property (nonatomic, readwrite) int isDeleted;
@property (nonatomic, readwrite) int isUpdated;

-(id) mutableCopyWithZone:(NSZone *)zone;
@end
