//
//  RecordObject.h
//  ImageOrganiser
//
//  Created by Kirti Nikam on 04/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecordObject : NSObject
@property (nonatomic, readwrite) int rowId;
@property (nonatomic, readwrite) int isFav;
@property (nonatomic, copy) NSString *imgNamePath;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *notes;
@property (nonatomic, copy) NSString *location;
@property (nonatomic, readwrite) double dateDouble;
@property (nonatomic, copy) NSString *groupsName;
@end
