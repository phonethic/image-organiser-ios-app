//
//  IOGalleryViewController.h
//  ImageOrganiser
//
//  Created by Kirti Nikam on 17/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IOGalleryViewController : UIViewController<UIScrollViewDelegate>
{
    BOOL pageControlUsed;

}
@property (nonatomic, retain) NSMutableArray *imageObjsArray;
@property (nonatomic, retain) NSMutableArray *viewControllers;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;

- (void)loadScrollViewWithPage:(int)page ;
- (void)unloadScrollViewWithPage:(int)page ;
- (IBAction)changePage:(id)sender;
@end
