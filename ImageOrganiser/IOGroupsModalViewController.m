//
//  IOGroupsModalViewController.m
//  ImageOrganiser
//
//  Created by Kirti Nikam on 02/05/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "IOGroupsModalViewController.h"
#import "IOAppDelegate.h"
#import "GroupObject.h"
#import "CommonCallback.h"

@interface IOGroupsModalViewController ()

@end

@implementation IOGroupsModalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelBtnClicked)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneBtnClicked)];
    
    _groupsTableView.allowsMultipleSelection = YES;
    
   // self.view.backgroundColor = BGCOLOR;
    self.view.backgroundColor = [UIColor clearColor];

    _groupsTableView.backgroundColor = [UIColor clearColor];
    _groupsTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _groupsTableView.separatorColor = [UIColor clearColor];
    _groupsTableView.layer.cornerRadius = 10.0;
    
//    UIButton *cancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
//    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
//    cancelButton.titleLabel.font = DEFAULT_FONT(15.0);
//    [cancelButton setBackgroundImage:[UIImage imageNamed:@"bar_button.png"] forState:UIControlStateNormal];
//    [cancelButton addTarget:self action:@selector(cancelBtnClicked) forControlEvents:UIControlEventTouchUpInside];
//    [cancelButton setFrame:CGRectMake(0, 0, 70, 30)];
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:cancelButton];
//    
//    UIButton *doneButton =  [UIButton buttonWithType:UIButtonTypeCustom];
//    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
//    doneButton.titleLabel.font = DEFAULT_FONT(15.0);
//    [doneButton setBackgroundImage:[UIImage imageNamed:@"bar_button.png"] forState:UIControlStateNormal];
//    [doneButton addTarget:self action:@selector(doneBtnClicked) forControlEvents:UIControlEventTouchUpInside];
//    [doneButton setFrame:CGRectMake(0, 0, 70, 30)];
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:doneButton];
//    
    [self loadGroupsArray];
    [self loadGroupsTempTable];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setGroupsTableView:nil];
    [super viewDidUnload];
}
-(void)cancelBtnClicked
{
    [_groupArray removeAllObjects];
    _groupArray = nil;
    DebugLog(@"GroupsModal : GroupModal cancelBtnClicked");
    [self deleteAllRowsFromGroupsTempTable];
    [self.navigationController dismissModalViewControllerAnimated:YES];
}
-(void)doneBtnClicked
{
    DebugLog(@"GroupsModal : _groupArray : %@",_groupArray);
    DebugLog(@"GroupsModal : 1) readGroupsFromGroupsTempTable");
    [self readGroupsFromGroupsTempTable];
    
    int  groupsSuccess = [self deleteAllRowsFromImageGroupsRelationTableWithImageId];
    if(groupsSuccess)
    {
        if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String],&ioDB) == SQLITE_OK)
        {
            int movesuccess = 0;
            for (GroupObject *grpObj in _groupArray)
            {
                if (grpObj.selected == 1) {
                    DebugLog(@"GroupsModal : IMAGE_GROUP_RELATION: group %@ : selected",grpObj.groupName);
                    NSString *insertSQL2 = [NSString stringWithFormat:
                                            @"INSERT INTO IMAGE_GROUP_RELATION(IMAGE_MASTER_ID,GROUP_ID) VALUES (%d,%d)",
                                            _currentImgId,grpObj.groupRowId];
                    DebugLog(@"GroupsModal : ---%@---",insertSQL2);
                    sqlite3_stmt *statement = nil;
                    const char *insert_group_stmt = [insertSQL2 UTF8String];
                    sqlite3_prepare_v2(ioDB, insert_group_stmt, -1, &statement, NULL);
                    if (sqlite3_step(statement) == SQLITE_DONE)
                    {
                        DebugLog(@"GroupsModal : IMAGE_GROUP_RELATION: successfully updated : %lld",sqlite3_last_insert_rowid(ioDB));
                        movesuccess = 1;
                    } else {
                        DebugLog(@"GroupsModal : IMAGE_GROUP_RELATION: failed to update");
                    }
                    sqlite3_finalize(statement);
                }
            }
            
            int copysuccess = 0;
            if(_mode == COPY_MODE)
            {
                NSString *insertSQL2 = [NSString stringWithFormat:
                                        @"INSERT INTO IMAGE_GROUP_RELATION(IMAGE_MASTER_ID,GROUP_ID) VALUES (%d,%d)",
                                        _currentImgId,_currentGroupId];
                DebugLog(@"GroupsModal : ---%@---",insertSQL2);
                sqlite3_stmt *statement = nil;
                const char *insert_group_stmt = [insertSQL2 UTF8String];
                sqlite3_prepare_v2(ioDB, insert_group_stmt, -1, &statement, NULL);
                if (sqlite3_step(statement) == SQLITE_DONE)
                {
                    copysuccess = 1;
                    DebugLog(@"GroupsModal : IMAGE_GROUP_RELATION: successfully updated : %lld",sqlite3_last_insert_rowid(ioDB));
                } else {
                    DebugLog(@"GroupsModal : IMAGE_GROUP_RELATION: failed to update");
                }
                sqlite3_finalize(statement);
                
                if (copysuccess) {
                    [self showAlert:@"Record copied to other group successfully."];
                }else if (movesuccess) {
                    [self showAlert:@"Record moved to other group successfully."];
                }
            }
            sqlite3_close(ioDB);
        }
    }
    [self cancelBtnClicked];
}
- (void)showAlert:(NSString *)message {
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
	[alertView show];
}

#pragma mark Tableview DataSource methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
     return [_groupArray count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"groupModalCell";
    UIImageView *checkMarkImgView;
    UILabel *lblTitle;
    UITableViewCell *cell= (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;

        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(10,2,150, 30)];
        lblTitle.tag = 101;
        lblTitle.font = DEFAULT_BOLD_FONT(15.0);
        lblTitle.textAlignment = UITextAlignmentLeft;
        lblTitle.textColor = [UIColor whiteColor];
        lblTitle.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblTitle];
        
        cell.contentView.backgroundColor = TABLE_CELL_BGCOLOR;
        cell.layer.borderColor = TABLE_CELL_BGCOLOR.CGColor;
        cell.layer.borderWidth = 0;
        
        checkMarkImgView = [[UIImageView alloc] initWithFrame:CGRectMake(250,10, 20, 20)];
        checkMarkImgView.tag = 102;
        checkMarkImgView.contentMode = UIViewContentModeScaleToFill;
        checkMarkImgView.layer.masksToBounds = YES;
        checkMarkImgView.image = [UIImage imageNamed:@"check_mark_white.png"];
        [cell.contentView addSubview:checkMarkImgView];
    }
    if (_groupArray != nil && _groupArray.count > 0) {
        GroupObject *recObj = (GroupObject *)[_groupArray objectAtIndex:indexPath.row];
        DebugLog(@"GroupsModal : groupCell %@",recObj.groupName);
        cell.tag = recObj.groupRowId;
        lblTitle = (UILabel *)[cell viewWithTag:101];
        lblTitle.text = [recObj.groupName capitalizedString];
        checkMarkImgView = (UIImageView *)[cell viewWithTag:102];
        if(recObj.selected)
        {
            checkMarkImgView.image = [UIImage imageNamed:@"check_mark_blue.png"];
            cell.contentView.backgroundColor = TABLE_EDITCELL_BGCOLOR;
            lblTitle.textColor = TABLE_CELL_BGCOLOR;
        }
        else
        {
            checkMarkImgView.image = nil;
            cell.contentView.backgroundColor = TABLE_CELL_BGCOLOR;
            lblTitle.textColor = [UIColor whiteColor];
        }
    }
    return cell;
}

#pragma mark Tableview DataSource methods
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    UILabel *lblTitle = (UILabel *)[cell viewWithTag:101];
    UIImageView *checkMarkImgView = (UIImageView *)[cell viewWithTag:102];
    GroupObject *recObj = (GroupObject *)[_groupArray objectAtIndex:indexPath.row];
    if(recObj.selected)
    {
        checkMarkImgView.image = [UIImage imageNamed:@"check_mark_blue.png"];
        cell.contentView.backgroundColor = TABLE_EDITCELL_BGCOLOR;
        lblTitle.textColor = TABLE_CELL_BGCOLOR;
    }
    else
    {
        checkMarkImgView.image = nil;
        cell.contentView.backgroundColor = TABLE_CELL_BGCOLOR;
        lblTitle.textColor = [UIColor whiteColor];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    UILabel *lblTitle = (UILabel *)[cell viewWithTag:101];
    UIImageView *checkMarkImgView = (UIImageView *)[cell viewWithTag:102];
    GroupObject *recObj = (GroupObject *)[_groupArray objectAtIndex:indexPath.row];
    if(recObj.selected)
    {
        recObj.selected = 0;
        [self updateInGroupTemp:cell.tag selected:0];
        checkMarkImgView.image = nil;
        cell.contentView.backgroundColor = TABLE_CELL_BGCOLOR;
        lblTitle.textColor = [UIColor whiteColor];
    }
    else
    {
        recObj.selected = 1;
        [self updateInGroupTemp:cell.tag selected:1];
        checkMarkImgView.image = [UIImage imageNamed:@"check_mark_blue.png"];
        cell.contentView.backgroundColor = TABLE_EDITCELL_BGCOLOR;
        lblTitle.textColor = TABLE_CELL_BGCOLOR;
    }
    [_groupArray replaceObjectAtIndex:indexPath.row withObject:recObj];
    recObj = nil;
}

-(void)loadGroupsArray
{
    if (_groupArray == nil) {
        _groupArray = [[NSMutableArray alloc] init];
    }
    else
    {
        [_groupArray removeAllObjects];
    }
    GroupObject *groupObj = nil;
    int rowId = 0;
    NSString *name = @"";
    int iconId = 0;
    int imgId = 0;
    
//    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
//        NSString *queryString = [NSString stringWithFormat:@"SELECT a.ID,a.GROUP_NAME,a.ICON_ID,b.IMAGE_MASTER_ID FROM GROUPS a LEFT JOIN IMAGE_GROUP_RELATION b ON a.id = b.GROUP_ID  where  a.GROUP_NAME <> \"%@\" AND a.ID <> %d",[UNGROUPED_GROUPNAME lowercaseString],_currentGroupId];
//        const char *queryCharP = [queryString UTF8String];
//        sqlite3_stmt *stmt = nil;
//        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
//            while (sqlite3_step(stmt) == SQLITE_ROW)
//            {
//                rowId = sqlite3_column_int(stmt, 0);
//                name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 1)];
//                iconId = sqlite3_column_int(stmt, 2);
//                imgId = sqlite3_column_int(stmt, 3);
//                
//                groupObj = [[GroupObject alloc]init];
//                groupObj.groupRowId = rowId;
//                groupObj.groupName = name;
//                groupObj.iconId = iconId;
//                
//                DebugLog(@"GroupsModal : current id %d ------ id %d",self.currentGroupId,rowId);
//                if (imgId == _currentImgId) {
//                    groupObj.selected = 1;
//                }
//                else
//                {
//                    groupObj.selected = 0;
//                }
//                [_groupArray addObject:groupObj];
//                
//                groupObj = nil;
//            }
//        }
//        sqlite3_finalize(stmt);
//        sqlite3_close(ioDB);
//    }
    
    int ungroupid = [self getGroupId:UNGROUPED_GROUPNAME];
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
        NSString *queryString = [NSString stringWithFormat:@"SELECT ID,GROUP_NAME,ICON_ID FROM GROUPS WHERE ID NOT IN (SELECT GROUP_ID FROM IMAGE_GROUP_RELATION WHERE IMAGE_MASTER_ID =  %d) AND ID <> %d AND ID <> %d",_currentImgId,ungroupid,self.currentGroupId];
        
        DebugLog(@"GroupsModal : query1 : %@",queryString);
        const char *queryCharP = [queryString UTF8String];
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            while (sqlite3_step(stmt) == SQLITE_ROW)
            {
                rowId = sqlite3_column_int(stmt, 0);
                name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 1)];
                iconId = sqlite3_column_int(stmt, 2);
                
                groupObj = [[GroupObject alloc]init];
                groupObj.groupRowId = rowId;
                groupObj.groupName = name;
                groupObj.iconId = iconId;
                groupObj.selected = 0;
                [_groupArray addObject:groupObj];
                groupObj = nil;
            }
        }
        sqlite3_finalize(stmt);
        NSString *queryString1 = [NSString stringWithFormat:@"SELECT ID,GROUP_NAME,ICON_ID FROM GROUPS WHERE ID IN (SELECT GROUP_ID FROM IMAGE_GROUP_RELATION WHERE IMAGE_MASTER_ID =  %d ) AND ID <> %d AND ID <> %d",_currentImgId,ungroupid,self.currentGroupId];
        
        DebugLog(@"GroupsModal : query2 : %@",queryString1);

        const char *queryCharP1 = [queryString1 UTF8String];
        if (sqlite3_prepare_v2(ioDB, queryCharP1, -1, &stmt, NULL) == SQLITE_OK) {
            while (sqlite3_step(stmt) == SQLITE_ROW)
            {
                rowId = sqlite3_column_int(stmt, 0);
                name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 1)];
                iconId = sqlite3_column_int(stmt, 2);
                
                groupObj = [[GroupObject alloc]init];
                groupObj.groupRowId = rowId;
                groupObj.groupName = name;
                groupObj.iconId = iconId;
                groupObj.selected = 1;
                [_groupArray addObject:groupObj];
                groupObj = nil;
            }
        }
        sqlite3_finalize(stmt);
        sqlite3_close(ioDB);
    }
}

-(int)getGroupId:(NSString *)groupName
{
    int groupId = -1;
	if(sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK)
    {
        // Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"Select ID from GROUPS WHERE GROUP_NAME=\"%@\"",[groupName lowercaseString]];
        const char *sqlStatement = [querySQL UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(ioDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                groupId = sqlite3_column_int(compiledStatement, 0);
                DebugLog(@"groupId -> %d",groupId);
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(ioDB);
    return groupId;
}
-(void)loadGroupsTempTable
{
    for (GroupObject *grpObj in _groupArray) {
        [self insertIntoGroupTemp:grpObj];
    }
}
-(void)insertIntoGroupTemp:(GroupObject *)groupObj
{
    DebugLog(@"GroupsModal : --insertIntoGroupTemp--");
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String],&ioDB) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat:
                               @"INSERT INTO GROUPS_TEMP(GROUP_ID,GROUP_NAME,ICON_ID,SELECTED) VALUES (%d,\"%@\",%d,%d)",
                               groupObj.groupRowId,[groupObj.groupName lowercaseString],groupObj.iconId,groupObj.selected];
        DebugLog(@"GroupsModal : ---%@---",insertSQL);
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_stmt *statement = nil;
        sqlite3_prepare_v2(ioDB, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"GroupsModal : GROUPS_TEMP : successfully saved :");
        } else {
            DebugLog(@"GroupsModal : GROUPS_TEMP : failed to save");
        }
        sqlite3_finalize(statement);
        sqlite3_close(ioDB);
    }
}
-(void)updateInGroupTemp:(int)groupId selected:(int)lvalue
{
    DebugLog(@"GroupsModal : --updateInGroupTemp--");
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String],&ioDB) == SQLITE_OK)
    {
        NSString *updateSQL = [NSString stringWithFormat:@"UPDATE GROUPS_TEMP SET SELECTED = %d WHERE GROUP_ID=\"%d\"",lvalue, groupId];
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_stmt *statement = nil;
        sqlite3_prepare_v2(ioDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"GroupsModal : updated in GROUPS_TEMP %d with %d",groupId,lvalue);
        } else {
            DebugLog(@"GroupsModal : Failed to update in GROUPS_TEMP");
        }
        sqlite3_finalize(statement);
        sqlite3_close(ioDB);
    }
}
-(void)readGroupsFromGroupsTempTable
{
    
    if (_groupArray == nil) {
        _groupArray = [[NSMutableArray alloc] init];
    }
    else
    {
        [_groupArray removeAllObjects];
    }
    GroupObject *groupObj = nil;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
        NSString *queryString = [NSString stringWithFormat:@"SELECT GROUP_ID,GROUP_NAME,ICON_ID,SELECTED FROM GROUPS_TEMP WHERE SELECTED = 1"];
        const char *queryCharP = [queryString UTF8String];
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            while (sqlite3_step(stmt) == SQLITE_ROW)
            {
                int rowId = sqlite3_column_int(stmt, 0);
                NSString *name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 1)];
                int iconId = sqlite3_column_int(stmt, 2);
                int selected = sqlite3_column_int(stmt, 3);
                
                groupObj = [[GroupObject alloc]init];
                groupObj.groupRowId = rowId;
                groupObj.groupName = name;
                groupObj.iconId = iconId;
                groupObj.selected = selected;
                DebugLog(@"GroupsModal : group Name : %@ selected %d",name,selected);
                [_groupArray addObject:groupObj];
                
                groupObj = nil;
            }
        }
        sqlite3_finalize(stmt);
        sqlite3_close(ioDB);
    }
}
- (int) deleteAllRowsFromImageGroupsRelationTableWithImageId
{
    DebugLog(@"GroupsModal : =============== Deleting  rows ================");
    int success = 0;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String],&ioDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM IMAGE_GROUP_RELATION WHERE IMAGE_MASTER_ID = %d",_currentImgId];
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_stmt *statement;
        sqlite3_prepare_v2(ioDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            success = 1;
            DebugLog(@"GroupsModal : =============== Success on deleting rows from IMAGE_GROUP_RELATION================");
            
        } else
        {
            DebugLog(@"GroupsModal : =============== Error while deleting rows from IMAGE_GROUP_RELATION ================");
        }
        sqlite3_finalize(statement);
        //   sqlite3_close(ioDB);
    }
    return success;
}
- (void) deleteAllRowsFromGroupsTempTable
{
    DebugLog(@"GroupsModal : =============== Deleting  rows ================");
    
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String],&ioDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM GROUPS_TEMP"];
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_stmt *statement;
        sqlite3_prepare_v2(ioDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"GroupsModal : =============== Success on deleting rows from GROUPS_TEMP================");
            
        } else
        {
            DebugLog(@"GroupsModal : =============== Error while deleting rows from GROUPS_TEMP ================");
        }
        sqlite3_finalize(statement);
        sqlite3_close(ioDB);
    }
}
@end
