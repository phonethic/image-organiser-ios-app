//
//  IOViewController.m
//  ImageOrganiser
//
//  Created by Kirti Nikam on 02/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import <objc/runtime.h>
#import "UIImage+Resize.h"

#import "IOViewController.h"
#import "IOAppDelegate.h"
#import "IOAddRecordViewController.h"
#import "IOManageGrpsViewController.h"
#import "IOAboutViewController.h"
#import "constants.h"
#import "CommonCallback.h"
#import "IOSettingsViewController.h"

#define SEARCH @"Search"
#define SORT @"Sort By"
#define MANAGE_GROUPS @"Manage Groups"
#define SETTINGS @"Settings"
#define ABOUT @"About"



static char * const kIndexPathAssociationKey = "IO_indexPath";

@interface IOViewController () <UIGestureRecognizerDelegate>

@end

@implementation IOViewController
@synthesize openCellIndexPath;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	
	if (self) {
//		imageCache = [[NSCache alloc] init];
//		[imageCache setName:@"IOImageCache"];
        self.title = @"List of records";
	}
	
	return self;
}


-(void)viewDidAppear:(BOOL)animated
{
    DebugLog(@"HomeViewList:viewDidAppear %d",self.navigationItem.rightBarButtonItems.count);
    if (self.navigationItem.rightBarButtonItems.count == 0) {
//        UIButton *addButton =  [UIButton buttonWithType:UIButtonTypeCustom];
//        [addButton setImage:[UIImage imageNamed:@"add_bar_button.png"] forState:UIControlStateNormal];
//        [addButton addTarget:self action:@selector(addClicked:) forControlEvents:UIControlEventTouchUpInside];
//        [addButton setFrame:CGRectMake(0, 0, 30, 30)];
//        
//        UIButton *moreButton =  [UIButton buttonWithType:UIButtonTypeCustom];
//        [moreButton setTitle:@"More" forState:UIControlStateNormal];
//        moreButton.titleLabel.font = DEFAULT_FONT(15.0);
//        [moreButton setBackgroundImage:[UIImage imageNamed:@"bar_button.png"] forState:UIControlStateNormal];
//        [moreButton addTarget:self action:@selector(moreClicked:) forControlEvents:UIControlEventTouchUpInside];
//        [moreButton setFrame:CGRectMake(CGRectGetMaxX(addButton.frame), 0, 50, 30)];
//        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:moreButton];
//        UIBarButtonItem *addBarButton = [[UIBarButtonItem alloc] initWithCustomView:addButton];
//        UIBarButtonItem *moreBarButton = [[UIBarButtonItem alloc] initWithCustomView:moreButton];
//        
//        self.navigationItem.rightBarButtonItems = [[NSArray alloc] initWithObjects:moreBarButton,addBarButton, nil];

    }
    if ([self getRowsCount] <= 0) {
        _listTableView.hidden   = YES;
//        _emptyInfoLabel.hidden  = NO;
        _emptyInfoImageView.hidden = NO;
    }
    else
    {
        _listTableView.hidden   = NO;
//        _emptyInfoLabel.hidden  = YES;
        _emptyInfoImageView.hidden = YES;

        if (!isSearchOn) {
            [self reloadData];
        }else{
            [self searchBarSearchButtonClicked:_searchBar];
        }
    }
    moreBtnState = 0;
    [super viewDidAppear:animated];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
-(void)viewDidDisappear:(BOOL)animated
{
    DebugLog(@"HomeViewList:viewDidDisappear");
    [super viewDidDisappear:animated];
   [self closeMorePullDownMenu];
//    if (_lastIndexPath != nil) {
//        UITableViewCell *cell  = (UITableViewCell *)[_listTableView cellForRowAtIndexPath:_lastIndexPath];
//        [self closeSwipedCell:cell];
//        cell = nil;
//    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    _emptyInfoLabel.textColor = YELLOW_COLOR;

//    self.view.backgroundColor = BGCOLOR;
    self.view.backgroundColor =  [UIColor clearColor];

    [_listTableView setBackgroundColor:[UIColor clearColor]];

    UIBarButtonItem *addBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addClicked:)];
    UIBarButtonItem *moreBarButton = [[UIBarButtonItem alloc] initWithTitle:@"More" style:UIBarButtonItemStyleBordered target:self action:@selector(moreClicked:)];
    self.navigationItem.rightBarButtonItems = [[NSArray alloc] initWithObjects:moreBarButton,addBarButton, nil];
    
    DebugLog(@"HomeViewList:rows count %d",[self getRowsCount]);
    
    _sortArray = [[NSMutableArray alloc] initWithObjects:@"Date",@"Title",@"Group", nil];
    [self addPickerWithDoneButton];
    _searchBar.autocorrectionType = UITextAutocorrectionTypeYes;
    _searchBar.tintColor = DEFAULT_COLOR;
    _searchBar.hidden = YES;
    isSearchOn = NO;
    canSelectRow = YES;
    addfirstResposerToSearchBar  = NO;
    [[UISearchBar appearance] setBackgroundImage:[IO_APP_DELEGATE blueBarBackground]];
    
 //   _searchBar.showsSearchResultsButton = YES;
    _searchBar.showsCancelButton = YES;
    
    for (UIView *searchBarSubview in [_searchBar subviews]) {
        if ([searchBarSubview conformsToProtocol:@protocol(UITextInputTraits)]) {
            @try {
                [(UITextField *)searchBarSubview setBorderStyle:UITextBorderStyleRoundedRect];
            }
            @catch (NSException * e) {
                // ignore exception
            }
        }
        if ([searchBarSubview isKindOfClass:[UIButton class]])
        {
//            UIButton *cancelButton = (UIButton*)searchBarSubview;
//            [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//            [cancelButton setTintColor:BGCOLOR];

//            cancelButton.enabled = YES;
//            [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
//            [cancelButton setBackgroundImage:[UIImage imageNamed:@"bar_button.png"] forState:UIControlStateNormal];
        }
    }
//    _searchBar.layer.shadowColor = [UIColor blackColor].CGColor;
//    _searchBar.layer.shadowOffset = CGSizeMake(0,4);
//    _searchBar.layer.shadowOpacity = 1;
    
    _buttonData = [[NSArray alloc] initWithObjects:@"star-hollow",@"action",@"trash", nil];

    _moreArray = [[NSMutableArray alloc] initWithObjects:SEARCH,SORT,MANAGE_GROUPS,SETTINGS,ABOUT, nil];
    
    [_moreTableView reloadData];
    [_moreTableView sizeToFit];
    _dropdownView.frame = CGRectMake(_dropdownView.frame.origin.x, -(_dropdownView.frame.size.height-_dropdownView.frame.origin.y), _moreTableView.frame.size.width,_moreTableView.frame.size.height);
    
    _transparentDropdownView.frame = CGRectMake(0, -(_transparentDropdownView.frame.size.height-_transparentDropdownView.frame.origin.y), _transparentDropdownView.frame.size.width, _transparentDropdownView.frame.size.height);
    _transparentDropdownView.backgroundColor = [UIColor clearColor];//[UIColor colorWithPatternImage:[UIImage imageNamed:@"black_transparent.png"]];
    
    //_moreTableView.layer.cornerRadius = 20.0;
    _moreTableView.backgroundColor = [UIColor colorWithRed:53/255.0 green:53/255.0 blue:52/255.0 alpha:1];
    _moreTableView.separatorColor = [UIColor colorWithPatternImage:[CommonCallback separatorImage]];
    
    _dropdownView.backgroundColor = [UIColor colorWithRed:53/255.0 green:53/255.0 blue:52/255.0 alpha:1];
    _dropdownView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    _dropdownView.layer.shadowColor = [UIColor blackColor].CGColor;
    _dropdownView.layer.shadowOffset = CGSizeMake(0, 3);
    _dropdownView.layer.shadowOpacity = 1;
    _dropdownView.layer.shadowRadius = 4;
    _dropdownView.layer.shouldRasterize = YES;
    _dropdownView.layer.rasterizationScale = [UIScreen mainScreen].scale;

//    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
//    viewTap.numberOfTapsRequired = 1;
//    viewTap.cancelsTouchesInView = NO;
//    [_transparentDropdownView  addGestureRecognizer:viewTap];
    
//    _dropdownView.layer.cornerRadius = 4;
//    _dropdownView.backgroundColor = [UIColor colorWithRed:53/255.0 green:53/255.0 blue:52/255.0 alpha:1];
//    _dropdownView.layer.borderColor =  [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:1].CGColor;
//    _dropdownView.layer.borderWidth = 1;
//    _dropdownView.layer.masksToBounds = YES;
    
    //to round bottom corners
//    // Create the path (with only the top-left corner rounded)
//    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_dropdownView.bounds
//                                                   byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight
//                                                         cornerRadii:CGSizeMake(4.0, 4.0)];
//    
//    // Create the shape layer and set its path
//    CAShapeLayer *maskLayer = [CAShapeLayer layer];
//    maskLayer.frame = _dropdownView.bounds;
//    maskLayer.path = maskPath.CGPath;
//    [maskLayer setShadowPath:maskPath.CGPath];
//
//    // Set the newly created shape layer as the mask for the image view's layer
//    _dropdownView.layer.mask = maskLayer;
    
    _listTableView.hidden   = YES;
    _listTableView.tableHeaderView = _searchBar;
    [_searchBar setHidden:NO];
}

- (void)tapDetected:(UIGestureRecognizer *)sender {
    DebugLog(@"HomeViewList: tapDetected");
    // get location of tap
    CGPoint tapLocation = [sender locationInView:self.view];
    UIView *view = [self.view hitTest:tapLocation withEvent:nil];
    DebugLog(@"HomeViewList:view %@",view);
    if ( [view.superview isEqual:[UITableViewCell class]])
    {
        DebugLog(@"HomeViewList:got UITableViewCell");

    }
    if ([view isKindOfClass:[UITableView class]] || [view isKindOfClass:[UITableViewCell class]]) {
        DebugLog(@"HomeViewList:UITableView");
//        if ([view isEqual:_moreTableView]) {
//            return;
//        }
        return;
    }
    if (moreBtnState == 1) {
        [self closeMorePullDownMenu];
    }
}


- (void)didReceiveMemoryWarning
{
    [[IO_APP_DELEGATE imageCache] removeAllObjects];
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setListTableView:nil];
    [self setEmptyInfoLabel:nil];
    [self setMoreTableView:nil];
    [self setSearchBar:nil];
    [self setDropdownView:nil];
    [self setTransparentDropdownView:nil];
    [self setDropDownBackBtn:nil];
    [self setEmptyInfoImageView:nil];
    [super viewDidUnload];
}

#pragma mark Orientation methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate
{
    return NO;
}
- (NSUInteger)supportedInterfaceOrientations{
#if __IPHONE_OS_VERSION_MAX_ALLOWED <= __IPHONE_5_1
    return UIInterfaceOrientationPortrait;
#else
    return UIInterfaceOrientationMaskPortrait;
#endif
}

-(void)reloadData{
    [self readImageMasterFromDatabase];
    NSSortDescriptor *alphaDesc = [[NSSortDescriptor alloc] initWithKey:@"dateDouble" ascending:YES selector:@selector(compare:)];
    [_recordObjectsArray sortUsingDescriptors:[NSMutableArray arrayWithObjects:alphaDesc, nil]];
    alphaDesc = nil;
    [_listTableView reloadData];
}
#pragma BarButtons mothods
-(void)addClicked:(id)sender
{
    DebugLog(@"HomeViewList:addclicked");
    IOAddRecordViewController *addRecordViewController = [[IOAddRecordViewController alloc] initWithNibName:@"IOAddRecordViewController" bundle:nil];
    addRecordViewController.currentImgRowId = -1;
    [self.navigationController pushViewController:addRecordViewController animated:YES];
}

-(void)moreClicked:(id)sender
{
    DebugLog(@"HomeViewList:moreClicked");
    if (moreBtnState == 0)
    {
        [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
        [UIView animateWithDuration:0.2 animations:^{
            _moreTableView.frame = CGRectMake(0, 20, self.navigationController.navigationBar.frame.size.width,_moreTableView.frame.size.height);
            _dropdownView.frame = CGRectMake(0, 0, self.navigationController.navigationBar.frame.size.width, _moreTableView.frame.size.height+20);
            _transparentDropdownView.frame = CGRectMake(0, 0, _transparentDropdownView.frame.size.width, _transparentDropdownView.frame.size.height);
        } completion:^(BOOL finished)
         {
             [UIView animateWithDuration:0.3 animations:^{
                 _moreTableView.frame = CGRectMake(0, 0, self.navigationController.navigationBar.frame.size.width, _moreTableView.frame.size.height);
                 _dropdownView.frame = CGRectMake(0, 0, self.navigationController.navigationBar.frame.size.width, _moreTableView.frame.size.height-20);
             }];
         }];
        moreBtnState = 1;
        _listTableView.userInteractionEnabled = FALSE;
    }
    else
    {
        [self closeMorePullDownMenu];
    }
}

-(void)closeMorePullDownMenu
{
//    _dropdownView.frame = CGRectMake(_dropdownView.frame.origin.x, -(_dropdownView.frame.size.height-_dropdownView.frame.origin.y), _dropdownView.frame.size.width,_dropdownView.frame.size.height);
//    [UIView commitAnimations];
    
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView animateWithDuration:0.2 animations:^{
        _moreTableView.frame = CGRectMake(0,20, _moreTableView.frame.size.width,_moreTableView.frame.size.height);
        _dropdownView.frame = CGRectMake(0, 0, self.navigationController.navigationBar.frame.size.width, _dropdownView.frame.size.height+20);

    } completion:^(BOOL finished)
     {
         [UIView animateWithDuration:0.3 animations:^{
             _moreTableView.frame = CGRectMake(0,0, _moreTableView.frame.size.width,_moreTableView.frame.size.height);
             _dropdownView.frame = CGRectMake(0, -(_dropdownView.frame.size.height-_dropdownView.frame.origin.y)-20, self.navigationController.navigationBar.frame.size.width, _dropdownView.frame.size.height-20);
             _transparentDropdownView.frame = CGRectMake(0, -(_transparentDropdownView.frame.size.height-_transparentDropdownView.frame.origin.y), _transparentDropdownView.frame.size.width, _transparentDropdownView.frame.size.height);
         }];
         if (addfirstResposerToSearchBar) {
            [_searchBar becomeFirstResponder];
         }
     }];
    moreBtnState = 0;
    _listTableView.userInteractionEnabled = TRUE;
}
-(void) addPickerWithDoneButton
{
    actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                              delegate:nil
                                     cancelButtonTitle:nil
                                destructiveButtonTitle:nil
                                     otherButtonTitles:nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    
    CGRect categoryPickerFrame = CGRectMake(0, 40, 0, 0);
    
    _pickerView = [[UIPickerView alloc] initWithFrame:categoryPickerFrame];
    _pickerView.showsSelectionIndicator = YES;
    _pickerView.dataSource = self;
    _pickerView.delegate = self;
    [actionSheet addSubview:_pickerView];
    
    UISegmentedControl *doneButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Done"]];
    doneButton.momentary = YES;
    doneButton.frame = CGRectMake(260, 7.0f, 50.0f, 30.0f);
    doneButton.segmentedControlStyle = UISegmentedControlStyleBar;
    doneButton.tintColor = BGCOLOR;
    [doneButton addTarget:self action:@selector(doneFilterBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:doneButton];
    
    UISegmentedControl *cancelButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Cancel"]];
    cancelButton.momentary = YES;
    cancelButton.frame = CGRectMake(10, 7.0f, 60.0f, 30.0f);
    cancelButton.segmentedControlStyle = UISegmentedControlStyleBar;
    cancelButton.tintColor = [UIColor blackColor];
    [cancelButton addTarget:self action:@selector(cancelFilterBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:cancelButton];
    
    [_pickerView reloadAllComponents];
    if([_pickerView numberOfRowsInComponent:0] != -1)
    {
        [_pickerView selectRow:0 inComponent:0 animated:YES];
        _sortByString = [_sortArray objectAtIndex:0];
    }
}
- (void)doneFilterBtnClicked:(id)sender
{
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    _sortByString = [_sortArray objectAtIndex:[_pickerView selectedRowInComponent:0]];
    if ([_sortByString isEqualToString:@"Title"]){
        NSSortDescriptor *alphaDesc = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES selector:@selector(compare:)];
        [_recordObjectsArray sortUsingDescriptors:[NSMutableArray arrayWithObjects:alphaDesc, nil]];
        alphaDesc = nil;
    }
    else if ([_sortByString isEqualToString:@"Group"]){
        NSSortDescriptor *alphaDesc = [[NSSortDescriptor alloc] initWithKey:@"groupsName" ascending:YES selector:@selector(compare:)];
        [_recordObjectsArray sortUsingDescriptors:[NSMutableArray arrayWithObjects:alphaDesc, nil]];
        alphaDesc = nil;
    }
    else
    {
        NSSortDescriptor *alphaDesc = [[NSSortDescriptor alloc] initWithKey:@"dateDouble" ascending:NO selector:@selector(compare:)];
        [_recordObjectsArray sortUsingDescriptors:[NSMutableArray arrayWithObjects:alphaDesc, nil]];
        alphaDesc = nil;
    }
    [_listTableView reloadData];
}

- (void)cancelFilterBtnClicked:(id)sender
{
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
}
-(void)showPicker
{
    [self closeMorePullDownMenu];
    [actionSheet showInView:self.view];
    [actionSheet setBounds:CGRectMake(0, 0, 320, 485)];
}

-(void)showSearchBar
{
     [self closeMorePullDownMenu];
    
    //_listTableView.tableHeaderView = _searchBar;
    [_searchBar setHidden:NO];
    [_searchBar becomeFirstResponder];
  
    self.navigationItem.rightBarButtonItems = nil;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneSearchBtnClicked:)];
    
    _searchBar.frame = CGRectMake(0,0,_searchBar.frame.size.width,_searchBar.frame.size.height);

    _listTableView.frame = CGRectMake(0, CGRectGetMaxY(_searchBar.frame),_listTableView.frame.size.width,_listTableView.frame.size.height - _searchBar.frame.size.height);

}
- (void)doneSearchBtnClicked:(id)sender
{
    DebugLog(@"HomeViewList:doneSearchBtnClicked");
    isSearchOn=NO;
    canSelectRow=YES;
    [_searchBar resignFirstResponder];


//    [_searchBar setHidden:YES];
//    _listTableView.tableHeaderView = nil;
    _listTableView.scrollEnabled = YES;
    
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addClicked:)];
    UIBarButtonItem *moreButton = [[UIBarButtonItem alloc] initWithTitle:@"more" style:UIBarButtonItemStyleBordered target:self action:@selector(moreClicked:)];
    self.navigationItem.rightBarButtonItems = [[NSArray alloc] initWithObjects:moreButton,addButton, nil];
    
    [self reloadData];
    
    //bounce animation
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView animateWithDuration:0.2 animations:^{
        _searchBar.frame = CGRectMake(0, -(_searchBar.frame.size.height-_searchBar.frame.origin.y)-20, _searchBar.frame.size.width,_searchBar.frame.size.height);
        
        _listTableView.frame = CGRectMake(0, CGRectGetMaxY(_searchBar.frame),_listTableView.frame.size.width,_listTableView.frame.size.height + _searchBar.frame.size.height);

    } completion:^(BOOL finished)
     {
         _searchBar.text=@"";
         [UIView animateWithDuration:0.3 animations:^{
             _listTableView.frame = CGRectMake(0,0,_listTableView.frame.size.width,_listTableView.frame.size.height);
         }];
     }];
}
- (IBAction)dropDownBackBtnClicked:(id)sender {
    if (moreBtnState == 1) {
        [self closeMorePullDownMenu];
    }
}
#pragma mark Picker Data Source Methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [_sortArray count];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return self.view.frame.size.width;
}

#pragma mark Picker Delegate Methods

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    DebugLog(@"HomeViewList:titleForRow");
    return [_sortArray objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)thePickerView  didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
}

#pragma mark Tableview DataSource methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([tableView isEqual:_listTableView]) {
        return 1;
    }
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:_listTableView]) {
        return _recordObjectsArray.count;
    }
    else  if([tableView isEqual:_moreTableView]) {
        return _moreArray.count;
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:_listTableView]) {
        return 90;
    }
    else if([tableView isEqual:_moreTableView]) {
        return 48;
    }
    return 30;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:_listTableView])
    {
        
        static NSString *cellIdentifier = @"listRecordsCell";
        
        UIImageView *thumbImgView;
        UIImageView *favImgView;
        UIImageView *arrowImgView;

        UIView *topView;
        UIView *swipeView;
        UILabel *lblTitle;
        UILabel *lblDate;
        UILabel *lblGroups;
        
        UITableViewCell *cell= (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];

//            cell.contentView.backgroundColor = [UIColor clearColor];
//            cell.backgroundColor = [UIColor clearColor];
            
            // Create the top view
            topView = [[UIView alloc] initWithFrame:CGRectMake(10,10,300, 80)];
            topView.tag = 444;
            topView.clipsToBounds = YES;
            [topView setBackgroundColor:YELLOW_COLOR];
            
            thumbImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, 80, 80)];
            thumbImgView.tag = 101;
            thumbImgView.contentMode = UIViewContentModeScaleToFill;
            thumbImgView.layer.masksToBounds = YES;
            thumbImgView.image = [UIImage imageNamed:@"Publish_ImageView.png"];
            //[cell.contentView addSubview:thumbImgView];
            [topView addSubview:thumbImgView];
            
            favImgView = [[UIImageView alloc] initWithFrame:CGRectMake(thumbImgView.frame.origin.x,thumbImgView.frame.origin.y, 30, 30)];
            favImgView.tag = 105;
            favImgView.contentMode = UIViewContentModeScaleToFill;
            favImgView.layer.masksToBounds = YES;
            [topView addSubview:favImgView];
            
            lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImgView.frame) + 10,2, topView.frame.size.width - (CGRectGetMaxX(thumbImgView.frame) +30), 30)];
            lblTitle.tag = 102;
            lblTitle.shadowOffset  = CGSizeMake(0.0, 2.0);
            lblTitle.font = DEFAULT_BOLD_FONT(18.0);
            lblTitle.textAlignment = UITextAlignmentLeft;
            lblTitle.textColor = DEFAULT_COLOR;
            lblTitle.backgroundColor =  [UIColor clearColor];
          //  [cell.contentView addSubview:lblTitle];
            [topView addSubview:lblTitle];

            lblDate = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImgView.frame) + 10,CGRectGetMaxY(lblTitle.frame)+3,160, 15 )];
            lblDate.tag = 103;
            lblDate.font = DEFAULT_FONT(13.0);
            lblDate.textAlignment = UITextAlignmentLeft;
            lblDate.textColor = [UIColor darkGrayColor];
            lblDate.backgroundColor =  [UIColor clearColor];
           // [cell.contentView addSubview:lblDate];
            [topView addSubview:lblDate];
            
            lblGroups = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImgView.frame) + 10, CGRectGetMaxY(lblDate.frame) + 2, lblTitle.frame.size.width, 20)];
            lblGroups.tag = 104;
            lblGroups.shadowOffset  = CGSizeMake(0.0, 2.0);
            lblGroups.font = DEFAULT_FONT(13.0);
            lblGroups.textAlignment = UITextAlignmentLeft;
            lblGroups.textColor = [UIColor darkGrayColor];
            lblGroups.backgroundColor =  [UIColor clearColor];
            lblGroups.numberOfLines = 3;
            [topView addSubview:lblGroups];
            
            arrowImgView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lblDate.frame) + 10,CGRectGetMaxY(lblTitle.frame)-8, 30, 30)];
            arrowImgView.tag = 105;
            arrowImgView.contentMode = UIViewContentModeScaleToFill;
            arrowImgView.layer.masksToBounds = YES;
            arrowImgView.image = [UIImage imageNamed:@"Mark.png"];
            [topView addSubview:arrowImgView];

            
            // Create the swipe view
            swipeView = [[UIView alloc] initWithFrame:CGRectMake(10, 10,300, 80)];
            swipeView.tag = 555;
            swipeView.clipsToBounds = YES;
            // Add the background pattern
            swipeView.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"dotted-pattern.png"]];
            
            // Overlay a shadow image that adds a subtle darker drop shadow around the edges
           /* UIImage* shadow = [[UIImage imageNamed:@"inner-shadow.png"] stretchableImageWithLeftCapWidth:0 topCapHeight:0];
            UIImageView* shadowImageView = [[UIImageView alloc] initWithFrame:swipeView.frame];
            shadowImageView.alpha = 0.6;
            shadowImageView.image = shadow;
            shadowImageView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
            [swipeView addSubview:shadowImageView];*/
            
            // Iterate through the button data and create a button for each entry
            CGFloat leftEdge = 160;
            CGFloat button_space = 20;
            DebugLog(@"HomeViewList:_buttonData.count %d",_buttonData.count);
            for (int btnindex = 0;btnindex < _buttonData.count;btnindex++)
            {
                DebugLog(@"HomeViewList:btnindex %d -- leftedge %f -- %@",btnindex,leftEdge,[_buttonData objectAtIndex:btnindex]);
                // Create the button
                UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
                // Make sure the button ends up in the right place when the cell is resized
                button.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
                
                NSString *imageName = [_buttonData objectAtIndex:btnindex];
                UIImage* buttonImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png",imageName]];
                UIImage* highlightedbuttonImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_act.png",imageName]];

                button.frame = CGRectMake(leftEdge, (swipeView.center.y - buttonImage.size.height/2.0) - 5, buttonImage.size.width, buttonImage.size.height);
              //  UIImage* grayImage = [self imageFilledWith:[UIColor colorWithWhite:0.9 alpha:1.0] using:buttonImage];
                [button setImage:buttonImage forState:UIControlStateNormal];
                [button setImage:highlightedbuttonImage forState:UIControlStateHighlighted];
                [button addTarget:self action:@selector(touchUpInsideAction:event:) forControlEvents:UIControlEventTouchUpInside];
                button.tag = [[NSString stringWithFormat:@"555%d",btnindex] intValue];
                [swipeView addSubview:button];
                // Move the left edge in prepartion for the next button
                leftEdge = leftEdge + buttonImage.size.width + button_space;
            }

            topView.layer.cornerRadius = 5.0;
            swipeView.layer.cornerRadius = 5.0;
            topView.layer.borderColor = [UIColor clearColor].CGColor;
            swipeView.layer.borderColor = [UIColor clearColor].CGColor;
            
            // Add views to contentView
            [cell.contentView addSubview:swipeView];
            [cell.contentView addSubview:topView];
            
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor colorWithRed:0.0542516 green:0.44115  blue:0.699654 alpha:1];
            cell.selectedBackgroundView = bgColorView;

        }

        if (_recordObjectsArray != nil && _recordObjectsArray.count > 0) {
            RecordObject *recObj = (RecordObject *)[_recordObjectsArray objectAtIndex:indexPath.row];
    //        if ([thumbImgView.image isEqual:[UIImage imageNamed:@"placeholder.png"]])
    //        {
    //            if (_listTableView.dragging == NO && _listTableView.decelerating == NO)
    //            {
    //                [self startImageLoad:recObj.imgNamePath forIndexPath:indexPath];
    //            }
    //            thumbImgView.image = [UIImage imageNamed:@"placeholder.png"];
    //        }
            
            topView = (UIView *)[cell viewWithTag:444];
            swipeView = (UIView *)[cell viewWithTag:555];
            UIButton* button = (UIButton *)[swipeView viewWithTag:5550];
            
            favImgView = (UIImageView *)[topView viewWithTag:105];
            
            if (recObj.isFav) {
                [button setImage:[UIImage imageNamed:@"yellowstar.png"] forState:UIControlStateNormal];
                favImgView.image = [UIImage imageNamed:@"yellowstar.png"];
            }
            else
            {
                [button setImage:[UIImage imageNamed:@"star-hollow.png"] forState:UIControlStateNormal];
                favImgView.image = nil;
            }
            if (recObj.imgNamePath != nil && ![recObj.imgNamePath isEqualToString:@""])
            {
                NSString *imageFilename = recObj.imgNamePath;
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString* imagePath = [documentsDirectory stringByAppendingPathComponent:recObj.imgNamePath];
                
                
                thumbImgView = (UIImageView *)[topView viewWithTag:101];
                UIImage *image = [[IO_APP_DELEGATE imageCache] objectForKey:imageFilename];
                
                if (image) {
                    thumbImgView.image = image;
                } else {
                    thumbImgView.image = [UIImage imageNamed:@"Publish_ImageView.png"];

                    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
                    
                    // Get the height of the cell to pass to the block.
                    CGFloat cellHeight = [tableView rowHeight];
                    
                    // Now, we can’t cancel a block once it begins, so we’ll use associated objects and compare
                    // index paths to see if we should continue once we have a resized image.
                    objc_setAssociatedObject(cell,
                                             kIndexPathAssociationKey,
                                             indexPath,
                                             OBJC_ASSOCIATION_RETAIN);
                    
                    dispatch_async(queue, ^{
                        UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
                        
                      //  UIImage *resizedImage = [IO_APP_DELEGATE resizeImage:image newSize:CGSizeMake(cellHeight, cellHeight)];
             
                        UIImage *resizedImage = [image resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:CGSizeMake(cellHeight, cellHeight) interpolationQuality:kCGInterpolationHigh];

                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSIndexPath *cellIndexPath =
                            (NSIndexPath *)objc_getAssociatedObject(cell, kIndexPathAssociationKey);
                            
                            if ([indexPath isEqual:cellIndexPath]) {
                                thumbImgView.image = resizedImage;
                            }
                            
                            [[IO_APP_DELEGATE imageCache] setObject:resizedImage forKey:imageFilename];
                        });
                    });
                }
            }
            else
            {
                thumbImgView = (UIImageView *)[topView viewWithTag:101];
                thumbImgView.image = [UIImage imageNamed:@"Publish_ImageView.png"];
            }
           
            lblTitle = (UILabel *)[topView viewWithTag:102];
            if ([recObj.title isEqualToString:@""]) {
                lblTitle.text = @"";
            }
            else
            {
                lblTitle.text = [IO_APP_DELEGATE doCapitalFirstLetter:recObj.title];
            }
            
            lblDate = (UILabel *)[topView viewWithTag:103];
            NSDate *date = [NSDate dateWithTimeIntervalSinceReferenceDate:recObj.dateDouble];
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"dd MMM yyyy HH:mm a"];
            lblDate.text = [dateFormat stringFromDate:date];
            
            
            lblGroups = (UILabel *)[topView viewWithTag:104];
            lblGroups.text = recObj.groupsName;
            cell.tag = recObj.rowId;
        }
        
        // Create the swipe gesture recognizers
//        UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipeLeftInCell:)];
//        [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
//        
//        
//        UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipeRightInCell:)];
//        [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
//        
//        [cell addGestureRecognizer:swipeLeft];
//        [cell addGestureRecognizer:swipeRight];
        
        
        // Create the pan gesture recognizers
        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
        pan.delegate = self;
        [cell addGestureRecognizer:pan];
        
        // Prevent selection highlighting
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.accessoryType = UITableViewCellAccessoryNone;
        [cell setNeedsLayout];
        
        [cell.contentView bringSubviewToFront:topView];

        return cell;
    }
    else if([tableView isEqual:_moreTableView]) {
        static NSString *cellIdentifier = @"more";
        UITableViewCell *cell= (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            cell.textLabel.textAlignment = UITextAlignmentLeft;
            cell.textLabel.font = DEFAULT_BOLD_FONT(21);

            cell.textLabel.backgroundColor = [UIColor clearColor];
           // cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.textLabel.textColor = [UIColor whiteColor];//[UIColor colorWithRed:128/255.0 green:126/255.0 blue:124/255.0 alpha:1];
            cell.textLabel.shadowColor = [UIColor blackColor];
            cell.textLabel.shadowOffset = CGSizeMake(0, -1);
            
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:1];
            cell.selectedBackgroundView = bgColorView;
            
            cell.textLabel.highlightedTextColor = [UIColor colorWithRed:128/255.0 green:126/255.0 blue:124/255.0 alpha:1];
        }
        NSString *text = [_moreArray objectAtIndex:indexPath.row];
        cell.textLabel.text = text;
        if ([text isEqualToString:MANAGE_GROUPS]) {
            cell.imageView.image = [UIImage imageNamed:@"menu_groups.png"];
        }
        else if ([text isEqualToString:SORT]) {
            cell.imageView.image = [UIImage imageNamed:@"menu_sortby.png"];
        }
        else if ([text isEqualToString:SEARCH]) {
            cell.imageView.image = [UIImage imageNamed:@"menu_search.png"];
        }
        else if ([text isEqualToString:SETTINGS]) {
            cell.imageView.image = [UIImage imageNamed:@"menu_settings.png"];
        }
        else if ([text isEqualToString:ABOUT]) {
            cell.imageView.image = [UIImage imageNamed:@"menu_about.png"];
        }
        return cell;
    }
    return nil;
}
- (UIImage *)scale:(UIImage *)image toSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}
#pragma mark Tableview Delegate methods
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if ([tableView isEqual:_moreTableView]) {
//        UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
//        NSString *text = [_moreArray objectAtIndex:indexPath.row];
//        if ([text isEqualToString:MANAGE_GROUPS]) {
//            cell.imageView.image = [UIImage imageNamed:@"menu_groups.png"];
//        }
//        else if ([text isEqualToString:SORT]) {
//            cell.imageView.image = [UIImage imageNamed:@"menu_sortby.png"];
//        }
//        else if ([text isEqualToString:SEARCH]) {
//            cell.imageView.image = [UIImage imageNamed:@"menu_search.png"];
//        }
//        else if ([text isEqualToString:SETTINGS]) {
//            cell.imageView.image = [UIImage imageNamed:@"menu_settings.png"];
//        }
//        else if ([text isEqualToString:ABOUT]) {
//            cell.imageView.image = [UIImage imageNamed:@"menu_about.png"];
//        }
//    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
//    UIView *topView = (UIView *)[cell viewWithTag:444];
//    UILabel *lblTitle = (UILabel *)[topView viewWithTag:102];
//    lblTitle.textColor = [UIColor whiteColor];
    
    if ([tableView isEqual:_listTableView])
    {
        if (![self isSwiped:indexPath]) {
            RecordObject *recObj = [_recordObjectsArray objectAtIndex:indexPath.row];
            if (recObj.imgNamePath != nil && ![recObj.imgNamePath isEqualToString:@""]) {
                [[IO_APP_DELEGATE imageCache] removeObjectForKey:recObj.imgNamePath];
            }
            recObj = nil;
            IOAddRecordViewController *addRecordViewController = [[IOAddRecordViewController alloc] initWithNibName:@"IOAddRecordViewController" bundle:nil];
            addRecordViewController.currentImgRowId = ((UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath]).tag;
            [self.navigationController pushViewController:addRecordViewController animated:YES];
        }
    }
    else if ([tableView isEqual:_moreTableView]) {
//        UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        NSString *text = [_moreArray objectAtIndex:indexPath.row];
//        if ([text isEqualToString:MANAGE_GROUPS]) {
//            cell.imageView.image = [UIImage imageNamed:@"menu_groups_gray.png"];
//        }
//        else if ([text isEqualToString:SORT]) {
//            cell.imageView.image = [UIImage imageNamed:@"menu_sortby_gray.png"];
//        }
//        else if ([text isEqualToString:SEARCH]) {
//            cell.imageView.image = [UIImage imageNamed:@"menu_search_gray.png"];
//        }
//        else if ([text isEqualToString:SETTINGS]) {
//            cell.imageView.image = [UIImage imageNamed:@"menu_settings_gray.png"];
//        }
//        else if ([text isEqualToString:ABOUT]) {
//            cell.imageView.image = [UIImage imageNamed:@"menu_about_gray.png"];
//        }
        
        if ([text isEqualToString:MANAGE_GROUPS]) {
            IOManageGrpsViewController *manageGrpsViewController = [[IOManageGrpsViewController alloc] initWithNibName:@"IOManageGrpsViewController" bundle:nil];
            [self.navigationController pushViewController:manageGrpsViewController animated:YES];
        }
        else if ([text isEqualToString:SORT]) {
            DebugLog(@"HomeViewList:Sorting");
            [self showPicker];
        }
        else if ([text isEqualToString:SEARCH]) {
            DebugLog(@"HomeViewList:SEARCH");
 //           [self showSearchBar];
            if (_recordObjectsArray.count > 0) {
                addfirstResposerToSearchBar = YES;
            }
             [self closeMorePullDownMenu];
        }
        else if ([text isEqualToString:SETTINGS]) {
            DebugLog(@"HomeViewList:SETTINGS");
            IOSettingsViewController *settingsviewController = [[IOSettingsViewController alloc] initWithNibName:@"IOSettingsViewController" bundle:nil] ;
            [self.navigationController pushViewController:settingsviewController animated:YES];
        }
        else if ([text isEqualToString:ABOUT]) {
            DebugLog(@"HomeViewList:ABOUT");
            IOAboutViewController *aboutviewController = [[IOAboutViewController alloc] initWithNibName:@"IOAboutViewController" bundle:nil] ;
            [self.navigationController pushViewController:aboutviewController animated:YES];
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES or NO
//    if ([tableView isEqual:_listTableView])
//    {
//        return YES;
//    }
    return NO;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath {
    
	 if ([tableView isEqual:_listTableView])
     {
        if(editingStyle == UITableViewCellEditingStyleDelete)
        {
//            RecordObject *recObj = [_recordObjectsArray objectAtIndex:indexPath.row];
//            if (recObj.imgNamePath != nil && ![recObj.imgNamePath isEqualToString:@""]) {
//                [imageCache removeObjectForKey:recObj.imgNamePath];
//            }
//            recObj = nil;
//            [_recordObjectsArray removeObjectAtIndex:indexPath.row];
//            [self DeleteRecord:((UITableViewCell *)[_listTableView cellForRowAtIndexPath:indexPath]).tag];
//            [_listTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationMiddle];
        }
     }
}
-(void)didSwipeRightInCell:(UISwipeGestureRecognizer *)sender
{

    UITableViewCell *cell = (UITableViewCell *)sender.view;
    [self closeSwipedCell:cell];
}

-(void)didSwipeLeftInCell:(UISwipeGestureRecognizer *)sender
{
    if (_lastIndexPath != nil) {
        [self closeSwipedCell:(UITableViewCell *)[_listTableView cellForRowAtIndexPath:_lastIndexPath]];
    }
    
    UITableViewCell *cell = (UITableViewCell *)sender.view;
    UIView *topView = (UIView *)[cell viewWithTag:444];
    UIView *swipeView = (UIView *)[cell viewWithTag:555];
    _lastIndexPath = [_listTableView indexPathForCell:cell];

    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    // Swipe top view left
    [UIView animateWithDuration:0.3 animations:^{
        [topView setFrame:CGRectMake(-160, 0, 320, 80)];
        
        } completion:^(BOOL finished) {
            // Bounce lower view.
            [UIView animateWithDuration:0.15 animations:^{
                [swipeView setFrame:CGRectMake(10, 0, 320, 80)];
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.15 animations:^{
                        [swipeView setFrame:CGRectMake(0, 0, 320, 80)]; }];
                }];
    }];
    
}
-(void) closeSwipedCell:(UITableViewCell *)cell
{
    _lastIndexPath = nil;
    UIView *topView = (UIView *)[cell viewWithTag:444];
    
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView animateWithDuration:0.3 animations:^{
        [topView setFrame:CGRectMake(10, 0, 320, 80)];
    } completion:^(BOOL finished)
     {
         [UIView animateWithDuration:0.15 animations:^{
             [topView setFrame:CGRectMake(0, 0, 320, 80)];
         }];
     }];

//    [UIView animateWithDuration:1.0 animations:^{
//        [topView setFrame:CGRectMake(-10, 0, 320, 60)];
//    } completion:^(BOOL finished)
//     {
//         [UIView animateWithDuration:0.15 animations:^{
//             [topView setFrame:CGRectMake(0, 0, 320, 60)];
//         }];
//     }];
}

-(BOOL)isSwiped:(NSIndexPath *)indexPath
{
    UITableViewCell *cell  = (UITableViewCell *)[_listTableView cellForRowAtIndexPath:indexPath];
    UIView *topView = (UIView *)[cell viewWithTag:444];
    DebugLog(@"HomeViewList:topView.frame.origin.x %f",topView.frame.origin.x);
    if (topView.frame.origin.x == -160) {
        return YES;
    }
    return NO;
}

// pandraggin Animation-------

#pragma mark - Gesture recognizer delegate
- (BOOL)gestureRecognizerShouldBegin:(UIPanGestureRecognizer *)panGestureRecognizer
{
    UITableViewCell *cell = (UITableViewCell *)[panGestureRecognizer view];
    CGPoint translation = [panGestureRecognizer translationInView:[cell superview]];
    return (fabs(translation.x) / fabs(translation.y) > 1) ? YES : NO;
}

#pragma mark - Gesture handlers

-(void)handlePan:(UIPanGestureRecognizer *)panGestureRecognizer
{
    float threshold = (PAN_OPEN_X+PAN_CLOSED_X)/2.0;
    float vX = 0.0;
    float compare = 0.0;
    float finalX = 0.0;
    DebugLog(@"HomeViewList:threshold %f compare %f vX %f finalX %f",threshold,compare,vX,finalX);

    UITableViewCell *cell = (UITableViewCell *)panGestureRecognizer.view;
    UIView *topView = (UIView *)[cell viewWithTag:444];
    NSIndexPath *indexPath = [self.listTableView indexPathForCell:cell];
    DebugLog(@"HomeViewList:topView %f",topView.transform.tx);

    switch ([panGestureRecognizer state]) {
        case UIGestureRecognizerStateBegan:
            DebugLog(@"HomeViewList:UIGestureRecognizerStateBegan");

            if (self.openCellIndexPath != nil || self.openCellIndexPath.section != indexPath.section || self.openCellIndexPath.row != indexPath.row) {
                DebugLog(@"HomeViewList:closing last one %d",openCellIndexPath.row);

                UITableViewCell *cell = (UITableViewCell *)[self.listTableView cellForRowAtIndexPath:openCellIndexPath];
                UIView *lasttopView = (UIView *)[cell viewWithTag:444];
                [self snapView:lasttopView toX:PAN_CLOSED_X animated:YES];
                [self setOpenCellIndexPath:nil];
               // [self setOpenCellLastTX:0];
            }
            break;
        case UIGestureRecognizerStateEnded:
            DebugLog(@"HomeViewList:UIGestureRecognizerStateEnded");

            vX = (FAST_ANIMATION_DURATION/2.0)*[panGestureRecognizer velocityInView:self.view].x;
            compare = topView.transform.tx + vX;
            
            DebugLog(@"HomeViewList:compare %f = %f + %f",compare,topView.transform.tx, vX);
            if (compare > threshold) {
                finalX = MAX(PAN_OPEN_X,PAN_CLOSED_X);
             //   [self setOpenCellLastTX:0];
            } else {
                finalX = MIN(PAN_OPEN_X,PAN_CLOSED_X);
            //    [self setOpenCellLastTX:topView.transform.tx];
            }
            DebugLog(@"HomeViewList:before openCellIndexPath %d ,_lastIndexPath %d,_deleteRecIndexPath %d",openCellIndexPath.row,_lastIndexPath.row,_deleteRecIndexPath.row);
            [self snapView:topView toX:finalX animated:YES];
            if (finalX == PAN_CLOSED_X) {
                [self setOpenCellIndexPath:nil];
            } else {
                [self setOpenCellIndexPath:indexPath];
            }
            break;
        case UIGestureRecognizerStateChanged:
            DebugLog(@"HomeViewList:UIGestureRecognizerStateChanged");

            //compare = self.openCellLastTX+[panGestureRecognizer translationInView:self.view].x;
            compare = [panGestureRecognizer translationInView:self.view].x;
            DebugLog(@"HomeViewList:---------UIGestureRecognizerStateChanged compare %f = %f ------------",compare,topView.transform.tx);
            if (compare > MAX(PAN_OPEN_X,PAN_CLOSED_X))
                compare = MAX(PAN_OPEN_X,PAN_CLOSED_X);
            else if (compare < MIN(PAN_OPEN_X,PAN_CLOSED_X))
                compare = MIN(PAN_OPEN_X,PAN_CLOSED_X);
            DebugLog(@"HomeViewList:---------UIGestureRecognizerStateChanged compare %f = %f ------------",compare,topView.transform.tx);
            [topView setTransform:CGAffineTransformMakeTranslation(compare, 0)];
            break;
        default:
            break;
    }
}

-(void)snapView:(UIView *)view toX:(float)x animated:(BOOL)animated
{
    [UIView animateWithDuration:0.15 animations:^{
        if (x == PAN_OPEN_X) {
            [view setTransform:CGAffineTransformMakeTranslation(x-10, 0)];
        }else{
            [view setTransform:CGAffineTransformMakeTranslation(x+10, 0)];
        }
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:FAST_ANIMATION_DURATION animations:^{
            [view setTransform:CGAffineTransformMakeTranslation(x, 0)];
        }];
    }];
    
    DebugLog(@"HomeViewList:snapView openCellIndexPath %d _lastIndexPath %d,_deleteRecIndexPath %d",openCellIndexPath.row,_lastIndexPath.row,_deleteRecIndexPath.row);
    
   // _lastIndexPath = openCellIndexPath;
}

#pragma mark -
#pragma UIScrollViewDelegate methods

- (void)scrollViewWillBeginDragging:(UIScrollView *)sender {
    if ([sender isEqual:[self listTableView]]) {
        UITableViewCell *openCell = (UITableViewCell *) [self.listTableView cellForRowAtIndexPath:openCellIndexPath];
        UIView *topView = (UIView *)[openCell viewWithTag:444];
        [self snapView:topView toX:PAN_CLOSED_X animated:YES];
    }
}

//// end of pandragging Animation-------


#pragma mark Button touch up inside action

- (IBAction) touchUpInsideAction:(id)sender event:(id)event
{
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:_listTableView];
    NSIndexPath *indexPath = [_listTableView indexPathForRowAtPoint: currentTouchPosition];
    if (indexPath != nil)
    {
        UITableViewCell *cell  = (UITableViewCell *)[_listTableView cellForRowAtIndexPath:indexPath];
//        UIButton *button = (UIButton *)[swipeView viewWithTag:5551];
        UIView *topView = (UIView *)[cell viewWithTag:444];
        
        [UIView animateWithDuration:FASTEST_ANIMATION_DURATION animations:^{
            [topView setTransform:CGAffineTransformMakeTranslation(PAN_CLOSED_X+10, 0)];
            
        } completion:^(BOOL finished) {
            [topView setTransform:CGAffineTransformMakeTranslation(PAN_CLOSED_X, 0)];
            [self setOpenCellIndexPath:nil];
            
            UIButton *btn = (UIButton *)sender;
            DebugLog(@"HomeViewList:btn.tag %d %d",btn.tag,cell.tag);
            if (btn.tag == 5552)
            {
                DebugLog(@"HomeViewList:trash");
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle: ALERT_TITLE message: @"Are you sure you want to delete this  data? If you delete this data then you can't recover it again." delegate: self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
                alert.tag = 1;
                [alert show];
                _deleteRecIndexPath = indexPath;
            }
            else if (btn.tag == 5551)
            {
                DebugLog(@"HomeViewList:action");
                if ([MFMailComposeViewController canSendMail])
                {
                    RecordObject *recObj = (RecordObject *)[_recordObjectsArray objectAtIndex:indexPath.row];
                    NSDate *date= [NSDate dateWithTimeIntervalSinceReferenceDate:recObj.dateDouble];
                    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                    [dateFormat setDateFormat:@"dd MMMM yyyy HH:mm a"];
                    NSString *dateString = [dateFormat stringFromDate:date];
                    
                    NSString *messageBody = [NSString stringWithFormat:@"<b>Title :</b> %@<br><br><b>Notes :</b> %@<br><br><b>Date :</b> %@<br><br><b>Location: </b>%@<br>",[recObj.title capitalizedString],recObj.notes,dateString,recObj.location];
                    MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
                    mailComposer.mailComposeDelegate = self;
                    mailComposer.navigationBar.tintColor = DEFAULT_COLOR;
                    [mailComposer setSubject:[NSString stringWithFormat:@"%@ Data",recObj.title]];
                    [mailComposer setToRecipients:nil];
                    [mailComposer setMessageBody:messageBody isHTML:YES];
                    NSMutableArray *imageArray = [self getImageAttachments:cell.tag];
                    for (int index = 0; index < imageArray.count; index++)
                    {
                        UIImage *image = [IO_APP_DELEGATE loadImage:[imageArray objectAtIndex:index]];
                        if (image != NULL)
                        {
                            NSData *data = UIImagePNGRepresentation(image);
                            [mailComposer addAttachmentData:data mimeType:@"image/png" fileName:[NSString stringWithFormat:@"screenshot_%d.png",index]];
                        }
                    }
                    [mailComposer setModalPresentationStyle:UIModalPresentationFormSheet];
                    [self presentModalViewController:mailComposer animated:YES];
                }
            }
            else if (btn.tag == 5550)
            {
                DebugLog(@"HomeViewList:fav");
                UIView *swipeView = (UIView *)[cell viewWithTag:555];
                UIButton* button = (UIButton *)[swipeView viewWithTag:5550];
                
                UIView *topView = (UIView *)[cell viewWithTag:444];
                UIImageView *favImgView = (UIImageView *)[topView viewWithTag:105];
                
                RecordObject *recObj = (RecordObject *)[_recordObjectsArray objectAtIndex:indexPath.row];
                int favGroupId = [self getGroupId:FAVOURITE_GROUPNAME];
                if (recObj.isFav) {
                    [button setImage:[UIImage imageNamed:@"star-hollow.png"] forState:UIControlStateNormal];
                    [self removeFromFavouriteGroup:cell.tag groupId:favGroupId];
                    recObj.isFav = 0;
                    favImgView.image = nil;
                }
                else
                {
                    [button setImage:[UIImage imageNamed:@"yellowstar.png"] forState:UIControlStateNormal];
                    [self insertIntoFavouriteGroup:cell.tag groupId:favGroupId];
                    recObj.isFav = 1;
                    favImgView.image = [UIImage imageNamed:@"yellowstar.png"];
                }
                [_recordObjectsArray replaceObjectAtIndex:indexPath.row withObject:recObj];
                [_listTableView reloadData];
            }
            //   [self closeSwipedCell:cell];
        }];
    }
}
#pragma alertView delegate Method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 1)
    {
        switch (buttonIndex) {
            case 0:
            {
                break;
            }
            case 1:
            {
                DebugLog(@"HomeViewList:Delete %d %d",_deleteRecIndexPath.row,openCellIndexPath.row);
                UITableViewCell *cell  = (UITableViewCell *)[_listTableView cellForRowAtIndexPath:_deleteRecIndexPath];
                RecordObject *recObj = [_recordObjectsArray objectAtIndex:_deleteRecIndexPath.row];
                if (recObj.imgNamePath != nil && ![recObj.imgNamePath isEqualToString:@""]) {
                    [[IO_APP_DELEGATE imageCache] removeObjectForKey:recObj.imgNamePath];
                }
                recObj = nil;

                [_recordObjectsArray removeObjectAtIndex:_deleteRecIndexPath.row];
                [self DeleteRecord:cell.tag];
                [_listTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:_deleteRecIndexPath] withRowAnimation:UITableViewRowAnimationMiddle];
            
                _deleteRecIndexPath = nil;
            }
                break;
            default:
                break;
        }

    }

}
#pragma mark MFMailComposeViewController Delegate Methods
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    DebugLog(@"Mail Result %u",result);
    switch (result)
    {
        case MFMailComposeResultCancelled :
            DebugLog(@"Mail Cancelled");
            break;
        case MFMailComposeResultSaved :
            DebugLog(@"Mail Saved");
            break;
        case MFMailComposeResultSent :
            DebugLog(@"Mail Sent");
            break;
        case MFMailComposeResultFailed :
        {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Unknown Error\nMail failed"
														   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
        }
            break;
        default:
            break;
    }
    [controller dismissModalViewControllerAnimated:YES];
}
#pragma mark SearchBar methods

//---fired when the user taps on the searchbar---
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    DebugLog(@"HomeViewList:searchBarTextDidBeginEditing");
    
    isSearchOn=YES;
    if (searchBar.text.length >0)
    {
        canSelectRow=YES;
        _listTableView.scrollEnabled=YES;
    }
    else
    {
        canSelectRow=NO;
        _listTableView.scrollEnabled=NO;
    }
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    DebugLog(@"HomeViewList:textDidChange");
    
    if ([searchText length] > 0)
    {
        canSelectRow=YES;
        _listTableView.scrollEnabled=YES;
    }
    else
    {
        canSelectRow=NO;
        _listTableView.scrollEnabled=NO;
    }
}
- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    DebugLog(@"HomeViewList:searchBarCancelButtonClicked ");
    _searchBar.text=@"";
    [_searchBar resignFirstResponder];
    _listTableView.scrollEnabled = YES;
    _listTableView.userInteractionEnabled = TRUE;
    isSearchOn=NO;
    addfirstResposerToSearchBar = NO;
    canSelectRow=YES;
    [self reloadData];
    DebugLog(@"HomeViewList:canSelectRow %d ",canSelectRow);

}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    DebugLog(@"HomeViewList:searchBarSearchButtonClicked");
    if (_recordObjectsArray == nil) {
        _recordObjectsArray = [[NSMutableArray alloc] init];
    }
    else
    {
        [_recordObjectsArray removeAllObjects];
    }
    int rowlId = 0;
    NSString *title = @"";
    double dateDouble = 0;
    RecordObject *recObj = nil;
    if(sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK)
    {
        //Search Text By Title, Notes,Location and tags
        NSString *queryString = [NSString stringWithFormat:@"SELECT ID,TITLE,DATE FROM IMAGE_MASTER WHERE ID IN (SELECT IMAGE_MASTER_ID FROM IMAGE_TAG_RELATION WHERE TAG_ID IN (SELECT ID FROM TAGS WHERE TAG_NAME LIKE \"%@%%\")) OR TITLE LIKE \"%@%%\" OR LOCATION  LIKE \"%@%%\" OR NOTES  LIKE \"%@%%\" OR ID IN (SELECT IMAGE_MASTER_ID FROM IMAGE_GROUP_RELATION WHERE GROUP_ID IN (SELECT ID FROM GROUPS WHERE GROUP_NAME LIKE \"%@%%\"))",searchBar.text,searchBar.text,searchBar.text,searchBar.text,searchBar.text];
        const char *queryCharP = [queryString UTF8String];
        
        sqlite3_stmt *stmt = nil;
        
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(stmt) == SQLITE_ROW)
            {
                rowlId = sqlite3_column_int(stmt, 0);
                title = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 1)];
                dateDouble = sqlite3_column_double(stmt, 2);
                
                recObj = [[RecordObject alloc] init];
                recObj.rowId = rowlId;
                recObj.title = title;
                recObj.imgNamePath = @"";
                recObj.dateDouble = dateDouble;
                
                [_recordObjectsArray addObject:recObj];
                recObj = nil;
            }
        }
        sqlite3_finalize(stmt);

        for (int index = 0; index < _recordObjectsArray.count; index++)
        {
            recObj = (RecordObject *)[_recordObjectsArray objectAtIndex:index];
            NSString *imagequeryString = [NSString stringWithFormat:@"SELECT IMAGE_PATH FROM IMAGE_SET_RELATION WHERE IMAGE_MASTER_ID = %d AND IMAGE_PATH <> \"\" LIMIT 1",recObj.rowId];
            DebugLog(@"HomeViewList:------%@-----",imagequeryString);
            const char *imgqueryCharP = [imagequeryString UTF8String];
            if (sqlite3_prepare_v2(ioDB, imgqueryCharP, -1, &stmt, NULL) == SQLITE_OK) {
                if(sqlite3_step(stmt) == SQLITE_ROW)
                {
                    NSString *path = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 0)];
                    DebugLog(@"HomeViewList:image path %@ ",path);
                    
                    recObj.imgNamePath = path;
                    [_recordObjectsArray replaceObjectAtIndex:index withObject:recObj];
                    path = nil;
                }
            }
            sqlite3_finalize(stmt);
            
            if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
                NSString *queryString = [NSString stringWithFormat:@"SELECT GROUP_NAME FROM GROUPS WHERE ID in (select GROUP_ID from IMAGE_GROUP_RELATION where IMAGE_MASTER_ID = %d)",recObj.rowId];
                const char *queryCharP = [queryString UTF8String];
                if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
                    NSMutableString *groupsString = [[NSMutableString alloc] initWithString:@""];
                    while (sqlite3_step(stmt) == SQLITE_ROW)
                    {
                        NSString *name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 0)];
                        if (name != nil && ![name isEqualToString:@""] && ![name isEqualToString:[FAVOURITE_GROUPNAME lowercaseString]] && ![name isEqualToString:[UNGROUPED_GROUPNAME lowercaseString]]) {
                            if ([groupsString isEqualToString:@""]) {
                                [groupsString appendFormat:@"%@",name];
                            }else{
                                [groupsString appendFormat:@",%@",name];
                            }
                        }
                    }
                    recObj.groupsName = groupsString;
                    [_recordObjectsArray replaceObjectAtIndex:index withObject:recObj];
                    groupsString = nil;
                }
                sqlite3_finalize(stmt);
            }
            recObj = nil;
        }
        sqlite3_close(ioDB);
    }

    DebugLog(@"HomeViewList:_recordObjectsArray %@",_recordObjectsArray);
    [_listTableView reloadData];
    if (_recordObjectsArray.count <= 0) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                            message:@"No results found"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
    else
    {
        [_searchBar resignFirstResponder];
    }
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:_listTableView]) {
        DebugLog(@"HomeViewList:canSelectRow %d ",canSelectRow);
        if (canSelectRow)
            return indexPath;
        else
            return nil;
    }
    return indexPath;
}


//#pragma mark -
//#pragma mark Deferred image loading (UIScrollViewDelegate)
//// Load images for all onscreen rows when scrolling is finished
//- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
//{
//    if (!decelerate)
//	{
//        [self loadImagesForOnscreenRows];
//    }
//}
//
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    [self loadImagesForOnscreenRows];
//}
//// this method is used in case the user scrolled into a set of cells that don't have their app icons yet
//- (void)loadImagesForOnscreenRows
//{
//    if ([_recordObjectsArray count] > 0)
//    {
//        NSArray *visiblePaths = [_listTableView indexPathsForVisibleRows];
//        for (NSIndexPath *indexPath in visiblePaths)
//        {
//            RecordObject *recObj = [_recordObjectsArray objectAtIndex:indexPath.row];
//            
//             if (![recObj.imgNamePath isEqualToString:@""] && recObj.imgNamePath != nil)// avoid the app icon download if the app already has an icon
//            {
//                    UITableViewCell *cell = [_listTableView cellForRowAtIndexPath:indexPath];
//                    // Display the newly loaded image
//                    UIImageView *thumbImgView = (UIImageView *)[cell viewWithTag:101];
//                    thumbImgView.image = [self loadImage:recObj.imgNamePath];
//            }
//        }
//    }
//}
//-(void)startImageLoad:(NSString *)stringPath forIndexPath:(NSIndexPath *)indexPath
//{
//    DebugLog(@"HomeViewList:startImageLoad %@ - indexPath.row %d ",stringPath,indexPath.row);
//    if (![stringPath isEqualToString:@""] && stringPath != nil)
//    {
//        UITableViewCell *cell = [_listTableView cellForRowAtIndexPath:indexPath];
//        // Display the newly loaded image
//        UIImageView *thumbImgView = (UIImageView *)[cell viewWithTag:101];
//        thumbImgView.image = [self loadImage:stringPath];
//    }
//}
#pragma internal methods


-(void)readImageMasterFromDatabase
{
    
    if (_recordObjectsArray == nil) {
        _recordObjectsArray = [[NSMutableArray alloc] init];
    }
    else
    {
        [_recordObjectsArray removeAllObjects];
    }
    
    int favGroupId = [self getGroupId:FAVOURITE_GROUPNAME];
    
    DebugLog(@"HomeViewList:readImageMasterFromDatabase");
    int rowlId = 0;
    NSString *title = @"";
    NSString *notes = @"";
    NSString *location = @"";
    double dateDouble = 0;
    int fav = 0;
    RecordObject *recObj = nil;
    if(sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK)
    {
        NSString *queryString = [NSString stringWithFormat:@"SELECT a.ID,a.TITLE,a.DATE,a.NOTES,a.LOCATION,b.GROUP_ID FROM IMAGE_MASTER a LEFT JOIN IMAGE_GROUP_RELATION b ON a.id = b.IMAGE_MASTER_ID and b.GROUP_ID = %d",favGroupId];
        const char *queryCharP = [queryString UTF8String];
        
        sqlite3_stmt *stmt = nil;
        
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(stmt) == SQLITE_ROW)
            {
                rowlId      = sqlite3_column_int(stmt, 0);
                title       = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 1)];
                dateDouble  = sqlite3_column_double(stmt, 2);
                notes       = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 3)];
                location    = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt,4)];
                fav         = sqlite3_column_int(stmt, 5);
                
                DebugLog(@"HomeViewList:rowlId %d fav %d",rowlId,fav);
                recObj = [[RecordObject alloc] init];
                recObj.rowId = rowlId;
                recObj.title = title;
                recObj.imgNamePath = @"";
                recObj.dateDouble = dateDouble;
                recObj.notes = notes;
                recObj.location = location;
                
                
                if (fav == 1) {
                    recObj.isFav = 1;
                }
                else
                {
                    recObj.isFav = 0;
                }
                [_recordObjectsArray addObject:recObj];
            }
        }
        sqlite3_finalize(stmt);
        DebugLog(@"HomeViewList:_recordObjectsArray %@ ",_recordObjectsArray);

        for (int index = 0; index < _recordObjectsArray.count; index++)
        {
            recObj = (RecordObject *)[_recordObjectsArray objectAtIndex:index];
            
            NSString *imagequeryString = [NSString stringWithFormat:@"SELECT IMAGE_PATH FROM IMAGE_SET_RELATION WHERE IMAGE_MASTER_ID = %d AND IMAGE_PATH <> \"\" LIMIT 1",recObj.rowId];
            DebugLog(@"HomeViewList:------%@-----",imagequeryString);
            const char *imgqueryCharP = [imagequeryString UTF8String];
            if (sqlite3_prepare_v2(ioDB, imgqueryCharP, -1, &stmt, NULL) == SQLITE_OK) {
                    if(sqlite3_step(stmt) == SQLITE_ROW)
                    {
                        NSString *path = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 0)];
                        DebugLog(@"HomeViewList:image path %@ ",path);
                        
                        recObj.imgNamePath = path;
                        [_recordObjectsArray replaceObjectAtIndex:index withObject:recObj];
                        path = nil;
                    }
            }
            sqlite3_finalize(stmt);
            
            NSString *queryString = [NSString stringWithFormat:@"SELECT GROUP_NAME FROM GROUPS WHERE ID in (select GROUP_ID from IMAGE_GROUP_RELATION where IMAGE_MASTER_ID = %d)",recObj.rowId];
            const char *queryCharP = [queryString UTF8String];
            if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
                NSMutableString *groupsString = [[NSMutableString alloc] initWithString:@""];
                while (sqlite3_step(stmt) == SQLITE_ROW)
                {

                    NSString *name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 0)];
                    if (name != nil && ![name isEqualToString:@""] && ![name isEqualToString:[FAVOURITE_GROUPNAME lowercaseString]] && ![name isEqualToString:[UNGROUPED_GROUPNAME lowercaseString]]) {
                        if ([groupsString isEqualToString:@""]) {
                            [groupsString appendFormat:@"%@",name];
                        }else{
                            [groupsString appendFormat:@",%@",name];
                        }
                    }
                }
                recObj.groupsName = groupsString;
                [_recordObjectsArray replaceObjectAtIndex:index withObject:recObj];
                groupsString = nil;
            }
            sqlite3_finalize(stmt);
            
            recObj = nil;
        }
        sqlite3_close(ioDB);
    }
}


-(NSInteger)getRowsCount
{
    NSInteger rowCount = 0;
    
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
         NSString *queryString = [NSString stringWithFormat:@"SELECT count (*) FROM IMAGE_MASTER"];
         const char *queryCharP = [queryString UTF8String];
        
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            if (sqlite3_step(stmt) == SQLITE_ROW) {
                rowCount = sqlite3_column_int(stmt, 0);
            }
        }
        sqlite3_finalize(stmt);
        sqlite3_close(ioDB);
    }
    DebugLog(@"IMAGE_MASTER rowCount %d",rowCount);
    return rowCount;
}
- (void) DeleteRecord :(int) imgId
{
    BOOL deleteSuccess = NO;
    sqlite3_stmt *statement;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM IMAGE_MASTER WHERE ID=\"%d\"", imgId];
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(ioDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            deleteSuccess = YES;
            DebugLog(@"HomeViewList:deleted");
        } else {
             DebugLog(@"HomeViewList:Failed to delete");
        }
        sqlite3_finalize(statement);
        
        if (deleteSuccess)
        {
            
            NSMutableArray *imageArray = [[NSMutableArray alloc] init];
            NSString *path = nil;
            NSString *imagequeryString = [NSString stringWithFormat:@"SELECT IMAGE_PATH FROM IMAGE_SET_RELATION WHERE IMAGE_MASTER_ID = %d AND IMAGE_PATH <> \"\"",imgId];
            DebugLog(@"HomeViewList:------%@-----",imagequeryString);
            const char *imgqueryCharP = [imagequeryString UTF8String];
            if (sqlite3_prepare_v2(ioDB, imgqueryCharP, -1, &statement, NULL) == SQLITE_OK) {
                while(sqlite3_step(statement) == SQLITE_ROW)
                {
                    path = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                    DebugLog(@"HomeViewList:image path %@ ",path);
                    [imageArray addObject:path];
                    path =nil;
                }
            }
            sqlite3_finalize(statement);
            
            NSString *imgSetdeleteSQL = [NSString stringWithFormat:@"DELETE FROM IMAGE_SET_RELATION WHERE IMAGE_MASTER_ID=\"%d\"", imgId];
            const char *imgset_delete_stmt = [imgSetdeleteSQL UTF8String];
            sqlite3_prepare_v2(ioDB, imgset_delete_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE) {
                DebugLog(@"HomeViewList:deleted from IMAGE_SET_RELATION");
                for (NSString *pathStr in imageArray) {
                    DebugLog(@"HomeViewList:remove image %@",pathStr);
                    [IO_APP_DELEGATE removeImage:pathStr];
                }
            }else{
                DebugLog(@"HomeViewList:Failed to delete from IMAGE_SET_RELATION");
            }
            sqlite3_finalize(statement);
            
            [imageArray removeAllObjects];
            imageArray = nil;
            
            NSString *imgtag_deleteSQL = [NSString stringWithFormat:@"DELETE FROM IMAGE_TAG_RELATION WHERE IMAGE_MASTER_ID=\"%d\"", imgId];
            const char *imgtag_delete_stmt = [imgtag_deleteSQL UTF8String];
            sqlite3_prepare_v2(ioDB, imgtag_delete_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                DebugLog(@"HomeViewList:deleted from IMAGE_TAG_RELATION");
            } else {
                DebugLog(@"HomeViewList:Failed to delete from IMAGE_TAG_RELATION");
            }
            sqlite3_finalize(statement);
            
            NSString *imggrp_deleteSQL = [NSString stringWithFormat:@"DELETE FROM IMAGE_GROUP_RELATION WHERE IMAGE_MASTER_ID=\"%d\"", imgId];
            const char *imggrp_delete_stmt = [imggrp_deleteSQL UTF8String];
            sqlite3_prepare_v2(ioDB, imggrp_delete_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                DebugLog(@"HomeViewList:deleted from IMAGE_GROUP_RELATION");
            } else {
                DebugLog(@"HomeViewList:Failed to delete from IMAGE_GROUP_RELATION");
            }
            sqlite3_finalize(statement);
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message: @"Record deleted successfully." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            if (_recordObjectsArray.count <= 0) {
                _listTableView.hidden   = YES;
//                _emptyInfoLabel.hidden  = NO;
                _emptyInfoImageView.hidden = NO;
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message: @"Failed to delete record." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }
    sqlite3_close(ioDB);
}
-(void)removeFromFavouriteGroup:(int)rowId groupId:(int)lgroupId
{
    sqlite3_stmt *statement;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM IMAGE_GROUP_RELATION WHERE IMAGE_MASTER_ID=%d AND GROUP_ID = %d",rowId,lgroupId];
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(ioDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"HomeViewList:deleted from IMAGE_GROUP_RELATION");
        } else {
            DebugLog(@"HomeViewList:Failed to delete from IMAGE_GROUP_RELATION");
        }
        sqlite3_finalize(statement);
        sqlite3_close(ioDB);
    }
}
-(void)insertIntoFavouriteGroup:(int)rowId groupId:(int)lgroupId
{
    DebugLog(@"HomeViewList:--insertIntoGroupTemp--");
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String],&ioDB) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat:
                               @"INSERT INTO IMAGE_GROUP_RELATION(IMAGE_MASTER_ID,GROUP_ID) VALUES (%d,%d)",
                               rowId,lgroupId];
        DebugLog(@"HomeViewList:---%@---",insertSQL);
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_stmt *statement = nil;
        sqlite3_prepare_v2(ioDB, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"HomeViewList:IMAGE_GROUP_RELATION : successfully saved :");
        } else {
            DebugLog(@"HomeViewList:IMAGE_GROUP_RELATION : failed to save");
        }
        sqlite3_finalize(statement);
        sqlite3_close(ioDB);
    }
}
-(int)getGroupId:(NSString *)groupName
{
    int groupId = -1;
	if(sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK)
    {
        // Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"Select ID from GROUPS WHERE GROUP_NAME=\"%@\"",[groupName lowercaseString]];
        const char *sqlStatement = [querySQL UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(ioDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                groupId = sqlite3_column_int(compiledStatement, 0);
                DebugLog(@"groupId -> %d",groupId);
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(ioDB);
    return groupId;
}
-(NSMutableArray *)getImageAttachments:(int)rowId
{
    NSMutableArray *imageArray = [[NSMutableArray alloc] init];
    if(sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK)
    {
        NSString *imagequeryString = [NSString stringWithFormat:@"SELECT IMAGE_PATH FROM IMAGE_SET_RELATION WHERE IMAGE_MASTER_ID = %d AND IMAGE_PATH <> \"\" LIMIT 1",rowId];
        DebugLog(@"HomeViewList:------%@-----",imagequeryString);
        const char *imgqueryCharP = [imagequeryString UTF8String];
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, imgqueryCharP, -1, &stmt, NULL) == SQLITE_OK) {
                while(sqlite3_step(stmt) == SQLITE_ROW)
                {
                    NSString *path = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 0)];
                    DebugLog(@"HomeViewList:image path %@ ",path);
                    
                    [imageArray addObject:path];
                }
            }
            sqlite3_finalize(stmt);
        sqlite3_close(ioDB);
    }
    return imageArray;
}

@end
