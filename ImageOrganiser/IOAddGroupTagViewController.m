//
//  IOAddGroupTagViewController.m
//  ImageOrganiser
//
//  Created by Kirti Nikam on 04/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>

#import "IOAddGroupTagViewController.h"
#import "IOAppDelegate.h"
#import "GroupObject.h"
#import "TagObject.h"
#import "IOAddRecordViewController.h"
#import "CommonCallback.h"

@interface IOAddGroupTagViewController ()

@end

@implementation IOAddGroupTagViewController
@synthesize createGroupBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    [self addIconsInGroupScrollView];
    if ((_checkMarkTableView.frame.origin.y+_checkMarkTableView.contentSize.height) < self.view.frame.size.height-50){
        [_checkMarkTableView setFrame:CGRectMake(_checkMarkTableView.frame.origin.x,
                                             _checkMarkTableView.frame.origin.y,
                                             _checkMarkTableView.frame.size.width,
                                             _checkMarkTableView.contentSize.height)];
    }else{
        [_checkMarkTableView setFrame:CGRectMake(_checkMarkTableView.frame.origin.x,
                                             _checkMarkTableView.frame.origin.y,
                                             _checkMarkTableView.frame.size.width,
                                             self.view.frame.size.height-100)];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   // self.view.backgroundColor = BGCOLOR;
    self.view.backgroundColor = [UIColor clearColor];

    _checkMarkTableView.backgroundColor = [UIColor clearColor];
    _checkMarkTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _checkMarkTableView.separatorColor = [UIColor clearColor];
    _checkMarkTableView.layer.cornerRadius = 10.0;
    
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveGroupClicked:)];
    
//    UIButton *saveButton =  [UIButton buttonWithType:UIButtonTypeCustom];
//    [saveButton setTitle:@"Save" forState:UIControlStateNormal];
//    saveButton.titleLabel.font = DEFAULT_FONT(15.0);
//    [saveButton setBackgroundImage:[UIImage imageNamed:@"bar_button.png"] forState:UIControlStateNormal];
//    [saveButton addTarget:self action:@selector(saveGroupClicked:) forControlEvents:UIControlEventTouchUpInside];
//    [saveButton setFrame:CGRectMake(0, 0, 70, 30)];
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:saveButton];
    
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    viewTap.numberOfTapsRequired = 1;
    viewTap.cancelsTouchesInView = NO;
    [self.view  addGestureRecognizer:viewTap];

    _addView.backgroundColor = [UIColor whiteColor];
    _addView.layer.borderWidth = 2.0;
    _addView.layer.borderColor = DEFAULT_COLOR.CGColor;
    _addView.layer.cornerRadius = 2;
    _addView.hidden = YES;
    
    [CommonCallback setTextFieldPropertiesWithBorder:_textField text:@""];
    _textField.delegate = self;

    if (_curretCase == GROUPCASE)
    {
        self.title = @"Groups";
        [self readFromGroupsTempTable];
    }
//    else
//    {
//        self.title = @"Tags";
//    //    [self reArrangeFrameOfControlsInAddView];
//        DebugLog(@"AddGroup : 1)check rows count in readFromTagsTempTable");
//        if ([self getTagsTempRowsCount] > 0) {
//            DebugLog(@"AddGroup : 2)Already have data in readFromTagsTempTable");
//            [self readFromTagsTempTable];
//        }
//        else
//        {
//            DebugLog(@"AddGroup : 2)Dont have data in TagsTemp Table");
//            [self readFromTagsTable];
//            DebugLog(@"AddGroup : 3)check Is this New Record");
//            if (_imgId == -1) {
//                DebugLog(@"AddGroup : 4)Yes this is New Record");
//            }
//            else
//            {
//                DebugLog(@"AddGroup : 4)No this is Old Record %d",_imgId);
//                [self readSelectedTagsFromImageTagRelationTable];
//            }
//            DebugLog(@"AddGroup : 5)Insert All tags in TagsTempTable");
//            for (TagObject *tagObj in _tableArray) {
//                [self insertIntoTagsTemp:tagObj];
//            }
//        }
//
//    }
    DebugLog(@"AddGroup : tableArray %@",_tableArray);
    [_checkMarkTableView reloadData];

    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 50, 32)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
}

-(void)addIconsInGroupScrollView
{

    [_iconsGridScrollView setCanCancelContentTouches:NO];
	_iconsGridScrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
	_iconsGridScrollView.clipsToBounds = YES;		// default is NO, we want to restrict drawing within our gridScrollView
	_iconsGridScrollView.scrollEnabled = YES;
    _iconsGridScrollView.bounces = NO;
    _iconsGridScrollView.showsVerticalScrollIndicator = NO;
    _iconsGridScrollView.directionalLockEnabled = YES;
    _iconsGridScrollView.delegate = self;
    _iconsGridScrollView.pagingEnabled = YES;
    _iconsGridScrollView.backgroundColor = [UIColor blackColor];
    _iconsGridScrollView.layer.cornerRadius = 2.0;
    _iconsGridScrollView.layer.borderWidth = 2.0;
    _iconsGridScrollView.layer.borderColor = [UIColor grayColor].CGColor;
    _iconsGridScrollView.frame = CGRectMake(_iconsGridScrollView.frame.origin.x, _imageView.frame.origin.y+_imageView.frame.size.height, _iconsGridScrollView.frame.size.width, _iconsGridScrollView.frame.size.height);
    
    double NUMBEROFBUTTONSPERPAGE = 9.0;
    int pageCount = (int)ceil([_iconsDic.allKeys count]/NUMBEROFBUTTONSPERPAGE);
    DebugLog(@"AddGroup : [_iconsDic.allKeys count] %d",[_iconsDic.allKeys count]);

    DebugLog(@"AddGroup : pageCount %d",pageCount);
    for (int pageIndex= 1; pageIndex <= pageCount; pageIndex++)
	{
        //	int tag = pageIndex * 100;
        
        UIView *view=[[UIView alloc] init];
		CGRect rect;
        view.backgroundColor=[UIColor clearColor];
        
		rect.size.height = _iconsGridScrollView.frame.size.height;
		rect.size.width = _iconsGridScrollView.frame.size.width;
		view.frame = rect;
		view.tag = pageIndex;	// tag our images for later use when we place them in serial fashion
		
        int X=35,Y=25;
        int Hspace=30;
        int Vspace=30;
        
        
        int width=50;
        int height=50;
        int labelHeight=0;//20;
        //  int labelWidth=110;
        UIButton *button;
        
        CGRect frame;
        for (int buttonIndexOnPage=0;buttonIndexOnPage<NUMBEROFBUTTONSPERPAGE; buttonIndexOnPage++) {
            
            if ((buttonIndexOnPage+(NUMBEROFBUTTONSPERPAGE*(pageIndex-1))) == [_iconsDic.allKeys count])
            {
                break;
            }
            int index = (buttonIndexOnPage+(NUMBEROFBUTTONSPERPAGE*(pageIndex-1)));
            NSString *name = [_iconsDic valueForKey:[_iconsDic.allKeys objectAtIndex:index]];
            frame = CGRectMake(X, Y, width, height);
            button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = frame;
            button.backgroundColor=[UIColor clearColor];
            button.titleLabel.font=DEFAULT_BOLD_FONT(10);
            
            [button setImage:[UIImage imageNamed:name] forState:UIControlStateNormal];
            // [button setTitle:name forState:UIControlStateNormal];
            button.tag = [[_iconsDic.allKeys objectAtIndex:index] intValue]; //tag+buttonIndexOnPage;
            [button addTarget:self
                       action:@selector(groupIconBtnClicked:)forControlEvents:UIControlEventTouchUpInside];
            
            [view addSubview:button];
            
            button=nil;
            int NextX = X+width+Hspace;
            
            if (NextX < view.frame.size.width-60)
            {
                X=NextX;
            }
            else
            {
                X=35;
                Y=Y+height+labelHeight+Vspace;
            }
            
        }
        
        [_iconsGridScrollView addSubview:view];
    }
    [self layoutScrollImages:pageCount];
    [_iconsGridScrollView setHidden:YES];
}
- (void)layoutScrollImages:(int)count
{
    DebugLog(@"AddGroup : count %d",count);
	UIView *view = nil;
	NSArray *subviews = [_iconsGridScrollView subviews];
    
	// reposition all subviews in a horizontal serial fashion
	CGFloat curXLoc = 0;
	for (view in subviews)
	{
		if ([view isKindOfClass:[UIView class]] && view.tag > 0)
		{
			CGRect frame = view.frame;
			frame.origin = CGPointMake(curXLoc, 0);
			view.frame = frame;
			
			curXLoc += (_iconsGridScrollView.frame.size.width);
		}
	}
	
	// set the content size so it can be scrollable
	[_iconsGridScrollView setContentSize:CGSizeMake((count * _iconsGridScrollView.frame.size.width), _iconsGridScrollView.frame.size.height)];
}
-(void)saveGroupClicked:(id)sender
{
    DebugLog(@"AddGroup : saveGroupClicked");

}

- (void)backBtnPressed:(id)sender
{
    UIViewController *previousViewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    DebugLog(@"parent %@",previousViewController);
    if ([previousViewController isKindOfClass:[IOAddRecordViewController class]])
    {
        IOAddRecordViewController *parent = (IOAddRecordViewController *) previousViewController;
        if(parent != nil)
        {
            if (_curretCase == GROUPCASE) {
            [parent reloadGroups];
            }
            else
            {
                [parent reloadTags];
            }
        }
        parent = nil;
    }
    previousViewController = nil;
    [self.navigationController popViewControllerAnimated:YES];

}
//-(void) reArrangeFrameOfControlsInAddView
//{
//    if (_curretCase == TAGCASE)
//    {
//        _textField.frame = CGRectMake(_textField.frame.origin.x, _textField.frame.origin.y+20, _textField.frame.size.width, _textField.frame.size.height);
//
//        _saveBtn.frame = CGRectMake(_saveBtn.frame.origin.x, _textField.frame.origin.y+_textField.frame.size.height+20, _saveBtn.frame.size.width, _saveBtn.frame.size.height);
//       
//        _addView.frame = CGRectMake(_addView.frame.origin.x, _addView.frame.origin.y+10, _addView.frame.size.width,_cancelBtn.frame.origin.y+_cancelBtn.frame.size.height+20);
//        _imageView.hidden = YES;
//    }
//}
-(void)changeBtnStyle:(UIButton *)btn
{
    // Set the button Text Color
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    [btn setBackgroundColor:DEFAULT_COLOR];
    
    // Round button corners
    CALayer *btnLayer = [btn layer];
    [btnLayer setMasksToBounds:YES];
    [btnLayer setCornerRadius:5.0f];
    // Apply a 1 pixel, black border
    [btnLayer setBorderWidth:1.0f];
    [btnLayer setBorderColor:[DEFAULT_COLOR CGColor]];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setCheckMarkTableView:nil];
    [self setAddView:nil];
    [self setTextField:nil];
    [self setImageView:nil];
    [self setLbl:nil];
    [self setIconGridButton:nil];
    [self setIconsGridScrollView:nil];
    [self setCreateGroupBtn:nil];
    [super viewDidUnload];
}
#pragma mark Orientation methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate
{
    return NO;
}
- (NSUInteger)supportedInterfaceOrientations{
#if __IPHONE_OS_VERSION_MAX_ALLOWED <= __IPHONE_5_1
    return UIInterfaceOrientationPortrait;
#else
    return UIInterfaceOrientationMaskPortrait;
#endif
}
- (void)tapDetected:(UIGestureRecognizer *)sender {
    DebugLog(@"AddGroup : tapDetected");
    [self.view endEditing:YES];
}
-(void)addClicked:(id)sender
{
    DebugLog(@"AddGroup : addclicked");
    if (_curretCase == GROUPCASE) {
        DebugLog(@"AddGroup : Add Group");
        _lbl.text = @"Group";
        _textField.placeholder = @"Add New Group";
    }
    else
    {
        DebugLog(@"AddGroup : Add Tag");
        _lbl.text = @"Tag";
        _textField.placeholder = @"Add New Tag";
    }
    _textField.text = @"";
    [_textField becomeFirstResponder];
    _addView.hidden = NO;
}
-(void)editClicked:(id)sender
{
    
}


#pragma UITextField delegate callback methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
   	return YES;
}
#pragma mark Tableview DataSource methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    if (_curretCase == GROUPCASE) {
//        return [self getGroupsRowsCount];
//    }
//    else
//    {
//        return [self getTagsRowsCount];
//    }
    return [_tableArray count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"GroupCell";
    UIImageView *checkMarkImgView;
    UILabel *lblTitle;
    UITableViewCell *cell= (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(10,2,150, 30)];
        lblTitle.tag = 101;
        lblTitle.font = DEFAULT_BOLD_FONT(15.0);
        lblTitle.textAlignment = UITextAlignmentLeft;
        lblTitle.textColor = [UIColor whiteColor];
        lblTitle.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblTitle];
        
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        //         cell.backgroundColor = [UIColor redColor];
        cell.contentView.backgroundColor = TABLE_CELL_BGCOLOR;
        cell.layer.borderColor = TABLE_CELL_BGCOLOR.CGColor;
        cell.layer.borderWidth = 0;

        checkMarkImgView = [[UIImageView alloc] initWithFrame:CGRectMake(250,10, 20, 20)];
        checkMarkImgView.tag = 102;
        checkMarkImgView.contentMode = UIViewContentModeScaleToFill;
        checkMarkImgView.layer.masksToBounds = YES;
        checkMarkImgView.image = [UIImage imageNamed:@"check_mark_white.png"];
        [cell.contentView addSubview:checkMarkImgView];
    }
    
    if (_tableArray != nil && _tableArray.count > 0) {
        GroupObject *recObj = (GroupObject *)[_tableArray objectAtIndex:indexPath.row];
        DebugLog(@"AddGroup : groupCell %@",recObj.groupName);
        cell.tag = recObj.groupRowId;
        lblTitle = (UILabel *)[cell viewWithTag:101];
        lblTitle.text = [recObj.groupName capitalizedString];
        checkMarkImgView = (UIImageView *)[cell viewWithTag:102];
        if(recObj.selected)
        {
            checkMarkImgView.image = [UIImage imageNamed:@"check_mark_blue.png"];
            cell.contentView.backgroundColor = TABLE_EDITCELL_BGCOLOR;
            lblTitle.textColor = TABLE_CELL_BGCOLOR;
        }
        else
        {
            checkMarkImgView.image = nil;
            cell.contentView.backgroundColor = TABLE_CELL_BGCOLOR;
            lblTitle.textColor = [UIColor whiteColor];
        }

//        if (_curretCase == GROUPCASE)
//        {
//            GroupObject *recObj = (GroupObject *)[_tableArray objectAtIndex:indexPath.row];
//            cell.textLabel.text = [recObj.groupName capitalizedString];
//            cell.tag = recObj.groupRowId;
//         //   cell.imageView.image = [UIImage imageNamed:[_iconsDic valueForKey:[NSString stringWithFormat:@"%d",recObj.iconId]]];
//            if(recObj.selected)
//            {
//                cell.accessoryType = UITableViewCellAccessoryCheckmark;
//            }
//            else
//            {
//                cell.accessoryType = UITableViewCellAccessoryNone;
//            }
//        }
//        else{
//            TagObject *recObj = (TagObject *)[_tableArray objectAtIndex:indexPath.row];
//            cell.textLabel.text = recObj.tagName;
//            cell.textLabel.font = [UIFont systemFontOfSize:15.0];
//            cell.tag = recObj.tagRowId;
//            if(recObj.selected)
//            {
//                cell.accessoryType = UITableViewCellAccessoryCheckmark;
//            }
//            else
//            {
//                cell.accessoryType = UITableViewCellAccessoryNone;
//            }
//        }
    }
    return cell;
}

#pragma mark Tableview DataSource methods
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    UILabel *lblTitle = (UILabel *)[cell viewWithTag:101];
    UIImageView *checkMarkImgView = (UIImageView *)[cell viewWithTag:102];
    GroupObject *recObj = (GroupObject *)[_tableArray objectAtIndex:indexPath.row];
    if(recObj.selected)
    {
        checkMarkImgView.image = [UIImage imageNamed:@"check_mark_blue.png"];
        cell.contentView.backgroundColor = TABLE_EDITCELL_BGCOLOR;
        lblTitle.textColor = TABLE_CELL_BGCOLOR;
    }
    else
    {
        checkMarkImgView.image = nil;
        cell.contentView.backgroundColor = TABLE_CELL_BGCOLOR;
        lblTitle.textColor = [UIColor whiteColor];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    UILabel *lblTitle = (UILabel *)[cell viewWithTag:101];
    UIImageView *checkMarkImgView = (UIImageView *)[cell viewWithTag:102];
    
    GroupObject *recObj = (GroupObject *)[_tableArray objectAtIndex:indexPath.row];
    if(recObj.selected)
    {
        recObj.selected = 0;
        [self updateInGroupTemp:cell.tag selected:0];
        checkMarkImgView.image = nil;
        cell.contentView.backgroundColor = TABLE_CELL_BGCOLOR;
        lblTitle.textColor = [UIColor whiteColor];
    }
    else
    {
        recObj.selected = 1;
        [self updateInGroupTemp:cell.tag selected:1];
        checkMarkImgView.image = [UIImage imageNamed:@"check_mark_blue.png"];
        cell.contentView.backgroundColor = TABLE_EDITCELL_BGCOLOR;
        lblTitle.textColor = TABLE_CELL_BGCOLOR;
    }
    [_tableArray replaceObjectAtIndex:indexPath.row withObject:recObj];
    recObj = nil;
//
//    if(cell.accessoryType == UITableViewCellAccessoryCheckmark)
//    {
//        cell.accessoryType = UITableViewCellAccessoryNone;
//        if (_curretCase == GROUPCASE) {
//            [self updateInGroupTemp:cell.tag selected:0];
//        }
//        else
//        {
//            [self updateInTagTemp:cell.tag selected:0];
//        }
//    }
//    else
//    {
//        cell.accessoryType = UITableViewCellAccessoryCheckmark;
//        if (_curretCase == GROUPCASE) {
//            [self updateInGroupTemp:cell.tag selected:1];
//        }
//        else
//        {
//            [self updateInTagTemp:cell.tag selected:1];
//        }
//    }
}

- (void)tableView:(UITableView *)tv commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if(editingStyle == UITableViewCellEditingStyleDelete)
    {
        GroupObject *obj =[_tableArray objectAtIndex:indexPath.row];
        if (![obj.groupName isEqualToString:FAVOURITE_GROUPNAME] || ![obj.groupName isEqualToString:UNGROUPED_GROUPNAME] ) {
            [_tableArray removeObjectAtIndex:indexPath.row];
            [self DeleteGroup:((UITableViewCell *)[_checkMarkTableView cellForRowAtIndexPath:indexPath]).tag];
            [_checkMarkTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationMiddle];
        }
	}
}

//-------------------------------------------------------------------------------------------
//--------------------------------- SQLITE INTENAL CALLBACKS---------------------------------
//-------------------------------------------------------------------------------------------
// * GROUP_TABLE    : Original
// * TAGS_TABLE     : Original
// * GROUPS_TEMP_TABLE  : Temporary
// * TAGS_TEMP_TABLE    : Temporary
// * IMAGEMASTER_GROUP_RELATION_TABLE : Original
// * IMAGEMASTER_TAG_RELATION_TABLE   : Original
//-------------------------------------------------------------------------------------------
//--------------------------------- GROUPS TABLE --------------------------------------------
//-------------------------------------------------------------------------------------------
-(NSInteger)getGroupsRowsCount
{
    NSInteger rowCount = 0;
    
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
        NSString *queryString = [NSString stringWithFormat:@"SELECT count (*) FROM GROUPS"];
        const char *queryCharP = [queryString UTF8String];
        
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            if (sqlite3_step(stmt) == SQLITE_ROW) {
                rowCount = sqlite3_column_int(stmt, 0);
            }
        }
        sqlite3_finalize(stmt);
        sqlite3_close(ioDB);
    }
    DebugLog(@"GROUPS rowCount %d",rowCount);
    return rowCount;
}
-(void)readFromGroupsTable
{
    DebugLog(@"AddGroup : --readFromGroupsTable--");
    
    if (_tableArray == nil) {
        _tableArray = [[NSMutableArray alloc] init];
    }
    else
    {
        [_tableArray removeAllObjects];
    }
    GroupObject *groupObj = nil;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
        NSString *queryString = [NSString stringWithFormat:@"SELECT ID,GROUP_NAME,ICON_ID FROM GROUPS"];
        const char *queryCharP = [queryString UTF8String];
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            while (sqlite3_step(stmt) == SQLITE_ROW)
            {
                int rowId = sqlite3_column_int(stmt, 0);
                NSString *name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 1)];
                int iconId = sqlite3_column_int(stmt, 2);
                
                groupObj = [[GroupObject alloc]init];
                groupObj.groupRowId = rowId;
                groupObj.groupName = name;
                groupObj.iconId = iconId;
                groupObj.selected = 0;
                [_tableArray addObject:groupObj];
                
                groupObj = nil;
            }
        }
        sqlite3_finalize(stmt);
        sqlite3_close(ioDB);
    }
}
-(void)insertIntoGroups:(NSString *)groupName iconid:(int)iconId
{
    DebugLog(@"AddGroup : --insertIntoGroups--");
    int RowId = -1;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String],&ioDB) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO GROUPS(GROUP_NAME,ICON_ID) VALUES (\"%@\",%d)",[groupName lowercaseString],iconId];
        DebugLog(@"AddGroup : ---%@---",insertSQL);
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_stmt *statement = nil;
        sqlite3_prepare_v2(ioDB, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            RowId = sqlite3_last_insert_rowid(ioDB);
            DebugLog(@"AddGroup : GROUPS : successfully saved :");
            
        } else {
            DebugLog(@"AddGroup : GROUPS : failed to save");
        }
        sqlite3_finalize(statement);
        sqlite3_close(ioDB);
    }
    if (RowId != -1) {
        GroupObject *grp = [[GroupObject alloc] init];
        grp.groupRowId = RowId;
        grp.groupName = groupName;
        grp.selected = 1;
        grp.iconId = _imageView.tag;
        [self insertIntoGroupTemp:grp];
        
        [_tableArray addObject:grp];
        
        
        [_checkMarkTableView beginUpdates];
        [_checkMarkTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:_tableArray.count-1 inSection:0]] withRowAnimation:UITableViewRowAnimationMiddle];
        [_checkMarkTableView endUpdates];
        if (_checkMarkTableView.frame.origin.y+_checkMarkTableView.contentSize.height < self.view.frame.size.height-50) {
            [UIView animateWithDuration:0.5 animations:^{
                [_checkMarkTableView setFrame:CGRectMake(_checkMarkTableView.frame.origin.x,
                                                     _checkMarkTableView.frame.origin.y,
                                                     _checkMarkTableView.frame.size.width,
                                                     _checkMarkTableView.contentSize.height)];
                
            } completion:^(BOOL finished) {
            }];
        }
        grp = nil;
    }
}
- (void) DeleteGroup :(int) groupId
{
    sqlite3_stmt *statement;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM GROUPS WHERE ID=\"%d\"", groupId];
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(ioDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"AddGroup : deleted from GROUPS");
        } else {
            DebugLog(@"AddGroup : Failed to delete from GROUPS");
        }
        sqlite3_finalize(statement);
        sqlite3_close(ioDB);
        
        [self deleteFromGroupTemp:groupId];
    }
}

//-------------------------------------------------------------------------------------------
//--------------------------------- GROUPS_TEMP TABLE ---------------------------------------
//-------------------------------------------------------------------------------------------
-(NSInteger)getGroupsTempRowsCount
{
    NSInteger rowCount = 0;
    
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
        NSString *queryString = [NSString stringWithFormat:@"SELECT count (*) FROM GROUPS_TEMP"];
        const char *queryCharP = [queryString UTF8String];
        
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            if (sqlite3_step(stmt) == SQLITE_ROW) {
                rowCount = sqlite3_column_int(stmt, 0);
            }
        }
        sqlite3_finalize(stmt);
        sqlite3_close(ioDB);
    }
    DebugLog(@"GROUPS_TEMP rowCount %d",rowCount);
    return rowCount;
}
-(void)readFromGroupsTempTable
{
    DebugLog(@"AddGroup : --readFromGroupsTempTable--");
    
    if (_tableArray == nil) {
        _tableArray = [[NSMutableArray alloc] init];
    }
    else
    {
        [_tableArray removeAllObjects];
    }
    GroupObject *groupObj = nil;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
        NSString *queryString = [NSString stringWithFormat:@"SELECT GROUP_ID,GROUP_NAME,ICON_ID,SELECTED FROM GROUPS_TEMP"];
        const char *queryCharP = [queryString UTF8String];
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            while (sqlite3_step(stmt) == SQLITE_ROW)
            {
                int rowId = sqlite3_column_int(stmt, 0);
                NSString *name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 1)];
                int iconId = sqlite3_column_int(stmt, 2);
                int selected = sqlite3_column_int(stmt, 3);
                
                groupObj = [[GroupObject alloc]init];
                groupObj.groupRowId = rowId;
                groupObj.groupName = name;
                groupObj.iconId = iconId;
                groupObj.selected = selected;
                DebugLog(@"AddGroup : group Name : %@ selected %d",name,selected);
                [_tableArray addObject:groupObj];
                
                groupObj = nil;
            }
        }
        sqlite3_finalize(stmt);
        sqlite3_close(ioDB);
    }
}
-(void)insertIntoGroupTemp:(GroupObject *)groupObj
{
    DebugLog(@"AddGroup : --insertIntoGroupTemp--");
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String],&ioDB) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat:
                               @"INSERT INTO GROUPS_TEMP(GROUP_ID,GROUP_NAME,ICON_ID,SELECTED) VALUES (%d,\"%@\",%d,%d)",
                               groupObj.groupRowId,[groupObj.groupName lowercaseString],groupObj.iconId,groupObj.selected];
        DebugLog(@"AddGroup : ---%@---",insertSQL);
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_stmt *statement = nil;
        sqlite3_prepare_v2(ioDB, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"AddGroup : GROUPS_TEMP : successfully saved :");
        } else {
            DebugLog(@"AddGroup : GROUPS_TEMP : failed to save");
        }
        sqlite3_finalize(statement);
        sqlite3_close(ioDB);
    }
}
-(void)updateInGroupTemp:(int)groupId selected:(int)lvalue
{
    DebugLog(@"AddGroup : --updateInGroupTemp--");
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String],&ioDB) == SQLITE_OK)
    {
        NSString *updateSQL = [NSString stringWithFormat:@"UPDATE GROUPS_TEMP SET SELECTED = %d WHERE GROUP_ID=\"%d\"",lvalue, groupId];
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_stmt *statement = nil;
        sqlite3_prepare_v2(ioDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"AddGroup : updated in GROUPS_TEMP with %d",lvalue);
        } else {
            DebugLog(@"AddGroup : Failed to update in GROUPS_TEMP");
        }
        sqlite3_finalize(statement);
        sqlite3_close(ioDB);
    }
}

-(void)deleteFromGroupTemp:(int)groupId
{
    DebugLog(@"AddGroup : --deleteFromGroupTemp--");
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String],&ioDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM GROUPS_TEMP WHERE GROUP_ID=\"%d\"", groupId];
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_stmt *statement = nil;
        sqlite3_prepare_v2(ioDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"AddGroup : deleted from GROUPS_TEMP");
        } else {
            DebugLog(@"AddGroup : Failed to delete from GROUPS_TEMP");
        }
        sqlite3_finalize(statement);
        sqlite3_close(ioDB);
    }
}
//-------------------------------------------------------------------------------------------
//--------------------------------- TAGS TABLE ----------------------------------------------
//-------------------------------------------------------------------------------------------
-(NSInteger)getTagsRowsCount
{
    NSInteger rowCount = 0;
    
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
        NSString *queryString = [NSString stringWithFormat:@"SELECT count (*) FROM TAGS"];
        const char *queryCharP = [queryString UTF8String];
        
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            if (sqlite3_step(stmt) == SQLITE_ROW) {
                rowCount = sqlite3_column_int(stmt, 0);
            }
        }
        sqlite3_finalize(stmt);
        sqlite3_close(ioDB);
    }
    DebugLog(@"TAGS rowCount %d",rowCount);
    return rowCount;
}
-(void)readFromTagsTable
{
    DebugLog(@"AddGroup : --readFromTagsTable--");
    
    if (_tableArray == nil) {
        _tableArray = [[NSMutableArray alloc] init];
    }
    else
    {
        [_tableArray removeAllObjects];
    }
    TagObject *tagObj = nil;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
        NSString *queryString = [NSString stringWithFormat:@"SELECT ID,TAG_NAME FROM TAGS"];
        const char *queryCharP = [queryString UTF8String];
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            while (sqlite3_step(stmt) == SQLITE_ROW)
            {
                int rowId = sqlite3_column_int(stmt, 0);
                NSString *name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 1)];
                
                tagObj = [[TagObject alloc]init];
                tagObj.tagRowId = rowId;
                tagObj.tagName = name;
                tagObj.selected = 0;
                [_tableArray addObject:tagObj];
                
                tagObj = nil;
            }
        }
        sqlite3_finalize(stmt);
        sqlite3_close(ioDB);
    }
}
-(void)insertIntoTags:(NSString *)tagName
{
    DebugLog(@"AddGroup : --insertIntoTags--");
    int RowId = -1;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String],&ioDB) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO TAGS(TAG_NAME) VALUES (\"%@\")",tagName];
        DebugLog(@"AddGroup : ---%@---",insertSQL);
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_stmt *statement = nil;
        sqlite3_prepare_v2(ioDB, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            RowId = sqlite3_last_insert_rowid(ioDB);
            DebugLog(@"AddGroup : TAGS : successfully saved :");
            
        } else {
            DebugLog(@"AddGroup : TAGS : failed to save");
        }
        sqlite3_finalize(statement);
        sqlite3_close(ioDB);
    }
    if (RowId != -1) {
        TagObject *tg = [[TagObject alloc] init];
        tg.tagRowId = RowId;
        tg.tagName = tagName;
        tg.selected = 1;
        [self insertIntoTagsTemp:tg];
        
        [_tableArray addObject:tg];
        [_checkMarkTableView beginUpdates];
        [_checkMarkTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:_tableArray.count-1 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        [_checkMarkTableView endUpdates];
        
        tg = nil;
    }
}
- (void) DeleteTag :(int) tagId
{
    sqlite3_stmt *statement;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM TAGS WHERE ID=\"%d\"", tagId];
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(ioDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"AddGroup : deleted from TAGS");
        } else {
            DebugLog(@"AddGroup : Failed to delete from TAGS");
        }
        sqlite3_finalize(statement);
        sqlite3_close(ioDB);
        
        [self deleteFromGroupTemp:tagId];
    }
}

//-------------------------------------------------------------------------------------------
//--------------------------------- TAGS_TEMP TABLE -----------------------------------------
//-------------------------------------------------------------------------------------------
-(NSInteger)getTagsTempRowsCount
{
    NSInteger rowCount = 0;
    
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
        NSString *queryString = [NSString stringWithFormat:@"SELECT count (*) FROM TAGS_TEMP"];
        const char *queryCharP = [queryString UTF8String];
        
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            if (sqlite3_step(stmt) == SQLITE_ROW) {
                rowCount = sqlite3_column_int(stmt, 0);
            }
        }
        sqlite3_finalize(stmt);
        sqlite3_close(ioDB);
    }
    DebugLog(@"TAGS_TEMP rowCount %d",rowCount);
    return rowCount;
}
-(void)readFromTagsTempTable
{
    DebugLog(@"AddGroup : --readFromTagsTempTable--");
    
    if (_tableArray == nil) {
        _tableArray = [[NSMutableArray alloc] init];
    }
    else
    {
        [_tableArray removeAllObjects];
    }
    TagObject *tagObj = nil;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
        NSString *queryString = [NSString stringWithFormat:@"SELECT TAG_ID,TAG_NAME,SELECTED FROM TAGS_TEMP"];
        const char *queryCharP = [queryString UTF8String];
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            while (sqlite3_step(stmt) == SQLITE_ROW)
            {
                int rowId = sqlite3_column_int(stmt, 0);
                NSString *name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 1)];
                int selected = sqlite3_column_int(stmt, 2);
                
                tagObj = [[TagObject alloc]init];
                tagObj.tagRowId = rowId;
                tagObj.tagName = name;
                tagObj.selected = selected;
                [_tableArray addObject:tagObj];
                
                tagObj = nil;
            }
        }
        sqlite3_finalize(stmt);
        sqlite3_close(ioDB);
    }
}

-(void)insertIntoTagsTemp:(TagObject *)tagObj
{
    DebugLog(@"AddGroup : --insertIntoGroupTemp--");
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String],&ioDB) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat:
                               @"INSERT INTO TAGS_TEMP(TAG_ID,TAG_NAME,SELECTED) VALUES (%d,\"%@\",%d)",
                               tagObj.tagRowId,tagObj.tagName,tagObj.selected];
        DebugLog(@"AddGroup : ---%@---",insertSQL);
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_stmt *statement = nil;
        sqlite3_prepare_v2(ioDB, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"AddGroup : TAGS_TEMP : successfully saved :");
        } else {
            DebugLog(@"AddGroup : TAGS_TEMP : failed to save");
        }
        sqlite3_finalize(statement);
        sqlite3_close(ioDB);
    }
}
-(void)updateInTagTemp:(int)tagId selected:(int)lvalue
{
    DebugLog(@"AddGroup : --updateInTagTemp--");
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String],&ioDB) == SQLITE_OK)
    {
        NSString *updateSQL = [NSString stringWithFormat:@"UPDATE TAGS_TEMP SET SELECTED = %d WHERE TAG_ID=\"%d\"",lvalue, tagId];
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_stmt *statement = nil;
        sqlite3_prepare_v2(ioDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"AddGroup : updated in TAGS_TEMP with %d",lvalue);
        } else {
            DebugLog(@"AddGroup : Failed to update in TAGS_TEMP");
        }
        sqlite3_finalize(statement);
        sqlite3_close(ioDB);
    }
}

-(void)deleteFromTagTemp:(int)tagId
{
    DebugLog(@"AddGroup : --deleteFromTagTemp--");
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String],&ioDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM TAGS_TEMP WHERE TAG_ID=\"%d\"", tagId];
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_stmt *statement = nil;
        sqlite3_prepare_v2(ioDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"AddGroup : deleted from TAGS_TEMP");
        } else {
            DebugLog(@"AddGroup : Failed to delete from TAGS_TEMP");
        }
        sqlite3_finalize(statement);
        sqlite3_close(ioDB);
    }
}

//-------------------------------------------------------------------------------------------
//--------------------------------- IMAGEMASTER_GROUP_RELATION TABLE ------------------------
//-------------------------------------------------------------------------------------------
-(void)readSelectedGroupsFromImageGroupRelationTable
{
    NSMutableArray *groupIDArray = [[NSMutableArray alloc] init];
    
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
        NSString *queryString = [NSString stringWithFormat:@"SELECT ID FROM GROUPS WHERE ID IN (SELECT GROUP_ID FROM IMAGE_GROUP_RELATION WHERE IMAGE_MASTER_ID = %d)",_imgId];
        const char *queryCharP = [queryString UTF8String];
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            while (sqlite3_step(stmt) == SQLITE_ROW)
            {
                int rowId = sqlite3_column_int(stmt, 0);
                [groupIDArray addObject:[NSNumber numberWithInt:rowId]];
            }
        }
        sqlite3_finalize(stmt);
        sqlite3_close(ioDB);
    }
    DebugLog(@"AddGroup : groupIDArray = %@ and tableArray %@",groupIDArray,_tableArray);
    GroupObject *gpObj = nil;
    for(int i = 0; i < groupIDArray.count ; i++ )
    {
        for(int arrIndex = 0; arrIndex < _tableArray.count ; arrIndex++ )
        {
            gpObj = (GroupObject *)[_tableArray objectAtIndex:arrIndex];
            DebugLog(@"AddGroup : gpObg.id = %d groupId %@",gpObj.groupRowId,[groupIDArray objectAtIndex:i]);
            if (gpObj.groupRowId   == [[groupIDArray objectAtIndex:i] intValue])
            {
                gpObj.selected = 1;
                DebugLog(@"AddGroup : replacing objectat index = %d",arrIndex);
                [_tableArray replaceObjectAtIndex:arrIndex withObject:gpObj];
                gpObj = nil;
                break;
            }
            gpObj = nil;
        }
    }
}
//-------------------------------------------------------------------------------------------
//--------------------------------- IMAGEMASTER_TAG_RELATION TABLE --------------------------
//-------------------------------------------------------------------------------------------
-(void)readSelectedTagsFromImageTagRelationTable
{
    NSMutableArray *tagIDArray = [[NSMutableArray alloc] init];
    
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
        NSString *queryString = [NSString stringWithFormat:@"SELECT ID FROM TAGS WHERE ID = (SELECT TAG_ID FROM IMAGE_TAG_RELATION WHERE IMAGE_MASTER_ID = %d)",_imgId];
        const char *queryCharP = [queryString UTF8String];
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            while (sqlite3_step(stmt) == SQLITE_ROW)
            {
                int rowId = sqlite3_column_int(stmt, 0);
                [tagIDArray addObject:[NSNumber numberWithInt:rowId]];
            }
        }
        sqlite3_finalize(stmt);
        sqlite3_close(ioDB);
    }
    DebugLog(@"AddGroup : tagIDArray = %@ and tableArray %@",tagIDArray,_tableArray);
    TagObject *tagObj = nil;
    for(int i = 0; i < tagIDArray.count ; i++ )
    {
        for(int arrIndex = 0; arrIndex < _tableArray.count ; arrIndex++ )
        {
            tagObj = (TagObject *)[_tableArray objectAtIndex:arrIndex];
            DebugLog(@"AddGroup : gpObg.id = %d groupId %@",tagObj.tagRowId,[tagIDArray objectAtIndex:i]);
            if (tagObj.tagRowId   == [[tagIDArray objectAtIndex:i] intValue])
            {
                tagObj.selected = 1;
                DebugLog(@"AddGroup : replacing objectat index = %d",arrIndex);
                [_tableArray replaceObjectAtIndex:arrIndex withObject:tagObj];
                tagObj = nil;
                break;
            }
            tagObj = nil;
        }
    }
}
//-------------------------------------------------------------------------------------------
//--------------------------------- END OF SQLITE INTERNAL CALLBACKS ------------------------
//-------------------------------------------------------------------------------------------
-(void)readIcons
{
    DebugLog(@"AddGroup : --readFromGroupsTempTable--");
    
    if (_groupIconArray == nil) {
        _groupIconArray = [[NSMutableArray alloc] init];
    }
    else
    {
        [_groupIconArray removeAllObjects];
    }

    [_groupIconArray addObject:@"folder1.png"];
    [_groupIconArray addObject:@"folder2.png"];
    [_groupIconArray addObject:@"folder3.png"];
    [_groupIconArray addObject:@"folder4.png"];
    [_groupIconArray addObject:@"folder5.png"];
    [_groupIconArray addObject:@"folder6.png"];
}
//- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
//{
//    return 1;
//}
//- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
//    
//    return [_groupIconArray count];
//}
////- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
////{
////    if(component == Primbanda)
////        return [_groupIconArray objectAtIndex:row];
////    return 0;
////    
////}
//- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
//{
//    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)]; //Set desired frame
//    UIImageView *myImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[_groupIconArray objectAtIndex:row]]];
//    [customView addSubview:myImageView];
//    return customView;
//}
//- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
//{
//}

- (IBAction)iconGridButtonClicked:(id)sender {
    DebugLog(@"AddGroup : openGroupIconGridBtnClicked");
    if (_iconsGridScrollView.hidden) {
        [_iconsGridScrollView setHidden:NO];
    }
    else
    {
        [_iconsGridScrollView setHidden:YES];
    }
}

- (void)groupIconBtnClicked:(id)sender {
    DebugLog(@"AddGroup : groupIconBtnClicked");
    UIButton *btn = (UIButton *)sender;
    DebugLog(@"AddGroup : You Clicked %d %@",btn.tag,[_iconsDic valueForKey:[NSString stringWithFormat:@"%d",btn.tag]]);
    _imageView.image   = [UIImage imageNamed:[_iconsDic valueForKey:[NSString stringWithFormat:@"%d",btn.tag]]];
    _imageView.tag     = btn.tag;
    [self iconGridButtonClicked:nil];
}

- (IBAction)createGroupBtnClicked:(id)sender
{
    if ([_textField.text isEqualToString:@""]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                            message:@"Enter group name"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
    else
    {
        if (_curretCase == GROUPCASE) {
            NSString *groupName = _textField.text;
            if ([self alredyExists:groupName] != -1) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                    message:[NSString stringWithFormat:@"%@ group already exists",groupName]
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                [alertView show];
            }
            else
            {
                [self insertIntoGroups:groupName iconid:_imageView.tag];
            }
        }
        else
        {
            NSString *tagName = _textField.text;
            [self insertIntoTags:tagName];
        }
    }
}

-(BOOL)alredyExists:(NSString *)groupName
{
    int groupId = -1;
	if(sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK)
    {
        // Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"Select ID from GROUPS WHERE GROUP_NAME=\"%@\"",[groupName lowercaseString]];
        const char *sqlStatement = [querySQL UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(ioDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                groupId = sqlite3_column_int(compiledStatement, 0);
                DebugLog(@"groupId -> %d",groupId);
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(ioDB);
    return groupId;
}

@end
