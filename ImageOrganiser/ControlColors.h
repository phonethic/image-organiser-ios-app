//
//  ControlColors.h
//  ImageOrganiser
//
//  Created by Kirti Nikam on 02/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ControlColors : NSObject
+(UIColor *)getViewBackgroundColor;
+(UIColor *)getTableViewBackgroundColor;
+(UIColor *)getTableViewCellBackgroundColor;
+(UIColor *)getTableViewEditCellBackgroundColor;
+(UIColor *)getScrollViewBackgroundColor;
+(UIColor *)getTextViewBackgroundColor;
+(UIColor *)getButtonBackgroundColor;
+(UIColor *)getLabelBackgroundColor;
+(UIColor *)getTextFieldBackgroundColor;

+(UIColor *)getTableViewEditCellTextColor;
+(UIColor *)getTextViewTextColor;
+(UIColor *)getButtonTextColor;
+(UIColor *)getLabelTextColor;
+(UIColor *)getTextFieldTextColor;

+(UIColor *)getNotesViewBackgroundColor;
+(UIColor *)getNotesViewTextColor;
@end
