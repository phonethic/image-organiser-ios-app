//
//  IOWebViewController.h
//  ImageOrganiser
//
//  Created by Kirti Nikam on 02/05/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IOWebViewController : UIViewController
@property (copy,nonatomic) NSString *urlString;
@property (copy,nonatomic) NSString *urlContent;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinnerIndicator;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *barbackBtn;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *barfwdBtn;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *barrefreshBtn;
@property (retain, nonatomic) IBOutlet UIToolbar *webviewtoolBar;

- (IBAction)webviewbackBtnPressed:(id)sender;
- (IBAction)webviewfwdBtnPressed:(id)sender;
- (IBAction)webviewrefreshBtnPressed:(id)sender;
@end
