//
//  IOAddRecordPageViewController.h
//  ImageOrganiser
//
//  Created by Kirti Nikam on 09/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IOAddRecordPageViewController : UIViewController
@property (nonatomic, readwrite) int currentImgRowId;
@end
