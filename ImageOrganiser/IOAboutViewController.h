//
//  IOAboutViewController.h
//  ImageOrganiser
//
//  Created by Kirti Nikam on 10/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface IOAboutViewController : UIViewController <MFMailComposeViewControllerDelegate,UIActionSheetDelegate>
@property (strong, nonatomic) IBOutlet UITableView *aboutTableView;
@property (strong, nonatomic) IBOutlet UILabel *appVersionLabel;
@end
