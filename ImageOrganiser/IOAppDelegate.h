//
//  IOAppDelegate.h
//  ImageOrganiser
//
//  Created by Kirti Nikam on 02/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "constants.h"
#define IO_APP_DELEGATE (IOAppDelegate *)[[UIApplication sharedApplication] delegate]
#define FAVOURITE_GROUPNAME @"Favourite"
#define UNGROUPED_GROUPNAME @"UnGrouped"

@class SplashScreenControllerViewController;

@interface IOAppDelegate : UIResponder <UIApplicationDelegate>
{
    Reachability* internetReach;
    Boolean networkavailable;
    NSCache *imageCache;

}
@property (nonatomic,strong)  NSCache *imageCache;


@property(nonatomic) Boolean networkavailable;
@property (nonatomic, retain)  NSString *databasePath;

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,strong)  UINavigationController *navController;
@property (nonatomic,strong)  SplashScreenControllerViewController *splashScreenViewController;


- (UIImage *)loadImage:(NSString *)limgName;
- (NSString *)saveImage: (UIImage*)image;
- (void)removeImage:(NSString*)lpath;

- (UIImage *)blueBarBackground;
-(NSString *)doCapitalFirstLetter:(NSString *)string;
-(void) removeSplashScreen;
@end
