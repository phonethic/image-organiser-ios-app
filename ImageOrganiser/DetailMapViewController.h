//
//  DetailMapViewController.h
//  ImageOrganiser
//
//  Created by Kirti Nikam on 15/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface DetailMapViewController : UIViewController<CLLocationManagerDelegate,NSURLConnectionDelegate,NSXMLParserDelegate,MKMapViewDelegate>
{
    CLLocationManager *locationManager;
    NSMutableData *webData;
	NSURLConnection *conn;
    NSXMLParser *xmlParser;
    BOOL elementFound;
}
@property (copy,nonatomic) NSString *reverseAddress;
@property (readwrite,nonatomic) double geoPointLatitude;
@property (readwrite,nonatomic) double geoPointLongitude;

@property (strong, nonatomic) IBOutlet MKMapView *mapView;

@end
