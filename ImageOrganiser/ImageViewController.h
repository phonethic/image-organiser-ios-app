//
//  ImageViewController.h
//  CarUtil
//
//  Created by Rishi Saxena on 03/04/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewController : UIViewController {
    int pageNumber;
}
@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property (copy,nonatomic) NSString *imagePath;
- (id)initWithPageNumber:(int)page imagepath:(NSString *)limagePath;
@end

