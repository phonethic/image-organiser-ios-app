//
//  CommonCallback.h
//  ImageOrganiser
//
//  Created by Kirti Nikam on 13/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommonCallback : NSObject

+(UIImage *)getFlatImage:(UIColor *)color;

+(void)roundeWithBorder:(UIView *)view;

+(void)addShadow:(UIView *)view;
+(void)removeShadow:(UIView *)view;

+(void)changeButtonFontAndTextColor:(UIButton *)btn textColor:(UIColor *)ltextColor bgcolor:(UIColor *)lbackgroundColor;

+(void)setTextFieldProperties:(UITextField *)textField text:(NSString *)textString;
+(void)setTextFieldPropertiesWithBorder:(UITextField *)textField text:(NSString *)textString;

+(void)setTextViewProperties:(UITextView *)textView text:(NSString *)textString;

+(void)setLabelProperties:(UILabel *)label text:(NSString *)textString;

+(UIImage *)separatorImage;
+ (UIImage *)resizeImage:(UIImage*)image newSize:(CGSize)newSize;
@end
