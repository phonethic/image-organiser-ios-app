//
//  ImageFilteringViewController.m
//  ImageOrganiser
//
//  Created by Kirti Nikam on 03/05/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "ImageFilteringViewController.h"
#import <CoreImage/CoreImage.h>
#import "IOAppDelegate.h"
#import "IOAddRecordViewController.h"
#import "CommonCallback.h"
#import "UIImage+fixOrientation.h"

#define CONTRAST 0
#define BRIGHTNESS 1

#define radians(degrees) ((degrees * M_PI) / 180.0)

typedef enum {
    ALPHA = 0,
    BLUE = 1,
    GREEN = 2,
    RED = 3
} PIXELS;

#pragma image processing
CFDataRef CopyImagePixels(CGImageRef inImage){
    return CGDataProviderCopyData(CGImageGetDataProvider(inImage));
}
@interface ImageFilteringViewController ()
{
    CIContext *filtercontext;
    CIFilter *filter;
    CIImage *beginImage;
    UIImageOrientation orientation;
}
@end

@implementation ImageFilteringViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self UISetUp];
    DebugLog(@"ImageFilerting : _imageObjectToFilter.imagePath = %@ isNew %d isDeleted %d isUpdated %d" , _imageObjectToFilter.imagePath,_imageObjectToFilter.isNew,_imageObjectToFilter.isDeleted,_imageObjectToFilter.isUpdated);
    
    if (_firsttime) {
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelBtnPressed:)];
//        UIButton *cancelbutton =  [UIButton buttonWithType:UIButtonTypeCustom];
//        [cancelbutton setTitle:@"Cancel" forState:UIControlStateNormal];
//        cancelbutton.titleLabel.font = DEFAULT_FONT(15.0);
//        [cancelbutton setBackgroundImage:[UIImage imageNamed:@"bar_button.png"] forState:UIControlStateNormal];
//        [cancelbutton addTarget:self action:@selector(cancelBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
//        [cancelbutton setFrame:CGRectMake(0, 0, 70, 30)];
//        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:cancelbutton];
    }
    else
    {

        UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(cancelBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [button setFrame:CGRectMake(0, 0, 50, 32)];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    }
    
    if (_imageObjectToFilter) {
        DebugLog(@"ImageFilerting : _imageObjectToFilter.imagePath = %@" , _imageObjectToFilter.imagePath);
        if(_imageObjectToFilter.imagePath != nil) {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString* fullPath = [documentsDirectory stringByAppendingPathComponent:_imageObjectToFilter.imagePath];
            DebugLog(@"fullpath = %@" , fullPath);
            UIImage* image = [UIImage imageWithContentsOfFile:fullPath];
            
            if(image != nil)
            {
                _imageView.image = image;
                originalImageByCamara = image;
                changedImage = image;
                
                // In order to find out what filters are available, we can use the [CIFilter filterNamesInCategory:kCICategoryBuiltIn]
                filter = [CIFilter filterWithName:@"CIColorControls"];
                filtercontext = [CIContext contextWithOptions:nil];
                beginImage = [[CIImage alloc] initWithImage:image];
                [filter setValue:beginImage forKey: kCIInputImageKey];
                
             self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneImageClicked:)];
//                UIButton *donebutton =  [UIButton buttonWithType:UIButtonTypeCustom];
//                [donebutton setTitle:@"Done" forState:UIControlStateNormal];
//                donebutton.titleLabel.font = DEFAULT_FONT(15.0);
//                [donebutton setBackgroundImage:[UIImage imageNamed:@"bar_button.png"] forState:UIControlStateNormal];
//                [donebutton addTarget:self action:@selector(doneImageClicked:) forControlEvents:UIControlEventTouchUpInside];
//                [donebutton setFrame:CGRectMake(0, 0, 70, 30)];
//                self.navigationItem.rightBarButtonItem  = [[UIBarButtonItem alloc] initWithCustomView:donebutton];
            
            }else
            {
                DebugLog(@"ImageFilerting : Image not found");
                _originalImageBarBtn.enabled = FALSE;
                _contrastImageBarBtn.enabled = FALSE;
                _brightImageBarBtn.enabled = FALSE;
                _grayScaleImageBarBtn.enabled = FALSE;
                _rotateBarBtn.enabled = FALSE;
            }
        }
    }
	[_slider setHidden:YES];
   // [self logAllFilters];
}
-(void)logAllFilters {
    NSArray *properties = [CIFilter filterNamesInCategory:
                           kCICategoryBuiltIn];
    DebugLog(@"ImageFilerting : %@", properties);
    for (NSString *filterName in properties) {
        CIFilter *fltr = [CIFilter filterWithName:filterName];
        DebugLog(@"ImageFilerting : %@", [fltr attributes]);
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setImageView:nil];
    [self setSlider:nil];
//    [self setToolbarView:nil];
//    [self setOriginalImageBtn:nil];
//    [self setContrastImageBtn:nil];
//    [self setBrightImageBtn:nil];
//    [self setGrayScaleImageBtn:nil];
//    [self setRotateBtn:nil];
    [self setToolBar:nil];
    [self setOriginalImageBarBtn:nil];
    [self setContrastImageBarBtn:nil];
    [self setBrightImageBarBtn:nil];
    [self setGrayScaleImageBarBtn:nil];
    [self setRotateBarBtn:nil];
    [super viewDidUnload];
}

-(void)UISetUp
{
//    _toolbarView.backgroundColor = TABLE_CELL_BGCOLOR;
    
    UIImage *toolbarimage = [CommonCallback getFlatImage:TABLE_CELL_BGCOLOR];
    
    if ([[UIToolbar class] respondsToSelector:@selector(appearance)]) {
        [_toolBar setBackgroundImage:toolbarimage forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
        [_toolBar setBackgroundImage:toolbarimage forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsLandscapePhone];
    }
    _toolBar.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
    
    UIImage *minImage = [UIImage imageNamed:@"slider_minimum.png"];
    UIImage *maxImage = [UIImage imageNamed:@"slider_maximum.png"];
    UIImage *thumbImage = [UIImage imageNamed:@"thumb.png"];
    
    [[UISlider appearance] setMaximumTrackImage:maxImage forState:UIControlStateNormal];
    [[UISlider appearance] setMinimumTrackImage:minImage forState:UIControlStateNormal];
    [[UISlider appearance] setThumbImage:thumbImage forState:UIControlStateNormal];
    
}


- (IBAction)originalImageBarBtnClicked:(id)sender {
}

- (IBAction)originalImageBtnClicked:(id)sender {
    [_slider setHidden:YES];
    _imageView.image = originalImageByCamara;
    changedImage = originalImageByCamara;
}

- (IBAction)contrastImageBtnClicked:(id)sender {
    [_slider setHidden:NO];
    currentFilter = CONTRAST;
    
    [_slider setMaximumValue:4.0];
    [_slider setMinimumValue:0.25];
    [_slider setValue:1.0];
}

- (IBAction)brightImageBtnClicked:(id)sender {
    [_slider setMinimumValue:-1];
	[_slider setMaximumValue:1];
	[_slider setValue:0];
    [_slider setHidden:NO];
    //    changedImage = _imageView.image;
    currentFilter = BRIGHTNESS;
}

- (IBAction)grayScaleImageBtnClicked:(id)sender {
    [_slider setHidden:YES];
    UIImage* gray = [self convertToGrayscale:originalImageByCamara];
    _imageView.image = gray;
    changedImage = gray;
}

- (IBAction)rotateBtnClicked:(id)sender {

    rotationInt=rotationInt+1;
	if(rotationInt==4)
		rotationInt=0;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
	switch(rotationInt)
	{
		case 0:
			
			_imageView.image=[UIImage imageWithCGImage:(CGImageRef)_imageView.image.CGImage scale:_imageView.image.scale orientation:UIImageOrientationUp];
            
			break;
		case 1:
			
			_imageView.image=[UIImage imageWithCGImage:(CGImageRef)_imageView.image.CGImage scale:_imageView.image.scale orientation:UIImageOrientationRight];
			
			
			
			break;
			
		case 2:
			_imageView.image=[UIImage imageWithCGImage:(CGImageRef)_imageView.image.CGImage scale:_imageView.image.scale orientation:UIImageOrientationDown];
			
			
			break;
		case 3:
			_imageView.image=[UIImage imageWithCGImage:(CGImageRef)_imageView.image.CGImage scale:_imageView.image.scale orientation:UIImageOrientationLeft];
			
			
			break;
			
	}
    [UIView commitAnimations];
}

- (IBAction)sliderDidSlide:(UISlider *)sender {
   // changedImage = _imageView.image;
//    DebugLog(@"ImageFilerting : slider value %f",_slider.value);
//    if (currentFilter == CONTRAST) {
//        [self contrastFilter:_slider.value];
//    }
//    else
//    {
//       [self brightnessFilter:_slider.value];
//     }
    
    float slideValue = [sender value];
    
    if (currentFilter== BRIGHTNESS)
        [filter setValue:[NSNumber numberWithFloat:slideValue]  forKey:@"inputBrightness"];
    if (currentFilter == CONTRAST)
        [filter setValue:[NSNumber numberWithFloat:slideValue] forKey:@"inputContrast"];
    
    beginImage = [CIImage imageWithCGImage:changedImage.CGImage];//[[CIImage alloc] initWithImage:changedImage];

    [filter setValue:beginImage forKey: kCIInputImageKey];

    CIImage *outputImage = [filter outputImage];
    CGImageRef cgimg = [filtercontext createCGImage:outputImage fromRect:[outputImage extent]];
    UIImage *newImg = [UIImage imageWithCGImage:cgimg];
    [_imageView setImage:newImg];    
    CGImageRelease(cgimg);    
}

- (UIImage *)convertToGrayscale:(UIImage *)oldImage {
    CGSize size = oldImage.size;
    int width = size.width;
    int height = size.height;
    
    // the pixels will be painted to this array
    uint32_t *pixels = (uint32_t *) malloc(width * height * sizeof(uint32_t));
    
    // clear the pixels so any transparency is preserved
    memset(pixels, 0, width * height * sizeof(uint32_t));
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // create a context with RGBA pixels
    CGContextRef context = CGBitmapContextCreate(pixels, width, height, 8, width * sizeof(uint32_t), colorSpace,
                                                 kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedLast);
    
    // paint the bitmap to our context which will fill in the pixels array
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), oldImage.CGImage);
    
    for(int y = 0; y < height; y++) {
        for(int x = 0; x < width; x++) {
            uint8_t *rgbaPixel = (uint8_t *) &pixels[y * width + x];
            
            // convert to grayscale using recommended method: http://en.wikipedia.org/wiki/Grayscale#Converting_color_to_grayscale
            uint32_t gray = 0.3 * rgbaPixel[RED] + 0.59 * rgbaPixel[GREEN] + 0.11 * rgbaPixel[BLUE];
            
            // set the pixels to gray
            rgbaPixel[RED] = gray;
            rgbaPixel[GREEN] = gray;
            rgbaPixel[BLUE] = gray;
        }
    }
    
    // create a new CGImageRef from our context with the modified pixels
    CGImageRef image = CGBitmapContextCreateImage(context);
    
    // we're done with the context, color space, and pixels
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    free(pixels);
    
    // make a new UIImage to return
    UIImage *resultUIImage = [UIImage imageWithCGImage:image];
    
    // we're done with image now too
    CGImageRelease(image);
    
    return resultUIImage;
}

-(void)contrastFilter:(float)value{
    CGImageRef img      =   changedImage.CGImage;
    CFDataRef dataref   =   CopyImagePixels(img);
    UInt8 *data=(UInt8 *)CFDataGetBytePtr(dataref);
    int length=CFDataGetLength(dataref);
    for(int index=0;index<length;index+=4){
        
        int alphaCount = data[index+0];
        int redCount = data[index+1];
        int greenCount = data[index+2];
        int blueCount = data[index+3];
              
        alphaCount = ((alphaCount-128)*value ) + 128;
        if (alphaCount < 0) alphaCount = 0; if (alphaCount>255) alphaCount =255;
        data[index+0] = (Byte) alphaCount;
        
        redCount = ((redCount-128)*value ) + 128;
        if (redCount < 0) redCount = 0; if (redCount>255) redCount =255;
        data[index+1] = (Byte) redCount;
        
        greenCount = ((greenCount-128)*value ) + 128;
        if (greenCount < 0) greenCount = 0; if (greenCount>255) greenCount =255;
        data[index+2] = (Byte) greenCount;
        
        blueCount = ((blueCount-128)*value ) + 128;
        if (blueCount < 0) blueCount = 0; if (blueCount>255) blueCount =255;
        data[index+3] = (Byte) blueCount;
    }
    size_t width=CGImageGetWidth(img);
    size_t height=CGImageGetHeight(img);
    size_t bitsPerComponent=CGImageGetBitsPerComponent(img);
    size_t bitsPerPixel=CGImageGetBitsPerPixel(img);
    size_t bytesPerRow=CGImageGetBytesPerRow(img);
    CGColorSpaceRef colorspace=CGImageGetColorSpace(img);
    CGBitmapInfo bitmapInfo=CGImageGetBitmapInfo(img);
    CFDataRef newData=CFDataCreate(NULL,data,length);
    CGDataProviderRef provider=CGDataProviderCreateWithCFData(newData);
    CGImageRef newImg=CGImageCreate(width,height,bitsPerComponent,bitsPerPixel,bytesPerRow,colorspace,bitmapInfo,provider,NULL,true,kCGRenderingIntentDefault);
    [_imageView setImage:[UIImage imageWithCGImage:newImg]];
    CGImageRelease(newImg);
    CGDataProviderRelease(provider);
    CFRelease(newData);
	CFRelease(dataref);
}

-(void)brightnessFilter:(float)value{
	CGImageRef img  = changedImage.CGImage;
    DebugLog(@"ImageFilerting : img %@",img);
	if(img==nil)
        return;
	CFDataRef dataref=CopyImagePixels(img);
	UInt8 *data=(UInt8 *)CFDataGetBytePtr(dataref);
	int length=CFDataGetLength(dataref);
	for(int index=0;index<length;index+=4){
		// BRIGHTNESS
		for(int i=0;i<3;i++){
			if(data[index+i]+value<0){
				data[index+i]=0;
			}else{
				if(data[index+i]+value>255){
					data[index+i]=255;
				}else{
					data[index+i]+=value;
				}
			}
		}
		
		
	}
	size_t width=CGImageGetWidth(img);
	size_t height=CGImageGetHeight(img);
	size_t bitsPerComponent=CGImageGetBitsPerComponent(img);
	size_t bitsPerPixel=CGImageGetBitsPerPixel(img);
	size_t bytesPerRow=CGImageGetBytesPerRow(img);
	CGColorSpaceRef colorspace=CGImageGetColorSpace(img);
	CGBitmapInfo bitmapInfo=CGImageGetBitmapInfo(img);
	CFDataRef newData=CFDataCreate(NULL,data,length);
	CGDataProviderRef provider=CGDataProviderCreateWithCFData(newData);
	CGImageRef newImg=CGImageCreate(width,height,bitsPerComponent,bitsPerPixel,bytesPerRow,colorspace,bitmapInfo,provider,NULL,true,kCGRenderingIntentDefault);
	[_imageView setImage:[UIImage imageWithCGImage:newImg]];
	CGImageRelease(newImg);
	CGDataProviderRelease(provider);
	CFRelease(newData);
	CFRelease(dataref);
}
-(void)doneImageClicked:(id)sender
{
    if (![_imageView.image isEqual:originalImageByCamara]) {
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,  NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
       
        if (_imageObjectToFilter.isUpdated) // 2 or 3 time updating same image
        {
            NSString *newfilename = _imageObjectToFilter.imagePath;
            DebugLog(@"ImageFilerting : newfilename name  = %@" , newfilename);
            NSString* newfullPath = [documentsDirectory stringByAppendingPathComponent:newfilename];
            
            UIImage *rotatedImage = [_imageView.image fixOrientation];
            NSData* newImagedata = UIImagePNGRepresentation(rotatedImage);
            BOOL newsuccess = [fileManager createFileAtPath:newfullPath contents:newImagedata attributes:nil];
            if(newsuccess == YES)
            {
                DebugLog(@"ImageFilerting : success on new file save");
            }
            else{
                DebugLog(@"ImageFilerting : failed to save new file");
            }
        }
        else // 1st time updating image
        {
            //rename oldImage to org_imagePath
            NSString *oldfilename = [NSString stringWithFormat: @"old_%@",_imageObjectToFilter.imagePath];
            DebugLog(@"ImageFilerting : old file name  = %@" , oldfilename);
            NSString* oldfullPath = [documentsDirectory stringByAppendingPathComponent:oldfilename];
            
            NSData* oldImagedata = UIImagePNGRepresentation(originalImageByCamara);
            BOOL oldsuccess = [fileManager createFileAtPath:oldfullPath contents:oldImagedata attributes:nil];
            if(oldsuccess == YES)
            {
                DebugLog(@"ImageFilerting : success on old file save");
                //save new filter image with same imagepath
                NSString *newfilename = _imageObjectToFilter.imagePath;
                DebugLog(@"ImageFilerting : newfilename name  = %@" , newfilename);
                NSString* newfullPath = [documentsDirectory stringByAppendingPathComponent:newfilename];
                
                UIImage *rotatedImage = [_imageView.image fixOrientation];
                NSData* newImagedata = UIImagePNGRepresentation(rotatedImage);
                BOOL newsuccess = [fileManager createFileAtPath:newfullPath contents:newImagedata attributes:nil];
                if(newsuccess == YES)
                {
                    DebugLog(@"ImageFilerting : success on new file save");
                    _imageObjectToFilter.isUpdated = YES;
                }
                else{
                    DebugLog(@"ImageFilerting : failed to save new file");
                }
            }
            else
            {
                DebugLog(@"ImageFilerting : failed to save old file");
            }
        }
    }
    [self reloadParentImageData];
}

- (void)cancelBtnPressed:(id)sender
{
    DebugLog(@"ImageFilerting : 1)_imageObjectToFilter.imagePath = %@ isNew %d isDeleted %d isUpdated %d" , _imageObjectToFilter.imagePath,_imageObjectToFilter.isNew,_imageObjectToFilter.isDeleted,_imageObjectToFilter.isUpdated);
    
    if (_imageObjectToFilter.isNew && _firsttime) {
        _imageObjectToFilter.isDeleted = YES;
    }
    [self reloadParentImageData];
}

-(void)reloadParentImageData
{
    DebugLog(@"ImageFilerting : 2)_imageObjectToFilter.imagePath = %@ isNew %d isDeleted %d isUpdated %d" , _imageObjectToFilter.imagePath,_imageObjectToFilter.isNew,_imageObjectToFilter.isDeleted,_imageObjectToFilter.isUpdated);
    
    if (_imageObjectToFilter.isUpdated || _imageObjectToFilter.isDeleted)
    {
        UIViewController *previousViewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
        DebugLog(@"ImageFilerting : parent %@",previousViewController);
        if ([previousViewController isKindOfClass:[IOAddRecordViewController class]])
        {
            IOAddRecordViewController *parent = (IOAddRecordViewController *) previousViewController;
            if(parent != nil)
            {
                [parent reloadFilteredImage:_imageObjectToFilter];
            }
            parent = nil;
        }
        else if ([previousViewController isKindOfClass:[MWPhotoBrowser class]])
        {
            MWPhotoBrowser *parent = (MWPhotoBrowser *) previousViewController;
            if(parent != nil)
            {
                [parent reloadImageFilteredData:_imageObjectToFilter];
            }
            parent = nil;
        }
        //  [self.navigationController popViewControllerAnimated:YES];
        previousViewController = nil;
    }
    [UIView  transitionWithView:self.navigationController.view duration:0.8  options:UIViewAnimationOptionTransitionFlipFromRight
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         [self.navigationController popViewControllerAnimated:YES];
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:^(BOOL finished) {
                         _imageObjectToFilter = nil;
                     }];
}
@end
