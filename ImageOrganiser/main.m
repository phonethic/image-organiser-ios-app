//
//  main.m
//  ImageOrganiser
//
//  Created by Kirti Nikam on 02/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IOAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IOAppDelegate class]));
    }
}
