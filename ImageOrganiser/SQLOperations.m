//
//  SQLOperations.m
//  ImageOrganiser
//
//  Created by Kirti Nikam on 05/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
#import <sqlite3.h>

#import "SQLOperations.h"
#import "IOAppDelegate.h"
#import "constants.h"


@implementation SQLOperations

// ================= Group Table ====================

+(NSString *)getGroupName:(int) objectId
{
    NSString *groupName = nil;
	// Open the database from the users filessytem
    sqlite3 *ioDB;

	if(sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK)
    {
		// Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"Select GROUP_NAME from GROUPS WHERE id=%d",objectId];
        const char *sqlStatement = [querySQL UTF8String];
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(ioDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
			while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
				groupName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                DebugLog(@"groupName -> %@",groupName);
            }
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(ioDB);
    return groupName;
}

+(int)getGroupId:(NSString *)groupName
{
    int groupId = -1;
    sqlite3 *ioDB;

	if(sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK)
    {
        // Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"Select id from GROUPS WHERE GROUP_NAME=\"%@\"",[groupName lowercaseString]];
        const char *sqlStatement = [querySQL UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(ioDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                groupId = sqlite3_column_int(compiledStatement, 0);
                NSLog(@"groupId -> %d",groupId);
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(ioDB);
    return groupId;
}

@end
