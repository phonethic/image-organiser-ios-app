
#ifndef _ALERT_MESSAGES_H_
#define _ALERT_MESSAGES_H_
#import "Flurry.h"
#import "Appirater.h"
//#import <Parse/Parse.h>
#import "Reachability.h"

#define DMDDelegate (DMDAppDelegate*)[[UIApplication sharedApplication] delegate]

#define FLURRY_APPID @"HRWPHQW8NZVW5YF4T5GN"

#define APPIRATER_APPID @""

//#define PARSE_APPID @"tWsjACyCVqMFJiJmL1PNfHloOk3sGEKMb10Tvkuj"
//#define PARSE_CLIENTKEY @"T7rQdUuoPf5fh7VbMg2J7eR7WFFAxoOMkhXmlf2o"
#define DATABASE_NAME @"emr.db"

#define ALERT_TITLE @"EMR"


//#define  DEFAULT_COLOR [UIColor colorWithRed:0.015 green:0.705 blue:0.372 alpha:1.0] green
//#define  DEFAULT_COLOR [UIColor colorWithRed:52/255.0 green:152/255.0 blue:219/255.0 alpha:1.0] //blue
#define  DEFAULT_COLOR [UIColor colorWithRed:0.0542516 green:0.44115  blue:0.699654 alpha:1] //dark bluecolor

#define DEFAULT_FONT(SIZE) [UIFont systemFontOfSize:SIZE]
#define DEFAULT_BOLD_FONT(SIZE) [UIFont boldSystemFontOfSize:SIZE]


//#define DEFAULT_FONT(SIZE) [UIFont fontWithName:@"Helvetica" size:SIZE]
//#define DEFAULT_BOLD_FONT(SIZE) [UIFont fontWithName:@"Helvetica-Bold" size:SIZE]


#define ADDRECORDNOTIFICATIONNAME @"add_record"
#define REVERSEADDRESS @"revaddress"


#define FASTEST_ANIMATION_DURATION 0.20
#define FAST_ANIMATION_DURATION 0.35
#define SLOW_ANIMATION_DURATION 0.75
#define PAN_CLOSED_X 0
#define PAN_OPEN_X -160

#define WEBSITELINK @"http://www.phonethics.in"
#define TEL @"+91-22-65160691"
#define EMAIL @"apps@phonethics.in"
#define EMR_APPSTORE_URL @"https://itunes.apple.com/in/app/safe-city/id616709366?mt=8"
#define PRIVACY_URL @"https://policy-portal.truste.com/core/privacy-policy/Phonethics-Mobile-Media/48bc6228-ed00-4f30-9cfb-f94a8180a80c"
#endif
