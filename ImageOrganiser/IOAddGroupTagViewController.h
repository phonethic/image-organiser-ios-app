//
//  IOAddGroupTagViewController.h
//  ImageOrganiser
//
//  Created by Kirti Nikam on 04/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

#define TAGCASE 1
#define GROUPCASE 2 

@interface IOAddGroupTagViewController : UIViewController <UIScrollViewDelegate,UITextFieldDelegate>
{
    sqlite3 *ioDB;
}
@property (readwrite,nonatomic) int imgId;
@property (readwrite,nonatomic) int curretCase;
@property (retain,nonatomic) NSMutableArray *tableArray;
@property (retain,nonatomic) NSMutableArray *groupIconArray;
@property (retain,nonatomic) NSMutableDictionary *iconsDic;

@property (strong, nonatomic) IBOutlet UITableView *checkMarkTableView;
@property (strong, nonatomic) IBOutlet UIButton *createGroupBtn;

@property (strong, nonatomic) IBOutlet UIView *addView;
@property (strong, nonatomic) IBOutlet UILabel *lbl;
@property (strong, nonatomic) IBOutlet UITextField *textField;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIButton *iconGridButton;
@property (strong, nonatomic) IBOutlet UIScrollView *iconsGridScrollView;

- (IBAction)iconGridButtonClicked:(id)sender;
- (IBAction)createGroupBtnClicked:(id)sender;
@end
