//
//  ImageFilteringViewController.h
//  ImageOrganiser
//
//  Created by Kirti Nikam on 03/05/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageObject.h"

@interface ImageFilteringViewController : UIViewController{
    int rotationInt;

    UIImage *changedImage;
    int currentFilter;
    UIImage *originalImageByCamara;
    
}
@property (retain,nonatomic) ImageObject *imageObjectToFilter;
@property (readwrite,nonatomic) int firsttime;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@property (strong, nonatomic) IBOutlet UISlider *slider;

//@property (strong, nonatomic) IBOutlet UIView *toolbarView;
//@property (strong, nonatomic) IBOutlet UIButton *originalImageBtn;
//@property (strong, nonatomic) IBOutlet UIButton *contrastImageBtn;
//@property (strong, nonatomic) IBOutlet UIButton *brightImageBtn;
//@property (strong, nonatomic) IBOutlet UIButton *grayScaleImageBtn;
//@property (strong, nonatomic) IBOutlet UIButton *rotateBtn;

@property (strong, nonatomic) IBOutlet UIToolbar *toolBar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *originalImageBarBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *contrastImageBarBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *brightImageBarBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *grayScaleImageBarBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *rotateBarBtn;


- (IBAction)originalImageBtnClicked:(id)sender;
- (IBAction)contrastImageBtnClicked:(id)sender;
- (IBAction)brightImageBtnClicked:(id)sender;
- (IBAction)grayScaleImageBtnClicked:(id)sender;
- (IBAction)rotateBtnClicked:(id)sender;

- (IBAction)sliderDidSlide:(id)sender;

@end
