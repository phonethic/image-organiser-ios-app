//
//  IOGroupDetailViewController.h
//  ImageOrganiser
//
//  Created by Kirti Nikam on 25/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface IOGroupDetailViewController : UIViewController <UIGestureRecognizerDelegate,UIActionSheetDelegate,UIAlertViewDelegate>
{
    sqlite3 *ioDB;
    int actionBtnState;
}
@property (readwrite,nonatomic) int currentGroupId;
@property (retain,nonatomic) NSMutableArray *recordObjectsArray;
@property (retain,nonatomic) NSArray* buttonData;
@property (retain,nonatomic) NSIndexPath* lastIndexPath;
@property (retain,nonatomic) NSMutableArray *moreArray;
@property (retain,nonatomic) NSIndexPath* deleteRecIndexPath;

@property (strong, nonatomic) IBOutlet UIView *dropDownMenuView;
@property (strong, nonatomic) IBOutlet UITableView *actionTableView;

@property (strong, nonatomic) IBOutlet UITableView *detailTableView;

@property (nonatomic, strong) NSIndexPath *openCellIndexPath;
@end
