//
//  DetailMapViewController.m
//  ImageOrganiser
//
//  Created by Kirti Nikam on 15/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>

#import "DetailMapViewController.h"
#import "constants.h"
#import "IOAppDelegate.h"
#import "DisplayMap.h"

#define REVERSE_API(LAT,LON) [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/xml?latlng=%f,%f&sensor=false",LAT,LON]

@interface DetailMapViewController ()

@end

@implementation DetailMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if (_geoPointLatitude != 0 && _geoPointLongitude != 0)
    {
        [_mapView setMapType:MKMapTypeStandard];
        [_mapView setZoomEnabled:YES];
        [_mapView setScrollEnabled:YES];
        _mapView.layer.cornerRadius = 10;
        
        MKCoordinateRegion region = { {0.0, 0.0 }, { 0.0, 0.0 } };
        
        region.center.latitude = _geoPointLatitude;
        region.center.longitude = _geoPointLongitude;
        
        region.span.longitudeDelta = 0.01f;
        region.span.latitudeDelta = 0.01;
        [_mapView setRegion:region animated:YES];
        
        DisplayMap *ann = [[DisplayMap alloc] init];
        ann.title = _reverseAddress;
        ann.subtitle = @"";
        ann.coordinate = region.center;
        [_mapView addAnnotation:ann];
    }
    if ([CLLocationManager locationServicesEnabled])
    {
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        [locationManager startUpdatingLocation];
        [self performSelector:@selector(stopUpdatingCoreLocation:) withObject:@"Timed Out" afterDelay:10];
    }
    else
    {
        UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle:@"Location Services Disabled" message:@"You currently have all location services for this device disabled. If you proceed, you will be asked to confirm whether location services should be reenabled." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [servicesDisabledAlert show];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setMapView:nil];
    [super viewDidUnload];
}
#pragma mark Orientation methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate
{
    return NO;
}
- (NSUInteger)supportedInterfaceOrientations{
#if __IPHONE_OS_VERSION_MAX_ALLOWED <= __IPHONE_5_1
    return UIInterfaceOrientationPortrait;
#else
    return UIInterfaceOrientationMaskPortrait;
#endif
}
- (void) locationManager:(CLLocationManager *) manager didFailWithError:(NSError *) error
{
    [locationManager stopUpdatingLocation];
}

- (void)stopUpdatingCoreLocation:(NSString *)state
{
    [locationManager stopUpdatingLocation];
    [self saveCurrentocation:[locationManager location]];
}

-(void)saveCurrentocation:(CLLocation *)lLocation
{
	if (!lLocation) {
		return;
	}
}

- (void)postNotificationWithString:(NSString *)value //post notification method and logic
{
    DebugLog(@"DetailMap:posting notification");
    NSDictionary *dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:value,REVERSEADDRESS,[NSNumber numberWithDouble:_geoPointLatitude],@"Latitude",[NSNumber numberWithDouble:_geoPointLongitude],@"Longitude", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:ADDRECORDNOTIFICATIONNAME object:nil userInfo:dictionary];
}

-(void)getReverseAddress
{
    NSURL *url = [NSURL URLWithString:REVERSE_API(_geoPointLatitude,_geoPointLongitude)];
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:YES timeoutInterval:30.0];
    
    conn = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    if (conn)
    {
        webData = [NSMutableData data];
    }
}
-(void)reloadAddressAnnotationTitle
{
    DisplayMap *geoPointAnnotation = (DisplayMap *)[[_mapView annotations] objectAtIndex:0];
    geoPointAnnotation.title = _reverseAddress;
}


#pragma mark - MKMapViewDelegate
-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    MKPinAnnotationView *pinView = nil;
    if(annotation != _mapView.userLocation)
    {
        static NSString *defaultPinID = @"Pin";
        pinView = (MKPinAnnotationView *)[_mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        if ( pinView == nil )
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultPinID];
        
        pinView.pinColor = MKPinAnnotationColorRed;
        pinView.canShowCallout = YES;
        pinView.animatesDrop = YES;
        pinView.draggable = YES;
        //    UIImage *pinImage = [UIImage imageNamed:@"micra_32x32.png"];
        //    [pinView setImage:pinImage];
    }
    else {
        [_mapView.userLocation setTitle:@"I am here"];
    }
    return pinView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)annotationView didChangeDragState:(MKAnnotationViewDragState)newState fromOldState:(MKAnnotationViewDragState)oldState
{
    if (![annotationView isKindOfClass:[MKPinAnnotationView class]]) {
        return;
    }
    if (newState == MKAnnotationViewDragStateEnding)
    {
        CLLocationCoordinate2D droppedAt = annotationView.annotation.coordinate;
        DebugLog(@"DetailMap:Pin dropped at %f,%f", droppedAt.latitude, droppedAt.longitude);
        _geoPointLatitude = droppedAt.latitude;
        _geoPointLongitude = droppedAt.longitude;
        [self getReverseAddress];
    }
}
#pragma mark Connection Delegate Methods

-(void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *) response
{
    [webData setLength: 0];
}

-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *) data
{
    [webData appendData:data];
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *) error
{
    DebugLog(@"DetailMap:didFailWithError error %@",error);
    [conn cancel];
}

-(void) connectionDidFinishLoading:(NSURLConnection *) connection
{
    if (webData)
    {
        NSString *result = [[NSString alloc] initWithData:webData encoding:NSASCIIStringEncoding];
		DebugLog(@"DetailMap:\n result:%@\n\n", result);
        
        NSString *xmlDataFromChannelSchemes = [[NSString alloc] initWithString:result];
		NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
		xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
		[xmlParser setDelegate:self];
		[xmlParser parse];
    }
}

#pragma mark XML Parser Delegate Methods

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"GeocodeResponse"])
    {
    }
    else if ([elementName isEqualToString:@"result"])
    {
    }
    else if ([elementName isEqualToString:@"formatted_address"])
    {
        elementFound = YES;
    }
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    
    if (elementFound)
    {
        _reverseAddress = string;
        DebugLog(@"DetailMap:-- %@ ---",_reverseAddress);
    }
    elementFound = FALSE;
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    
    DebugLog(@"DetailMap:reverseAddress %@ and elementName %@",_reverseAddress,elementName);
    if ([elementName isEqualToString:@"formatted_address"])
    {
        if(_reverseAddress != nil)
        {
            DebugLog(@"DetailMap:%@",_reverseAddress);
            [self reloadAddressAnnotationTitle];
            [self postNotificationWithString:_reverseAddress];
        }
        [xmlParser abortParsing];
    }
}

-(void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError{
}

-(void)parserDidEndDocument:(NSXMLParser *)parser{
    
}
@end
