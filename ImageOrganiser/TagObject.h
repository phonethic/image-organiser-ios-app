//
//  TagObject.h
//  ImageOrganiser
//
//  Created by Kirti Nikam on 05/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TagObject : NSObject
@property (nonatomic, readwrite) int tagRowId;
@property (nonatomic, copy) NSString *tagName;
@property (nonatomic, readwrite) int selected;
@end
