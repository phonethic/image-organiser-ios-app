//
//  IOAddRecordViewController.h
//  ImageOrganiser
//
//  Created by Kirti Nikam on 02/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <sqlite3.h>
#import <CoreLocation/CoreLocation.h>
#import "TITokenField.h"
#import "NoteView.h"
#import <MapKit/MapKit.h>
#import "MWPhotoBrowser.h"
#import "ImageObject.h"

#define REVERSE_API(LAT,LON) [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/xml?latlng=%f,%f&sensor=false",LAT,LON]

@interface IOAddRecordViewController : UIViewController <UITextViewDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate,CLLocationManagerDelegate,NSXMLParserDelegate,TITokenFieldDelegate,MKMapViewDelegate,MWPhotoBrowserDelegate>
{
    UIActionSheet *actionSheet;
    int CURRENTCASE;
    sqlite3 *ioDB;
    
    int numberOfGroups;
    int numberOfTags;
    
    CLLocationManager *locationManager;
    NSMutableData *webData;
    NSXMLParser *xmlParser;
    BOOL elementFound;
    NSURLConnection *theConnection;
    
    double tagsListHeight;
    
    BOOL pageControlUsed;
    
    TITokenFieldView * tokenFieldView;
    TITokenFieldView * tokenFieldViewInPage;
    
    int nextGroupIndex;
    
    BOOL savedInDB;
    
    NoteView *notesTextView;
    CGRect originalTextViewFrame;
    
    
    double imageCellHeight;
}
@property (copy,nonatomic) NSString *reverseAddress;
@property (readwrite,nonatomic) double currentLatitude;
@property (readwrite,nonatomic) double currentLongitude;
@property (nonatomic, readwrite) int currentImgRowId;
@property (retain,nonatomic) NSIndexPath *selectedIndexPath;

@property (nonatomic, readwrite) int selectedGroupId; //From managegroup - Add new record


@property (strong, nonatomic) IBOutlet UITableView *addRecordTableView;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerView;
@property (strong, nonatomic) IBOutlet UIDatePicker *datetimePickerView;

@property (retain,nonatomic) NSMutableArray *categoryArray;
@property (retain,nonatomic) NSMutableArray *groupArray;
@property (retain,nonatomic) NSMutableArray *tagsArray;
@property (retain,nonatomic) NSMutableArray *imagesArray;
@property (retain,nonatomic) NSMutableDictionary *iconsDic;

@property (retain,nonatomic) NSMutableArray *tagObjectsArray;
@property (nonatomic, retain) NSMutableArray *photos;

@property (nonatomic, copy) NSString *selectedTitle;
@property (nonatomic, copy) NSString *selectedCategory;
@property (nonatomic, copy) NSString *selectedTag;
@property (nonatomic, copy) NSString *selectedGroup;
@property (copy,nonatomic)  NSString *selectedDate;
@property (copy,nonatomic)  NSString *selectedTime;
@property (strong, nonatomic) IBOutlet UILabel *tagInfoLabel;
@property (strong, nonatomic) IBOutlet UILabel *addTagsLabel;

/// PageWise Navigation
@property (strong, nonatomic) IBOutlet UIScrollView *addRecordScrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (strong, nonatomic) IBOutlet UIView *titleimgView;
@property (strong, nonatomic) IBOutlet UIView *categoryView;
@property (strong, nonatomic) IBOutlet UIView *tagView;
@property (strong, nonatomic) IBOutlet UIView *groupView;
@property (strong, nonatomic) IBOutlet UIView *locationDateTimeView;
@property (strong, nonatomic) IBOutlet UITextField *titleTextField;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UITextField *categoryTextField;
@property (strong, nonatomic) IBOutlet UITextField *groupTextField;
@property (strong, nonatomic) IBOutlet UITextView *addressTextView;
@property (strong, nonatomic) IBOutlet UITextField *geopointsTextField;
@property (strong, nonatomic) IBOutlet UITextField *dateTextField;
@property (strong, nonatomic) IBOutlet UITextField *timeTextField;
@property (strong, nonatomic) IBOutlet UITableView *categoryTableView;
@property (strong, nonatomic) IBOutlet UITableView *groupTableView;
@property (strong, nonatomic) IBOutlet UIImageView *groupImageView;
@property (strong, nonatomic) IBOutlet UIButton *createGroup;
@property (strong, nonatomic) IBOutlet UIScrollView *groupImagesScrollView;
@property (strong, nonatomic) IBOutlet UIButton *changeGroupIconBtn;
@property (strong, nonatomic) IBOutlet UIButton *openNotesBtn;
//@property (strong, nonatomic) IBOutlet UITextView *notesTextView;
@property (strong, nonatomic) IBOutlet UIButton *addImageBtn;
@property (strong, nonatomic) IBOutlet UIButton *openGalleryBtn;
@property (strong, nonatomic) IBOutlet UIView *notesView;
@property (strong, nonatomic) IBOutlet UIView *keyBoardHideView;
@property (strong, nonatomic) IBOutlet UIButton *hideKeyboardBtn;
@property (strong, nonatomic) IBOutlet UIImageView *pinImageView;
@property (strong, nonatomic) IBOutlet UIImageView *PinArrowRightImageView;
@property (strong, nonatomic) IBOutlet MKMapView *pageMapView;

-(void)reloadGroups;
-(void)reloadTags;
-(void)reloadImageCell;

-(void)reloadFilteredImage:(ImageObject *)imgObj;

- (IBAction)openGroupIconGridBtnClicked:(id)sender;
- (IBAction)openNotesBtnClicked:(id)sender;
- (IBAction)addImageBtnClicked:(id)sender;
- (IBAction)openGalleryBtnClicked:(id)sender;
- (IBAction)hideKeyboardBtnClicked:(id)sender;


- (IBAction)createGroupBtnClicked:(id)sender;
- (IBAction)pageChange:(id)sender;
- (IBAction)textChanged:(id)sender;
@end
