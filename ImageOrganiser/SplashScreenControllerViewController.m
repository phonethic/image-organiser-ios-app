//
//  SplashScreenControllerViewController.m
//  ImageOrganiser
//
//  Created by Kirti Nikam on 13/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "SplashScreenControllerViewController.h"
#import "IOAppDelegate.h"

#define SPLASHSCREEN_DELAY 6.0

@interface SplashScreenControllerViewController ()

@end

@implementation SplashScreenControllerViewController
@synthesize imageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    imageView.frame = CGRectMake(0, 0, self.view.frame.size.width,480);
    if ([[UIScreen mainScreen] respondsToSelector: @selector(scale)]) {
            CGSize result = [[UIScreen mainScreen] bounds].size;
            result = CGSizeMake(result.width * [UIScreen mainScreen].scale, result.height * [UIScreen mainScreen].scale);
        DebugLog(@"SplashView: result.height %f",result.height);
            if (result.height == 480){
                    // iPhone 1,3,3GS Standard Resolution   (320x480px)
                imageView.image = [UIImage imageNamed:@"Default.png"];
            }else if(result.height == 960){
                   // iPhone 4,4S High Resolution          (640x960px)
                imageView.image = [UIImage imageNamed:@"Default@2x.png"];
            }else{
                // iPhone 5 High Resolution             (640x1136px)
                 imageView.image = [UIImage imageNamed:@"Default-568h@2x.png"];
                 imageView.frame = CGRectMake(0, 0, self.view.frame.size.width,568);
            }
    } else{
        // iPhone 1,3,3GS Standard Resolution   (320x480px)
            imageView.image = [UIImage imageNamed:@"Default.png"];
    }
    [self performSelector:@selector(imageTapDetected:) withObject:nil afterDelay:SPLASHSCREEN_DELAY];

    UITapGestureRecognizer *imageViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(imageTapDetected:)];
    imageViewTap.numberOfTapsRequired = 1;
    imageViewTap.cancelsTouchesInView = NO;
    [imageView addGestureRecognizer:imageViewTap];
}
-(void)imageTapDetected:(UIGestureRecognizer *)sender {
    DebugLog(@"SplashView: ImageTapped");
    if (self.isViewLoaded && self.view.window) {
        [IO_APP_DELEGATE removeSplashScreen];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setImageView:nil];
    [super viewDidUnload];
}
@end
