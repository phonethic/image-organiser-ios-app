//
//  ControlColors.m
//  ImageOrganiser
//
//  Created by Kirti Nikam on 02/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "ControlColors.h"
#import "constants.h"

@implementation ControlColors
+(UIColor *)getViewBackgroundColor{
    return BGCOLOR;
}
+(UIColor *)getTableViewBackgroundColor{
    return [UIColor clearColor];
}

+(UIColor *)getTableViewCellBackgroundColor{
    return TABLE_EDITCELL_BGCOLOR;//TABLE_CELL_BGCOLOR;
}

+(UIColor *)getTableViewEditCellBackgroundColor{
    return TABLE_EDITCELL_BGCOLOR;
}

+(UIColor *)getScrollViewBackgroundColor{
    return [UIColor clearColor];
}

+(UIColor *)getTextViewBackgroundColor{
    return TABLE_EDITCELL_BGCOLOR;
}

+(UIColor *)getButtonBackgroundColor{
    return TABLE_CELL_BGCOLOR;
}

+(UIColor *)getLabelBackgroundColor{
    return TABLE_EDITCELL_BGCOLOR;
}

+(UIColor *)getTextFieldBackgroundColor{
    return TABLE_EDITCELL_BGCOLOR;
}

+(UIColor *)getNotesViewBackgroundColor{
    return [UIColor colorWithPatternImage:[UIImage imageNamed:@"noteBg.jpg"]];
}

+(UIColor *)getTableViewEditCellTextColor{
    return [UIColor blackColor];
}

+(UIColor *)getTextViewTextColor{
    return [UIColor blackColor];
}

+(UIColor *)getButtonTextColor{
    return [UIColor whiteColor];
}

+(UIColor *)getLabelTextColor{
    return [UIColor blackColor];
}

+(UIColor *)getTextFieldTextColor{
    return [UIColor blackColor];
}

+(UIColor *)getNotesViewTextColor{
    return [UIColor blackColor];
}
@end
