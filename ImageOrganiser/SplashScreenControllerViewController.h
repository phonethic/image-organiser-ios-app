//
//  SplashScreenControllerViewController.h
//  ImageOrganiser
//
//  Created by Kirti Nikam on 13/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplashScreenControllerViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@end
