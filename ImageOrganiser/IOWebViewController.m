//
//  IOWebViewController.m
//  ImageOrganiser
//
//  Created by Kirti Nikam on 02/05/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "IOWebViewController.h"

@interface IOWebViewController ()

@end

@implementation IOWebViewController
@synthesize urlContent;
@synthesize urlString;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_webView stopLoading];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIButton *backbutton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [backbutton setImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
    [backbutton addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [backbutton setFrame:CGRectMake(0, 0, 50, 30)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backbutton];
    
    _barbackBtn.enabled      = NO;
    _barfwdBtn.enabled       = NO;
    _barrefreshBtn.enabled   = NO;
    
    if (urlString != nil && ![urlString isEqualToString:@""]) {
        NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        [_webView loadRequest:req];
    }else if (urlContent != nil && ![urlContent isEqualToString:@""]) {
        [_webView loadHTMLString:urlContent baseURL:nil];
        _webviewtoolBar.hidden = YES;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setWebView:nil];
    [self setSpinnerIndicator:nil];
    [self setBarbackBtn:nil];
    [self setBarfwdBtn:nil];
    [self setBarrefreshBtn:nil];
    [self setWebviewtoolBar:nil];
    [super viewDidUnload];
}
- (void)backBtnPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark webView Delegate methods
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [_spinnerIndicator startAnimating];
     _barrefreshBtn.enabled   = NO;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    DebugLog(@"IOWebView : webViewDidFinishLoad");
    if (self.webView.canGoBack)
    {
        _barbackBtn.enabled = YES;
    }
    else
    {
        _barbackBtn.enabled = NO;
    }
    if (self.webView.canGoForward)
    {
        _barfwdBtn.enabled = YES;
    }
    else
    {
        _barfwdBtn.enabled = NO;
    }
    _barrefreshBtn.enabled = YES;
    [_spinnerIndicator stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    DebugLog(@"IOWebView : didFailLoadWithError %@",error);
    _barrefreshBtn.enabled = YES;
    [_spinnerIndicator stopAnimating];
}
- (IBAction)webviewbackBtnPressed:(id)sender {
    [_webView goBack];
}

- (IBAction)webviewfwdBtnPressed:(id)sender {
    [_webView goForward];
}

- (IBAction)webviewrefreshBtnPressed:(id)sender {
    [_webView reload];
}
@end
