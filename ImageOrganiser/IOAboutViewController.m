//
//  IOAboutViewController.m
//  ImageOrganiser
//
//  Created by Kirti Nikam on 10/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "IOAboutViewController.h"
#import "constants.h"
#import "IOAppDelegate.h"
#import "IOWebViewController.h"
#import "IOHelpInfoViewController.h"

@interface IOAboutViewController ()

@end

@implementation IOAboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
         self.title = @"About";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIButton *backbutton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [backbutton setImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
    [backbutton addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [backbutton setFrame:CGRectMake(0, 0, 50, 30)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backbutton];
    
  //  self.view.backgroundColor = BGCOLOR;
    self.view.backgroundColor = [UIColor clearColor];

    _aboutTableView.backgroundColor = [UIColor clearColor];
    _aboutTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _aboutTableView.separatorColor = [UIColor clearColor];
    _aboutTableView.scrollEnabled = FALSE;
    _aboutTableView.bounces = NO;
    
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [infoDict objectForKey:@"CFBundleVersion"];
    _appVersionLabel.text = [NSString stringWithFormat:@"EMR iPhone Version %@",version];
    _appVersionLabel.textColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setAboutTableView:nil];
    [super viewDidUnload];
}

#pragma mark Orientation methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate
{
    return NO;
}
- (NSUInteger)supportedInterfaceOrientations{
#if __IPHONE_OS_VERSION_MAX_ALLOWED <= __IPHONE_5_1
    return UIInterfaceOrientationPortrait;
#else
    return UIInterfaceOrientationMaskPortrait;
#endif
}
- (void)backBtnPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (indexPath.section == 0 && indexPath.row == 0) {
//        return 100;
//    }
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 2) {
        return 10;
    }
    return 40;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(0,0,300,30)];
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.frame = CGRectMake(15, 5, 300, 30);
    headerLabel.font = DEFAULT_BOLD_FONT(16.0f);
    switch (section) {
        case 0:
            headerLabel.text = @"About App";
            break;
        case 1:
            headerLabel.text = @"Contact Us";
            break;
        default:
            headerLabel.text = @"";
            break;
    }
    headerLabel.textColor = [UIColor whiteColor];
    [customView addSubview:headerLabel];
    return customView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 2;
    }
    else if (section == 1) {
        return 2;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSString *CellIdentifier = [NSString stringWithFormat:@"about%d%d",indexPath.section,indexPath.row] ;
    static  NSString *CellIdentifier = @"aboutCell";

    UIImageView *accessoryImgView;
    UIImageView *dividerImgView;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        DebugLog(@"AboutView : id = %@",CellIdentifier);
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = TABLE_CELL_BGCOLOR;
        bgColorView.layer.borderColor = [UIColor clearColor].CGColor;
        bgColorView.layer.cornerRadius = 10.0;
        cell.selectedBackgroundView = bgColorView;
        
        cell.textLabel.highlightedTextColor = TABLE_EDITCELL_BGCOLOR;
        cell.textLabel.font = DEFAULT_BOLD_FONT(15.0);
        cell.textLabel.textColor = TABLE_CELL_BGCOLOR;
        
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
//        //         cell.backgroundColor = [UIColor redColor];
//        cell.contentView.backgroundColor = TABLE_CELL_BGCOLOR;
//        cell.layer.borderColor = TABLE_CELL_BGCOLOR.CGColor;
//        cell.layer.borderWidth = 0;
        
        accessoryImgView = [[UIImageView alloc] initWithFrame:CGRectMake(270,15, 20, 20)];
        accessoryImgView.tag = 1;
        accessoryImgView.contentMode = UIViewContentModeScaleToFill;
        accessoryImgView.layer.masksToBounds = YES;
        accessoryImgView.image = [UIImage imageNamed:@"Mark.png"];
        [cell.contentView addSubview:accessoryImgView];
        
        dividerImgView = [[UIImageView alloc] initWithFrame:CGRectMake(25,49, 244, 2)];
        dividerImgView.tag = 2;
        dividerImgView.contentMode = UIViewContentModeScaleToFill;
        dividerImgView.layer.masksToBounds = YES;
        [cell.contentView addSubview:dividerImgView];

    }
    dividerImgView = (UIImageView *)[cell.contentView viewWithTag:2];
    switch (indexPath.section) {
        case 0:
        {
            if (indexPath.row == 0)
            {
                cell.textLabel.text = @"EMR Description";
                cell.textLabel.numberOfLines = 4;
                cell.imageView.image = [UIImage imageNamed:@"description.png"];
                dividerImgView.image = [UIImage imageNamed:@"cell_divider.png"];
            }
            else
            {
                cell.textLabel.text = @"How EMR Works";
                cell.imageView.image = [UIImage imageNamed:@"help.png"];
                dividerImgView.image = nil;
            }
        }
            break;
        case 1:
        {
            if (indexPath.row == 0) {
                cell.textLabel.text = TEL;
                cell.imageView.image = [UIImage imageNamed:@"telephone.png"];
                dividerImgView.image = [UIImage imageNamed:@"cell_divider.png"];
            }
            else
            {
                cell.textLabel.text = EMAIL;
                cell.imageView.image = [UIImage imageNamed:@"email.png"];
                dividerImgView.image = nil;
            }
        }
            break;
        case 2:
        {
            if (indexPath.row == 0) {
                cell.textLabel.text = @"Private Policy";
                cell.imageView.image = [UIImage imageNamed:@"privacy.png"];
                dividerImgView.image = nil;
            }
        }
            break;
//        case 3:
//        {
//            if (indexPath.row == 0) {
//                cell.textLabel.text = @"Review us on iTunes";
//                cell.imageView.image = [UIImage imageNamed:@"review.png"];
//            }
//            else
//            {
//                cell.textLabel.text = @"Share this app";
//                cell.imageView.image = [UIImage imageNamed:@"share.png"];
//            }
//        }
//            break;
        default:
            break;
    }
    accessoryImgView = (UIImageView *)[cell.contentView viewWithTag:1];
    accessoryImgView.image = [UIImage imageNamed:@"Mark.png"];
    return cell;
}

#pragma mark Tableview Delegate methods
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    UIImageView *accessoryImgView = (UIImageView *)[cell.contentView viewWithTag:1];
    accessoryImgView.image = [UIImage imageNamed:@"Mark_white.png"];
    switch (indexPath.section) {
        case 0:
        {
            if (indexPath.row == 0) {
                DebugLog(@"AboutView : description");
                IOWebViewController *webViewController = [[IOWebViewController alloc] initWithNibName:@"IOWebViewController" bundle:nil] ;
                webViewController.title = @"App Description";
                NSString *content = @"EMR is a handy tool to track your bills. You can quickly scan or save a copy of the receipt with filling its detailed information.\n More than just manage groups, where you can categorize receipts by their groups.\n\n All your information are saved directly in your phone and nothing on our servers.";
                
                webViewController.urlContent = [NSString stringWithFormat:@"<!DOCTYPE html><head><style type=\"text/css\">h1{font-family:\"arial\";font-size:80px;width:300px;color:rgb(70,124,139);}</style></head><body><br><p><center><h1>%@</h1></center></p><font face = \"arial\" size=\"15\"><p>%@</p></font></body></html>",ALERT_TITLE,[content stringByReplacingOccurrencesOfString:@"\n" withString:@"</p><p>"]];
                webViewController.urlString = @"";
                [self.navigationController pushViewController:webViewController animated:YES];

            }
            else
            {
                IOHelpInfoViewController *helpViewController = [[IOHelpInfoViewController alloc] initWithNibName:@"IOHelpInfoViewController" bundle:nil];
                helpViewController.title =  @"How EMR Works";
                [self.navigationController pushViewController:helpViewController animated:YES];
            }
        }
            break;
        case 1:
        {
            if (indexPath.row == 0)
            {
                [self callFunction];
            }
            else
            {
                [self emailFunction: EMAIL];
            }
        }
            break;
        case 2:
        {
            if (indexPath.row == 0) {
                DebugLog(@"AboutView : privacy");
                IOWebViewController *webViewController = [[IOWebViewController alloc] initWithNibName:@"IOWebViewController" bundle:nil];
                webViewController.urlString = PRIVACY_URL;
                webViewController.title = @"Private Policy";
                [self.navigationController pushViewController:webViewController animated:YES];
            }
            

//            if (indexPath.row == 0) {
//                //cell.textLabel.text = @"Review us on iTunes";
//                if([IO_APP_DELEGATE networkavailable])
//                {
//                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:EMR_APPSTORE_URL]];
//                }
//                else {
//                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: ALERT_TITLE message: @"Please check your internet connection and try again." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                    [alert show];
//                }
//            }
//            else
//            {
//                // cell.textLabel.text = @"Share this app";
//                [self shareBtnFunction];
//            }
        }
            break;
        default:
            break;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    accessoryImgView.image = [UIImage imageNamed:@"Mark.png"];
}

#pragma mark internal methods
- (void)callFunction
{
    NSString *deviceType = [UIDevice currentDevice].model;
    if([deviceType isEqualToString:@"iPhone"])
    {
        UIAlertView *alert = [[ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                          message: [NSString stringWithFormat: @"Do you want to call %@ number ?",TEL]
                                                         delegate: self
                                                cancelButtonTitle: nil
                                                otherButtonTitles: @"NO", @"YES", nil
                              ];
        [alert show];
    }
    else
    {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                           message: @"Calling functionaltiy is not available in this device. "
                                                          delegate: nil
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"OK", nil
                              ];
        [ alert show ];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        UIApplication *myApp = [UIApplication sharedApplication];
        [myApp openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",TEL]]];
    }
}

-(void)emailFunction:(NSString *)mailID
{
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
        mailComposer.mailComposeDelegate = self;
        mailComposer.navigationBar.tintColor = DEFAULT_COLOR;
        if (mailID == nil)
        {
            [mailComposer setToRecipients:nil];
            [mailComposer setMessageBody:[NSString stringWithFormat:@"%@ iOS app: %@",ALERT_TITLE,EMR_APPSTORE_URL] isHTML:YES];
        }
        else
        {
            [mailComposer setToRecipients:[NSArray arrayWithObjects:mailID,nil]];
            [mailComposer setSubject:@"Ref: EMR"];
            [mailComposer setMessageBody:@"Enter your message here." isHTML:YES];
        }
        [mailComposer setModalPresentationStyle:UIModalPresentationFormSheet];
        [self presentModalViewController:mailComposer animated:YES];
    }
}
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    DebugLog(@"Mail Result %u",result);
    switch (result)
    {
        case MFMailComposeResultCancelled :
            DebugLog(@"Mail Cancelled");
            break;
        case MFMailComposeResultSaved :
            DebugLog(@"Mail Saved");
            break;
        case MFMailComposeResultSent :
            DebugLog(@"Mail Sent");
            break;
        case MFMailComposeResultFailed :
            DebugLog(@"Mail Failed");
            break;
        default:
            break;
    }
    [controller dismissModalViewControllerAnimated:YES];
}

//- (void)shareBtnFunction
//{
//    DebugLog(@"share btn clicked");
//    UIActionSheet *shareActionSheet= [[UIActionSheet alloc]
//                                      initWithTitle: @""
//                                      delegate:self
//                                      cancelButtonTitle:@"CANCEL"
//                                      destructiveButtonTitle:nil
//                                      otherButtonTitles:@"Facebook",@"Twitter",@"Email", nil];
//	shareActionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
//    shareActionSheet.title = @"Share on";
//	[shareActionSheet showInView:self.navigationController.view];
//}

//-(void)actionSheet:(UIActionSheet *)localactionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    DebugLog(@"%d",buttonIndex);
//    DebugLog(@"share actionsheet callback with [localactionSheet numberOfButtons] %d",[localactionSheet numberOfButtons]);
//    if (buttonIndex == [localactionSheet numberOfButtons]-1)
//        return;
//    else if ([[localactionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Facebook"])
//    {
//        DebugLog(@"facebook");
//        if([IO_APP_DELEGATE networkavailable]) {
//            [self facebookSharePressed];
//        } else {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle: ALERT_TITLE message: @"Please check your internet connection and try again." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alert show];
//        }
//    }
//    else if ([[localactionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Twitter"])
//    {
//        DebugLog(@"twitter");
//        [self twitterSharePressed];
//    }
//    else if ([[localactionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Email"])
//    {
//        DebugLog(@"email");
//        [self emailFunction:nil];
//    }
//}
//-(void)facebookSharePressed
//{
//    if (SYSTEM_VERSION_LESS_THAN(@"6.0"))
//    {
//        DebugLog(@"Got < ios 6");
//        FacebookViewController *facebookViewComposer = [[FacebookViewController alloc] initWithNibName:@"FacebookViewController" bundle:nil];
//        facebookViewComposer.title = @"FACEBOOK";
//        facebookViewComposer.postTitle = ALERT_TITLE;
//        facebookViewComposer.postLink = SAFECITY_APPSTORE_URL;
//        facebookViewComposer.postmessageBody = @"Safe City App";
//        facebookViewComposer.modalPresentationStyle = UIModalPresentationCurrentContext;
//        [self.navigationController presentModalViewController:facebookViewComposer animated:YES];
//    }
//    else
//    {
//        DebugLog(@"Got ios 6");
//        if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
//        {
//            SLComposeViewController *slFBComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
//            [slFBComposer setTitle: ALERT_TITLE];
//            [slFBComposer setInitialText:[NSString stringWithFormat:@"%@ iOS app",ALERT_TITLE]];
//            [slFBComposer addURL:[NSURL URLWithString:SAFECITY_APPSTORE_URL]];
//            [self presentViewController:slFBComposer animated:YES completion:nil];
//            //                slFBComposer.completionHandler = ^(SLComposeViewControllerResult result)
//            //                {
//            //                    if (result == SLComposeViewControllerResultCancelled) {
//            //                        DebugLog(@"User has cancelled composing the post, and tapped the cancel button");
//            //                    }
//            //                    else if (result == SLComposeViewControllerResultDone) {
//            //                        DebugLog(@"User has finished composing the post, and tapped the send button");
//            //                        [Flurry logEvent:@"FBShare_App_Event"];
//            //                    }
//            //                };
//        }
//        else {
//            UIAlertView *alertView = [[UIAlertView alloc]
//                                      initWithTitle:ALERT_TITLE
//                                      message:@"You may not have set up facebook service on your device or You may not connected to internet.\nPlease check and try again."
//                                      delegate:self
//                                      cancelButtonTitle:@"OK"
//                                      otherButtonTitles: nil];
//            [alertView show];
//        }
//    }
//}
//-(void)twitterSharePressed
//{
//    if ([TWTweetComposeViewController canSendTweet])
//    {
//        TWTweetComposeViewController *tweetComposer = [[TWTweetComposeViewController alloc] init];
//        [tweetComposer setInitialText:[NSString stringWithFormat:@"%@ iOS app",ALERT_TITLE]];
//        [tweetComposer addURL:[NSURL URLWithString:SAFECITY_APPSTORE_URL]];
//        [self presentModalViewController:tweetComposer animated:YES];
//        //        tweetComposer.completionHandler = ^(SLComposeViewControllerResult result) {
//        //            if (result == SLComposeViewControllerResultCancelled) {
//        //                DebugLog(@"User has cancelled composing the post, and tapped the cancel button");
//        //            }
//        //
//        //            if (result == SLComposeViewControllerResultDone) {
//        //                DebugLog(@"User has finished composing the post, and tapped the send button");
//        //                [Flurry logEvent:@"TwitterShare_App_Event"];
//        //            }
//        //        };
//    }
//    else
//    {
//        UIAlertView *alertView = [[UIAlertView alloc]
//                                  initWithTitle:ALERT_TITLE
//                                  message:@"You may not have set up twitter service on your device or You may not connected to internet.\nPlease check and try again."
//                                  delegate:self
//                                  cancelButtonTitle:@"OK"
//                                  otherButtonTitles:nil];
//        [alertView show];
//    }
//}
@end
