//
//  GroupObject.h
//  ImageOrganiser
//
//  Created by Kirti Nikam on 04/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GroupObject : NSObject
@property (nonatomic, readwrite) int groupRowId;
@property (nonatomic, readwrite) int iconId;
@property (nonatomic, copy) NSString *groupName;
@property (nonatomic, readwrite) int selected;
@end
