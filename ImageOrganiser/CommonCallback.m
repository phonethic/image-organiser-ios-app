//
//  CommonCallback.m
//  ImageOrganiser
//
//  Created by Kirti Nikam on 13/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "CommonCallback.h"
#import "constants.h"

@implementation CommonCallback
+(UIImage *)getFlatImage:(UIColor *)color
{
    /* Create a DeviceRGB color space. */
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    /* Create a bitmap context. The context draws into a bitmap which is `width'
     pixels wide and `height' pixels high*/
    CGContextRef composedImageContext = CGBitmapContextCreate(NULL,
                                                              10,
                                                              10,
                                                              8,
                                                              10*4,
                                                              colorSpace,
                                                              kCGImageAlphaPremultipliedFirst);
    
    CGColorSpaceRelease(colorSpace);
    
    
    CGContextSetFillColorWithColor(composedImageContext, [color CGColor]);
    CGContextFillRect(composedImageContext, CGRectMake(0, 0, 10, 10));
    /* Return an image containing a snapshot of the bitmap context `context'.*/
    CGImageRef cgImage = CGBitmapContextCreateImage(composedImageContext);
    
    return [UIImage imageWithCGImage:cgImage];
}
+(void)roundeWithBorder:(UIView *)view
{
    view.layer.borderColor = [UIColor grayColor].CGColor;
    view.layer.borderWidth = 2.0;
    view.layer.cornerRadius = 5.0;
}
+(void)addShadow:(UIView *)view
{
    [view.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [view.layer setShadowOffset:CGSizeMake(0, 3)];
    [view.layer setShadowOpacity:1];
}
+(void)removeShadow:(UIView *)view
{
    [view.layer setShadowColor:nil];
    [view.layer setShadowOffset:CGSizeMake(0, 0)];
    [view.layer setShadowOpacity:0];
}
+(void)changeButtonFontAndTextColor:(UIButton *)btn textColor:(UIColor *)ltextColor bgcolor:(UIColor *)lbackgroundColor
{
    [btn setTitleColor:ltextColor forState:UIControlStateNormal];
    [btn setTitleColor:ltextColor forState:UIControlStateHighlighted];
    [btn setBackgroundColor:lbackgroundColor];
    btn.titleLabel.font = DEFAULT_FONT(15.0);
    btn.layer.borderColor = lbackgroundColor.CGColor;
    btn.layer.borderWidth = 2.0;
    btn.layer.cornerRadius = 10.0;
}
+(void)setTextFieldProperties:(UITextField *)textField text:(NSString *)textString
{
    textField.text = textString;
    textField.backgroundColor = TABLE_EDITCELL_BGCOLOR;
    textField.textColor = [UIColor blackColor];
    textField.font = DEFAULT_FONT(14.0f);
    textField.borderStyle = UITextBorderStyleNone;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.returnKeyType = UIReturnKeyDone;
    textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    textField.enabled = TRUE;
}
+(void)setTextFieldPropertiesWithBorder:(UITextField *)textField text:(NSString *)textString
{
    textField.text = textString;
    textField.backgroundColor = TABLE_EDITCELL_BGCOLOR;
    textField.textColor = [UIColor blackColor];
    textField.font = DEFAULT_FONT(14.0f);
    textField.borderStyle = UITextBorderStyleBezel;
    textField.layer.borderColor = TABLE_EDITCELL_BGCOLOR.CGColor;
    textField.layer.borderWidth = 4.0;
    textField.layer.cornerRadius = 8.0;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.returnKeyType = UIReturnKeyDone;
    textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    textField.enabled = TRUE;
}
+(void)setTextViewProperties:(UITextView *)textView text:(NSString *)textString
{
    textView.text = textString;
    textView.backgroundColor =  TABLE_EDITCELL_BGCOLOR;
    textView.textColor       =  [UIColor blackColor];
    textView.font = DEFAULT_FONT(14.0f);
    textView.returnKeyType = UIReturnKeyDefault;
    textView.layer.cornerRadius = 2.0;
    textView.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    textView.editable = TRUE;

}


+(void)setLabelProperties:(UILabel *)label text:(NSString *)textString
{
    label.text = textString;
    label.font = DEFAULT_BOLD_FONT(15.0);
    label.textAlignment = UITextAlignmentLeft;
    label.textColor = TABLE_CELL_BGCOLOR;
    label.highlightedTextColor =  TABLE_EDITCELL_BGCOLOR;
    label.backgroundColor =  [UIColor clearColor];
}
#pragma mark -
#pragma mark - Seperator
+(UIImage *)separatorImage
{
    UIGraphicsBeginImageContext(CGSizeMake(1, 4));
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIGraphicsPushContext(context);
    CGContextSetFillColorWithColor(context, [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:1].CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, 1, 2));
    CGContextSetFillColorWithColor(context, [UIColor colorWithRed:79/255.0 green:79/255.0 blue:77/255.0 alpha:1].CGColor);
    CGContextFillRect(context, CGRectMake(0, 3, 1, 2));
    UIGraphicsPopContext();
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithCGImage:outputImage.CGImage scale:2.0 orientation:UIImageOrientationUp];
}

#pragma mark resize image Method
+(UIImage *)resizeImage:(UIImage*)image newSize:(CGSize)newSize {
    CGRect newRect = CGRectIntegral(CGRectMake(0, 0, newSize.width, newSize.height));
    CGImageRef imageRef = image.CGImage;
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Set the quality level to use when rescaling
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    CGAffineTransform flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, newSize.height);
    
    CGContextConcatCTM(context, flipVertical);
    // Draw into the context; this scales the image
    CGContextDrawImage(context, newRect, imageRef);
    
    // Get the resized image from the context and a UIImage
    CGImageRef newImageRef = CGBitmapContextCreateImage(context);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef];
    
    CGImageRelease(newImageRef);
    UIGraphicsEndImageContext();
    
    return newImage;
}
@end
