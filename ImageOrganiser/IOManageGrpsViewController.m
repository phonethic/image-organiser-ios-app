//
//  IOManageGrpsViewController.m
//  ImageOrganiser
//
//  Created by Kirti Nikam on 10/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "IOManageGrpsViewController.h"
#import "GroupObject.h"
#import "IOAppDelegate.h"
#import "IOGroupDetailViewController.h"
#import "CommonCallback.h"

@interface IOManageGrpsViewController ()

@end

@implementation IOManageGrpsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
            self.title = @"Manage Groups";
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    DebugLog(@"ManageGroups : viewWillAppear");
    [self readFromGroupsTable];
    [self.groupTableView reloadData];
    if ((_groupTableView.frame.origin.y+_groupTableView.contentSize.height) < self.view.frame.size.height-50){
        [_groupTableView setFrame:CGRectMake(_groupTableView.frame.origin.x,
                                                 _groupTableView.frame.origin.y,
                                                 _groupTableView.frame.size.width,
                                                 _groupTableView.contentSize.height)];
    }else{
        [_groupTableView setFrame:CGRectMake(_groupTableView.frame.origin.x,
                                                 _groupTableView.frame.origin.y,
                                                 _groupTableView.frame.size.width,
                                                 self.view.frame.size.height-100)];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 50, 32)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    
   // self.view.backgroundColor = BGCOLOR;
    self.view.backgroundColor = [UIColor clearColor];

    _groupTableView.backgroundColor = [UIColor clearColor];
    _groupTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _groupTableView.separatorColor = [UIColor clearColor];
    _groupTableView.layer.cornerRadius = 10.0;
    [CommonCallback setTextFieldPropertiesWithBorder:_groupTextField text:@""];
    _groupTextField.delegate = self;
    
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    viewTap.numberOfTapsRequired = 1;
    viewTap.cancelsTouchesInView = NO;
    [self.view  addGestureRecognizer:viewTap];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setGroupTableView:nil];
    [self setGroupTextField:nil];
    [self setCreateBtn:nil];
    [super viewDidUnload];
}
#pragma mark Orientation methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate
{
    return NO;
}
- (NSUInteger)supportedInterfaceOrientations{
#if __IPHONE_OS_VERSION_MAX_ALLOWED <= __IPHONE_5_1
    return UIInterfaceOrientationPortrait;
#else
    return UIInterfaceOrientationMaskPortrait;
#endif
}
- (void)tapDetected:(UIGestureRecognizer *)sender {
    
    [self.view endEditing:YES];
}
- (void)backBtnPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)createBtnClicked:(id)sender {
    if ([_groupTextField.text isEqualToString:@""]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                            message:@"Enter group name"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
    else
    {
    
        NSString *groupName = _groupTextField.text;
        if ([self getGroupId:groupName] != -1) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                message:[NSString stringWithFormat:@"%@ group already exists",groupName]
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            [alertView show];
        }
        else
        {
            [self insertIntoGroups:groupName iconid:1];
        }
        
    }
}
#pragma UITextField delegate callback methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
   	return YES;
}

#pragma mark Tableview DataSource methods
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 50;
//}
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView* customView          = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,50)];
//    customView.backgroundColor = [UIColor blackColor];
//    UILabel *headerLabel        = [[UILabel alloc] initWithFrame:CGRectZero];
//    headerLabel.backgroundColor = [UIColor clearColor];
//    headerLabel.frame           = CGRectMake(5,5,290,40);
//    headerLabel.textAlignment   = UITextAlignmentCenter;
//    headerLabel.font            = [UIFont boldSystemFontOfSize:20];
//    headerLabel.text            = @"GroupList";
//    headerLabel.textColor       = DEFAULT_COLOR;
//    [customView addSubview:headerLabel];
//    return customView;
//}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
   return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self getGroupsRowsCount];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"ManageGroupCell";
    UIImageView *accessoryImgView;

    UITableViewCell *cell= (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = TABLE_EDITCELL_BGCOLOR;
        bgColorView.layer.borderColor = [UIColor clearColor].CGColor;
        cell.selectedBackgroundView = bgColorView;
        
        cell.textLabel.highlightedTextColor = TABLE_CELL_BGCOLOR;
        cell.textLabel.font = DEFAULT_BOLD_FONT(15.0);
        cell.textLabel.textColor = [UIColor whiteColor];
        
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        //         cell.backgroundColor = [UIColor redColor];
        cell.contentView.backgroundColor = TABLE_CELL_BGCOLOR;
        cell.layer.borderColor = TABLE_CELL_BGCOLOR.CGColor;
        cell.layer.borderWidth = 0;
        
        accessoryImgView = [[UIImageView alloc] initWithFrame:CGRectMake(250,10, 20, 20)];
        accessoryImgView.tag = 1;
        accessoryImgView.contentMode = UIViewContentModeScaleToFill;
        accessoryImgView.layer.masksToBounds = YES;
        accessoryImgView.image = [UIImage imageNamed:@"Mark_white.png"];
        [cell.contentView addSubview:accessoryImgView];
    }
    
    if ( _groupArray!= nil && _groupArray.count > 0) {
        GroupObject *recObj = (GroupObject *)[_groupArray objectAtIndex:indexPath.row];
        cell.textLabel.text = [recObj.groupName capitalizedString];
        cell.textLabel.font = DEFAULT_BOLD_FONT(15.0);
        cell.tag = recObj.groupRowId;
   //     cell.imageView.image = [UIImage imageNamed:[_iconsDic valueForKey:[NSString stringWithFormat:@"%d",recObj.iconId]]];
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        accessoryImgView = (UIImageView *)[cell.contentView viewWithTag:1];
        accessoryImgView.image = [UIImage imageNamed:@"Mark_white.png"];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    return cell;
}

#pragma mark Tableview Delegate methods
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    UIImageView *accessoryImgView = (UIImageView *)[cell.contentView viewWithTag:1];
    accessoryImgView.image = [UIImage imageNamed:@"Mark_white.png"];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    UIImageView *accessoryImgView = (UIImageView *)[cell.contentView viewWithTag:1];
    accessoryImgView.image = [UIImage imageNamed:@"Mark.png"];
    
    IOGroupDetailViewController *detailViewController = [[IOGroupDetailViewController alloc] initWithNibName:@"IOGroupDetailViewController" bundle:nil];
    detailViewController.title = ((UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath]).textLabel.text;
    
    detailViewController.currentGroupId = ((UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath]).tag;
    [self.navigationController pushViewController:detailViewController animated:YES];

   [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    GroupObject *recObj = (GroupObject *)[_groupArray objectAtIndex:indexPath.row];
    DebugLog(@"ManageGroups : rec GroupName %@ -- %@ %d %d",recObj.groupName, [recObj.groupName lowercaseString],[[recObj.groupName lowercaseString] isEqualToString:[FAVOURITE_GROUPNAME lowercaseString]],[[recObj.groupName lowercaseString] isEqualToString:[UNGROUPED_GROUPNAME lowercaseString]]);
    if (recObj.groupName == nil) {
        return NO;
    }
    if ([[recObj.groupName lowercaseString] isEqualToString:[FAVOURITE_GROUPNAME lowercaseString]] || [[recObj.groupName lowercaseString] isEqualToString:[UNGROUPED_GROUPNAME lowercaseString]])
    {
        return NO;
    }
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath {
    
        if(editingStyle == UITableViewCellEditingStyleDelete)
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle: ALERT_TITLE message: @"Are you sure, you want to delete this group ?" delegate: self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
            alert.tag = 1;
            [alert show];
            _deletegroupIndexPath = indexPath;
        }
}
#pragma alertView delegate Method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 1)
    {
        switch (buttonIndex) {
            case 0:
            {
                break;
            }
            case 1:
            {
                DebugLog(@"ManageGroups : Delete group");
                GroupObject *recObj = (GroupObject *)[_groupArray objectAtIndex:_deletegroupIndexPath.row];
                if (recObj.groupName != nil && (![[recObj.groupName lowercaseString] isEqualToString:[FAVOURITE_GROUPNAME lowercaseString]] || ![[recObj.groupName lowercaseString] isEqualToString:[UNGROUPED_GROUPNAME lowercaseString]]))
                {
                    UITableViewCell *cell  = (UITableViewCell *)[_groupTableView cellForRowAtIndexPath:_deletegroupIndexPath];
                    [_groupArray removeObjectAtIndex:_deletegroupIndexPath.row];
                    [self moveSelectedGroupRecordsIntoUnGrouped:cell.tag];
                    [self DeleteGroup:cell.tag];
                    [_groupTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:_deletegroupIndexPath] withRowAnimation:UITableViewRowAnimationMiddle];
                    [_groupTableView reloadData];
                    
                    if (_groupTableView.frame.origin.y+_groupTableView.contentSize.height > 100 && _groupTableView.frame.origin.y+_groupTableView.contentSize.height < self.view.frame.size.height-50)
                    {
                        [UIView animateWithDuration:0.5 animations:^{
                            [_groupTableView setFrame:CGRectMake(_groupTableView.frame.origin.x,
                                                                     _groupTableView.frame.origin.y,
                                                                     _groupTableView.frame.size.width,
                                                                     _groupTableView.contentSize.height)];
                        } completion:^(BOOL finished) {
                        }];
                    }
                }
            }
                break;
            default:
                break;
        }
        
    }
    
}

-(NSInteger)getGroupsRowsCount
{
    NSInteger rowCount = 0;
    
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
        NSString *queryString = [NSString stringWithFormat:@"SELECT count (*) FROM GROUPS"];
        const char *queryCharP = [queryString UTF8String];
        
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            if (sqlite3_step(stmt) == SQLITE_ROW) {
                rowCount = sqlite3_column_int(stmt, 0);
            }
        }
        sqlite3_finalize(stmt);
        sqlite3_close(ioDB);
    }
    DebugLog(@"GROUPS rowCount %d",rowCount);
    return rowCount;
}
-(void)readFromGroupsTable
{
    DebugLog(@"ManageGroups : --readFromGroupsTable--");
    
    if (_groupArray == nil) {
        _groupArray = [[NSMutableArray alloc] init];
    }
    else
    {
        [_groupArray removeAllObjects];
    }
    GroupObject *groupObj = nil;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK) {
        NSString *queryString = [NSString stringWithFormat:@"SELECT ID,GROUP_NAME,ICON_ID FROM GROUPS"];
        const char *queryCharP = [queryString UTF8String];
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            while (sqlite3_step(stmt) == SQLITE_ROW)
            {
                int rowId = sqlite3_column_int(stmt, 0);
                NSString *name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 1)];
                int iconId = sqlite3_column_int(stmt, 2);
                
                groupObj = [[GroupObject alloc]init];
                groupObj.groupRowId = rowId;
                groupObj.groupName = name;
                groupObj.iconId = iconId;
                groupObj.selected = 0;
                [_groupArray addObject:groupObj];
                
                groupObj = nil;
            }
        }
        sqlite3_finalize(stmt);
        sqlite3_close(ioDB);
    }
}
-(void)insertIntoGroups:(NSString *)groupName iconid:(int)iconId
{
    DebugLog(@"ManageGroups : --insertIntoGroups--");
    int RowId = -1;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String],&ioDB) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO GROUPS(GROUP_NAME,ICON_ID) VALUES (\"%@\",%d)",[groupName lowercaseString],iconId];
        DebugLog(@"ManageGroups : ---%@---",insertSQL);
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_stmt *statement = nil;
        sqlite3_prepare_v2(ioDB, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            RowId = sqlite3_last_insert_rowid(ioDB);
            DebugLog(@"ManageGroups : GROUPS : successfully saved :");
            
        } else {
            DebugLog(@"ManageGroups : GROUPS : failed to save");
        }
        sqlite3_finalize(statement);
        sqlite3_close(ioDB);
    }
    if (RowId != -1) {
        GroupObject *grp = [[GroupObject alloc] init];
        grp.groupRowId = RowId;
        grp.groupName = groupName;
        grp.selected = 1;
        grp.iconId = 1;
        
        [_groupArray addObject:grp];
        [_groupTableView beginUpdates];
        [_groupTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:_groupArray.count-1 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        [_groupTableView endUpdates];
        
        if (_groupTableView.frame.origin.y+_groupTableView.contentSize.height < self.view.frame.size.height-50) {
            [UIView animateWithDuration:0.5 animations:^{
                [_groupTableView setFrame:CGRectMake(_groupTableView.frame.origin.x,
                                                         _groupTableView.frame.origin.y,
                                                         _groupTableView.frame.size.width,
                                                        _groupTableView.contentSize.height)];
            } completion:^(BOOL finished) {
            }];
        }
        grp = nil;
    }
}
-(void)moveSelectedGroupRecordsIntoUnGrouped:(int) groupId
{
    DebugLog(@"ManageGroups : --moveSelectedGroupRecordsIntoUnGrouped-- %d",groupId);
    int ungroupId = [self getGroupId:UNGROUPED_GROUPNAME];    
    
    int imgId = 0;
    NSMutableArray *recordIdArray = [[NSMutableArray alloc] init];
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String],&ioDB) == SQLITE_OK)
    {
        NSString *queryString = [NSString stringWithFormat:@"SELECT IMAGE_MASTER_ID FROM IMAGE_GROUP_RELATION WHERE GROUP_ID = %d",groupId];
        const char *queryCharP = [queryString UTF8String];
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            while (sqlite3_step(stmt) == SQLITE_ROW) {
                imgId = sqlite3_column_int(stmt, 0);
                [recordIdArray addObject:[NSNumber numberWithInt:imgId]];
            }
        }
        sqlite3_finalize(stmt);
        
        DebugLog(@"ManageGroups : recordIdArray %@",recordIdArray);

        for (int index = 0; index < recordIdArray.count; index++) {
            
            int count = 0;
            if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String],&ioDB) == SQLITE_OK)
            {
                NSString *queryString1 = [NSString stringWithFormat:@"SELECT COUNT(*) FROM IMAGE_GROUP_RELATION WHERE IMAGE_MASTER_ID = %d",[[recordIdArray objectAtIndex:index] intValue]];
                const char *queryCharP1 = [queryString1 UTF8String];
                if (sqlite3_prepare_v2(ioDB, queryCharP1, -1, &stmt, NULL) == SQLITE_OK) {
                    if (sqlite3_step(stmt) == SQLITE_ROW) {
                        count = sqlite3_column_int(stmt, 0);
                    }
                }
                sqlite3_finalize(stmt);
            }
            
            DebugLog(@"ManageGroups : IMAGE_MASTER_ID %d count %d",[[recordIdArray objectAtIndex:index] intValue],count);
            if (count == 1) {
                NSString *updateSQL = [NSString stringWithFormat:@"UPDATE IMAGE_GROUP_RELATION SET GROUP_ID = %d WHERE GROUP_ID = %d AND IMAGE_MASTER_ID = %d",ungroupId,groupId,[[recordIdArray objectAtIndex:index] intValue]];
                const char *update_stmt = [updateSQL UTF8String];
                sqlite3_prepare_v2(ioDB, update_stmt, -1, &stmt, NULL);
                if (sqlite3_step(stmt) == SQLITE_DONE)
                {
                    DebugLog(@"ManageGroups : updated in IMAGE_GROUP_RELATION with %d",ungroupId);
                } else {
                    DebugLog(@"ManageGroups : Failed to update in IMAGE_GROUP_RELATION");
                }
                sqlite3_finalize(stmt);
            }
        }
        sqlite3_close(ioDB);
    }
    [recordIdArray removeAllObjects];
    recordIdArray = nil;
}
-(int)getRecordCount:(int) recordId
{
    int count = 0;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String],&ioDB) == SQLITE_OK)
    {
        NSString *queryString = [NSString stringWithFormat:@"SELECT COUNT(*) FROM IMAGE_GROUP_RELATION WHERE IMAGE_MASTER_ID = %d",recordId];
        const char *queryCharP = [queryString UTF8String];
        sqlite3_stmt *stmt = nil;
        if (sqlite3_prepare_v2(ioDB, queryCharP, -1, &stmt, NULL) == SQLITE_OK) {
            if (sqlite3_step(stmt) == SQLITE_ROW) {
                count = sqlite3_column_int(stmt, 0);
            }
        }
    sqlite3_finalize(stmt);
    }
    return count;
}

- (void) DeleteGroup :(int) groupId
{
    sqlite3_stmt *statement;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM GROUPS WHERE ID=\"%d\"", groupId];
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(ioDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"ManageGroups : deleted from GROUPS");
        } else {
            DebugLog(@"ManageGroups : Failed to delete from GROUPS");
        }
        sqlite3_finalize(statement);
        sqlite3_close(ioDB);
    }
}
-(int)getGroupId:(NSString *)groupName
{
    int groupId = -1;
	if(sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String], &ioDB) == SQLITE_OK)
    {
        // Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"Select ID from GROUPS WHERE GROUP_NAME=\"%@\"",[groupName lowercaseString]];
        const char *sqlStatement = [querySQL UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(ioDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                groupId = sqlite3_column_int(compiledStatement, 0);
                DebugLog(@"groupId -> %d",groupId);
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(ioDB);
    return groupId;
}

@end
