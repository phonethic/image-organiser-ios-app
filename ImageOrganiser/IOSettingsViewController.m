//
//  IOSettingsViewController.m
//  ImageOrganiser
//
//  Created by Kirti Nikam on 10/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>

#import "IOSettingsViewController.h"
#import "constants.h"
#import "IOAppDelegate.h"
#import "CommonCallback.h"
#import "IOWebViewController.h"

@interface IOSettingsViewController ()

@end

@implementation IOSettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Settings";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIButton *backbutton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [backbutton setImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
    [backbutton addTarget:self action:@selector(backBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [backbutton setFrame:CGRectMake(0, 0, 50, 30)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backbutton];
    
  //  self.view.backgroundColor = BGCOLOR;
    self.view.backgroundColor = [UIColor clearColor];

    _settingsTableView.backgroundColor = [UIColor clearColor];
    _settingsTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _settingsTableView.separatorColor = [UIColor clearColor];
    
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(doneBtnClicked:)];
    
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    viewTap.numberOfTapsRequired = 1;
    viewTap.cancelsTouchesInView = NO;
    [_settingsTableView  addGestureRecognizer:viewTap];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setSettingsTableView:nil];
    [super viewDidUnload];
}
#pragma mark Orientation methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate
{
    return NO;
}
- (NSUInteger)supportedInterfaceOrientations{
#if __IPHONE_OS_VERSION_MAX_ALLOWED <= __IPHONE_5_1
    return UIInterfaceOrientationPortrait;
#else
    return UIInterfaceOrientationMaskPortrait;
#endif
}
- (void)backBtnPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)doneBtnClicked:(id)sender
{
 
    [Flurry logEvent:@"Updating_User_Details_Event"];
    NSString *firstName;
    NSString *lastName;
    NSString *email;
    NSString *phone;
    for (int index = 0 ; index < 4; index ++ ) {
        UITableViewCell *cell = [_settingsTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
        if (cell != nil)
        {
            UITextField *textField   = (UITextField *) [cell viewWithTag:index+1];
            switch (textField.tag) {
                case 1:
                    if (textField== nil || [textField.text isEqualToString:@""]) {
                        [self showAlert:@"Please enter first name."];
                        return;
                    }else{
                        firstName = textField.text;
                    }
                    break;
                case 2:
                    if ([textField.text isEqualToString:@""]) {
                        [self showAlert:@"Please enter last name."];
                        return;
                    }else{
                        lastName = textField.text;
                    }
                    break;
                case 3:
                    if ([textField.text isEqualToString:@""]) {
                        [self showAlert:@"Please enter email."];
                        return;
                    }else if (![textField.text isEqualToString:@""] && ![self validateEmail:textField.text]) {
                        [self showAlert:@"Invalid email address.\nPlease check your email address and try again."];
                        return;
                    }else{
                        email = textField.text;
                    }
                    break;
                case 4:
                    if ([textField.text isEqualToString:@""]) {
                        [self showAlert:@"Please enter phone number."];
                        return;
                    }else{
                        phone = textField.text;
                    }
                    break;
                default:
                    break;
            }
        }
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    [defaults setObject:firstName forKey:@"firstname"];
    [defaults setObject:lastName forKey:@"lastname"];
    [defaults setObject:email forKey:@"email"];
    [defaults setObject:phone forKey:@"phone"];
    [defaults synchronize];
    
    [self showAlert:@"User Information saved successfully."];
}
-(void)showAlert:(NSString*) message
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}

-(BOOL)validateEmail: (NSString *) email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL isValid = [emailTest evaluateWithObject:email];
    return isValid;
}

- (void)tapDetected:(UIGestureRecognizer *)sender {
    
    [self.view endEditing:YES];
    [self scrollTobottom];
}
-(void)scrollTobottom
{
    CGPoint bottomOffset = CGPointMake(0, 0);
    [_settingsTableView setContentOffset:bottomOffset animated:YES];
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 2 && indexPath.row == 2)
        return 120;
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(0,0,300,30)];
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.frame = CGRectMake(15, 5, 300, 30);
    headerLabel.font = DEFAULT_BOLD_FONT(16.0f);
    switch (section) {
        case 0:
            headerLabel.text = @"User Information";
            break;
        case 1:
            headerLabel.text = @"Personal Data";
            break;
        case 2:
            headerLabel.text = @"About Us";
            break;
        default:
            headerLabel.text = @"";
            break;
    }
    headerLabel.textColor = [UIColor whiteColor];
    [customView addSubview:headerLabel];
    return customView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 4;
    }
    else if (section == 2) {
        return 3;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        NSString *CellIdentifier = [NSString stringWithFormat:@"settings%d%d",indexPath.section,indexPath.row] ;
//    static  NSString *CellIdentifier = @"settingsCell";
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    UIImageView *accessoryImgView;
    UIImageView *dividerImgView;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.contentView.clipsToBounds = YES;
        cell.clipsToBounds = YES;

        DebugLog(@"Settings : id = %@",CellIdentifier);
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = TABLE_CELL_BGCOLOR;
        bgColorView.layer.borderColor = [UIColor clearColor].CGColor;
        bgColorView.layer.cornerRadius = 10.0;
        cell.selectedBackgroundView = bgColorView;
        
        accessoryImgView = [[UIImageView alloc] initWithFrame:CGRectMake(270,15, 20, 20)];
        accessoryImgView.tag = 9;
        accessoryImgView.contentMode = UIViewContentModeScaleToFill;
        accessoryImgView.layer.masksToBounds = YES;
        [cell.contentView addSubview:accessoryImgView];
        
        dividerImgView = [[UIImageView alloc] initWithFrame:CGRectMake(25,49, 244, 2)];
        dividerImgView.tag = 10;
        dividerImgView.contentMode = UIViewContentModeScaleToFill;
        dividerImgView.layer.masksToBounds = YES;
        [cell.contentView addSubview:dividerImgView];
        
        switch (indexPath.section) {
            case 0:
            {
                if(indexPath.row==0)
                {
                    UITextField *firstnameTextField = [[UITextField alloc] initWithFrame:CGRectMake(50.0f, 10.0f, 240.0f, 31.0f)];
                    firstnameTextField.placeholder = @"Enter first name";
                    [self setTextFieldProperties:firstnameTextField text:[defaults objectForKey:@"firstname"]];
                    firstnameTextField.tag = 1;
                    firstnameTextField.keyboardType = UIKeyboardTypeDefault;
                    firstnameTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
                    [cell.contentView addSubview:firstnameTextField];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    cell.imageView.image = [UIImage imageNamed:@"username.png"];
                    accessoryImgView.image = nil;
                    dividerImgView.image = [UIImage imageNamed:@"cell_divider.png"];
                } else if(indexPath.row==1) {
                    UITextField *lastnameTextField = [[UITextField alloc] initWithFrame:CGRectMake(50.0f, 10.0f, 240.0f, 31.0f)];
                    lastnameTextField.placeholder = @"Enter last name";
                    [self setTextFieldProperties:lastnameTextField text:[defaults objectForKey:@"lastname"]];
                    lastnameTextField.tag = 2;
                    lastnameTextField.keyboardType = UIKeyboardTypeDefault;
                    lastnameTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
                    [cell.contentView addSubview:lastnameTextField];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    cell.imageView.image = [UIImage imageNamed:@"username.png"];
                    dividerImgView.image = [UIImage imageNamed:@"cell_divider.png"];
                    accessoryImgView.image = nil;
                } else if(indexPath.row==2) {
                    UITextField *emailTextField = [[UITextField alloc] initWithFrame:CGRectMake(50.0f, 10.0f, 240.0f, 31.0f)];
                    emailTextField.placeholder = @"Enter email address";
                    [self setTextFieldProperties:emailTextField text:[defaults objectForKey:@"email"]];
                    emailTextField.tag = 3;
                    emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
                    emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
                    [cell.contentView addSubview:emailTextField];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    cell.imageView.image = [UIImage imageNamed:@"email.png"];
                    dividerImgView.image = [UIImage imageNamed:@"cell_divider.png"];
                    accessoryImgView.image = nil;
                } else if(indexPath.row==3) {
                    UITextField *numberTextField = [[UITextField alloc] initWithFrame:CGRectMake(50.0f, 10.0f, 240.0f, 31.0f)];
                    numberTextField.placeholder = @"Enter phone number";
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    [self setTextFieldProperties:numberTextField text:[defaults objectForKey:@"phone"]];
                    numberTextField.tag = 4;
                    numberTextField.keyboardType = UIKeyboardTypePhonePad;
                    [cell.contentView addSubview:numberTextField];
                    cell.imageView.image = [UIImage imageNamed:@"telephone.png"];
                    dividerImgView.image = nil;
                    accessoryImgView.image = nil;
                }
            }
                break;
            case 1:
            {

//                UIImage *image = [CommonCallback getFlatImage:[UIColor colorWithRed:192/255.0 green:57/255.0 blue:43/255.0 alpha:1.0]];
//                UIImage *image = [CommonCallback getFlatImage:TABLE_CELL_BGCOLOR];
                UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                [btn setFrame:CGRectMake(0,0, 300, 50)];
                btn.layer.masksToBounds = YES;
                btn.tag = 5;
//                btn.layer.cornerRadius = 5.0;
//                btn.layer.borderColor = [UIColor clearColor].CGColor;
//                [btn setBackgroundImage:image forState:UIControlStateNormal];
//                [btn setTitle:@"Clear All Data" forState:UIControlStateNormal];
//                btn.titleLabel.font = DEFAULT_BOLD_FONT(15.0);
//                [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//                [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
                
                btn.backgroundColor = [UIColor colorWithRed:222.0/255 green:59.0/255 blue:47.0/255 alpha:1.0f];
                btn.layer.cornerRadius = 3.0f;
                btn.titleLabel.font = DEFAULT_BOLD_FONT(20.0);
                [btn setTitle:@"Clear All Data" forState:UIControlStateNormal];
                [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [btn setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];

                [btn addTarget:self action:@selector(deleteAllRecords:) forControlEvents:UIControlEventTouchUpInside];
                [cell.contentView addSubview:btn];
                accessoryImgView.image = nil;
            }
                break;
            case 2:
            {
                if(indexPath.row==0) {
                    UILabel *supportLabel = [[UILabel alloc] initWithFrame:CGRectMake(50.0f, 10.0f, 250.0f, 31.0f)];
                    supportLabel.tag = 6;
                    [CommonCallback setLabelProperties:supportLabel text:EMAIL];
                    [cell.contentView addSubview:supportLabel];
                    cell.imageView.image = [UIImage imageNamed:@"email.png"];
                    accessoryImgView.image = [UIImage imageNamed:@"Mark.png"];
                    dividerImgView.image = [UIImage imageNamed:@"cell_divider.png"];
                } else if(indexPath.row==1) {
                    UILabel *emailLabel = [[UILabel alloc] initWithFrame:CGRectMake(50.0f, 10.0f, 250.0f, 31.0f)];
                    emailLabel.tag = 7;
                    [CommonCallback setLabelProperties:emailLabel text: WEBSITELINK];
                    [cell.contentView addSubview:emailLabel];
                    cell.imageView.image = [UIImage imageNamed:@"weblink.png"];
                    accessoryImgView.image = [UIImage imageNamed:@"Mark.png"];
                    dividerImgView.image = [UIImage imageNamed:@"cell_divider.png"];
                } else if(indexPath.row==2) {
                    UIImageView *thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 280, 100)];
                    thumbImg.tag = 8;
                    thumbImg.image = [UIImage imageNamed:@"phonethics.png"];
                    thumbImg.contentMode = UIViewContentModeScaleAspectFit;
                    thumbImg.layer.masksToBounds = YES;
//                    thumbImg.layer.cornerRadius = 10;
//                    thumbImg.layer.borderColor = TABLE_CELL_BGCOLOR.CGColor;
//                    thumbImg.layer.borderWidth = 2.0;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    [cell.contentView addSubview:thumbImg];
                    dividerImgView.image = nil;
                    accessoryImgView.image = nil;
                }
            }
                break;
            default:
                break;
        }
    }
    else {
        
        switch (indexPath.section) {
            case 0:
            {
                UITextField *tempTextField = (UITextField *) [cell viewWithTag:indexPath.row+1];
                
                if(indexPath.row==0)
                {
                    tempTextField.text = [defaults objectForKey:@"firstname"];
                } else if(indexPath.row==1) {
                    tempTextField.text = [defaults objectForKey:@"lastname"];
                } else if(indexPath.row==2) {
                    tempTextField.text = [defaults objectForKey:@"email"];
                } else if(indexPath.row==3) {
                    tempTextField.text = [defaults objectForKey:@"phone"];
                }
            }
                break;
        }
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 2:
        {
            switch (indexPath.row) {
                case 0: //email
                {
                    MFMailComposeViewController *mailComposeViewController=[[MFMailComposeViewController alloc]init];
                    mailComposeViewController.mailComposeDelegate = self;
                    [mailComposeViewController setToRecipients:[NSArray arrayWithObjects:EMAIL,nil]];
                    [mailComposeViewController setSubject:@"Ref: EMR"];
                    [mailComposeViewController setMessageBody:@"Enter your message here." isHTML:YES];
                    [mailComposeViewController setModalPresentationStyle:UIModalPresentationFormSheet];
                    [self.navigationController presentModalViewController:mailComposeViewController animated:YES];
                }
                    break;
                case 1: //website
                {
                    if([IO_APP_DELEGATE networkavailable]) {
                        IOWebViewController *webSiteviewController = [[IOWebViewController alloc] initWithNibName:@"IOWebViewController" bundle:nil] ;
                        webSiteviewController.urlString = WEBSITELINK;
                        webSiteviewController.title = @"Phonethics.in";
                        [self.navigationController pushViewController:webSiteviewController animated:YES];
                    } else {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: ALERT_TITLE message: @"Please check your internet connection and try again." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [alert show];
                    }
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
        default:
            break;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark MFMailComposeViewControllerDelegate methods
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    DebugLog(@"Mail Result %u",result);
    switch (result)
    {
        case MFMailComposeResultCancelled :
            DebugLog(@"Mail Cancelled");
            break;
        case MFMailComposeResultSaved :
            DebugLog(@"Mail Saved");
            break;
        case MFMailComposeResultSent :
            DebugLog(@"Mail Sent");
            break;
        case MFMailComposeResultFailed :
            DebugLog(@"Mail Failed");
            break;
        default:
            break;
    }
    [controller dismissModalViewControllerAnimated:YES];
}


#pragma UIButton Delegatew methods 
-(void)deleteAllRecords:(id)sender{
    DebugLog(@"Settings : deleteAllRecords");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: ALERT_TITLE message: @"Are you sure, you want to clear all data ?" delegate: self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
    alert.tag = 1;
    [alert show];
}
#pragma alertView delegate Method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 1)
    {
        switch (buttonIndex) {
            case 0:
            {
                break;
            }
            case 1:
            {
                DebugLog(@"Settings : Delete All");
                [self clearDatabase];
            }
                break;
            default:
                break;
        }
        
    }
    
}

-(void)setTextFieldProperties:(UITextField *)textField text:(NSString *)textString
{
    DebugLog(@"Settings : textString %@",textString);
    if (textString == nil)
         textField.text = @"";
    else
        textField.text = textString;
    textField.delegate = self;
    textField.backgroundColor = [UIColor clearColor];
    textField.textColor = [UIColor blackColor];
    textField.font = DEFAULT_FONT(14.0f);
    textField.borderStyle = UITextBorderStyleNone;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.returnKeyType = UIReturnKeyNext;
    textField.textAlignment = UITextAlignmentLeft;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    textField.enabled = TRUE;
}

#pragma mark TextField Delegate methods
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField.tag == 1 && textField.frame.origin.y < _settingsTableView.contentOffset.y)
        [_settingsTableView setContentOffset:CGPointMake(0,textField.frame.origin.y-30) animated:YES];
    else if(textField.tag==3 && textField.frame.origin.y > _settingsTableView.contentOffset.y)
        [_settingsTableView setContentOffset:CGPointMake(0,textField.frame.origin.y+60) animated:YES];
    else if(textField.tag==4 && textField.frame.origin.y > _settingsTableView.contentOffset.y)
        [_settingsTableView setContentOffset:CGPointMake(0,textField.frame.origin.y+60) animated:YES];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    DebugLog(@"textField.tag %d",textField.tag);
    UITableViewCell *cell = [_settingsTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:textField.tag inSection:0]];
    if (cell != nil)
    {
        UITextField *tempTextField = (UITextField *) [cell viewWithTag:textField.tag + 1];
        DebugLog(@"temp %@",tempTextField.text);
        [tempTextField becomeFirstResponder];
    }
    [textField resignFirstResponder];
    return NO;
}


- (void) clearDatabase
{
    DebugLog(@"=============== Deleting  rows ================");
    int success = 0;
    if (sqlite3_open([[IO_APP_DELEGATE databasePath] UTF8String],&ioDB) == SQLITE_OK)
    {
        sqlite3_stmt *statement = nil;
        //TAGS_TEMP
        NSString *deleteSQL_tagstemp = [NSString stringWithFormat:@"DELETE FROM TAGS_TEMP"];
        const char *delete_stmt_tagstemp = [deleteSQL_tagstemp UTF8String];
        sqlite3_prepare_v2(ioDB, delete_stmt_tagstemp, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"Settings : =============== Success on deleting rows from TAGS_TEMP================");
            
        } else
        {
            DebugLog(@"Settings : =============== Error while deleting rows from TAGS_TEMP %s================",sqlite3_errmsg(ioDB));
        }
        sqlite3_finalize(statement);
        
        //GROUPS_TEMP
        NSString *deleteSQL_grpTemp = [NSString stringWithFormat:@"DELETE FROM GROUPS_TEMP"];
        const char *delete_stmt_grpTemp = [deleteSQL_grpTemp UTF8String];
        sqlite3_prepare_v2(ioDB, delete_stmt_grpTemp, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"Settings : =============== Success on deleting rows from GROUPS_TEMP================");
            
        } else
        {
            DebugLog(@"Settings : =============== Error while deleting rows from GROUPS_TEMP %s================",sqlite3_errmsg(ioDB));
        }
        sqlite3_finalize(statement);

        //IMAGE_TAG_RELATION
        NSString *deleteSQL_tagsRel = @"DELETE FROM IMAGE_TAG_RELATION";
        const char *delete_stmt_tagsRel = [deleteSQL_tagsRel UTF8String];
        sqlite3_prepare_v2(ioDB, delete_stmt_tagsRel, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            success = 1;
            DebugLog(@"Settings : =============== Success on deleting rows from IMAGE_TAG_RELATION================");
            
        } else
        {
            DebugLog(@"Settings : =============== Error while deleting rows from IMAGE_TAG_RELATION %s================",sqlite3_errmsg(ioDB));
        }
        sqlite3_finalize(statement);
        
        //IMAGE_GROUP_RELATION
        NSString *deleteSQL_GrpRel = @"DELETE FROM IMAGE_GROUP_RELATION";
        const char *delete_stmt_GrpRel = [deleteSQL_GrpRel UTF8String];
        sqlite3_prepare_v2(ioDB, delete_stmt_GrpRel, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            success = 1;
            DebugLog(@"Settings : =============== Success on deleting rows from IMAGE_GROUP_RELATION================");
            
        } else
        {
            DebugLog(@"Settings : =============== Error while deleting rows from IMAGE_GROUP_RELATION %s================",sqlite3_errmsg(ioDB));
        }
        sqlite3_finalize(statement);
        
        //TAGS
        NSString *deleteSQL_tags = @"DELETE FROM TAGS";
        const char *delete_stmt_tags = [deleteSQL_tags UTF8String];
        sqlite3_prepare_v2(ioDB, delete_stmt_tags, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            success = 1;
            DebugLog(@"Settings : =============== Success on deleting rows from TAGS================");
            
        } else
        {
            DebugLog(@"Settings : =============== Error while deleting rows from TAGS %s================",sqlite3_errmsg(ioDB));
        }
        sqlite3_finalize(statement);
        
        //GROUPS
        NSString *deleteSQL_Grp = [NSString stringWithFormat:@"DELETE FROM GROUPS WHERE GROUP_NAME NOT IN ('%@','%@')",[FAVOURITE_GROUPNAME lowercaseString],[UNGROUPED_GROUPNAME lowercaseString]];
        const char *delete_stmt_Grp = [deleteSQL_Grp UTF8String];
        sqlite3_prepare_v2(ioDB, delete_stmt_Grp, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            success = 1;
            DebugLog(@"Settings : =============== Success on deleting rows from GROUPS================");
            
        } else
        {
            DebugLog(@"Settings : =============== Error while deleting rows from GROUPS %s================",sqlite3_errmsg(ioDB));
        }
        sqlite3_finalize(statement);
        
        //IMAGE_SET_RELATION
        NSMutableArray *imageArray = [[NSMutableArray alloc] init];
        NSString *path = nil;
        NSString *imagequeryString = @"SELECT IMAGE_PATH FROM IMAGE_SET_RELATION";
        DebugLog(@"Settings : ------%@-----",imagequeryString);
        const char *imgqueryCharP = [imagequeryString UTF8String];
        if (sqlite3_prepare_v2(ioDB, imgqueryCharP, -1, &statement, NULL) == SQLITE_OK) {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                path = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                DebugLog(@"Settings : image path %@ ",path);
                [imageArray addObject:path];
                path =nil;
            }
        }
        sqlite3_finalize(statement);
        
        NSString *deleteSQL_imgRel = @"DELETE FROM IMAGE_SET_RELATION";
        const char *delete_stmt_imgRel = [deleteSQL_imgRel UTF8String];
        sqlite3_prepare_v2(ioDB, delete_stmt_imgRel, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE) {
            DebugLog(@"Settings : =============== Success on deleting rows from IMAGE_SET_RELATION================");
            for (NSString *pathStr in imageArray) {
                DebugLog(@"Settings : remove image %@",pathStr);
                [IO_APP_DELEGATE removeImage:pathStr];
            }
        }else{
            DebugLog(@"Settings : =============== Error while deleting rows from IMAGE_SET_RELATION %s================",sqlite3_errmsg(ioDB));
        }
        sqlite3_finalize(statement);
        
        [imageArray removeAllObjects];
        imageArray = nil;
        
        //IMAGE_MASTER
        NSString *deleteSQL_imgMaster = @"DELETE FROM IMAGE_MASTER";
        const char *delete_stmt_imgMaster = [deleteSQL_imgMaster UTF8String];
        sqlite3_prepare_v2(ioDB, delete_stmt_imgMaster, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            success = 1;
            DebugLog(@"Settings : =============== Success on deleting rows from IMAGE_MASTER================");
            
        } else
        {
            DebugLog(@"Settings : =============== Error while deleting rows from IMAGE_MASTER %s================",sqlite3_errmsg(ioDB));
        }
        sqlite3_finalize(statement);
        sqlite3_close(ioDB);

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message: @"Cleared all data successfully." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}
@end
