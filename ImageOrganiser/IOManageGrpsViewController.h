//
//  IOManageGrpsViewController.h
//  ImageOrganiser
//
//  Created by Kirti Nikam on 10/04/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface IOManageGrpsViewController : UIViewController<UITextFieldDelegate>
{
    sqlite3 *ioDB;
}
@property (retain,nonatomic) NSMutableArray *groupArray;
@property (strong, nonatomic) IBOutlet UITextField *groupTextField;
@property (strong, nonatomic) IBOutlet UITableView *groupTableView;
@property (strong, nonatomic) IBOutlet UIButton *createBtn;
@property (retain,nonatomic) NSIndexPath* deletegroupIndexPath;
- (IBAction)createBtnClicked:(id)sender;
@end
