//
//  ImageViewController.m
//  CarUtil
//
//  Created by Rishi Saxena on 03/04/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "ImageViewController.h"
#import "IOAppDelegate.h"

@interface ImageViewController ()

@end

@implementation ImageViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithPageNumber:(int)page imagepath:(NSString *)limagePath
{
    if (self = [super initWithNibName:@"ImageViewController" bundle:nil])
    {
        pageNumber = page;
        _imagePath = limagePath;
        DebugLog(@"ImageViewController : page init = %d _imagePath %@",page,_imagePath);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _imgView.image = [IO_APP_DELEGATE loadImage:_imagePath];
    [_imgView setContentMode:UIViewContentModeScaleToFill];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}
@end
